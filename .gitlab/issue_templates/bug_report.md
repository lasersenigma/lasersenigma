## Summary

(Summarize the bug encountered concisely)

Lasers Enigma version: 
Minecraft version: 
Server Software version (SpigotMC or PaperMC) : 

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Possible fixes or workaround

(If you can, link to the line of code that might be responsible for the problem)

/label ~"Type: Bug" 
/cc @soisyhpe @bZx 
/assign me
