## Summary

(Provide a concise summary of the requested feature)

Is this feature is related to a specific Minecraft Version ? YES or NO

## Feature Description

(Describe the new feature or enhancement in detail. What is the feature, and how should it work?)

## Motivation and Benefits

(Explain why this feature is needed and the benefits it would bring to users or the project)

## Use Cases

(Provide specific examples or scenarios where this feature would be useful)

## Additional Context

(Add any other relevant information, such as screenshots, links to discussions, or related issues)

## Possible Implementation

(If applicable, suggest ideas or reference code snippets that could help implement this feature)

/label ~"Type: Feature Request"
/cc @soisyhpe @bZx 
/assign me
