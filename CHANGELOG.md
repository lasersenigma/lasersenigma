[[_TOC_]]

# V6.23.15

## Fixed bugs
- v1_20_R3 was remapping incorrect Minecraft version

# V6.23.14

## New feature
- Add a 'No mirror' option in Color Selection menu when placing a Mirror Support.

## Fixed bug
- [#341](https://gitlab.com/lasersenigma/lasersenigma/-/issues/341): Add missing translations for "No color" option in Color Selector inventory.

# V6.23.13

## New feature
- Update `/le component color` command logic. It's now based on coordinates instead of component ID and player: `/le component color <x> <y> <z> <color>`.
- Update wiki based on the previous change.

## Fixed bugs
- `/le component color` was not executable in a Command Block.

# V6.23.12
- improve continuous integration pipeline checks pre-merge

# V6.23.11
## New feature

- Add a command to retrieve component's information: `/le component look [range=5]`.
- Add a command usable from Command Block to edit colorable components: `/le component color <player> <component_id> <color>`.
- Update the wiki based on latest feature addition.

# V6.23.10
- [#333](https://gitlab.com/lasersenigma/lasersenigma/-/issues/333): Fixed a bug related to laser receivers occurring when any color is accepted.

# V6.23.9
- [#333](https://gitlab.com/lasersenigma/lasersenigma/-/issues/333): Fix an exception related to null mirror colors.
- Fix an issue preventing lasers from impacting players when lasers light was disabled.

# V6.23.8
## Fixed bugs
- [#340](https://gitlab.com/lasersenigma/lasersenigma/-/issues/340): Attempt to save task during deactivation

# V6.23.7
- Fixed a bug that occurred when a player exited one zone and entered the next without having passed through an intermediate space.
  This made it impossible to orient the mirrors, and the shortcut bar would open and close for no reason.

# V6.23.6
- Victory zones can now be placed anywhere in the puzzle area (and not just on the edges of the puzzle area).
  This makes it possible to create puzzles where the player is teleported out of the zone and still wins (this was possible with other victory conditions, but not with victory zones).

# V6.23.5
- When a player retrieves a mirror, the rotation menu will close. This was already the case before, except that now the rotation menu closes even if it had been opened on a first mirror support and the mirror is retrieved from another mirror support. 
- Fix a bug that prevented the loading of some data when the plugin were loaded.

# V6.23.4
- Refactoring of translation system

# V6.23.3
- Fix a bug related to laser receivers texture when changing their color

# V6.23.2
- Fix a bug with laser receivers that let lasers through when they shouldn't.

# V6.23.1
- Fix a bug occurring when creating laser receivers

# V6.23.0
- Receivers can now let the laser pass through them. This feature is configurable in the receiver's configuration menu.

# V6.22.1
- Fix a bug related to the new burnable blocks feature

# V6.22.0

- Replace meltable clay by burnable blocks. Burnable blocks allows to select the material.
  As before, you can select the color of the laser expected to trigger the block melting.

# V6.21.10

## Chores

- Update classes names to be more relevant and consistent

# V6.21.9
- Fix a Typo
- Remove the unavailable black color when placing glass, glass panes and mirror blocks.
- Include black in color change loop for mirror supports

# V6.21.8

Fix a bug placing the wrong block when selecting stained glass in place component menu.

# V6.21.7

Fixed a bug related to area components not being loaded from database.
This occurred if the component were on the puzzle area border.

# V6.21.6

## New features

- Lasers now going through following blocks:
  - Light
  - Air (Cave, Void and Structure)
  - Water
  - Barrier
  - Comparator
  - Repeater
  - Redstone
  - Redstone Wire
  - Tripewire
  - Tripewire Hook
  - Lever
  - Activator Rail
  - Detector Rail
  - Powered Rail
  - Buttons
  - Pressure Plates
  - Scaffolding
  - Chain
  - Iron Bars
  - Candles
  - Vine
  - Lily Pad
  - Slabs
  - Carpets
- You can add your own blocks in "crossable_materials" option from config.yml

## Bug fixes

- Fixed a behaviour when players were able to drop items on death (#192)
- Fixed missing translation erros.invalid_schematic (#214)

# V6.21.5
- Fixed a bug when players were able to interact with component through walls
- Fixed a bug with /le area show
- Added an alias for /le schematic: /le schem

# V6.21.4
- Fixed a bug that prevents edition mode to turn off when player leaves the server
- Fixed an out-of-bounds error when using /le schematic list command
- Fixed missing translations
  - items.area_delete_confirm_delete.name
  - area.commands.show.description

# V6.21.3
- Fixed a bug related to armorstands duplication

# V6.21.2
- Fixed a bug related to scheduled actions next/previous buttons

# V6.21.1
- Fixed a bug related to glass and glass panes lasers color filtering

# V6.21.0
- Fixed another bug related to components without mirrors
- Allow schematics copy and save from API
- Lasers emit light back again. This feature is now based on light block creation/deletion instead of LightAPI.

# V6.20.8
- Fixed a bug keeping laser particles instead of removing them when the area is deactivated.
  Particles will not reappear when the area is activated again.

# V6.20.7
- Fixed a bug that prevented components that can contain a mirror from being empty when the puzzle area is initialised.
  Mirror supports, filtering spheres and reflecting spheres can now have NO mirror at the start of the puzzle if the puzzle creator so wishes.

# V6.20.6
- Fix a bug in particle/glass interaction
- Fix a bug in scheduled actions edition menu

# V6.20.5
- Fix a bug in meltable clay component creation

# V6.20.4
- Fix revision injection in continuous deployment for developers using the plugin as dependency

# V6.20.3
- Fix a bug related to custom heads
- Fix permissions

# V6.20.2
- deactivate lightAPI by default

# V6.20.1
- Fix a bug related to custom heads

# V6.19 and V6.20

## Minecraft 1.20.3 and 1.20.4 compatibility

Lasers-Enigma is now compatible with Minecraft versions 1.20.3 and 1.20.4.
On the other side, we removed 1.12 - 1.16 compatibility. The plugin is now compatible with 1.17+ versions of Minecraft.
Don't forget to use Java 16 for 1.17 version and Java 17 for 1.18+ versions of Minecraft.

## Website refactoring

The website has been completely redesigned to be more user-friendly and to provide clear information about the plugin.
The website does not contain news anymore, but the changelog is now available here.

## New features
- Locks are now emitting a redstone signal when activated
- WorldEdit compatibility is now fixed for every version of Lasers-Enigma
  => Please use WorldEdit 7.2.20

## Feature improvements
- Instead of being spawn then updated, components are now directly spawned with the correct display
- The item title and lore have been colored
- Error messages have been improved and colored
- Update notifier now undertake the semantic version during comparison
- Clipboard / Schematics error messages have been improved and completed
- Undertake the ability to change the height of worlds in the clipboard / schematics features

## Other changes
- We removed Twitter/Facebook accounts (we don't like these platforms and posting automation is painful)
- Test plan updated and improved
- The translations are not available anymore. Only english is available. The translations will be available again in the future.

## Bugfixes
- We fixed a bug related to arrows blocked under a component. The invisible ArmorStand’s hit box prevented arrows from going through.
- Component skin now changes during scheduled actions
- When a key was found, left keys count were not updated
- An error occurred when player leaved an area
- Mirror support reset have been fixed
- Filtering sphere color changed have been fixed
- Lock options are now correctly based on configuration
- Components are not correctly deactivated when the puzzle area is deactivated
- Refactoring commands hierarchy
- Enchantment are now hidden in item lore
- Add some missing translations
- and many more ...

## Technical changes
- Replacing the old command system with CommandLib
- Replacing the old translation system with TranslationLib
- Replacing the old Item system with ItemLib
- A lot of code refactoring / splitting to improve maintainability and readability
- Now we are not compatible with 1.12/1.13 anymore, we have managed to remove a lot of NMS code
- CI/CD now compile NMS instead of illegally retrieving them.
- Deployment have been fixed and the maven repository 
- CI/CD now publish on our test server.
- CI/CD now publish on discord (including changelog differences).

# V6.18.0

## Video tutorial
Thanks to the combined work of several people from the Lasers-Enigma and Skytale teams, a video tutorial is finally available!

See [Lasers-Enigma tutorial](https://youtu.be/-A-5SMYaVsI)

I’d like to do more tutorials to make it easier for users to understand the different features of the plugin. However, this requires a considerable amount of time so I’ll take it step by step.

Who is behind this tutorial?
- Matéo on the editing side
- Marco who lent his voice
- Dams who realized the logo and its motion design
- Luca who composed the sound effects of this motion design
- Skytale for the accompaniment (an association in which I volunteer)
- OMGServ, who sponsor us (the website and other technical tools are hosted by them)
- And on my side (Benjamin/bZx), I wrote the script, directed and managed the project

## Minecraft 1.19.3 and 1.19.4 compatibility
Crilian, Martin and I have updated Lasers-Enigma to ensure compatibility with Minecraft versions 1.19.3 and 1.19.4.
However, some plugins and software libraries that Lasers-Enigma depends on are not yet compatible with version 1.19. Thus, the following functionality is unavailable on Minecraft 1.19 for now:
- DungeonsXL : Compatibility with DungeonsXL is no longer guaranteed (pending the release of a new version of this plugin).

On the other hand, a plugin that was previously unavailable in version 1.19 now works:
- LightAPI : An issue that prevented lasers from actually emitting light on the latest versions of Minecraft has been fixed.
    => If you are interested in this feature, please:
     - Update LightAPI (by getting this version).
     - activate (again ?) this feature in the configuration of Lasers-Enigma plugin.
       In the file plugins/LasersEnigma/config.yml:
       ```yaml
       laser_light: true
       ```
       
     - configure LightApi correctly:
       In this file plugins/LightAPI/config.yml:
       enable-compatibility-mode: false
       force-enable-legacy: false

Don’t forget to update other dependencies for 1.19.4 compatibility (FastAsyncWorldEdit / Noteblock API).

## New sound effects and sound system redesign
Based on feedback from Skytale’s game designers, new sounds have been added to enhance the player experience:

- When a laser emitter (previously deactivated) is activated. 
  This happens when the laser emitter has been configured to be activated only under predefined conditions (redstone, number of active receivers in the area, …).

- When the color of the laser coming out of a hub changes.

- When the puzzle is successful (depending on the configuration of the puzzle zone).

The sound channel on which the sound effects are played is configurable (by default MASTER).

The part of the source code dedicated to sound management has been redesigned to allow you to change these sounds easily by programming. So, if your server uses a resource pack containing sounds, you will be able to replace the default sounds with your own sound effects.

## Minor fixes and improvements

- As a player, it is no longer necessary to look at a component when changing its orientation. It is now sufficient to stay within 6 blocks of it.
- As a player, you can now select a component in the water.
- A bug concerning phantom components has been fixed. This bug appeared as a duplicate component or a component that could not be removed. It will not appear anymore. However, if you have had the bug before, deleting phantom armor stands will have to be done manually.
  To do this, place yourself next to a component in error and type the following command:
  ```
  kill @e[type=minecraft:armor_stand,distance=3]
  ```
  Then exit the puzzle area and re-enter. You will see that the problem is corrected.

# V6.17.0

## Minecraft 1.19 compatibility
Mainly thanks to the work of Crilian, the update of Lasers-Enigma to ensure compatibility with version 1.19 is functional.

However, some plugins and software libraries that Lasers-Enigma depends on are not yet compatible with version 1.19. Thus, the following features are unavailable on Minecraft 1.19 for now:

- DungeonsXL: Compatibility with DungeonsXL is no longer assured (waiting for an update of this plugin).
- LightAPI : Lasers do not emit light (waiting for an update of this library).
  => You have to disable (temporarily) this feature in the plugin configuration:
  ```yaml
  laser_light: false
  ```

Don’t forget to update other dependencies for 1.19 compatibility (LightAPI / FastAsyncWorldEdit / NoteBlockAPI)!

## New discord server
For reasons a bit long to explain here, it has been decided to move to a new discord server!
It has been completely redesigned to be more practical and simple. I must however apologize to the French speakers: To suit my availability, I have chosen to put everything in English.
Here is the new address: https://discord.gg/SAtexFXyBq

# V6.16.0

## Minecraft 1.18 compatibility
Due to the increase of my professional activities, the update of Lasers Enigma to ensure compatibility with the 1.18 version of Minecraft has been delayed. However, with the help of several people (Crilian, Ady, Vincent), I was able to finish this version and check its good working.

The dependencies of Lasers-Enigma have changed! Be careful to update the different jars in your plugins folder!

## Bug fixes
Many bugs have been fixed. Here is a non-exhaustive list:

- Corrections and compatibility update for /lasers commands (allowing to copy and paste puzzle areas or store them in schematic). So now the plugin works with the latest versions of WorldEdit and FastAsyncWorldEdit.
- Correction of various translations.
- Corrections and compatibility update for the custom armor creation system.
- Corrections to the interface of prisms and redstone sensors.
- Back to the original version of LightAPI which, following updates, is now compatible with 1.18 (while LightAPI-fork does not seem to be updated anymore)


# V6.15.0

## Universal receivers
In the continuity of my numerous exchanges with François (a game designer), I have developed a new feature concerning laser receivers. It is now possible to have laser receivers that accept any color.

This consists of a small option accessible in your shortcut bar when you have selected a laser receiver.

With this button, the receiver can either accept any color or only lasers of the selected color.
This same option is also accessible through the configuration menu of the component.

This checkbox again allows you to set the laser receiver to accept either any color or a specific color.
Once this option is activated, the receivers are symbolized by a multitude of colors.
From then on, it will be enough to send at least one laser, whatever its color, to activate this receiver.

# V6.14.2

## New textures for greater clarity
After many discussions with a gamedesigner (François), we started to redesign the textures of some components of Lasers-Enigma. This concerns the laser emitters and receivers, the concentrators (which combine colors) and the locks.
The redesign of these textures achieves three major objectives:

- From now on, the player can see directly if he has the right to orientate a component or not. This is visible at a glance, on the texture. It is symbolized by white bands, vertical or horizontal (or both, depending on the player’s rights in this puzzle area). This modification concerns emitters, receivers and concentrators. However, it does not affect the mirrors which keep their original texture.
- The player can more clearly see the color of each receiver or transmitter, activated or not.
- The player can recognize more easily if a laser receiver is activated or not because of the difference in brightness of the textures.

Thanks to Gabin (Ma0ling) and Bryan who also worked on these textures.

By the way, to make this evolution easily, we have coded 2 utilities that could be useful if you need to create skins in a chain.

- I created the first one to build textures from many layers. It realizes all the combinatorics of these layers and thus allows to create many skins very quickly. It is available here.
- The second one allows you to send skins in bulk to textures.minecraft.net (so you can use them as skulls). Bryan can send it to you on request.

# V6.13.0

## Minecraft 1.17 Compatibility
Lasers-Enigma is now compatible with Minecraft 1.17 (bukkit/spigot/paper).

## Mirrors remain in place
When you place a mirror as a puzzle creator, the mirror will stay in place. It will also retain the orientation you set. Thus, you can make sure that mirrors are already placed and oriented when a player enters a puzzle area.

## Blocked mirrors
Fixed mirrors can now be created.
Previously, it was possible to prevent players from orienting mirrors within a puzzle area. This was possible through the configuration menu of each puzzle area. Unfortunately, this affected all the mirrors in the puzzle area. It was therefore not possible to configure each mirror independently.
Now you can choose to let your players orientate some mirrors while blocking others.

# V6.11.2

## Dungeon XL compatibility
You can now use [DungeonsXL](https://www.spigotmc.org/resources/dungeonsxl.9488/) with LasersEnigma. This plugin can be very useful if you want several groups of players to play simultaneously on the same series of puzzles!

Please note that compatibility between DungeonsXL and LasersEnigma is only available from version 0.18 build #1070, which is available within their [Dev Builds](https://erethon.de/repo/de/erethon/dungeonsxl/dungeonsxl-dist/) repository.

Bug fixes
Many corrections have been made. Here is a brief list:

– Correction of a bug in the edition of the lifts.

– Fixed a bug when a player was leaving a puzzle by teleporting to a different world.

– Component rotations, when performed as programmed actions, are now silent.

– Programmed actions took very long to save in the database. This sometimes caused the server to freeze for about 3 seconds. This backup is now performed asynchronously.

– By using plugins such as Multiverse, you may delete or unload worlds that contain puzzles. An error could occur when the server started if there was an unloaded world containing a puzzle area where a player had a checkpoint. This problem has been fixed!

# V6.10.0

## 1.16.4 compatibility

The plugin is now compatible with Minecraft 1.16.4.

# V6.9.0

## Commands and autocompletion

Wanting to change the size of a puzzle area is not uncommon. This is often the case after placing the components and building the surrounding construction. Until now, in order to do this, it has been necessary to remove and recreate the puzzle room and all its contents. This was extremely laborious.

To remedy this, commands have been added:

```
/lasers area expand <amount> [direction]
/lasers area contract <amount> [direction]
```

These two commands can be used to expand or contract the size of the puzzle areas. They work in the same way as the corresponding WorldEdit commands.
A third command displays the shape of the puzzle areas:
```
/lasers area show
```

I took the opportunity to code the autocompletion. This way, you will be able to simply base your commands on the suggestions displayed as you type your commands, without having to go and consult the documentation.

![Autocompletion](https://lasers-enigma.eu/wp-content/uploads/2020/11/area-show.png)

## Empty inventory in areas

Following a Shimaletik request, it is now possible to empty the inventory of players entering a puzzle room. They will get their inventory back on exit. To enable this feature, you will need to set the following option in the configuration:
```
clear_inventory_on_enter_area: true
```

## Russian translation
Thanks to Shimaletik, the Russian translation was added. Thanks to him and welcome to the Russian speaking people.

## (Un)loading worlds
If you load a world that contains puzzles when the server is already running (e.g. using the Multiverse plugin), the lasers will work correctly.

## Interface
Added a few back buttons within the nested menus of the scheduled actions.

## Many bugs fixed
- A bug related to the elevators (Following a recent update, they could not go up as high as before. For a lift … it’s a pity).
- A bug concerning the gravitational spheres (This problem prevented to access the advanced configuration menu of this component).
- A bug concerning the keychests and the call buttons of the lifts (these components were well saved in database but they were no more loaded at the plugin boot and this since 6.8.0. As a result, they disappeared when the server was restarted. All those that disappeared will reappear after you update the plugin).
- A bug concerning scheduled actions (when editing a scheduled action, if this action type was not “wait for a delay”, it was impossible to delete it or to use the back button).



# V6.8.0

## Lasers emitting light

![Light showcase](https://i0.wp.com/lasers-enigma.eu/wp-content/uploads/2020/10/light-level-12.png?ssl=1)

People have done a great job of allowing developers to manipulate the Minecraft brightness level in their plug-ins. Following a suggestion from DoreK, I used this tool, called LightAPI, to make lasers light up the darkest rooms.
So from this version on, if you put LightAPI.jar in your /plugins/ folder, you will see that lasers are finally emitting light.

- Configuration by component:
  It is possible to set the brightness level of each light component. That is, the laser transmitters and receivers. To do this, you will find, in the configuration menu of these two types of components, a small button that opens a dedicated menu.

- Global configuration:
  Since you may already be using LightAPI for other plugins, but do not want LasersEnigma to use it (for performance reasons), you can disable this feature through a line in config.yml:
  ```
  laser_light: <true|false>
  ```
  ![Light configuration menu](https://lasers-enigma.eu/wp-content/uploads/2020/10/light-menu.png)

  At the same place, you can also set the default brightness level that will be registered in each new component (until it is changed through the menu explained previously):
  ```
  laser_default_light_level: <1-15>
  ```

## New translations
As usual, restart the server twice so that the translations are taken into account.

## For Developers
The API is therefore enhanced with a new event allowing to deny an administrator the possibility to modify the brightness level of a component:

```
PlayerTryToChangeLightLevelLEEvent
```

And as always, it is possible to directly change the brightness level of a component by using the following instruction:
```
((ILightComponent)Areas.getInstance()
  .getAreaFromLocation(loc)
  .getComponentFromLocation(loc))
  .setLightLevel(12);
```

Or for all components in an area:
```
Areas.getInstance()
  .getAreaFromLocation(loc)
  .getComponents()
  .stream()
  .filter(c -> c instanceof ILightComponent)
  .forEach(c -> ((ILightComponent)c).setLightLevel(12));
```

From now on, the only monsters that will appear in your puzzle rooms are those that you have consciously chosen to place there.

# V6.7.2

## 1.16.X compatibility
The plugin now supports Minecraft versions 1.16.1, 1.16.2 and 1.16.3.

## Pass through blocks configurable

Following Dorek’s suggestion, I made the list of blocks that the laser can cross configurable. You will find it under the name crossable_materials in the configuration file.

# V6.6.0

## Publication

The first one, not the least, is the publication of the plugin. It is now available for all free of charge. The source code is also available to all.
The private beta phase is now over. Thanks to all those who participated! I’d like to take this opportunity to thank all the people who participated in the plugin evolution or who supported me so that we could reach this point.

## 1.15 compatibility

The second news is the support of version 1.15.1. The plugin is now compatible with this last version!

## Player's victory detection
Creation of new ways to know that a player has won a puzzle: In addition to the victory areas it is now possible to use the same criteria as for the conditional components to determine whether a player has won a puzzle or not.

## Scheduled actions
Creation of scheduled actions that can be programmed on different components. This evolution opens the door to many possibilities (including puzzles where the player will have to synchronize themselves with automated actions).

## Minor changes

- Add german translation.
- Improvement of the translation system so that the file is updated automatically when new translations are added in the plugin.
- Particle number reduction for key chests and locks opening animations.
- Reducing the delay between two checks of who’s in the area to avoid certain error cases.
- Buttons to change the orientation of components are now displayed according to the player’s rights in the area.
- Many optimizations to improve the plugin’s performance!
- Add additional anonymous statistics to track the plugin usage. 

# V5.2.0

## Multi-version compatibility


Minecraft is evolving and ensuring compatibility with all versions is very technical, long and laborious.


That’s why, with the help and moral support of WriteEscape, a developer who recently joined the project, we have made a big change in the software architecture.


Version 1.14 is now supported. Better yet, we will now be able to adapt the code much faster to ensure compatibility with older or more recent versions. So if you want to use the plugin in a version that is not yet supported, let us know!

## Colors

You know that until now we used these colors in the plugin:
![Colors wheel old](https://lasers-enigma.eu/wp-content/uploads/2019/10/Colors_pygments.png)
These colours are the ones we discovered in our childhood using paint or modelling paws. Pigments are mixed and the pigments absorb frequencies of light. In the end, when you have mixed everything up, you get black in theory (and brown in practice) and not white at all as it was in the plugin. This way of doing things, of removing more and more frequencies, is what is called a subtractive scale.
The problem is that light does not work that way.

Light, for example in a CRT screen, would rather use this scale, which is additive: frequencies are gradually being added.
![Colors wheel new](https://lasers-enigma.eu/wp-content/uploads/2019/10/Colors_light-1.png)

From now on, the plugin will work like this. It’s probably a little harder for a player to understand, but at least it fits with physics.
Well, I hope I haven’t lost you too much…
![Homer Simpsons does not understand](https://lasers-enigma.eu/wp-content/uploads/2019/10/homer-brain.gif)

## Mirror blocks

A new block has been introduced: the mirror block. You can use it to make reflective walls.
I think you’ll enjoy it…

![Mirror blocks showcase](https://lasers-enigma.eu/wp-content/uploads/2019/11/2019-11-02_18.36.00.png)

## Elevator blockage

We welcome Skyhighjinks, a new developer, who worked alongside me to produce this function for the plugin as his first contribution to the project!

When building a series of puzzles with a central elevator, it may be convenient to prevent the access to some floors until the player has successfully completed the previous floors.
To allow this, the elevators now get blocked when obstructed rather than rampaging through the material. After making a sound to indicate to the player that there was an error, they then return to the floor they had just left.

## Armor Menu

Once again with Skyhighjinks, we have created a new menu to simplify armor creation. Before, it was necessary to have a minimum knowledge of the command blocks to be able to do this. Today everything can be done through a simple menu.

## Elevator
Who has never dreamed of crossing the sky like a bird … in an elevator.

![Zombi in an elevator](https://lasers-enigma.eu/wp-content/uploads/2019/05/zombie-elevator.gif)

Start by selecting the area that will make up your elevator shaft (two points, like when creating a puzzle area).

Then get in the elevator and you will just have to jump up or down to get down.

![Elevator going up](https://lasers-enigma.eu/wp-content/uploads/2019/05/elevator-large.gif)

You will then have to create floors and, if you wish, place call buttons.
Finally, you can make the elevator conditional and define very precisely its activation conditions.

You will also find in this update, in addition to many bug fixes, an improvement in the ergonomics of the user interface:
From now on, when a player right-clicks on a steerable component (a mirror for example), his shortcut bar changes to contain 4 objects representing arrows which, if he right-clicks with them in his hand, will allow him to turn the component in any direction.

![Pushing all buttons in an elevator](https://lasers-enigma.eu/wp-content/uploads/2019/05/elevator-troll.gif)

# V5.1.0

![V5.1.0 fry meme: with those two last updates, not sure if I made the plugin simpler OR MORE COMPLEX](https://lasers-enigma.eu/wp-content/uploads/2019/02/simpler-or-more-complex-en.jpeg)

I come back to you with a major update that, in addition to an optimization that should relieve the least powerful computers, finally includes the features essential to creators of adventure cards, dungeons and other RPGs.

## Keys and locks
![Keys and locks schema](https://lasers-enigma.eu/wp-content/uploads/2019/02/Area_keys_and_locks.png)
In this example, succeeding in the red room is essential, while succeeding in only one of the yellow and blue rooms is sufficient.

Previously it was very complex to link rooms together, this involved the use of redstone or command blocks. Today all this is becoming much easier thanks to the new system of keys and locks.
- Create a lock (in the same way as any other component).
- Create a key chest (from the lock).
- Assign the color of your choice to the key box.

If several key chests have the same color, then players will only need to obtain one of the keys of these chests to be able to open the lock.

## Checkpoints
![Checkpoints schema](https://lasers-enigma.eu/wp-content/uploads/2019/02/checkpoints.png)
If you die in the puzzle, you will return to the beginning of the puzzle. If you die after succeeding, you will respawn where you got out of the puzzle area.

One of the difficult aspects to manage when creating adventure maps is the management of backup points, also called control points. Now, the plugin manages this entirely. You will finally be able to die in complete serenity!


In the puzzle area configuration, you can define a control point, this is where your players will respawn if they die in the puzzle zone.


You will also find, in this same menu, the possibility to create victory areas. These places (symbolized in green on the diagram above, must be at the edge of the enigma area. Players who pass inside as they exit the puzzle get a new checkpoint, located just outside the puzzle area. They will reappear at this place if they die after exiting the puzzle area. Moreover, it is there that their statistics are saved (they are therefore essential if you want to use the plugin’s statistics system).

## Armors
![Armors schema](https://lasers-enigma.eu/wp-content/uploads/2019/03/armors.png)
By adding tags to the armors you generate using command blocks, you can change the way they interacts with lasers.

Some players may find it somewhat unpleasant to be continuously burned and repelled by lasers. To reassure these fragile little things, but also for the few dreamers who wanted to interact with lasers directly by placing themselves on their path, I added a new feature.


By adding a few cleverly chosen words to the tags (in 1.13) or to the description (in 1.12) of your armor, you can make them react differently to the lasers that hit you.


For example, the command below will allow you to obtain an armor that will make you a kind of living prism, decomposing the light that passes through you:
```
/replaceitem entity @p armor.chest minecraft:diamond_chestplate{ Tags:[ "le_prism", "le_no_burn", "le_no_damage", "le_no_knockback", "le_durability0" ] } 1
```

In this command, the tag le_prism allows the decomposition of the light. The following tags will allow you to avoid the burning (le_no_burn), damage (le_no_damage), or knockback (le_no_knockback) usually caused by the impact of a laser. The last tag (le_durability0) makes your armor unbreakable by lasers.

![Armor prism effect](https://lasers-enigma.eu/wp-content/uploads/2019/03/prism.jpg)
Other tags allow you to become a mirror (the_reflect) or a concentrator (the_focus). But where it really gets crazy is when you combine all this!

Of course all this is highly configurable in the “config.yml” file located in the plugin configuration directory. I invite you to take a look at it and adapt these lines to the specificities of your server (or even deactivate them, purely and simply, if they bother you).

![Fry meme: I made it more complex](https://lasers-enigma.eu/wp-content/uploads/2019/02/more-complex-en.jpeg)

# V5.0.7

The main innovation is the refactoring of the entire user interface.
No more sneek right-clicks and other complex features that are difficult for neophytes to understand and use.
Now everything goes through the shortbar and inventory menus. It’s highly practical but above all, it’s much easier to understand.

![v5.0.7 new interface drawing](https://lasers-enigma.eu/wp-content/uploads/2019/02/4.8.2-vs-5.0.7.png)

Even better, this version is now available for Minecraft 1.13.2.

Of course, 1.12.2 compatibility is preserved.

Apart from that, there have been far too many changes, corrections and improvements for me to detail here, so I invite you to try it for yourself.

# V4.8

![V4.8 showcase](https://lasers-enigma.eu/wp-content/uploads/2018/07/Meltable-clay-2.gif)

(Yes, we know that we never published the version 4.7)

![Obi Wan Kenobi saying that the missing 4.7 isn't the version you're looking for](https://lasers-enigma.eu/wp-content/uploads/2018/07/4.8-Kenobille-.jpg)

New component: Redstone Sensor
    A redstone sensor is activated when it receives a redstone signal.
    The detection components now have a new detection mode, able to count the number of redstone sensors activated inside the puzzle area.

![redstone sensor component skin](https://lasers-enigma.eu/wp-content/uploads/2018/07/redstone-sensor-activated-screenshot-279x300.png)

Color Selector upgraded

    The tool used to change the color of components (the laser receivers and senders) now works on meltable clays, mirror supports (containing a mirror), reflecting and filtering spheres.

Burnable Block explosion improved

    The meltable clay explosion now knockback and damage players.

![meltable clay explosion](https://lasers-enigma.eu/wp-content/uploads/2018/07/Meltable-clay-2.gif)

Laser Receivers: Redstone connectivity

    Laser receivers now send a redstone signal in a 2 blocks radius.

![activated laser receiver sending redstone signal](https://lasers-enigma.eu/wp-content/uploads/2018/07/laser-receiver-redstone-300x165.jpg)

Laser entity impact

    Sparks are now shown when an entity is affected (burnt or/and damaged) by a laser.
    Entities affected by lasers now stops the light flow. The laser does not pass through entities anymore (configurable).

Textures

    We switched textures of the reflecting and filtering spheres (the reflecting sphere textures now have kinds of glints. The filtering sphere textures are made of solid colors).

Bugfixes

    The bug appearing when attraction/repulsion spheres were overlapping each other has been fixed.

Translations

    Translations completed according to the last additions.

# V4.6

![V4.6 showcase](https://lasers-enigma.eu/wp-content/uploads/2018/06/2018-05-06_19.23.40.png)

– Attraction and Repulsion sphere components have been created.
– The mode (attraction or repulsion mode) of those spheres can be changed using [sneek + left clic].
– The radius of the sphere where laser particles will be affected can be increased using [right clic] and decreased using [sneak right clic].
– The ability for players to modify the radius of the sphere (where laser particles will be affected) can be deactivated using a new area attribute item.

# V4.5

– All the Detection Component (Redstone Victory Block, Appearing Victory Block, Disappearing Victory block, Music Block and Laser Sender) have a new detection mode available:
– They can now activates themselves according to the number of players inside the area.
– Changing the current mod is still available using sneak + left clic.
The most simple use of this new functionnality is the creation of doors that will block the area entrance after a player come inside.

# V4.4

![V4.4 showcase](https://lasers-enigma.eu/wp-content/uploads/2018/06/2018-05-03_20.37.23.png)

– The lasers now affect mobs and players.
– They do damages and burns.
– They throws you away (knockback)

Since you may want to deactivate that the following parameters are now available in configuration:

laser_affect_mobs: true
laser_affect_players: true
laser_burns_tick_duration: 40
laser_additional_damage: 2
laser_knockback_multiplier: 1

# V4.3

![v4.3 showcase](![img.png](https://lasers-enigma.eu/wp-content/uploads/2018/06/2018-05-02_14.43.29.png)

– The Reflecting Sphere reflection process have been reworked and improved:
Before this modification, the reflecting spheres were simply redirecting the laser to where it was coming from.
From now on, those spheres will work like a disco ball: the reflection angle will be calculated according to the intersection point between the laser and the sphere.
A cache system have also been implemented to avoid recalculate reflection result continuously.

# V4.2

![v4.2 showcase](https://lasers-enigma.eu/wp-content/uploads/2018/06/plop-300x49.png)

– The Laser Senders and Laser Receivers can now be rotated by players. Those rotations are not allowed to players by default but they can be (de)activated by area editors using Area Attributes Items.

– Refactoring of the plugin’s bukkit events handlers …

– bugfixes

# V4.1

![v4.1 new logo](https://lasers-enigma.eu/wp-content/uploads/2018/06/lasers-1.png)

– Some bugfixes.

– A statistics system have been implemented:

/lasers stats [playerName|top|clear|link|unlink]

Using this command you can see your stats / a specific player stats / the top10 stats for every area.

Stats corresponds to:

– The time past inside the area.

– The number of steps done inside the area.

– The number of actions done inside the area.

You can retrieve those stats from another plugin by the following ways:

– Using the Area API area.getStats() (and its sub methods).

– Using the PlayerLeavedAreaLEEvent getStats() method.

The link and unlink commands can be used to have the same statistics inside 2 areas (in case you have multiple instances of the same enigma in order to let many players play inside the same campain at the same time).

– I designed a new logo (very simple … I’m not a designer ^^).

# V4.0

![v4.0 showcase](https://lasers-enigma.eu/wp-content/uploads/2018/06/laser3.png)

Some already spoiled changes:

  - /lasers <copy/paste/load/save> [schematic-file-name] is working fine.
  - MySQL backend is now fully functional (as well as the older sqlite backend).
  - The ability to modify the lasers particles movement speed and refresh frequency as been removed (for very good reasons …).

And some nice news for devs:

  - A lot of events are now available for developers to interact with this plugin:
    - Area events:
      NBActivatedReceiversChangeLEEvent
      NBActivatedReceiversChangeNoDelayLEEvent
      PlayerCreatedAreaLEEvent
      PlayerDeletedAreaLEEvent
      PlayerEnteredAreaLEEvent
      PlayerLeavedAreaLEEvent
      PlayerTryToCreateAreaLEEvent
      PlayerTryToDeleteAreaLEEvent
      PlayerTryToSelectFirstLocCreateAreaLEEvent
      PlayerTryToSelectFirstLocDeleteAreaLEEvent
      PlayerTryToToogleHConcentratorsRotationAreaLEEvent
      PlayerTryToToogleHMirrorsRotationAreaLEEvent
      PlayerTryToToogleVConcentratorsRotationAreaLEEvent
      PlayerTryToToogleVMirrorsRotationAreaLEEvent
    
    - Component events:
      PlayerTryToChangeColorLEEvent
      PlayerTryToChangeLaserSenderModeLEEvent
      PlayerTryToChangeMusicBlockSongLEEvent
      PlayerTryToChangeRangeLEEvent
      PlayerTryToCreateComponentLEEvent
      PlayerTryToDecreaseMirrorChestLEEvent
      PlayerTryToDeleteComponentLEEvent
      PlayerTryToIncreaseMirrorChestLEEvent
      PlayerTryToPlaceMirrorLEEvent
      PlayerTryToRetrieveMirrorLEEvent
      PlayerTryToRotateComponentLEEvent
      PlayerTryToToogleMusicBlockLoopLEEvent
      PlayerTryToToogleMusicBlockStopOnExitLEEvent
    
    - Command events:
      PlayerTryToUseLasersClipboardCommandLEEvent
      PlayerTryToUseLasersCommandLEEvent
      TryToUseReloadCommandLEEvent
    
    - Laser Particle events:
      ParticleTryToHitBlockLEEvent
      ParticleTryToHitComponentLEEvent
      ParticleTryToMoveInTheAirLEEvent
    
    - Permissions :
      PlayerPermissionsCheckLEEvent

# V3.8

Some small update to keep you informed… but nothing for beta testers yet.
==> @Ak_Hiro has joined the plugin dev team. Give him the welcome that is his due !
==> Version 3.8 will never be available because we are on the way of the version 4.0 !

Done:
– Add mysql backend and make it configurable through config.yml (sqlite is still available)
– Remove the ability to configure lasers frequency and speed (useless and potentially a big problem cause).
– The ability for administrators to copy/paste/load/save enigma. @Ak_Hiro just finished debugging the worldedit part ! Nice work by the way !

Next:
– I’m doing a gigantic refactoring which will add many cancellable events to the plugin. The consequences of this transformation will be very nice mainly for two reasons:
  - All plugin developers will be able to interact with this plugin smoothly !
  - The code will be very easier to read, understand and modify.

# V3.7

![v3.7 showcase](https://lasers-enigma.eu/wp-content/uploads/2018/06/laser2.png)

– The Lasers Senders now supports modes. You can change its mode using shift left click on it.

– Default Mode: Nothing new here. The laser sender will always work and send a (colored) laser.

– Redstone Conditionnal Mode: The laser sender will only work if it is powered by redstone.

– Range Conditionnal Mode: The laser sender conditions can be defined using the Detection range selector. It means you can define the minimum and maximum number of activated laser receivers needed for the laser sender to work. I take this occasion to thank @Fogux for his help

# V3.5

![v3.5 showcase](https://lasers-enigma.eu/wp-content/uploads/2018/06/laser1.png)

– There is new component: Music block ! You will now be able to add many songs to your enigma.

– A music block is able to play any .nbs songs. You can select the song you want to play by scrolling a list. This can be done using the new tool Song Selector and by left/right clicking the Music Block.

– You can add or remove .nbs songs from the songs/ folder inside the plugin folder. By default almost 200 .nbs songs are available.

– By right clicking the Music Block you can define whether the song will be stopped when the player leaves the area or if it will continue until its end (stopOnExit mode).

– By shift right clicking the Music Block you can define whether the song will loop for players that stays inside the area or stop after its end (loop mode).

– The modes (stopOnExit and loop) are visible on the Music Block skin (thanks to @Redomfighter for the skin creation).

– Using the Detection range selector you can select the range of activation of the music block. It means you can define the minimum and maximum number of activated laser receivers needed for the song to be played (Working exactly like for Redstone victory block, Appearing victory block, Disappearing victory block). In order to do that you need to do (shift) left/right clicks on the block with the Detection range selector in your hand.

# V3.4

– Now all resolving detection components (Disapearing winner block, Appearing winner block, Redstone winner block) handles a range of activation. Before you had to define the minimum value: The block was activated (and appeared / disappeared / transformed itself into redstone) when AT LEAST that minimum value was reached. From now on you will be able to define that the block will be activated between that minimum number and a maximum number using a new item: the Detection range selector.

- The backend have been set for the next major update which will be available very soon :). It doesn’t seem much since it is not visible for players but it was a lot of work … This major update, like I said earlier, is about sound. I continue the trailer: Do you know the NBS format ?

I also take this occasion to thank @Redomfighter for his help on custom head skins edition and @LeRoiTigre and other beta testers for their work !

# V3.3:

– A new component: Appearing winner block.
  The block will appear only when enough laser receivers are activated. It was made to disable some parts of the enigma while its resolution progress.

– Some bugfixes and minor improvements: Many things have been done but the most important is the inventory switchers. They are working really better right now. You will be able to switch from an inventory to another very quickly !

# V3.2

– The plugin have been renamed “Lasers Enigma”

– 2 new components:

    The Reflecting sphere: When you put a white mirror inside it it will reflect all lasers to the direction they were coming from.
    Using different colors will change the reflected colors (same behavior as mirrors). For information this component was added in version 3.1
    The Filtering sphere: When you put a white mirror inside it it will let pass all lasers.
    Using different colors will change the lasers colors (same behavior as glass blocks).

– The mirror placing and retrieving animations have been improved and optimized.

– I also did some code cleaning, a bit of refactoring (arround MirrorsContainers like mirrorSupport) and some small issues were fixed.

# V3.0
– The great update content is the Prisms adding. They work perfectly and the result is wonderful !
– A deep refactoring has been made in order to make many components appear inside an invisible block.
  I did this mainly for 2 reasons:
  – It will be easier to rotate all rotatable components (laser senders/laser receivers/mirrors): their hitbox is now as large as the invisible block which contain them.
  – Those elements are now solid. We can’t pass through them anymore.
– Black mirrors now reflect every laser colors they receive but also let every laser colors pass. So they can be used to “duplicate” a laser.
– Many minor issues have been fixed but I won’t list them here. First because I can’t remember each issue I fixed, then because I suppose you just don’t care.
