/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.lasersenigma.nms.v1_20_R1;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_20_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashSet;
import java.util.List;

/**
 * @author Benjamin
 */
public class NMSItems {

    @SuppressWarnings("SetReplaceableByEnumSet")
    public static final HashSet<Material> CROSSABLE_MATERIALS = new HashSet<>();

    public static ItemStack getArmor(String tags) {
        ItemStack itemStack = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
        net.minecraft.world.item.ItemStack chestPlateNMS = CraftItemStack.asNMSCopy(itemStack);
        CompoundTag rootNBTtagCompound = new CompoundTag();
        ListTag tagsList = new ListTag();
        tagsList.add(StringTag.valueOf(tags));
        rootNBTtagCompound.put("Tags", tagsList);
        chestPlateNMS.setTag(rootNBTtagCompound);
        return CraftItemStack.asBukkitCopy(CraftItemStack.copyNMSStack(chestPlateNMS, 1));
    }

    static void appendToItemLore(ItemStack itemStack, String txtToAdd) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        List<String> loreLines = itemMeta.getLore();
        loreLines.add(txtToAdd);
        itemMeta.setLore(loreLines);
        itemStack.setItemMeta(itemMeta);
    }
}
