/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.lasersenigma.nms.v1_19_R3;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_19_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;

/**
 * @author Benjamin
 */
public class NMSItems {

    @SuppressWarnings("SetReplaceableByEnumSet")
    public static final HashSet<Material> CROSSABLE_MATERIALS = new HashSet<>();

    public static ItemStack getArmor(String tags) {
        ItemStack itemStack = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
        net.minecraft.world.item.ItemStack chestPlateNMS = CraftItemStack.asNMSCopy(itemStack);
        CompoundTag rootNBTtagCompound = new CompoundTag();
        ListTag tagsList = new ListTag();
        tagsList.add(StringTag.valueOf(tags));
        rootNBTtagCompound.put("Tags", tagsList);
        chestPlateNMS.setTag(rootNBTtagCompound);
        return CraftItemStack.asBukkitCopy(CraftItemStack.copyNMSStack(chestPlateNMS, 1));
    }
}
