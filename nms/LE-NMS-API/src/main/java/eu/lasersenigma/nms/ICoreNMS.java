package eu.lasersenigma.nms;

import eu.lasersenigma.items.AArmorActionProcessor;
import eu.lasersenigma.items.ArmorAction;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface ICoreNMS {

    // TODO some classes are still in LS-NMS-API but not required anymore in LE-NMS-v1_XX. They should be moved to the core module.

    AItemStackFlagsProcessor getItemStackFlagsProcessor();

    AArmorActionProcessor getArmorActionProcessor(Player bukkitPlayer, ArmorAction selectedArmorAction, Location newMaterialLocation, boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber, String armor_no_knockback_tag, String armor_no_burn_tag, String armor_no_damage_tag, String armor_reflect_tag, String armor_prism_tag, String armor_focus_tag, String armor_lasers_durability_regex);
}
