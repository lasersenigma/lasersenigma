/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.lasersenigma.nms.v1_17_R1;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_17_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;

/**
 * @author Benjamin
 */
public class NMSItems {

    @SuppressWarnings("SetReplaceableByEnumSet")
    public static final HashSet<Material> CROSSABLE_MATERIALS = new HashSet<>();

    public static ItemStack getArmor(String tags) {
        ItemStack itemStack = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
        net.minecraft.world.item.ItemStack chestPlateNMS = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound rootNBTtagCompound = new NBTTagCompound();
        NBTTagList tagsList = new NBTTagList();
        tagsList.add(NBTTagString.a(tags));
        rootNBTtagCompound.set("Tags", tagsList);
        chestPlateNMS.setTag(rootNBTtagCompound);
        return CraftItemStack.asBukkitCopy(CraftItemStack.copyNMSStack(chestPlateNMS, 1));
    }
}
