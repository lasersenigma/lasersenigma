package eu.lasersenigma.nms.v1_17_R1;

import eu.lasersenigma.items.AArmorActionProcessor;
import eu.lasersenigma.items.ArmorAction;
import eu.lasersenigma.nms.AItemStackFlagsProcessor;
import eu.lasersenigma.nms.ICoreNMS;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class NMS implements ICoreNMS {

    public NMS() {
    }

    @Override
    public AItemStackFlagsProcessor getItemStackFlagsProcessor() {
        return new NMSItemStackFlagsProcessor();
    }

    @Override
    public AArmorActionProcessor getArmorActionProcessor(Player bukkitPlayer, ArmorAction selectedArmorAction, Location newMaterialLocation, boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber, String armor_no_knockback_tag, String armor_no_burn_tag, String armor_no_damage_tag, String armor_reflect_tag, String armor_prism_tag, String armor_focus_tag, String armor_lasers_durability_regex) {
        return new NMSArmorActionProcessor(bukkitPlayer, selectedArmorAction, newMaterialLocation, noBurnCheckbox, noDamageCheckbox, noKnockbackCheckbox, reflectionCheckbox, focusCheckbox, prismCheckbox, durabilityNumber, armor_no_knockback_tag, armor_no_burn_tag, armor_no_damage_tag, armor_reflect_tag, armor_prism_tag, armor_focus_tag, armor_lasers_durability_regex);
    }
}
