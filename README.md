# Welcome to Lasers Enigma Minecraft plugin !

[![Lasers Enigma banner](http://lasers-enigma.eu/wp-content/uploads/2018/08/logo-line-02.jpg)](https://lasers-enigma.eu)

## Introduction

This is a minecraft plugin (works on spigot/paper) made to create, solve and share puzzles based on light.

For a start, create a laser sender, redirect the light flow using mirrors and win by sending it into a laser receiver !
Colors and 3D rotation are available.
This is the basis, but I highly suggest you to try and see how huge the possibilities are.
This plugin will allow you to create simple puzzles to difficult and complex ones!

## Download

> To download the **latest stable version**, go to [our website download page](https://lasers-enigma.eu/en/download/) or simply follow [this link](https://lasers-enigma.eu/download.php).
> You can also download **archived or beta versions** from this same page.

## Installation

See our [documentation's "Get started" section](https://gitlab.com/lasersenigma/lasersenigma/-/wikis/Installation/Get-started).

# Usage

You can also take a look at the [documentation's "Commands" section](https://gitlab.com/lasersenigma/lasersenigma/-/wikis/In-game/Commands).

Don't forget to give necessary [permissions](https://gitlab.com/lasersenigma/lasersenigma/-/wikis/Installation/Permissions) to puzzle makers.

## Documentation

[**Wiki**](https://gitlab.com/lasersenigma/lasersenigma/-/wikis/home)

## Use the API from your own plugin

In the following sample, replace version number X.Y.Z by the latest version of the plugin.
Consult the [list of available plugin versions here](https://repository.skytale.fr/ui/native/public/eu/lasersenigma/LasersEnigma/).

Using maven:
```
    <repositories>
        <repository>
            <id>skytale-public</id>
            <url>https://repository.skytale.fr/artifactory/public</url>
        </repository>
    </repositories>
    <dependencies>
        <dependency>
            <groupId>eu.lasersenigma</groupId>
            <artifactId>LasersEnigma</artifactId>
            <version>X.Y.Z</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>
```


Using gradle:
```
repositories {
    maven {
        url 'https://repository.skytale.fr/artifactory/public'
    }
}

dependencies {
    compileOnly 'eu.lasersenigma:LasersEnigma:X.Y.Z'
}
```


We've created an example plugin that shows how to create a plugin interfaced with lasers-enigma. It interacts with and modifies the behavior of lasers-enigma.
This plugin is available on a separate git repository: [API usage example](https://gitlab.com/lasersenigma/lasersenigma-sample-api-usage).

You can also take a look at the [Developer API section in our documentation](https://gitlab.com/lasersenigma/lasersenigma/-/wikis/Develop/Use-API).

## Javadoc

Download our [javadoc right here](https://lasers-enigma.eu/download.php?includeSnapshot=true&artifact=javadoc).

## Contributing

* [Contributing](https://gitlab.com/lasersenigma/lasersenigma/-/wikis/Develop/Contribute)

## License

Licenced under "Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)". See LICENCE.txt for details.

## Website

* [Lasers-enigma.eu](http://lasers-enigma.eu)

## Contact & Support

See [Support wiki page](https://gitlab.com/lasersenigma/lasersenigma/-/wikis/Support).

If necessary, you can also contact us:
* By mail[contact@lasers-enigma.eu](mailto:contact@lasers-enigma.eu).
* By discord private message.

## Review and rate the plugin

* [Spigot](https://www.spigotmc.org/resources/lasers-enigma-light-puzzles-editor.73626/)
* [Builtbybit](https://builtbybit.com/resources/lasers-enigma-light-puzzles-editor.13226/)
* [Hangar](https://hangar.papermc.io/bZx/lasersenigma)
* [Modrinth](https://modrinth.com/plugin/lasersenigma)