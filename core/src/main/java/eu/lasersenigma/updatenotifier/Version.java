package eu.lasersenigma.updatenotifier;

public class Version {
    private final int major, minor, patch;

    public Version(String versionText) {
        String[] parts = versionText.split("-")[0].split("\\.");
        major = Integer.parseInt(parts[0]);
        minor = Integer.parseInt(parts[1]);
        patch = Integer.parseInt(parts[2]);
    }

    public Version(int major, int minor, int patch) {
        this.major = major;
        this.minor = minor;
        this.patch = patch;
    }

    public boolean isGreater(Version version) {
        return !isLowerOrEqual(version);
    }

    public boolean isLowerOrEqual(Version version) {
        if (this.major <= version.major) return true;
        if (this.minor <= version.minor) return true;
        return this.patch <= version.patch;
    }
}