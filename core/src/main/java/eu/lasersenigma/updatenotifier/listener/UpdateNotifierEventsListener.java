package eu.lasersenigma.updatenotifier.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.updatenotifier.UpdateNotifier;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.logging.Level;

public class UpdateNotifierEventsListener implements Listener {

    public UpdateNotifierEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "UpdateNotifierEventsListener: onPlayerJoin");

        Player player = event.getPlayer();

        if (!Permission.ADMIN.hasPermission(player)) return;
        UpdateNotifier.getInstance().getUpdateMessages().forEach(player::sendMessage);
    }

}
