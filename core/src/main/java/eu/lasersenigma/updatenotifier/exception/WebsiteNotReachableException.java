package eu.lasersenigma.updatenotifier.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class WebsiteNotReachableException extends AbstractLasersException {

    public WebsiteNotReachableException(Object... params) {
        super("errors.update_notifier.messages.website_not_reachable", params);
    }
}
