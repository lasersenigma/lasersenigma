package eu.lasersenigma.updatenotifier;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.updatenotifier.exception.WebsiteNotReachableException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.logging.Level;

public class UpdateNotifier {

    private static UpdateNotifier instance;

    /**
     * Paths to configuration's booleans (config.yml).
     */
    private static final String CHECK_FOR_NEW_VERSIONS_CONFIG_NODE = "check_for_new_versions";
    private static final String CHECK_FOR_NEW_BETA_VERSIONS_CONFIG_NODE = "check_for_new_beta_versions";

    private static final long DAYS_BETWEEN_TWO_CHECKS = 1;

    /**
     * Online URL of the text file containing the version number of the last release.
     */
    private static final String VERSION_NUMBER_FILE_URL = "https://lasers-enigma.eu/plugin-update/lasers-enigma-version.txt";

    /**
     * Online URL of the text file containing the commit hash of the last release.
     */
    private static final String COMMIT_HASH_FILE_URL = "https://lasers-enigma.eu/plugin-update/lasers-enigma-commit-hash.txt";

    /**
     * This attribute stores the previous time the update check has been done.
     */
    private Instant previousCheck = Instant.now();

    /**
     * This attribute stores messages for administrators in case of available update.
     */
    private final ArrayList<String> updateMessages = new ArrayList<>();

    private UpdateNotifier() {
        this.check();
    }

    public static UpdateNotifier getInstance() {
        if (instance == null) {
            instance = new UpdateNotifier();
        }

        // Checks if we should perform a new check or not
        if (Duration.between(instance.getPreviousCheck(), Instant.now()).toDays() > DAYS_BETWEEN_TWO_CHECKS) {
            instance.check();
        }

        return instance;
    }

    public Instant getPreviousCheck() {
        return previousCheck;
    }

    public ArrayList<String> getUpdateMessages() {
        return updateMessages;
    }

    /**
     * This method checks even if a greater online version exists or not. If it does exist, we're sending
     */
    public void check() {
        updateMessages.clear();

        boolean shouldCheckForNewVersions = LasersEnigmaPlugin.getInstance().getConfig().getBoolean(CHECK_FOR_NEW_VERSIONS_CONFIG_NODE, true);
        boolean shouldCheckForNewBetaVersions = LasersEnigmaPlugin.getInstance().getConfig().getBoolean(CHECK_FOR_NEW_BETA_VERSIONS_CONFIG_NODE, false);

        try {

            if (shouldCheckForNewVersions) {
                checkForNewVersions();
                return; // >e don't want to check for beta versions if a new stable version is available to avoid getting duplicate messages
            }

            if (shouldCheckForNewBetaVersions) {
                checkForNewBetaVersions();
            }
        } catch (WebsiteNotReachableException exception) {
            TranslationUtils.sendExceptionMessage(exception);
        }
    }

    private void checkForNewVersions() throws WebsiteNotReachableException {
        String version = getStringFromResourceTxtFile("/lasers-enigma-version.txt");
        String onlineVersion = getStringFromOnlineTxtFile(VERSION_NUMBER_FILE_URL);

        if (version != null && onlineVersion != null) {
            compareVersions(version, onlineVersion, "A new version of LasersEnigma is available.", "Installed version: ", "Available version: ", true);
            previousCheck = Instant.now();
        }
    }

    private void checkForNewBetaVersions() throws WebsiteNotReachableException {
        String commitHash = getStringFromResourceTxtFile("/lasers-enigma-commit-hash.txt");
        String onlineCommitHash = getStringFromOnlineTxtFile(COMMIT_HASH_FILE_URL);

        if (commitHash != null && onlineCommitHash != null) {
            compareVersions(commitHash, onlineCommitHash, "A new beta version of LasersEnigma is available.", "Installed version hashcode: ", "Available version hashcode: ", false);
            previousCheck = Instant.now();
        }
    }

    private void compareVersions(String localVersionText, String onlineVersionText, String title, String s2, String s3, boolean isSemVer) {
        if (localVersionText.equals(onlineVersionText)) return;

        if (isSemVer) {
            Version localVersion = new Version(localVersionText);
            Version onlineVersion = new Version(onlineVersionText);

            if (onlineVersion.isLowerOrEqual(localVersion)) return;
        }

        String installedVersionMsg = s2 + localVersionText;
        String availableVersionMsg = s3 + onlineVersionText;

        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().warning(title);
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().warning(installedVersionMsg);
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().warning(availableVersionMsg);

        updateMessages.add(title);
        updateMessages.add(installedVersionMsg);
        updateMessages.add(availableVersionMsg);
    }

    private String getStringFromResourceTxtFile(String file_uri) {
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        String result = null;
        String error_message = "Could not load " + file_uri + " file content from within the jar";
        try {
            is = getClass().getResourceAsStream(file_uri);
            if (is == null) {
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().info(error_message);
                return null;
            }
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
            result = br.readLine();
        } catch (IOException ex) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().severe(error_message, ex);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, error_message, ex);
                }
            }
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException ex) {
                    LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, error_message, ex);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, error_message, ex);
                }
            }
        }
        return result;
    }

    private String getStringFromOnlineTxtFile(String fileURL) throws WebsiteNotReachableException {
        String readLine = "";

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL(fileURL).openStream()))) {
            readLine = bufferedReader.readLine(); // only first line to read...
        } catch (MalformedURLException ex) {
            throw new WebsiteNotReachableException("Invalid URL: " + fileURL);
        } catch (IOException ex) {
            throw new WebsiteNotReachableException("Could not get content from " + fileURL);
        }

        if (readLine.isEmpty()) {
            throw new WebsiteNotReachableException("Could not get last version from " + fileURL);
        }

        return readLine;
    }

}
