package eu.lasersenigma.permission;

import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.permission.event.PlayerPermissionsCheckEvent;
import eu.lasersenigma.permission.exception.NoPermissionException;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public enum Permission {
    EDIT("lasers.edit"),
    ADMIN("lasers.admin");

    private final String permissionPath;

    Permission(String permissionPath) {
        this.permissionPath = permissionPath;
    }

    public String getPath() {
        return this.permissionPath;
    }

    public boolean hasPermission(Player p) {
        boolean checkResult = p.hasPermission(permissionPath);
        PlayerPermissionsCheckEvent permEvent = new PlayerPermissionsCheckEvent(p, checkResult);
        Bukkit.getServer().getPluginManager().callEvent(permEvent);
        return checkResult;
    }

    public boolean hasPermission(CommandSender cs) {
        return cs.hasPermission(permissionPath);
    }

    public void checkPermission(Player p) throws NoPermissionException {
        if (!hasPermission(p)) {
            throw new NoPermissionException();
        }
    }

    public boolean checkPermissionAndSendMsg(CommandSender cs) {
        if (hasPermission(cs)) {
            return true;
        }
        TranslationUtils.sendExceptionMessage(cs, new NoPermissionException());
        return false;

    }
}
