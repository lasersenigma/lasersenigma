package eu.lasersenigma.permission.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class NoPermissionException extends AbstractLasersException {

    public NoPermissionException() {
        super("errors.no_permission");
    }

}
