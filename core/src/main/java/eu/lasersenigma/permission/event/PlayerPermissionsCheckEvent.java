package eu.lasersenigma.permission.event;

import eu.lasersenigma.common.event.AEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import org.bukkit.entity.Player;

public class PlayerPermissionsCheckEvent extends AEvent implements IPlayerEvent {

    private final Player player;

    private boolean checkResult;

    public PlayerPermissionsCheckEvent(Player player, boolean checkResult) {
        this.player = player;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public boolean getPermissionsCheckResult() {
        return checkResult;
    }

    public void setPermissionsCheckResult(boolean checkResult) {
        this.checkResult = checkResult;
    }

}
