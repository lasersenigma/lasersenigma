package eu.lasersenigma.permission.event;

import eu.lasersenigma.common.event.ABeforeActionEvent;

public abstract class ABeforeActionPermissionEvent extends ABeforeActionEvent {

    private boolean bypassPermissions = false;

    public final boolean getBypassPermissions() {
        return bypassPermissions;
    }

    public final void setBypassPermissions(boolean byPassPermissions) {
        this.bypassPermissions = byPassPermissions;
    }

}
