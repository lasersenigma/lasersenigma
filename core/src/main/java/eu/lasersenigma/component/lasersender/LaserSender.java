package eu.lasersenigma.component.lasersender;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Laser Sender
 */
public final class LaserSender extends AArmorStandComponent implements IColorableComponent, IDetectionComponent, IRotatableComponent, ILightComponent, IPlayerModifiableComponent {

    // The color of this component as saved in database
    private LasersColor color = LasersColor.WHITE;

    // Current color of this component (not saved in the database)
    private LasersColor colorCurrent;

    // If the laser sender is conditionnal (redstone signal / range of activated laser receivers / number of player inside the area) or not.
    private DetectionMode detectionMode = DetectionMode.PERMANENTLY_ENABLED;

    // The minmum number of [activated laser receivers | players inside the area] needed to activate this component
    private int min = 1;

    // The maximum number of [activated laser receivers | players inside the area] needed to activate this component
    private int max = 10;

    // The actual number of (laser receivers activated | players inside the area)
    private int nbActivated = -1;

    private boolean powered = false;

    private int lightLevel;

    // Component's armor stands
    private final ComponentArmorStand LASER_SENDER_AS = new ComponentArmorStand(this, "laser_sender");

    // Component's options
    // Empty

    // Events listener
    // private final Listener eventsListener = new LaserSenderEventsListener(this);

    /**
     * Constructor used for creation from database
     *
     * @param area          The area of the laser sender
     * @param componentId   The id of the component inside the database
     * @param location      The location of the laser sender
     * @param face          The face of the laser sender
     * @param rotation      The rotation of the laser sender
     * @param color         The color of the laser sender
     * @param min           The minimum number of activated laser receivers | players needed to make this block appear
     * @param max           The maximum number of activated laser receivers | players needed to make this block appear
     * @param detectionMode The detection mode
     */
    public LaserSender(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, LasersColor color, DetectionMode detectionMode, int min, int max, int lightLevel) {
        super(area, componentId, location, ComponentType.LASER_SENDER, face, rotation);
        this.color = color;
        colorCurrent = color;
        this.detectionMode = detectionMode;
        this.min = min;
        this.max = max;
        this.lightLevel = lightLevel;
    }

    /**
     * Constructor
     *
     * @param area     The area of the laser sender
     * @param location The location of the laser sender
     * @param face     The face of the laser sender
     */
    public LaserSender(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.LASER_SENDER, face);
        colorCurrent = color;
        lightLevel = LasersEnigmaPlugin.getInstance().getConfig().getInt("laser_default_light_level");
        dbCreate();
    }

    public boolean isPowered() {
        return powered;
    }

    public void setPowered(boolean powered) {
        if (powered != this.powered) {
            this.powered = powered;
            showOrUpdateComponent();
        }
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public boolean isActivated() {
        return switch (detectionMode) {
            case DETECTION_PLAYERS, DETECTION_LASER_RECEIVERS, DETECTION_REDSTONE_SENSORS, DETECTION_LOCKS ->
                    (this.min <= this.nbActivated && this.nbActivated <= this.max);
            case DETECTION_REDSTONE -> powered;
            default -> true;
        };
    }

    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated && detectionMode != DetectionMode.PERMANENTLY_ENABLED) {
            this.nbActivated = nbActivated;
            showOrUpdateComponent();
        }
    }

    @Override
    public DetectionMode getDetectionMode() {
        return detectionMode;
    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.detectionMode = mode;
        dbUpdate();
        showOrUpdateComponent();
    }

    @Override
    public void activateComponent() {
        if (!rotation.equals(rotationCurrent)) {
            LASER_SENDER_AS.getArmorStand().setHeadPose(rotationCurrent);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getComponentLocation(), componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
            LASER_SENDER_AS.getArmorStand().teleport(nextLoc);
        }
        nbActivated = 0;
        rotationCurrent = rotation;
        colorCurrent = color;
    }

    @Override
    public void deactivateComponent() {
        if (!rotation.equals(rotationCurrent)) {
            LASER_SENDER_AS.getArmorStand().setHeadPose(rotationCurrent);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getComponentLocation(), componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
            LASER_SENDER_AS.getArmorStand().teleport(nextLoc);
        }
        nbActivated = 0;
        rotationCurrent = rotation;
        colorCurrent = color;
    }

    /**
     * updates the display
     */
    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(LASER_SENDER_AS, baseLocation, rotationCurrent, getItem(), false, false, null);
    }

    /**
     * Deletes the component
     */
    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();
        rotationCurrent = rotation;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        return new LaserReceptionResult(!this.equals(laserParticle.getLastComponent()));
    }

    private Item getItem() {
        String enumName = "LASER_SENDER_";
        enumName += colorCurrent.name();
        switch (detectionMode) {
            case DETECTION_PLAYERS:
            case DETECTION_LASER_RECEIVERS:
            case DETECTION_REDSTONE_SENSORS:
            case DETECTION_LOCKS:
                enumName += "_RANGE";
                break;
            case DETECTION_REDSTONE:
                enumName += "_REDSTONE";
                break;
            default:
                enumName += "_ALWAYS";
                break;
        }
        enumName += isActivated() ? "_ON" : "_OFF";
        if (getArea().isHLaserSendersRotationAllowed() && getArea().isVLaserSendersRotationAllowed()) {
            enumName += "_HV";
        } else if (getArea().isHLaserSendersRotationAllowed()) {
            enumName += "_H";
        } else if (getArea().isVLaserSendersRotationAllowed()) {
            enumName += "_V";
        }
        return Item.valueOf(enumName);
    }

    /*@Override
    public void clearEntitiesAttribute() {
        armorStandLaserSender = null;
    }*/

    @Override
    public void rotate(RotationType rotationType, boolean save, ActionCause actionCause) {
        rotationCurrent = rotationCurrent.getNextRotation(rotationType);
        LASER_SENDER_AS.getArmorStand().setHeadPose(rotationCurrent);
        Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getComponentLocation(), componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        LASER_SENDER_AS.getArmorStand().teleport(nextLoc);

        ComponentRotationEvent componentRotationEvent = new ComponentRotationEvent(this, actionCause);
        Bukkit.getServer().getPluginManager().callEvent(componentRotationEvent);

        if (save) {
            rotation = rotationCurrent;
            dbUpdate();
        }
    }

    @Override
    public void changeColor() {
        changeColor(true);
    }

    @Override
    public void changeColor(boolean save) {
        setColor(colorCurrent.getNextColor(false), save);
    }

    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }
        this.colorCurrent = color;
        if (save) {
            this.color = color;
        }
        if (save) {
            dbUpdate();
        }
        showOrUpdateComponent();
    }

    @Override
    public LasersColor getColorCurrent() {
        return colorCurrent;
    }

    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public void setColor(LasersColor colorCurrent) {
        setColor(colorCurrent, true);
    }

    @Override
    public int getLightLevel() {
        return lightLevel;
    }

    @Override
    public void setLightLevel(int lightLevel) {
        this.lightLevel = lightLevel;
    }
}
