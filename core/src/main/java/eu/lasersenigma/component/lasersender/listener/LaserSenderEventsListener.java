package eu.lasersenigma.component.lasersender.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.component.conditionnalwinnerblock.event.ConditionalComponentActivationEvent;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.lasersender.LaserSender;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

public class LaserSenderEventsListener implements Listener {

    public LaserSenderEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * Executed on a ConditionalComponentActivatedLEEvent
     * Play a sound when a laser sender is activated (but not when this activation happens when the area is activated)
     *
     * @param event a ConditionalComponentActivatedLEEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onConditionalComponentActivation(ConditionalComponentActivationEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onConditionalComponentActivation");

        if (!(event.getComponent() instanceof LaserSender)) return;

        // When lasers senders are activated except if it's during area activation.
        if (event.getComponent().getArea().getUpdateLasersCount() != 0) {
            SoundLauncher.playSound(event.getComponent().getComponentLocation(), PlaySoundCause.LASER_SENDER_ACTIVATED);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onComponentRotation(ComponentRotationEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onComponentRotation");

        if (!event.getActionCause().equals(ActionCause.PLAYER)) return;
        if (!(event.getComponent() instanceof LaserSender)) return;

        SoundLauncher.playSound(event.getComponent().getComponentLocation(), PlaySoundCause.LASER_SENDER_ROTATE);
    }

}
