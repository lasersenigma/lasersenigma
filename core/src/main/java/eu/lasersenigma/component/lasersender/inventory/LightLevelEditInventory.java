package eu.lasersenigma.component.lasersender.inventory;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.ILightComponent;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LightLevelEditInventory extends AOpenableInventory {

    private final ILightComponent component;
    private final Area area;

    public LightLevelEditInventory(LEPlayer player, ILightComponent component) {
        super(player, "messages.light_level_menu");
        this.component = component;
        this.area = component.getArea();
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        List<Item> firstLine = new ArrayList<>();

        firstLine.add(Item.LIGHT_LEVEL_DECREMENT);
        firstLine.addAll(getNumberAsItems(component.getLightLevel(), true));
        firstLine.add(Item.LIGHT_LEVEL_INCREMENT);

        inv.add(firstLine);
        return inv;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case LIGHT_LEVEL_DECREMENT:
                ComponentController.changeLightLevel(player, component, -1);
                break;
            case LIGHT_LEVEL_INCREMENT:
                ComponentController.changeLightLevel(player, component, 1);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return Arrays.asList(
                Item.LIGHT_LEVEL_DECREMENT,
                Item.LIGHT_LEVEL_INCREMENT
        ).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.LIGHT_LEVEL_EDIT_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
