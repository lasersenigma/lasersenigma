package eu.lasersenigma.component;

import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;

/**
 * Represents an interface for components that can be colored
 */
public interface IColorableComponent extends IComponent {

    /**
     * Gets the current color of the component
     *
     * @return The current color of the component
     */
    LasersColor getColor();

    /**
     * Gets the saved color of the component
     *
     * @return The saved color of the component
     */
    LasersColor getColorCurrent();

    /**
     * Changes the current color of the component
     */
    void changeColor();

    /**
     * Changes the current color of the component
     *
     * @param save True if the color change should be saved, false otherwise
     */
    void changeColor(boolean save);

    /**
     * Sets the color of the component
     *
     * @param colorCurrent The new color of the component
     */
    void setColor(LasersColor colorCurrent);

    /**
     * Sets the color of the component
     *
     * @param color The new color of the component
     * @param save  True if the color change should be saved, false otherwise
     */
    void setColor(LasersColor color, boolean save);

    default int getSavedColor() {
        if (
                (this instanceof LaserReceiver laserReceiver && laserReceiver.isAnyColorAccepted())
                        || (this instanceof IMirrorContainer mirrorContainer && !mirrorContainer.hasMirror())
                        || (this.getColor() == null)
        ) {
            return 16;
        }
        return this.getColor().getInt();
    }

}
