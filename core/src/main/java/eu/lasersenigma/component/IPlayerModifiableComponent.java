package eu.lasersenigma.component;

/**
 * Represents an interface for components that can be modified by a player
 */
public interface IPlayerModifiableComponent extends IComponent {
}
