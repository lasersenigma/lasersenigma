package eu.lasersenigma.component.laserreceiver.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.laserreceiver.event.LaserReceiverActivationEvent;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

public class LaserReceiverEventsListener implements Listener {

    public LaserReceiverEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * Executed on a LaserReceiverActivatedLEEvent
     * Play a sound when a laser receiver is activated (but at least 0.5s after the area activation)
     */
    @EventHandler(ignoreCancelled = true)
    public void onLaserReceiverActivation(LaserReceiverActivationEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onLaserReceiverActivation");

        // When lasers senders are activated except if it's during area activation.
        if (event.getComponent().getArea().getUpdateLasersCount() > 10) {
            SoundLauncher.playSound(event.getComponent().getComponentLocation(), PlaySoundCause.LASER_RECEIVER_ACTIVATED);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onComponentRotation(ComponentRotationEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onComponentRotation");

        if (!event.getActionCause().equals(ActionCause.PLAYER)) return;
        if (!(event.getComponent() instanceof LaserReceiver)) return;

        SoundLauncher.playSound(event.getComponent().getComponentLocation(), PlaySoundCause.LASER_RECEIVER_ROTATE);
    }

}
