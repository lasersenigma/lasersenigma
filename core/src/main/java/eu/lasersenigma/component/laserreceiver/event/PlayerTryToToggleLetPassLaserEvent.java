package eu.lasersenigma.component.laserreceiver.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToToggleLetPassLaserEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final LaserReceiver laserReceiver;

    public PlayerTryToToggleLetPassLaserEvent(Player player, LaserReceiver laserReceiver) {
        super();
        this.player = player;
        this.laserReceiver = laserReceiver;
    }

    @Override
    public IComponent getComponent() {
        return laserReceiver;
    }

    public LaserReceiver getLaserReceiver() {
        return laserReceiver;
    }

    @Override
    public Player getPlayer() {
        return player;
    }
}
