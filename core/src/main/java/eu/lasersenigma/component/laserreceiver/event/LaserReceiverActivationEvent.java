package eu.lasersenigma.component.laserreceiver.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;

public class LaserReceiverActivationEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final LaserReceiver component;

    public LaserReceiverActivationEvent(LaserReceiver component) {
        super();
        this.component = component;
    }

    @Override
    public LaserReceiver getComponent() {
        return component;
    }

}
