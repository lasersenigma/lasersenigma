package eu.lasersenigma.component.laserreceiver;

import eu.lasersenigma.particles.LaserParticle;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents the result of processing a laser particle by a component.
 * This class encapsulates information about whether the original particle
 * should be destroyed, if an additional check for blocks is required, and
 * the set of new particles that should be created as a result.
 */
public class LaserReceptionResult {

    private final boolean shouldDestroyOriginalParticle;
    private final boolean requireBlocksChecking;
    private final Set<LaserParticle> newParticles;

    /**
     * Creates a bean representing the result of a laser particle processing by a component
     *
     * @param shouldDestroyOriginalParticle should the input particle disappear
     * @param requireBlocksChecking          is it required to check blocks (in addition to components) for the input particle.
     *                                    This parameter can be true only if mustDestroyOriginalParticle is false.
     * @param newParticles                the new particles that should be created
     */
    public LaserReceptionResult(boolean shouldDestroyOriginalParticle, boolean requireBlocksChecking, Set<LaserParticle> newParticles) {
        if (requireBlocksChecking && shouldDestroyOriginalParticle) {
            throw new IllegalArgumentException("A particle that will be destroyed does not care about any blocks");
        }
        this.shouldDestroyOriginalParticle = shouldDestroyOriginalParticle;
        this.newParticles = newParticles;
        this.requireBlocksChecking = requireBlocksChecking;
    }

    /**
     * Creates a bean representing the result of a laser particle processing by a component
     *
     * @param shouldDestroyOriginalParticle should the input particle disappear
     * @param newParticles                the new particles that should be created
     */
    public LaserReceptionResult(boolean shouldDestroyOriginalParticle, Set<LaserParticle> newParticles) {
        this(shouldDestroyOriginalParticle, false, newParticles);
    }

    /**
     * Creates a bean representing the result of a laser particle processing by a component
     *
     * @param shouldDestroyOriginalParticle should the input particle disappear
     * @param newParticles                the new particles that should be created
     */
    public LaserReceptionResult(boolean shouldDestroyOriginalParticle, LaserParticle... newParticles) {
        this(shouldDestroyOriginalParticle, new HashSet<>(Arrays.asList(newParticles)));
    }

    /**
     * Creates a bean representing the result of a laser particle processing by a component
     *
     * @param shouldDestroyOriginalParticle should the input particle disappear
     * @param requireBlocksChecking          is it required to check blocks (in addition to components) for the input particle.
     *                                    This parameter can be true only if mustDestroyOriginalParticle is false.
     */
    public LaserReceptionResult(boolean shouldDestroyOriginalParticle, boolean requireBlocksChecking) {
        this(shouldDestroyOriginalParticle, requireBlocksChecking, new HashSet<>());
    }

    /**
     * Creates a bean representing the result of a laser particle processing by a component
     *
     * @param shouldDestroyOriginalParticle should the input particle disappear
     */
    public LaserReceptionResult(boolean shouldDestroyOriginalParticle) {
        this(shouldDestroyOriginalParticle, new HashSet<>());
    }

    /**
     * Retrieves a boolean allowing the system to know if the input particle should be destroyed
     *
     * @return should the input particle disappear
     */
    public boolean shouldDestroyOriginalParticle() {
        return shouldDestroyOriginalParticle;
    }

    /**
     * Retrieves a boolean allowing the system to know if the input particle should continue moving
     *
     * @return should the input particle continue existing
     */
    public boolean shouldKeepAliveOriginalParticle() {
        return !shouldDestroyOriginalParticle;
    }


    /**
     * If the original particle must not be destroyed, allows knowing if an additional check must be done about blocks
     *
     * @return true if a check must be done about blocks
     */
    public boolean requireBlocksChecking() {
        return requireBlocksChecking;
    }

    /**
     * Retrieves the particles that should be created
     *
     * @return the new particles that should be created
     */
    public Set<LaserParticle> getNewParticles() {
        return newParticles;
    }

    @Override
    public String toString() {
        return "LaserReceptionResult{" +
                "mustDestroyOriginalParticle=" + shouldDestroyOriginalParticle +
                ", requireCheckBlocks=" + requireBlocksChecking +
                ", newParticles=" + newParticles +
                '}';
    }
}
