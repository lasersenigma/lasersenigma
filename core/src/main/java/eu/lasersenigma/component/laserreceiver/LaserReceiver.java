package eu.lasersenigma.component.laserreceiver;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.task.ITaskComponent;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.laserreceiver.event.LaserReceiverActivationEvent;
import eu.lasersenigma.component.laserreceiver.event.LaserReceiverDeactivationEvent;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Laser Receiver
 */
public final class LaserReceiver extends AArmorStandComponent implements IColorableComponent, ITaskComponent, IRotatableComponent, ILightComponent, IPlayerModifiableComponent {

    public static final double MAX_ACCEPTED_INPUT_RADIAL_ANGLE = 0.8;

    public static Random random = new Random(System.currentTimeMillis());

    // The colors that are received in this laser receiver associated with an integer value representing the moment they were received
    private final HashMap<LasersColor, Integer> receivedColorsLifeTime = new HashMap<>();

    // The 4 corners of the front face of the ArmorStand Head representing this laser receiver (used when letPassLaserThrough is false)
    private List<Location> frontCorners;

    // The 8 corners of the ArmorStand Head representing this laser receiver (used when letPassLaserThrough is true)
    private List<Location> allCorners;

    // The color of this component as saved in database
    private LasersColor color;

    // Current color of this component (not saved in the database)
    private LasersColor currentColor;

    /**
     * the state of the laser receiver
     *
     * @see LaserReceiver#startTask() that updates this status
     */
    private boolean activated = false;

    private boolean anyColorAccepted = false;

    private boolean letPassLaserThrough = false;

    private int lightLevel;

    // The id of the task used to scan the lasers input
    private Integer taskId = null;

    // Component's armor stands
    private final ComponentArmorStand LASER_RECEIVER_AS = new ComponentArmorStand(this, "laser_receiver");

    // Component's options
    // Emptys

    // Events listener
    // private final Listener eventsListener = new LaserReceiverEventsListener(this);

    /**
     * Constructor used for creation from the database
     *
     * @param area        The area containing this laser receiver
     * @param componentId The id of the component inside the database
     * @param location    The location of the laser receiver
     * @param face        The blockface of the component
     * @param rotation    The rotation of the component
     * @param color       The color of the laser receiver
     */
    public LaserReceiver(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, LasersColor color, int lightLevel, boolean letPassLaserThrough) {
        super(area, componentId, location, ComponentType.LASER_RECEIVER, face, rotation);
        this.frontCorners = rotationCurrent.getFrontCorners(getASHeadCenterLocation());
        this.allCorners = rotationCurrent.getAllCorners(getASHeadCenterLocation());
        this.anyColorAccepted = color == null;
        this.color = color == null ? LasersColor.WHITE : color;
        this.currentColor = this.color;
        this.lightLevel = lightLevel;
        this.letPassLaserThrough = letPassLaserThrough;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this laser receiver
     * @param location The location of the laser receiver
     * @param face     The BlockFace of the component
     */
    public LaserReceiver(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.LASER_RECEIVER, face);
        this.frontCorners = rotationCurrent.getFrontCorners(getASHeadCenterLocation());
        this.allCorners = rotationCurrent.getAllCorners(getASHeadCenterLocation());
        color = LasersColor.WHITE;
        currentColor = color;
        this.lightLevel = LasersEnigmaPlugin.getInstance().getConfig().getInt("laser_default_light_level");
        dbCreate();
    }

    public boolean isAnyColorAccepted() {
        return anyColorAccepted;
    }

    public void setAnyColorAccepted(boolean anyColorAccepted) {
        this.anyColorAccepted = anyColorAccepted;
        dbUpdate();
        showOrUpdateComponent();
    }

    public List<Location> getParticleDisplayLocations() {
        return letPassLaserThrough ? allCorners : frontCorners;
    }

    public boolean isActivated() {
        return activated;
    }

    public boolean isLetPassLaserThrough() {
        return letPassLaserThrough;
    }

    public void setLetPassLaserThrough(boolean letPassLaserThrough) {
        if (letPassLaserThrough != this.letPassLaserThrough) {
            this.letPassLaserThrough = letPassLaserThrough;
            showOrUpdateComponent();
            dbUpdate();
        }
    }

    @Override
    public void activateComponent() {
        /*if (!rotation.equals(rotationCurrent)) {
            LASER_RECEIVER_AS.getArmorStand().setHeadPose(rotationCurrent);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getComponentLocation(), componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
            LASER_RECEIVER_AS.getArmorStand().teleport(nextLoc);
        }*/

        // Restore previous state
        activated = false;
        currentColor = color;
        rotationCurrent = rotation;

        this.frontCorners = rotation.getFrontCorners(getASHeadCenterLocation());
        this.allCorners = rotationCurrent.getAllCorners(getASHeadCenterLocation());
        this.receivedColorsLifeTime.clear();
    }

    @Override
    public void deactivateComponent() {
        /*if (!rotation.equals(rotationCurrent)) {
            LASER_RECEIVER_AS.getArmorStand().setHeadPose(rotationCurrent);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getComponentLocation(), componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
            LASER_RECEIVER_AS.getArmorStand().teleport(nextLoc);
        }*/

        // Reset default state
        activated = false;
        currentColor = color;
        rotationCurrent = rotation;

        this.frontCorners = rotation.getFrontCorners(getASHeadCenterLocation());
        this.allCorners = rotationCurrent.getAllCorners(getASHeadCenterLocation());
        this.receivedColorsLifeTime.clear();
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation;

        baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(LASER_RECEIVER_AS, baseLocation, rotationCurrent, getItem(), false, false, null);
        area.setPower(componentLocation, activated);
    }

    private Item getItem() {
        String enumName = "LASERS_RECEIVER_";

        //color
        enumName += anyColorAccepted ? "ANY" : currentColor.name();

        //activated
        enumName += activated ? "_ON" : "_OFF";

        // intermediate or rotation allowed
        if (letPassLaserThrough) {
            enumName += "_LET_PASS";
        } else {
            if (getArea().isHLaserReceiversRotationAllowed() && getArea().isVLaserReceiversRotationAllowed()) {
                enumName += "_HV";
            } else if (getArea().isHLaserReceiversRotationAllowed()) {
                enumName += "_H";
            } else if (getArea().isVLaserReceiversRotationAllowed()) {
                enumName += "_V";
            }
        }
        return Item.valueOf(enumName);
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();
        cancelTask();

        /*rotationCurrent = rotation;
        currentColor = color;
        this.frontCorners = rotation.getFrontCorners(getASHeadCenterLocation());
        this.activated = false;
        this.receivedColorsLifeTime.clear();
        area.setPower(componentLocation, false);*/
    }

    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getColorCurrent() {
        return currentColor;
    }

    public LasersColor getCornersColor() {
        if (!isAnyColorAccepted()) {
            return currentColor;
        } else {
            final LasersColor[] colors = LasersColor.values(false);
            return colors[random.nextInt(colors.length)];
        }
    }

    @Override
    public void changeColor() {
        changeColor(true);
    }

    @Override
    public void changeColor(boolean save) {
        setColor(currentColor.getNextColor(false), save);
    }

    @Override
    public void setColor(LasersColor color) {
        setColor(color, true);
    }

    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }

        this.currentColor = color;
        showOrUpdateComponent();

        if (save) {
            this.color = color;
            dbUpdate();
        }
    }

    @Override
    public void rotate(RotationType rotationType, boolean save, ActionCause actionCause) {
        rotationCurrent = rotationCurrent.getNextRotation(rotationType);
        this.frontCorners = rotationCurrent.getFrontCorners(getASHeadCenterLocation());
        this.allCorners = rotationCurrent.getAllCorners(getASHeadCenterLocation());
        LASER_RECEIVER_AS.getArmorStand().setHeadPose(rotationCurrent);
        Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        LASER_RECEIVER_AS.getArmorStand().teleport(nextLoc);

        ComponentRotationEvent componentRotationEvent = new ComponentRotationEvent(this, actionCause);
        Bukkit.getServer().getPluginManager().callEvent(componentRotationEvent);

        if (save) {
            rotation = rotationCurrent;
            dbUpdate();
        }
    }

    private void updateRedstone() {
        area.setPower(componentLocation, activated);
    }

    @Override
    public int getLightLevel() {
        return lightLevel;
    }

    @Override
    public void setLightLevel(int lightLevel) {
        this.lightLevel = lightLevel;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }
        if (this.letPassLaserThrough ||
                laserParticle.getDirection().isAcceptedInputAngleFor(rotationCurrent.toEyeDirection(), MAX_ACCEPTED_INPUT_RADIAL_ANGLE)) {
            this.receivedColorsLifeTime.put(laserParticle.getColor(), 0);
        }
        boolean shouldDestroyOriginalParticle = !this.letPassLaserThrough;
        if (shouldDestroyOriginalParticle) {
            return new LaserReceptionResult(true);
        } else {
            return new LaserReceptionResult(
                    true,
                    new LaserParticle(
                            this,
                            laserParticle.getLocation(),
                            laserParticle.getDirection(),
                            laserParticle.getColor(),
                            getArea(),
                            laserParticle.getLightLevel()
                    ));
        }

    }

    @Override
    public void startTask() {
        if (!area.isActivated()) return;

        if (taskId != null) return;

        final LaserReceiver instance = this;

        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), (() -> {
            if (!getArea().isActivated()) {
                cancelTask();
                return;
            }

            receivedColorsLifeTime.replaceAll((k, v) -> v + 1); //increment LifeTime
            receivedColorsLifeTime.entrySet().removeIf(e -> e.getValue() >= 2);

            boolean oldActivated = activated;

            if (anyColorAccepted) {
                activated = receivedColorsLifeTime.entrySet().stream().anyMatch(e -> e.getValue() == 1);
                changeColor(false);
            } else {
                Set<LasersColor> colors = receivedColorsLifeTime.entrySet().stream().filter(e -> e.getValue() == 1).map(Map.Entry::getKey).collect(Collectors.toSet());
                activated = currentColor.isComposedOf(colors, LasersEnigmaPlugin.getInstance().getConfig().getBoolean(LasersColor.ONLY_NEED_COLORS_CONFIG_PATH));
            }

            if (oldActivated != activated) {
                if (activated) {
                    Bukkit.getServer().getPluginManager().callEvent(new LaserReceiverActivationEvent(instance));
                } else {
                    Bukkit.getServer().getPluginManager().callEvent(new LaserReceiverDeactivationEvent(instance));
                }

                showOrUpdateComponent();
                updateRedstone();
            }
        }), 0, LaserParticle.LASERS_FREQUENCY);
        taskId = task.getTaskId();
    }

    @Override
    public void cancelTask() {
        if (taskId == null) return;

        Bukkit.getScheduler().cancelTask(taskId);
        receivedColorsLifeTime.clear();
        taskId = null;
    }
}
