package eu.lasersenigma.component.reflectingsphere;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.mirrorsupport.task.MirrorPlacementAnimationTask;
import eu.lasersenigma.component.mirrorsupport.task.MirrorRemovalAnimationTask;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.particles.ReflectionData;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Reflecting sphere component
 */
public final class ReflectingSphere extends AArmorStandComponent implements IColorableComponent, IMirrorContainer {

    public static final double SPHERE_RADIUS = 0.42;

    // The difference between the location where the mirror appears and the location where it starts entering inside the sphere
    public static final double MIRROR_ANIMATION_CLIP_LOCATION_DIFF = 0.6d;

    // The difference between the location where the mirror starts entering the sphere and the location where it stops and disappears
    public static final double MIRROR_ANIMATION_LOCATION_DIFF = 0.7d;

    // Does this reflecting sphere has a mirror inside
    private boolean hasMirrorCurrent = true;

    // The saved color of this component's mirror. Used only on reset
    private LasersColor color = null;

    // The color of this reflecting sphere
    private LasersColor colorCurrent = LasersColor.WHITE;

    // Is this reflecting sphere currently showing an animation
    private boolean onGoingAnimation = false;
    private MirrorPlacementAnimationTask mirrorPlacementAnimationTask = null;
    private MirrorRemovalAnimationTask mirrorRemovalAnimationTask = null;

    // Cache for lasers reflection calculation
    private final HashMap<ReflectionData, ReflectionData> reflectionCache = new HashMap<>();

    // Component's armor stands
    private final ComponentArmorStand REFLECTING_SPHERE_AS = new ComponentArmorStand(this, "reflecting_sphere");
    private final ComponentArmorStand REFLECTING_SPHERE_MIRROR_AS = new ComponentArmorStand(this, "reflecting_sphere_mirror");

    // Component's options
    // Empty

    // Component's events listener
    // Empty

    /**
     * Constructor used for creation from database
     *
     * @param area        The area containing this component
     * @param componentId The id of the component inside the database
     * @param location    The location of this component
     * @param face        The face the component is on
     */
    public ReflectingSphere(Area area, int componentId, Location location, ComponentFace face, LasersColor color) {
        super(area, componentId, location, ComponentType.REFLECTING_SPHERE, face, face.getDefaultRotation(ComponentType.REFLECTING_SPHERE));
        this.color = color;
        hasMirrorCurrent = color != null;
        colorCurrent = color != null ? color : LasersColor.BLACK;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this component
     * @param location The location of this component
     * @param face     The face the component is on
     */
    public ReflectingSphere(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.REFLECTING_SPHERE, face);
        showOrUpdateComponent();
        dbCreate();
    }

    public HashMap<ReflectionData, ReflectionData> getReflectionCache() {
        return reflectionCache;
    }

    @Override
    public boolean hasMirror() {
        return color != null;
    }

    @Override
    public boolean hasMirrorCurrent() {
        return hasMirrorCurrent;
    }

    @Override
    public void setHasMirror(boolean hasMirror, boolean save) {
        if (onGoingAnimation) return;

        hasMirrorCurrent = hasMirror;
        if (!hasMirrorCurrent) colorCurrent = LasersColor.BLACK;

        showOrUpdateComponent();

        if (save) {
            color = hasMirrorCurrent ? colorCurrent : null;
            dbUpdate();
        }
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean placeMirror(LasersColor color, boolean save) {
        if (onGoingAnimation) return false;

        if (hasMirrorCurrent) return false;

        if (isRemoved()) return false;

        if (REFLECTING_SPHERE_MIRROR_AS.getArmorStand() != null) {
            removeArmorStandDisplay(REFLECTING_SPHERE_MIRROR_AS);
        }
        onGoingAnimation = true;
        colorCurrent = color;

        Location mirrorLocation = getDefaultArmorStandBaseLocation(componentLocation);
        mirrorLocation = mirrorLocation.add(getAnimationClipVector()).add(getAnimationVector());
        mirrorLocation = getArmorStandLocationWithOffsets(mirrorLocation, ArmorStandItemOffset.GLASS_PANE, rotationCurrent);
        newCreateArmorStand(REFLECTING_SPHERE_MIRROR_AS, mirrorLocation, rotationCurrent, Item.getMirror(colorCurrent));
        mirrorPlacementAnimationTask = new MirrorPlacementAnimationTask(this, Item.getReflectingSphereItem(colorCurrent), save);

        return true;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean removeMirror(boolean save) {
        if (onGoingAnimation) return false;

        if (!hasMirrorCurrent) return false;

        if (isRemoved()) return false;

        onGoingAnimation = true;
        mirrorRemovalAnimationTask = new MirrorRemovalAnimationTask(this, Item.REFLECTING_SPHERE, save);

        return true;
    }

    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getColorCurrent() {
        return colorCurrent;
    }

    @Override
    public void changeColor() {
        setColor(colorCurrent.getNextColor(false));
    }

    @Override
    public void changeColor(boolean save) {
        setColor(colorCurrent.getNextColor(false), save);
    }

    @Override
    public void setColor(LasersColor color) {
        setColor(color, false);
    }

    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }

        if (!hasMirrorCurrent || onGoingAnimation) {
            return;
        }

        colorCurrent = color;
        showOrUpdateComponent();

        if (save) {
            this.color = colorCurrent;
            dbUpdate();
        }
    }

    @Override
    public boolean isOnGoingAnimation() {
        return onGoingAnimation;
    }

    @Override
    public void setOnGoingAnimation(boolean onGoingAnimation) {
        this.onGoingAnimation = onGoingAnimation;
    }

    @Override
    public boolean canBeDeleted() {
        return !onGoingAnimation;
    }

    @Override
    public Vector getAnimationVector() {
        return componentFace.getVector().multiply(MIRROR_ANIMATION_LOCATION_DIFF);
    }

    @Override
    public Vector getAnimationClipVector() {
        return componentFace.getVector().multiply(MIRROR_ANIMATION_CLIP_LOCATION_DIFF);
    }

    @Override
    public ArmorStand getArmorStandMirror() {
        return REFLECTING_SPHERE_MIRROR_AS.getArmorStand();
    }

    @Override
    public ArmorStand getArmorStandMirrorContainer() {
        return REFLECTING_SPHERE_AS.getArmorStand();
    }

    @Override
    public void activateComponent() {
        // Reset component's animation tasks
        mirrorRemovalAnimationTask = null;
        mirrorPlacementAnimationTask = null;
        onGoingAnimation = false;

        // Restore cached states
        hasMirrorCurrent = color != null;
        colorCurrent = color != null ? color : LasersColor.BLACK;
        rotationCurrent = rotation != null ? rotation : componentFace.getDefaultRotation(componentType);
    }

    @Override
    public void deactivateComponent() {
        // Cancel component's animation tasks
        if (mirrorRemovalAnimationTask != null) {
            mirrorRemovalAnimationTask.cancel();
            mirrorRemovalAnimationTask = null;
        }
        if (mirrorPlacementAnimationTask != null) {
            mirrorPlacementAnimationTask.cancel();
            mirrorPlacementAnimationTask = null;
        }
        onGoingAnimation = false;

        // Reset default states
        hasMirrorCurrent = color != null;
        colorCurrent = color != null ? color : LasersColor.BLACK;
        rotationCurrent = rotation != null ? rotation : componentFace.getDefaultRotation(componentType);
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation;

        // Reflecting sphere armor stand
        baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(REFLECTING_SPHERE_AS, baseLocation, rotationCurrent, Item.getReflectingSphereItem(colorCurrent), false, false, null);

        // Reflecting sphere mirror armor stand
        baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.GLASS_PANE, rotationCurrent);
        updateComponentArmorStand(REFLECTING_SPHERE_MIRROR_AS, baseLocation, rotationCurrent, Item.getMirror(colorCurrent), true, hasMirrorCurrent, null);
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();

        // Cancel component's animation tasks
        if (mirrorRemovalAnimationTask != null) {
            mirrorRemovalAnimationTask.cancel();
            mirrorRemovalAnimationTask = null;
        }
        if (mirrorPlacementAnimationTask != null) {
            mirrorPlacementAnimationTask.cancel();
            mirrorPlacementAnimationTask = null;
        }
        onGoingAnimation = false;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }
        // It has no mirror inside, it is an opaque component
        if (hasMirrorCurrent() && !isOnGoingAnimation()) {
            LasersColor reflectedColor;
            if (colorCurrent == LasersColor.WHITE) {
                reflectedColor = laserParticle.getColor();
            } else {
                reflectedColor = laserParticle.getColor().filterBy(colorCurrent).get(LasersColor.FilterResult.REFLECTED);
            }
            if (reflectedColor != null) {
                ReflectionData inputRD = new ReflectionData(laserParticle.getDirection(), laserParticle.getLocation());
                //Using a reflection cache to avoid recomputing the same reflection two times
                ReflectionData outputRD = getReflectionCache().get(inputRD);
                if (outputRD == null) {
                    outputRD = reflect(this, inputRD);
                    getReflectionCache().put(inputRD, outputRD);
                }
                return new LaserReceptionResult(
                        true,
                        new LaserParticle(this, outputRD.getLocation(), outputRD.getDirection(), reflectedColor, getArea(), laserParticle.getLightLevel())
                );
            }
        }
        return new LaserReceptionResult(true);
    }

    public static ReflectionData reflect(ReflectingSphere rs, ReflectionData laserParticleData) {
        // Reference:
        // - http://www.codeproject.com/Articles/19799/Simple-Ray-Tracing-in-C-Part-II-Triangles-Intersec

        Location sphereCenter = rs.getASHeadCenterLocation();

        double cx = sphereCenter.getX();
        double cy = sphereCenter.getY();
        double cz = sphereCenter.getZ();

        double px = laserParticleData.getLocation().getX();
        double py = laserParticleData.getLocation().getY();
        double pz = laserParticleData.getLocation().getZ();

        double vx = laserParticleData.getDirection().getX();
        double vy = laserParticleData.getDirection().getY();
        double vz = laserParticleData.getDirection().getZ();

        double a = vx * vx + vy * vy + vz * vz;
        double b = 2.0 * (px * vx + py * vy + pz * vz - vx * cx - vy * cy - vz * cz);
        double c = px * px - 2 * px * cx + cx * cx + py * py - 2 * py * cy + cy * cy
                + pz * pz - 2 * pz * cz + cz * cz - SPHERE_RADIUS * SPHERE_RADIUS;

        // discriminant
        double d = b * b - 4 * a * c;

        if (d < 0) {
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation());
        }

        double t1 = (-b - Math.sqrt(d)) / (2.0 * a);

        Location solution1 = new Location(
                laserParticleData.getLocation().getWorld(),
                px * (1 - t1) + t1 * (px + vx),
                py * (1 - t1) + t1 * (py + vy),
                pz * (1 - t1) + t1 * (pz + vz));
        Location finalIntersection;
        if (d == 0) {
            finalIntersection = solution1;
        } else {

            double t2 = (-b + Math.sqrt(d)) / (2.0 * a);
            Location solution2 = new Location(
                    laserParticleData.getLocation().getWorld(),
                    px * (1 - t2) + t2 * (px + vx),
                    py * (1 - t2) + t2 * (py + vy),
                    pz * (1 - t2) + t2 * (pz + vz));

            // prefer a solution that's the first intersection between the laser and the sphere.
            Location prevParticleLoc = new Location(
                    laserParticleData.getLocation().getWorld(),
                    px - vx,
                    py - vy,
                    pz - vz
            );

            if (solution1.distance(prevParticleLoc) < solution2.distance(prevParticleLoc)) {
                finalIntersection = solution1;
            } else {
                finalIntersection = solution2;
            }

        }

        double normalVectorX = cx - finalIntersection.getX();
        double normalVectorY = cy - finalIntersection.getY();
        double normalVectorZ = cz - finalIntersection.getZ();
        Vector normalVector = new Vector(normalVectorX, normalVectorY, normalVectorZ).normalize();
        double dotProd = laserParticleData.getDirection().clone().normalize().dot(normalVector);
        if (dotProd == 0) {
            return new ReflectionData(new Direction(laserParticleData.getDirection().clone().multiply(-1)), laserParticleData.getLocation());
        }
        Direction resultDirection = new Direction(
                vx - (2 * dotProd * normalVectorX),
                vy - (2 * dotProd * normalVectorY),
                vz - (2 * dotProd * normalVectorZ));
        return new ReflectionData(resultDirection, finalIntersection);

    }

}
