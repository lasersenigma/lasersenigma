package eu.lasersenigma.component.redstonesensor;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.redstonesensor.event.RedstoneSensorActivateEvent;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Redstone Sensor
 */
public final class RedstoneSensor extends AArmorStandComponent {

    private boolean isPowered = false;

    // Component's armor stand
    private final ComponentArmorStand REDSTONE_SENSOR_AS = new ComponentArmorStand(this, "redstone_sensor");

    // Component's options
    // Empty

    // Component's events listener
    // Empty

    /**
     * Constructor used for creation from the database
     *
     * @param area        The area containing this redstone sensor
     * @param componentId The id of the component inside the database
     * @param location    The location of the redstone sensor
     * @param face        The blockface of the component
     * @param rotation    The rotation of the component
     */
    public RedstoneSensor(Area area, int componentId, Location location, ComponentFace face, Rotation rotation) {
        super(area, componentId, location, ComponentType.REDSTONE_SENSOR, face, rotation);
    }

    /**
     * Constructor
     *
     * @param area     The area containing this
     * @param location The location of the component
     * @param face     The blockface of the component
     */
    public RedstoneSensor(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.REDSTONE_SENSOR, face);
        dbCreate();
    }

    /**
     * Checks if redstone sensor is powered
     *
     * @return True if redstone sensor is powered, false elsewhere
     */
    public boolean isPowered() {
        return isPowered;
    }

    /**
     * Changes power state of redstone sensor
     *
     * @param powered Should redstone sensor be activated?
     * @return True if redstone sensor's state change
     */
    public boolean setPowered(boolean powered) {
        if (powered != this.isPowered) {
            this.isPowered = powered;
            showOrUpdateComponent();

            // Fire RedstoneSensorActivateEvent
            RedstoneSensorActivateEvent redstoneSensorActivateEvent = new RedstoneSensorActivateEvent(this);
            Bukkit.getPluginManager().callEvent(redstoneSensorActivateEvent);

            return true;
        }
        return false;
    }

    @Override
    public void activateComponent() {
    }

    @Override
    public void deactivateComponent() {
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(REDSTONE_SENSOR_AS, baseLocation, rotation, getHelmetItem(), false, false, null);
    }

    private Item getHelmetItem() {
        if (isPowered) {
            return Item.REDSTONE_SENSOR_ACTIVATED;
        } else {
            return Item.REDSTONE_SENSOR_DEACTIVATED;
        }
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
