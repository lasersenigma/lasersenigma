package eu.lasersenigma.component.redstonesensor.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;

public class RedstoneSensorActivatedChangeEvent extends AAfterActionEvent implements IAreaEvent {

    private final Area area;

    private final int oldNbActivatedLaserReceivers;

    private final int newNbActivatedLaserReceivers;

    public RedstoneSensorActivatedChangeEvent(Area area,
                                              int oldNbActivatedLaserReceivers, int newNbActivatedLaserReceivers) {
        super();
        this.area = area;
        this.oldNbActivatedLaserReceivers = oldNbActivatedLaserReceivers;
        this.newNbActivatedLaserReceivers = newNbActivatedLaserReceivers;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public int getOldNbActivatedLaserReceivers() {
        return oldNbActivatedLaserReceivers;
    }

    public int getNewNbActivatedLaserReceivers() {
        return newNbActivatedLaserReceivers;
    }

}
