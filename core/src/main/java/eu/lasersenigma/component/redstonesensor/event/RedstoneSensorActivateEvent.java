package eu.lasersenigma.component.redstonesensor.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.redstonesensor.RedstoneSensor;

public class RedstoneSensorActivateEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final RedstoneSensor redstoneSensor;

    public RedstoneSensorActivateEvent(RedstoneSensor redstoneSensor) {
        this.redstoneSensor = redstoneSensor;
    }

    @Override
    public RedstoneSensor getComponent() {
        return redstoneSensor;
    }
}
