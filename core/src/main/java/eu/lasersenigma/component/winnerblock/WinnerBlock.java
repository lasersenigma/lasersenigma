package eu.lasersenigma.component.winnerblock;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.component.AComponent;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.IDetectionComponent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

/**
 * A block that transforms itself in redstone block if enough laser receivers are activated
 */
public final class WinnerBlock extends AComponent implements IDetectionComponent {

    // The minimum number of [activated laser receivers | players inside the area] needed to activate this component
    private int min = 1;

    // The maximum number of [activated laser receivers | players inside the area] needed to activate this component
    private int max = 10;

    // The actual number of (laser receivers activated | players inside the area
    private int nbActivated = -1;

    private int nbActivatedCache = -1;

    // The detection mode of the component ( number of activated laser receivers or number of players inside the area)
    private DetectionMode detectionMode = DetectionMode.DETECTION_LASER_RECEIVERS;

    /**
     * Constructor used for creation from database
     *
     * @param area          The area containing this winner block
     * @param componentId   The id of the component inside the database
     * @param location      The location of this winner block
     * @param min           The minimum number of activated laser receivers | players needed to make this block transform into a redstone block
     * @param max           The maximum number of activated laser receivers | players needed to make this block transform into a redstone block
     * @param detectionMode The detection mode
     */
    public WinnerBlock(Area area, int componentId, Location location, int min, int max, DetectionMode detectionMode) {
        super(area, componentId, location, ComponentType.REDSTONE_WINNER_BLOCK);
        this.min = min;
        this.max = max;
        this.detectionMode = detectionMode;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this winner block
     * @param location The location of this winner block
     */
    public WinnerBlock(Area area, Location location) {
        super(area, location, ComponentType.REDSTONE_WINNER_BLOCK);
        dbCreate();
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public boolean isActivated() {
        return (this.min <= this.nbActivated && this.nbActivated <= this.max);
    }

    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated) {
            this.nbActivated = nbActivated;
            showOrUpdateComponent();
        }

    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToLaserSender() || mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }

        this.detectionMode = mode;
        dbUpdate();
        showOrUpdateComponent();
    }

    @Override
    public DetectionMode getDetectionMode() {
        return detectionMode;
    }

    @Override
    public void activateComponent() {
        nbActivated = nbActivatedCache != -1 ? nbActivatedCache : 0;
    }

    @Override
    public void deactivateComponent() {
        nbActivatedCache = nbActivated;
        nbActivated = 0;
    }

    @Override
    public void showOrUpdateComponent() {
        Block block = componentLocation.getBlock();

        if (isActivated()) block.setType(Material.REDSTONE_BLOCK);
        else block.setType(Material.COAL_BLOCK);

        block.getState().update(true, true);
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }

}
