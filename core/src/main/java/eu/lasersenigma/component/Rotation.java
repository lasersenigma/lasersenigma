package eu.lasersenigma.component;

import org.bukkit.Location;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a rotation in Euler angles with additional methods for handling rotations
 * related to armor stands.
 */
public class Rotation extends EulerAngle {

    private static final double ANGLE_DIFF = Math.toRadians(22.5);

    private static final double AS_HEAD_RADIUS = 0.28;

    /**
     * Constructs a Rotation with the specified pitch and yaw angles
     *
     * @param x The pitch angle
     * @param y The yaw angle
     */
    public Rotation(double x, double y) {
        super(x, y, 0);
    }

    /**
     * Constructs a Rotation from an existing EulerAngle
     *
     * @param eulerAngle The EulerAngle to create the Rotation from
     */
    public Rotation(EulerAngle eulerAngle) {
        super(eulerAngle.getX(), eulerAngle.getY(), 0);
    }

    /**
     * Retrieves the next rotation based on the current Euler angle and the type of rotation
     *
     * @param rotationType The type of rotation to be performed
     * @return The new Euler angle representing the armor stand head
     */
    public Rotation getNextRotation(RotationType rotationType) {
        return switch (rotationType) {
            case RIGHT -> new Rotation(getX(), (getY() + ANGLE_DIFF));
            case LEFT -> new Rotation(getX(), (getY() - ANGLE_DIFF));
            case DOWN -> new Rotation((getX() + ANGLE_DIFF), getY());
            case UP -> new Rotation((getX() - ANGLE_DIFF), getY());
            default -> throw new UnsupportedOperationException("What type of rotation is that ?");
        };
    }

    /**
     * Retrieves the previous rotation based on the current Euler angle and the type of rotation
     *
     * @param rotationType The type of rotation to be performed
     * @return The new Euler angle representing the armor stand head
     */
    public Rotation getPrevRotation(RotationType rotationType) {
        return switch (rotationType) {
            case RIGHT -> new Rotation(getX(), (getY() - ANGLE_DIFF));
            case LEFT -> new Rotation(getX(), (getY() + ANGLE_DIFF));
            case DOWN -> new Rotation((getX() - ANGLE_DIFF), getY());
            case UP -> new Rotation((getX() + ANGLE_DIFF), getY());
            default -> throw new UnsupportedOperationException("What type of rotation is that ?");
        };
    }

    /**
     * Converts the Euler angle to a normalized vector representing the eye direction
     *
     * @return The normalized vector representing the eye direction
     */
    public Vector toEyeDirection() {

        //head rotation offset
        double doubleRotX = getX() + Math.toRadians(90);
        double doubleRotY = getY();
        double x = Math.sin(doubleRotY) * Math.sin(doubleRotX + Math.toRadians(180));
        double y = Math.cos(doubleRotX);
        double z = Math.cos(doubleRotY) * Math.sin(doubleRotX);

        //rounding
        x = (double) (Math.round(x * 100000d) / 100000d);
        y = (double) (Math.round(y * 100000d) / 100000d);
        z = (double) (Math.round(z * 100000d) / 100000d);

        Vector v = new Vector(x, y, z).normalize();

        return v;
    }

    /**
     * Converts the Euler angle to a normalized vector representing the head direction
     *
     * @return The normalized vector representing the head direction
     */
    public Vector toHeadDirection() {

        //head rotation offset
        double doubleRotX = getX();
        double doubleRotY = getY();
        double x = Math.sin(doubleRotY) * Math.sin(doubleRotX + Math.toRadians(180));
        double y = Math.cos(doubleRotX);
        double z = Math.cos(doubleRotY) * Math.sin(doubleRotX);

        //rounding
        x = (double) (Math.round(x * 100000d) / 100000d);
        y = (double) (Math.round(y * 100000d) / 100000d);
        z = (double) (Math.round(z * 100000d) / 100000d);

        Vector v = new Vector(x, y, z).normalize();

        return v;
    }

    /**
     * Converts the Euler angle to a normalized vector representing the side direction
     *
     * @return The normalized vector representing the side direction
     */
    public Vector toSideDirection() {
        return toEyeDirection().crossProduct(toHeadDirection()).normalize();
    }

    /**
     * Retrieves the front corners of the armor stand head based on its center location
     *
     * @param armorStandHeadCenterLoc The center location of the armor stand head
     * @return A list of locations representing the front corners of the armor stand head
     */
    public List<Location> getFrontCorners(Location armorStandHeadCenterLoc) {
        List<Location> result = new ArrayList<>();
        armorStandHeadCenterLoc.add(toEyeDirection().multiply(AS_HEAD_RADIUS));
        Vector top = toHeadDirection().multiply(AS_HEAD_RADIUS);
        Vector side = toSideDirection().multiply(AS_HEAD_RADIUS);
        Vector minusSide = side.clone().multiply(-1);
        Vector minusTop = top.clone().multiply(-1);
        result.add(armorStandHeadCenterLoc.clone().add(top).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(top).add(minusSide));
        result.add(armorStandHeadCenterLoc.clone().add(minusTop).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(minusTop).add(minusSide));
        return result;
    }

    public List<Location> getAllCorners(Location armorStandHeadCenterLoc) {
        List<Location> result = new ArrayList<>();
        Vector depth = toEyeDirection().multiply(AS_HEAD_RADIUS);
        Vector top = toHeadDirection().multiply(AS_HEAD_RADIUS);
        Vector side = toSideDirection().multiply(AS_HEAD_RADIUS);
        Vector minusSide = side.clone().multiply(-1);
        Vector minusTop = top.clone().multiply(-1);
        Vector minusDepth = depth.clone().multiply(-1);
        result.add(armorStandHeadCenterLoc.clone().add(depth).add(top).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(depth).add(top).add(minusSide));
        result.add(armorStandHeadCenterLoc.clone().add(depth).add(minusTop).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(depth).add(minusTop).add(minusSide));
        result.add(armorStandHeadCenterLoc.clone().add(minusDepth).add(top).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(minusDepth).add(top).add(minusSide));
        result.add(armorStandHeadCenterLoc.clone().add(minusDepth).add(minusTop).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(minusDepth).add(minusTop).add(minusSide));
        return result;

    }
}
