package eu.lasersenigma.component;

import eu.lasersenigma.common.items.LasersColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;

/**
 * Represents an interface for components that can contain mirrors
 */
public interface IMirrorContainer extends IColorableComponent {

    /**
     * Retrieves the face of the container
     *
     * @return The face of the container
     */
    ComponentFace getComponentFace();

    /**
     * Checks if a mirror has been saved
     *
     * @return True if a mirror has been saved, false otherwise
     */
    boolean hasMirror();

    /**
     * Checks if the container has a mirror
     *
     * @return True if the container has a mirror, false otherwise
     */
    boolean hasMirrorCurrent();

    /**
     * Sets whether the container has a mirror or not
     *
     * @param hasMirror True if the container has a mirror, false otherwise
     * @param save      True if the change should be saved, false otherwise
     */
    void setHasMirror(boolean hasMirror, boolean save);

    /**
     * Places a mirror in the container with the specified color
     *
     * @param color The color of the mirror
     * @param save  True if the placement should be saved, false otherwise
     * @return True if the mirror is successfully placed, false otherwise
     */
    boolean placeMirror(LasersColor color, boolean save);

    /**
     * Removes the mirror from the container
     *
     * @param save True if the removal should be saved, false otherwise
     * @return True if the mirror is successfully removed, false otherwise
     */
    boolean removeMirror(boolean save);

    /**
     * Gets the vector for the animation clip
     *
     * @return The vector for the animation clip
     */
    Vector getAnimationClipVector();

    /**
     * Gets the vector for the animation
     *
     * @return The vector for the animation
     */
    Vector getAnimationVector();

    /**
     * Checks if there is an ongoing animation
     *
     * @return True if there is an ongoing animation, false otherwise
     */
    boolean isOnGoingAnimation();

    /**
     * Sets whether there is an ongoing animation or not
     *
     * @param onGoingAnimation True if there is an ongoing animation, false otherwise
     */
    void setOnGoingAnimation(boolean onGoingAnimation);

    /**
     * Gets the location of the armor stand with offsets
     *
     * @param baseLocation The base location
     * @param offset       The offset of the armor stand
     * @param rot          The rotation of the armor stand
     * @return The location of the armor stand with offsets
     */
    Location getArmorStandLocationWithOffsets(Location baseLocation, ArmorStandItemOffset offset, Rotation rot);

    /**
     * Gets the container's armor stand
     *
     * @return The container's armor stand
     */
    ArmorStand getArmorStandMirrorContainer();

    /**
     * Gets the mirror's armor stand
     *
     * @return The mirror's armor stand
     */
    ArmorStand getArmorStandMirror();
}
