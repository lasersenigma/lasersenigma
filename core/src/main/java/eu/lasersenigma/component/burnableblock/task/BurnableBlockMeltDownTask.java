package eu.lasersenigma.component.burnableblock.task;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.component.burnableblock.BurnableBlock;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.HashSet;

/**
 * Task used to melt down a burnable block
 */
public class BurnableBlockMeltDownTask extends BukkitRunnable {

    /**
     * the burnable block
     */
    private final BurnableBlock burnableBlock;
    /**
     * the location of the burnable block
     */
    private final Location location;
    /**
     * the number of times this task must be ran
     */
    private int nbTimesRemaining = 50;

    /**
     * Constructor
     *
     * @param burnableBlock the burnable block that must melt down
     */
    public BurnableBlockMeltDownTask(BurnableBlock burnableBlock) {
        this.burnableBlock = burnableBlock;
        this.location = burnableBlock.getComponentLocation().clone().add(0.5, 0.5, 0.5);
        SoundLauncher.playSound(location, PlaySoundCause.CLAY_MELTDOWN);
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 0);
    }

    /**
     * the method called on each execution of the task
     */
    @Override
    public void run() {
        HashSet<Player> players = burnableBlock.getArea().getPlayersInsideArea();
        if (burnableBlock.isRemoved()) {
            this.cancel();
            return;
        }
        if (nbTimesRemaining <= 1) {
            burnableBlock.setIsMelting(false);
            burnableBlock.setMelted(true);
            LaserParticle.playEffect(players, location, Particle.LAVA, 0.5, 0.5, 0.5, 0.1, 15);
            Bukkit.getOnlinePlayers().forEach((player) -> {
                Location playerLocation = player.getLocation();
                if (player.getWorld() == location.getWorld()) {
                    double distance = playerLocation.distance(location);
                    if (distance < 3) {
                        Vector v = new Vector(
                                playerLocation.getX() - location.getX(),
                                playerLocation.getY() - location.getY(),
                                playerLocation.getZ() - location.getZ()
                        );
                        v.normalize().multiply(LasersEnigmaPlugin.getInstance().getConfig().getDouble("laser_knockback_multiplier")).multiply(1 / distance);
                        player.setVelocity(v);
                        player.damage(distance);
                    }
                }
            });
            this.cancel();
            return;
        }
        LaserParticle.playEffect(players, location, Particle.SMOKE_LARGE, 0, 0, 0, 0.1, 5);
        LaserParticle.playEffect(players, location, Particle.LAVA, 0, 0, 0, 0.1, 1);
        nbTimesRemaining--;
    }

}
