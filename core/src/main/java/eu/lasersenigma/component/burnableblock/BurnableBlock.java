package eu.lasersenigma.component.burnableblock;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.task.ITaskComponent;
import eu.lasersenigma.component.AComponent;
import eu.lasersenigma.component.IColorableComponent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.burnableblock.event.BurnableBlockIgnitionEvent;
import eu.lasersenigma.component.burnableblock.task.BurnableBlockMeltDownTask;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Colored clay block that will melt and explode if a laser of the correct color touches it.
 */
public final class BurnableBlock extends AComponent implements IComponent, IColorableComponent, ITaskComponent {

    // The colors that are received in this laser receiver associated with an integer value representing the moment they were received
    private final HashMap<LasersColor, Integer> receivedColorsLifeTime;

    // The material of this component as saved in database
    private Material material;

    // The color of this component as saved in database
    private LasersColor color;

    // Current color of this component (not saved in the database)
    private LasersColor currentColor;

    // The id of the task scanning received lasers
    private Integer taskId;

    // Is the clay currently melting
    private boolean isMelting;

    // Did the clay already melt
    private boolean melted;

    /**
     * Constructor used for creation from database
     *
     * @param area        The area containing this burnable block
     * @param componentId The id of the component inside the database
     * @param location    The location of this burnable block
     * @param color       The color of this burnable block
     */
    public BurnableBlock(Area area, int componentId, Location location, LasersColor color, Material material) {
        super(area, componentId, location, ComponentType.BURNABLE_BLOCK);
        receivedColorsLifeTime = new HashMap<>();
        melted = false;
        isMelting = false;
        this.color = color;
        currentColor = color;
        this.material = material == null ? Item.getBurnableBlock(color).getMaterial() : material;
        this.taskId = null;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this burnable block
     * @param location The location of this burnable block
     * @param color    The color of this burnable block
     */
    public BurnableBlock(Area area, Location location, LasersColor color) {
        super(area, location, ComponentType.BURNABLE_BLOCK);
        receivedColorsLifeTime = new HashMap<>();
        melted = false;
        isMelting = false;
        this.color = color;
        this.material = Item.getBurnableBlock(color).getMaterial();
        currentColor = color;
        this.taskId = null;
        dbCreate();
    }

    /**
     * Constructor
     *
     * @param area     The area containing this burnable block
     * @param location The location containing this burnable block
     */
    public BurnableBlock(Area area, Location location) {
        this(
                area,
                location,
                LasersColor.WHITE
        );
    }

    /**
     * Checks if this burnable block is currently melting
     */
    public boolean isMelting() {
        return isMelting;
    }

    /**
     * Defines burnable block melting state
     *
     * @param melting true to set this burnable block melting
     */
    public void setIsMelting(boolean melting) {
        this.isMelting = melting;
    }

    /**
     * is this burnable block already melted
     *
     * @return true if this burnable block is melted
     */
    public boolean isMelted() {
        return this.melted;
    }

    /**
     * sets if this burnable block is melted
     *
     * @param melted true to set this burnable block melted
     */
    public void setMelted(boolean melted) {
        this.melted = melted;
        showOrUpdateComponent();
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public void activateComponent() {
        isMelting = false;
        melted = false;
        currentColor = color;
        receivedColorsLifeTime.clear();
    }

    @Override
    public void deactivateComponent() {
        isMelting = false;
        melted = false;
        currentColor = color;
        receivedColorsLifeTime.clear();
    }

    /**
     * updates this burnable block
     */
    @Override
    public void showOrUpdateComponent() {
        if (melted) {
            Block b = getComponentLocation().getBlock();
            b.setType(Material.AIR);
            b.getState().update();
            return;
        }
        if (!shouldMeltDown()) {
            Block b = getComponentLocation().getBlock();
            b.setType(material);
            b.getState().update();
        }
    }

    /**
     * deletes this burnable block
     */
    /*@Override
    public void hideComponent() {
        super.hideComponent();
        cancelTask();
        this.receivedColorsLifeTime.clear();
    }*/

    /**
     * resets the burnable block
     */
    /*@Override
    public final void resetComponent() {
        isMelting = false;
        melted = false;
        currentColor = color;
        receivedColorsLifeTime.clear();
        showOrUpdateDisplay();
    }*/
    @Override
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        if (isMelted()) {
            return new LaserReceptionResult(false);
        } else {
            if (!isMelting && shouldMeltDown()) {
                isMelting = true;
                Bukkit.getServer().getPluginManager().callEvent(new BurnableBlockIgnitionEvent(this));
                new BurnableBlockMeltDownTask(this);
            } else {
                this.receivedColorsLifeTime.put(laserParticle.getColor(), 0);
            }
            return new LaserReceptionResult(true);
        }
    }

    private boolean shouldMeltDown() {
        Set<LasersColor> colors = receivedColorsLifeTime.entrySet().stream().filter((e) -> e.getValue() == 1).map(Map.Entry::getKey).collect(Collectors.toSet());
        return currentColor.isComposedOf(colors, LasersEnigmaPlugin.getInstance().getConfig().getBoolean(LasersColor.ONLY_NEED_COLORS_CONFIG_PATH));
    }

    @Override
    public LasersColor getColorCurrent() {
        return currentColor;
    }

    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public void changeColor() {
        changeColor(true);
    }

    @Override
    public void changeColor(boolean save) {
        setColor(currentColor.getNextColor(false), save);
    }

    @Override
    public void setColor(LasersColor colorCurrent) {
        setColor(colorCurrent, true);
    }

    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }

        if (isMelting) {
            return;
        }

        this.currentColor = color;

        if (save) {
            this.color = color;
        }

        this.material = Item.getBurnableBlock(color).getMaterial();

        showOrUpdateComponent();

        if (save) {
            dbUpdate();
        }
    }

    @Override
    public void startTask() {
        if (!getArea().isActivated()) {
            return;
        }

        if (taskId != null) {
            return;
        }

        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), (() -> {
            if (!getArea().isActivated()) {
                cancelTask();
                return;
            }
            receivedColorsLifeTime.replaceAll((k, v) -> v + 1); // increment LifeTime
            receivedColorsLifeTime.entrySet().removeIf(e -> e.getValue() >= 2);
        }), 0, LaserParticle.LASERS_FREQUENCY);
        taskId = task.getTaskId();
    }

    @Override
    public void cancelTask() {
        if (taskId == null) {
            return;
        }

        Bukkit.getScheduler().cancelTask(taskId);
        receivedColorsLifeTime.clear();
        taskId = null;
    }
}
