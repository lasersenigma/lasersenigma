package eu.lasersenigma.component.burnableblock.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.burnableblock.BurnableBlock;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class PlayerTryToChangeBurnableBlockMaterialEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final BurnableBlock component;

    private final Material newMaterial;

    public PlayerTryToChangeBurnableBlockMaterialEvent(Player player, BurnableBlock component, Material newMaterial) {
        super();
        this.player = player;
        this.component = component;
        this.newMaterial = newMaterial;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public BurnableBlock getBurnableBlockComponent() {
        return component;
    }

    public Material getNewMaterial() {
        return newMaterial;
    }

}
