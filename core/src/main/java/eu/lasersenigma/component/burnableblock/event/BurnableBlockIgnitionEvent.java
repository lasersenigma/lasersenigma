package eu.lasersenigma.component.burnableblock.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.burnableblock.BurnableBlock;

public class BurnableBlockIgnitionEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final BurnableBlock component;

    public BurnableBlockIgnitionEvent(BurnableBlock component) {
        super();
        this.component = component;
    }

    @Override
    public BurnableBlock getComponent() {
        return component;
    }

}
