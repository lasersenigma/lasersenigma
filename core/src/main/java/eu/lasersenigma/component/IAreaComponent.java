package eu.lasersenigma.component;

import org.bukkit.Location;

/**
 * Represents an interface for components that define an area
 */
public interface IAreaComponent extends IComponent {

    /**
     * Gets the minimum location of the component's area
     *
     * @return The minimum location of the component's area
     */
    Location getMinLocation();

    /**
     * Gets the maximum location of the component's area
     *
     * @return The maximum location of the component's area
     */
    Location getMaxLocation();
}
