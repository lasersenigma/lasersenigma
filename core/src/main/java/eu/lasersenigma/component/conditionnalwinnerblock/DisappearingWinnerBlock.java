package eu.lasersenigma.component.conditionnalwinnerblock;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.component.AComponent;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.IDetectionComponent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;

/**
 * A block that disappears when enough laser receivers are activated
 */
public final class DisappearingWinnerBlock extends AComponent implements IDetectionComponent {

    // The minimum number of [activated laser receivers | players inside the area] needed to activate this component
    private int min;

    // The maximum number of [activated laser receivers | players inside the area] needed to activate this component
    private int max;

    // The actual number of (laser receivers activated | players inside the area)
    private int nbActivated;

    // The detection mode of the component ( number of activated laser receivers or number of players inside the area)
    private DetectionMode detectionMode;

    /**
     * Constructor used for creation from database
     *
     * @param area          The area containing this component
     * @param componentId   The id of the component inside the database
     * @param location      The component location
     * @param min           The minimum number of activated laser receivers | players needed to make this block disappear
     * @param max           The maximum number of activated laser receivers | players needed to make this block disappear
     * @param detectionMode The detection mode
     */
    public DisappearingWinnerBlock(Area area, int componentId, Location location, int min, int max, DetectionMode detectionMode) {
        super(area, componentId, location, ComponentType.DISAPPEARING_WINNER_BLOCK);
        this.min = min;
        this.max = max;
        this.nbActivated = -1;
        this.detectionMode = detectionMode;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this component
     * @param location The component location
     */
    public DisappearingWinnerBlock(Area area, Location location) {
        super(area, location, ComponentType.DISAPPEARING_WINNER_BLOCK);
        this.min = 1;
        this.max = 10;
        this.nbActivated = -1;
        this.detectionMode = DetectionMode.DETECTION_LASER_RECEIVERS;
        dbCreate();
    }

    @Override
    public final int getMin() {
        return min;
    }

    @Override
    public final void setMin(int min) {
        this.min = min;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public final int getMax() {
        return max;
    }

    @Override
    public final void setMax(int max) {
        this.max = max;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public final boolean isActivated() {
        return (this.min <= this.nbActivated && this.nbActivated <= this.max);
    }

    @Override
    public final void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated) {
            this.nbActivated = nbActivated;
            showOrUpdateComponent();
        }
    }

    @Override
    public final void changeMode(DetectionMode mode) {
        if (mode.isSpecificToLaserSender() || mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.detectionMode = mode;
        dbUpdate();
        showOrUpdateComponent();
    }

    @Override
    public final DetectionMode getDetectionMode() {
        return detectionMode;
    }

    /**
     * Checks if lasers can pass through this component
     *
     * @return True if the lasers can pass through this component, false elsewhere
     */
    public final boolean isCrossable() {
        return isActivated();
    }

    @Override
    public final void activateComponent() {
        nbActivated = 0;
    }

    @Override
    public final void deactivateComponent() {
        nbActivated = 0;
    }

    /**
     * updates this component
     */
    @Override
    public final void showOrUpdateComponent() {
        if (isActivated()) getComponentLocation().getBlock().setType(Material.AIR);
        else getComponentLocation().getBlock().setType(Material.OBSIDIAN);

        getComponentLocation().getBlock().getState().update();
    }

    @Override
    public final LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(!isCrossable());
    }

}
