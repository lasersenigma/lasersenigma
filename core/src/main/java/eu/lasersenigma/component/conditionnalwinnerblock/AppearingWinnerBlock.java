package eu.lasersenigma.component.conditionnalwinnerblock;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.component.AComponent;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.IDetectionComponent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

/**
 * A block that appears when enough laser receivers are activated
 */
public final class AppearingWinnerBlock extends AComponent implements IDetectionComponent {

    // The minimum number of [activated laser receivers | players inside the area] needed to activate this component
    private int min = 1;

    // Maximum number of [activated laser receivers | players inside the area] needed to activate this component
    private int max = 10;

    // The detection mode of the component ( number of activated laser receivers or number of players inside the area)
    private DetectionMode mode = DetectionMode.DETECTION_LASER_RECEIVERS;

    // The actual number of (laser receivers activated | players inside the area)
    private int nbActivated = -1;

    /**
     * Constructor used for creation from database
     *
     * @param area        The area containing this component
     * @param componentId The id of the component inside the database
     * @param location    The component location
     * @param min         The minimum number of activated laser receivers | players needed to make this block appear
     * @param max         The maximum number of activated laser receivers | players needed to make this block appear
     * @param mode        The detection mode
     */
    public AppearingWinnerBlock(Area area, int componentId, Location location, int min, int max, DetectionMode mode) {
        super(area, componentId, location, ComponentType.APPEARING_WINNER_BLOCK);
        this.min = min;
        this.max = max;
        this.mode = mode;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this component
     * @param location The component location
     */
    public AppearingWinnerBlock(Area area, Location location) {
        super(area, location, ComponentType.APPEARING_WINNER_BLOCK);
        dbCreate();
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public boolean isActivated() {
        return (this.min <= this.nbActivated && this.nbActivated <= this.max);
    }

    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated) {
            this.nbActivated = nbActivated;
            showOrUpdateComponent();
        }
    }

    /**
     * Checks if lasers can pass through this component
     *
     * @return True if the lasers can pass through this component, false elsewhere
     */
    public boolean isCrossable() {
        return !isActivated();
    }

    @Override
    public DetectionMode getDetectionMode() {
        return mode;
    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToLaserSender() || mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.mode = mode;
        dbUpdate();
        showOrUpdateComponent();
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(!isCrossable());
    }

    @Override
    public void activateComponent() {
        nbActivated = 0;
    }

    @Override
    public void deactivateComponent() {
        nbActivated = 0;
    }

    @Override
    public void showOrUpdateComponent() {
        Block block = getComponentLocation().getBlock();

        if (isActivated()) block.setType(Material.OBSIDIAN);
        else block.setType(Material.AIR);

        block.getState().update();
    }

}
