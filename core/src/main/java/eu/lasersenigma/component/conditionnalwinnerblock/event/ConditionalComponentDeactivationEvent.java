package eu.lasersenigma.component.conditionnalwinnerblock.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IDetectionComponent;

public class ConditionalComponentDeactivationEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final IDetectionComponent component;

    public ConditionalComponentDeactivationEvent(IDetectionComponent component) {
        super();
        this.component = component;
    }

    @Override
    public IDetectionComponent getComponent() {
        return component;
    }

}
