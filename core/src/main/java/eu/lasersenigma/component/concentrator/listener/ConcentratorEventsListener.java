package eu.lasersenigma.component.concentrator.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.component.concentrator.Concentrator;
import eu.lasersenigma.component.concentrator.event.ConcentratorColorChangeEvent;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

public class ConcentratorEventsListener implements Listener {

    @SuppressWarnings("LeakingThisInConstructor")
    public ConcentratorEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * Executed on a ConcentratorColorChangedLEEvent
     * Play a sound when a concentrator resulting color changes (but at least 2s after the area activation)
     */
    @EventHandler(ignoreCancelled = true)
    public void onConcentratorColorChange(ConcentratorColorChangeEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onConcentratorColorChange");

        Concentrator eventConcentrator = event.getComponent();

        // When lasers senders are activated except if it's during area activation.
        if (eventConcentrator.getArea().getUpdateLasersCount() > 40) {
            SoundLauncher.playSound(eventConcentrator.getComponentLocation(), PlaySoundCause.CONCENTRATOR_COLOR_CHANGE);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onComponentRotation(ComponentRotationEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onComponentRotation");

        if (!(event.getComponent() instanceof Concentrator)) return;
        if (!event.getActionCause().equals(ActionCause.PLAYER)) return;

        SoundLauncher.playSound(event.getComponent().getComponentLocation(), PlaySoundCause.CONCENTRATOR_ROTATE);
    }
}
