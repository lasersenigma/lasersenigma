package eu.lasersenigma.component.concentrator.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.concentrator.Concentrator;

public class ConcentratorColorChangeEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final Concentrator concentrator;

    public ConcentratorColorChangeEvent(Concentrator concentrator) {
        super();
        this.concentrator = concentrator;
    }

    @Override
    public Concentrator getComponent() {
        return concentrator;
    }

}
