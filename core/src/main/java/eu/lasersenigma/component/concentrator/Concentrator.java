package eu.lasersenigma.component.concentrator;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.task.ITaskComponent;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.concentrator.event.ConcentratorColorChangeEvent;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Concentrator
 */
public final class Concentrator extends AArmorStandComponent implements IRotatableComponent, IPlayerModifiableComponent, ITaskComponent {

    // Colors that received by the concentrator associated with an integer value representing the moment they were received
    private final HashMap<Integer, Set<LasersColor>> receivedColorsLifeTime = new HashMap<>();

    // Light levels received by the concentrator associated with an integer value representing the moment they were received
    private final HashMap<Integer, Set<Integer>> receivedLightsLifeTime = new HashMap<>();

    private LasersColor resultingColor;

    // Task's id used to scan the lasers input
    private Integer taskId = null;

    // Component's armor stand
    private final ComponentArmorStand CONCENTRATOR_AS = new ComponentArmorStand(this, "concentrator");

    // Component's events listener
    // private final Listener eventsListener = new ConcentratorEventsListener(this);

    /**
     * Constructor used for creation from the database
     *
     * @param area        The area containing this concentrator
     * @param componentId The id of the component inside the database
     * @param location    The location of the concentrator
     * @param face        The blockface of the concentrator
     * @param rotation    The rotation of the concentrator
     */
    public Concentrator(Area area, int componentId, Location location, ComponentFace face, Rotation rotation) {
        super(area, componentId, location, ComponentType.CONCENTRATOR, face, rotation);
    }

    /**
     * Constructor
     *
     * @param area     The area containing this concentrator
     * @param location The location of the concentrator
     * @param face     The blockface of the concentrator
     */
    public Concentrator(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.CONCENTRATOR, face);
        dbCreate();
    }

    /**
     * Checks if the concentrator is currently activated and retrieves the recalculated light Level
     *
     * @return The resulting light level
     */
    public int getResultingLightLevel() {
        return Collections.max(receivedLightsLifeTime.get(1));
    }

    /**
     * Checks if the concentrator is currently activated and retrieves the recomposed color
     *
     * @return The resulting laser color
     */
    public LasersColor getResultingColor() {
        LasersColor previousColor = resultingColor;
        resultingColor = LasersColor.merge(receivedColorsLifeTime.get(1));

        if (resultingColor != null && previousColor != resultingColor) {
            ConcentratorColorChangeEvent concentratorColorChangeEvent = new ConcentratorColorChangeEvent(this);
            Bukkit.getServer().getPluginManager().callEvent(concentratorColorChangeEvent);
        }

        return resultingColor;
    }

    @Override
    public void activateComponent() {
        receivedColorsLifeTime.clear();
        receivedLightsLifeTime.clear();
        resultingColor = null;

        // Restore cache state
        rotationCurrent = rotation;
    }

    @Override
    public void deactivateComponent() {
        receivedColorsLifeTime.clear();
        receivedLightsLifeTime.clear();
        resultingColor = null;

        // Reset default state
        rotationCurrent = rotation;
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(CONCENTRATOR_AS, baseLocation, rotationCurrent, getHelmetItem(), false, false, null);
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();
        cancelTask();
    }

    private Item getHelmetItem() {
        if (area.isHConcentratorsRotationAllowed() && area.isVConcentratorsRotationAllowed()) {
            return Item.CONCENTRATOR_HV;
        } else if (area.isHConcentratorsRotationAllowed()) {
            return Item.CONCENTRATOR_H;
        } else if (area.isVConcentratorsRotationAllowed()) {
            return Item.CONCENTRATOR_V;
        } else {
            return Item.CONCENTRATOR;
        }
    }

    @Override
    public void rotate(RotationType rotationType, boolean save, ActionCause actionCause) {
        Location nextLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);

        rotationCurrent = rotationCurrent.getNextRotation(rotationType);
        CONCENTRATOR_AS.getArmorStand().setHeadPose(rotationCurrent);
        CONCENTRATOR_AS.getArmorStand().teleport(nextLocation);

        ComponentRotationEvent componentRotationEvent = new ComponentRotationEvent(this, actionCause);
        Bukkit.getServer().getPluginManager().callEvent(componentRotationEvent);

        if (save) {
            rotation = rotationCurrent;
            dbUpdate();
        }
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }

        // WARNING: NOT SURE ABOUT CHECKING != NULL HAVE ANY UNDESIRED IMPACTS
        if (receivedColorsLifeTime.get(0) != null) receivedColorsLifeTime.get(0).add(laserParticle.getColor());
        if (receivedLightsLifeTime.get(0) != null) receivedLightsLifeTime.get(0).add(laserParticle.getLightLevel());

        return new LaserReceptionResult(true);
    }

    @Override
    public void startTask() {
        if (!area.isActivated()) return;

        if (taskId != null) return;

        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), () -> {
            if (!area.isActivated()) {
                cancelTask();
                return;
            }

            for (int i = 0; i < 3; ++i) {
                receivedColorsLifeTime.put(i + 1, receivedColorsLifeTime.get(i));
                receivedLightsLifeTime.put(i + 1, receivedLightsLifeTime.get(i));
            }

            receivedColorsLifeTime.put(0, new HashSet<>());
            receivedLightsLifeTime.put(0, new HashSet<>());

        }, 0, LaserParticle.LASERS_FREQUENCY);
        taskId = task.getTaskId();
    }

    @Override
    public void cancelTask() {
        // Check if there is something to cancel
        if (taskId == null) return;

        // Cancel the task
        Bukkit.getScheduler().cancelTask(taskId);

        taskId = null;
    }
}
