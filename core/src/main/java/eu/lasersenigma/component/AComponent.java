package eu.lasersenigma.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.component.scheduledactions.ActionType;
import eu.lasersenigma.component.scheduledactions.ScheduledAction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.logging.Level;

/**
 * This abstract class serves as a common base for all components. It defines
 * essential attributes and methods that are shared among different types of
 * components, allowing for a consistent and unified implementation.
 */

public abstract class AComponent implements IComponent {

    // Component's area
    protected final Area area;

    // Component's id
    protected int componentId;

    // Component's type
    protected final ComponentType componentType;

    // Component's location
    protected final Location componentLocation;

    // The scheduled actions which will be executed in a loop
    protected ArrayList<ScheduledAction> scheduledActions = new ArrayList<>();

    // The id of the task handling the next scheduledAction;
    protected Integer scheduledActionsTaskId = null;

    private Integer dbUpdateComponentScheduledActionsTaskId;

    // Component's options
    private LinkedHashSet<ComponentOption> componentOptions = new LinkedHashSet<>();

    // Component's events listener
    private Listener componentEventsListener = null;

    /**
     * Constructor
     *
     * @param area              The area containing this component
     * @param componentLocation The location of the component
     * @param componentType     The type of the component
     */
    public AComponent(Area area, Location componentLocation, ComponentType componentType) {
        this.componentLocation = componentLocation;
        this.componentType = componentType;
        this.area = area;
    }

    /**
     * Constructor used for creation from database
     *
     * @param area              The area containing this component
     * @param componentId       the id of the component
     * @param componentLocation The location of the component
     * @param componentType     The type of the component
     */
    public AComponent(Area area, int componentId, Location componentLocation, ComponentType componentType) {
        this(area, componentLocation, componentType);
        this.componentId = componentId;
    }

    @Override
    public final Area getArea() {
        return area;
    }

    @Override
    public final int getComponentId() {
        return componentId;
    }

    @Override
    public final ComponentType getComponentType() {
        return componentType;
    }

    @Override
    public final Location getComponentLocation() {
        return componentLocation.clone();
    }

    @Override
    public final int getComponentOptionsBitmap() {
        int bitmap = 0;

        for (ComponentOption option : componentOptions) {
            if (!option.isEnabled()) continue;
            bitmap += 1 << findPosition(option);
        }

        return bitmap;
    }

    @Override
    public final void setComponentOptionsFromBitmap(int bitmap) {
        int log2 = (int) (Math.log(bitmap) / Math.log(2));
        int highestPower = (int) Math.floor(log2);

        int i, j;

        for (i = highestPower; i > 0; i--) {
            j = (int) Math.pow(2, i);

            if ((bitmap - j) < 0) break;

            ComponentOption option = getComponentOptionFromBitmapPosition(j);
            option.setEnabled(true);

        }
    }

    private ComponentOption getComponentOptionFromBitmapPosition(int position) {
        if (position < 0 || position >= componentOptions.size()) throw new IllegalArgumentException("Error: Position is invalid!");
        return componentOptions.stream().toList().get(position);
    }

    private boolean isComponentOptionEnabled(int bitmap, ComponentOption option) {
        int optionPosition = getBitmapPositionFromComponentOption(option);
        return (bitmap & optionPosition) == optionPosition;
    }

    private int getBitmapPositionFromComponentOption(ComponentOption option) {
        int position = findPosition(option);
        if (position < 0) throw new IllegalArgumentException("Error: Specified component's option seems not to be part of this component's options!");
        return 1 << position;
    }

    private <T> int findPosition(ComponentOption desiredOption) {
        return componentOptions.stream().toList().indexOf(desiredOption);
    }

    /**
     * Get component's options
     *
     * @return Set of options
     */
    public final LinkedHashSet<ComponentOption> getComponentOptions() {
        return componentOptions;
    }

    /**
     * Add an option to current component
     *
     * @param option Option to add
     * @return True if this component did not already contain the option
     */
    public final boolean addOption(ComponentOption option) {
        return componentOptions.add(option);
    }

    /**
     * Retrieves component's events listener
     *
     * @return Component's events listener
     */
    public final Listener getComponentEventsListener() {
        return componentEventsListener;
    }

    /**
     * Define component's events listener
     *
     * @param componentEventsListener Listener to add
     */
    public final void setComponentEventsListener(Listener componentEventsListener) {
        if (this.componentEventsListener != null) {
            return;
        }
        this.componentEventsListener = componentEventsListener;
        Bukkit.getPluginManager().registerEvents(componentEventsListener, LasersEnigmaPlugin.getInstance());
    }

    /**
     * Removes a component from the database
     */
    @Override
    public final void dbRemove() {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removeComponent(this);
    }

    /**
     * Updates the component into the database
     */
    @Override
    public final void dbUpdate() {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().updateComponent(this);
    }

    /**
     * Creates the component into the database
     */
    @Override
    public final void dbCreate() {
        this.componentId = LasersEnigmaPlugin.getInstance().getPluginDatabase().createComponent(this);
    }

    /**
     * Removes the component from the database
     */
    @Override
    public void deleteComponent() {
        Block block = componentLocation.getBlock();
        block.setType(Material.AIR);
        block.getState().update();

        // Remove component from database
        dbRemove();
    }

    @Override
    public final boolean isRemoved() {
        return getArea().getComponent(this.getComponentId()) != this;
    }

    @Override
    public boolean canBeDeleted() {
        return true;
    }

    @Override
    public final ArrayList<ScheduledAction> getScheduledActions() {
        return scheduledActions;
    }

    @Override
    public final void startScheduledActions() {
        if (scheduledActionsTaskId != null) {
            Bukkit.getScheduler().cancelTask(scheduledActionsTaskId);
        } else if (area.isActivated() && !scheduledActions.isEmpty()) {
            scheduledActionsTaskId = Bukkit.getScheduler().runTask(LasersEnigmaPlugin.getInstance(), () -> callAction(0)).getTaskId();
        }
    }

    @Override
    public final void stopScheduledActions() {
        if (scheduledActionsTaskId != null) {
            Bukkit.getScheduler().cancelTask(scheduledActionsTaskId);
            scheduledActionsTaskId = null;
        }
    }

    @Override
    public final void updateActions() {
        startScheduledActions();

        // Cancel the previously scheduled task if any
        if (dbUpdateComponentScheduledActionsTaskId != null) {
            Bukkit.getScheduler().cancelTask(dbUpdateComponentScheduledActionsTaskId);
        }

        // Schedule the task to update component actions asynchronously, with a delay of 10 seconds (200 ticks)
        dbUpdateComponentScheduledActionsTaskId = Bukkit.getScheduler().runTaskLaterAsynchronously(
                LasersEnigmaPlugin.getInstance(),
                () -> {
                    dbUpdateComponentScheduledActionsTaskId = null;
                    LasersEnigmaPlugin.getInstance().getPluginDatabase().updateComponentScheduledActions(this);
                },
                20 * 10
        ).getTaskId();
    }


    private void callAction(int i) {
        if (scheduledActions.isEmpty() || (!area.isActivated())) {
            scheduledActionsTaskId = null;
            return;
        }
        int delay = scheduledActions.stream().filter(action -> action.getType() == ActionType.WAIT).map(action -> action.getDelay()).reduce(Integer::sum).orElse(0);
        if (delay < 10) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Scheduled actions disallowed whithout at least 10 ticks delay");
            scheduledActionsTaskId = null;
            return;
        }
        if (i >= scheduledActions.size()) {
            i = 0;
        }
        ScheduledAction action = scheduledActions.get(i);
        int newIndex = i + 1;

        if (action.getType() == ActionType.WAIT) {
            scheduledActionsTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                this.callAction(newIndex);
            }, action.getDelay()).getTaskId();
        } else {
            if (action.getType().isDoableNow(this)) {
                action.doAction();
            }
            scheduledActionsTaskId = Bukkit.getScheduler().runTask(LasersEnigmaPlugin.getInstance(), () -> {
                callAction(newIndex);
            }).getTaskId();
        }
    }

    // TODO: clone

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AComponent that = (AComponent) o;
        return componentId == that.componentId && Objects.equals(area, that.area) && componentType == that.componentType && Objects.equals(componentLocation, that.componentLocation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(area, componentId, componentType, componentLocation);
    }
}
