package eu.lasersenigma.component;

import org.bukkit.entity.ArmorStand;

import java.util.Objects;

/**
 * Represents an armor stand associated with a specific {@link AArmorStandComponent}.
 * This class acts as a wrapper for a Bukkit {@link ArmorStand} and provides
 * functionality to associate it with a component and manage its properties.
 */
public final class ComponentArmorStand {

    // Related component
    private final AArmorStandComponent component;

    // Armor stand's name
    private final String name;

    // Related armor stand entity
    private ArmorStand armorStand = null;

    /**
     * Constructor for ComponentArmorStand
     *
     * @param component The AArmorStandComponent associated with this armor stand
     * @param name      The name of this armor stand
     */
    public ComponentArmorStand(AArmorStandComponent component, String name) {
        this.component = component;
        this.name = name;

        component.addComponentArmorStand(this);
    }

    /**
     * Get the AArmorStandComponent associated with this armor stand
     *
     * @return The AArmorStandComponent
     */
    public AArmorStandComponent getComponent() {
        return component;
    }

    /**
     * Get the name of this armor stand
     *
     * @return The name of the armor stand
     */
    public String getName() {
        return name;
    }

    /**
     * Get the Bukkit ArmorStand associated with this ComponentArmorStand
     *
     * @return The associated ArmorStand
     */
    public ArmorStand getArmorStand() {
        return armorStand;
    }

    /**
     * Set the Bukkit ArmorStand associated with this ComponentArmorStand
     *
     * @param armorStand The ArmorStand to set
     */
    public void setArmorStand(ArmorStand armorStand) {
        this.armorStand = armorStand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponentArmorStand that = (ComponentArmorStand) o;
        return Objects.equals(component, that.component) && Objects.equals(name, that.name) && Objects.equals(armorStand, that.armorStand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component, name, armorStand);
    }
}
