package eu.lasersenigma.component;

/**
 * Used for components that react to the number of activated laser receivers or players inside the area
 */
public interface IDetectionComponent extends IComponent {

    /**
     * Gets the maximum number of activated laser receivers or players inside the area needed to activate this component
     *
     * @return The maximum number of activated laser receivers or players inside the area needed to activate this component
     */
    int getMax();

    /**
     * Sets the maximum number of activated laser receivers or players inside the area needed to activate this component
     *
     * @param max The maximum number of activated laser receivers or players inside the area needed to activate this component
     */
    void setMax(int max);

    /**
     * Gets the minimum number of activated laser receivers or players inside the area needed to activate this component
     *
     * @return The minimum number of activated laser receivers or players inside the area needed to activate this component
     */
    int getMin();

    /**
     * Sets the minimum number of activated laser receivers or players inside the area needed to activate this component
     *
     * @param min The minimum number of activated laser receivers or players inside the area needed to activate this component
     */
    void setMin(int min);

    /**
     * Checks if the component is currently activated or not
     *
     * @return True if the component is activated, false otherwise
     */
    boolean isActivated();

    /**
     * Sets the actual number of activated laser receivers or players inside the area
     *
     * @param nbActivated The actual number of activated laser receivers or players inside the area
     */
    void setNbActivated(int nbActivated);

    /**
     * Gets the detection mode
     *
     * @return The actual detection mode
     */
    DetectionMode getDetectionMode();

    /**
     * Changes the detection mode
     *
     * @param mode The new detection mode
     */
    void changeMode(DetectionMode mode);
}
