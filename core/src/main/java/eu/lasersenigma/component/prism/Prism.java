package eu.lasersenigma.component.prism;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.*;

/**
 * Prism component
 */
public final class Prism extends AArmorStandComponent {

    private final HashMap<Direction, HashMap<Integer, ArrayList<Direction>>> cache = new HashMap<>();

    // Component's armor stands
    private final ComponentArmorStand PRISM_AS = new ComponentArmorStand(this, "prism");

    // Component's options
    // Empty

    // Component's events listener
    // Empty

    /**
     * Constructor used for creation from the database
     *
     * @param area        The area containing this laser receiver
     * @param componentId The id of the component inside the database
     * @param location    The location of the laser receiver
     * @param face        The blockface of the component
     * @param rotation    The rotation of the component
     */
    public Prism(Area area, int componentId, Location location, ComponentFace face, Rotation rotation) {
        super(area, componentId, location, ComponentType.PRISM, face, rotation);
    }

    /**
     * Constructor
     *
     * @param area     The area containing this
     * @param location The location of the component
     * @param face     The blockface of the component
     */
    public Prism(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.PRISM, face);
        dbCreate();
    }

    @Override
    public void activateComponent() {
    }

    @Override
    public void deactivateComponent() {
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(PRISM_AS, baseLocation, rotation, Item.PRISM, false, false, null);
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }
        ArrayList<LasersColor> newColors = laserParticle.getColor().decompose();
        ArrayList<Direction> newDirections = getNewDirection(laserParticle.getDirection(), newColors.size());
        Set<LaserParticle> newParticles = new HashSet<>();
        for (int i = 0; i < newColors.size(); ++i) {
            newParticles.add(new LaserParticle(
                    this,
                    laserParticle.getLocation().clone().add(newDirections.get(i)),
                    newDirections.get(i),
                    newColors.get(i),
                    getArea(),
                    laserParticle.getLightLevel()
            ));
        }
        return new LaserReceptionResult(true, newParticles);
    }

    public ArrayList<Direction> getNewDirection(Direction direction, int size) {
        HashMap<Integer, ArrayList<Direction>> cacheBySize = cache.get(direction);
        ArrayList<Direction> cachedResult;
        if (cacheBySize != null) {
            cachedResult = cacheBySize.get(size);
            if (cachedResult != null) {
                return cachedResult;
            }
        }
        LinkedList<Direction> res = new LinkedList<>();
        if (size > 0) {
            boolean even = size % 2 == 0;
            Vector diffVector = direction
                    .clone()
                    .crossProduct(getRotationCurrent().toHeadDirection())
                    .normalize().multiply(0.1);
            Vector minusDiffVector = diffVector.clone().multiply(-1);
            Vector lastDiffVector, minusLastDiffVector;
            int nbRemaining = size;
            if (even) {
                Vector halfDiffVector = diffVector.clone().multiply(0.5);
                lastDiffVector = halfDiffVector;
                minusLastDiffVector = halfDiffVector.clone().multiply(-1);
                res.add(new Direction(direction.clone().add(minusLastDiffVector)));
                res.add(new Direction(direction.clone().add(lastDiffVector)));
                nbRemaining -= 2;
            } else {
                lastDiffVector = new Vector(0, 0, 0);
                minusLastDiffVector = lastDiffVector;
                res.add(direction.clone());
                nbRemaining--;
            }
            while (nbRemaining > 0) {
                lastDiffVector = lastDiffVector.add(diffVector);
                minusLastDiffVector = minusLastDiffVector.add(minusDiffVector);
                res.add(0, new Direction(direction.clone().add(minusLastDiffVector)));
                res.add(res.size(), new Direction(direction.clone().add(lastDiffVector)));
                nbRemaining -= 2;
            }
        }
        cachedResult = new ArrayList<>(res);
        if (cacheBySize == null) {
            cacheBySize = new HashMap<>();
            cacheBySize.put(size, cachedResult);
        }
        cache.put(direction.clone(), cacheBySize);
        return cachedResult;
    }
}
