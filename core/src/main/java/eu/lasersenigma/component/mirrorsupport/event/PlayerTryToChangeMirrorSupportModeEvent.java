package eu.lasersenigma.component.mirrorsupport.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeMirrorSupportModeEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final MirrorSupport component;

    public PlayerTryToChangeMirrorSupportModeEvent(Player player, MirrorSupport component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public MirrorSupport getMirrorSupport() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
