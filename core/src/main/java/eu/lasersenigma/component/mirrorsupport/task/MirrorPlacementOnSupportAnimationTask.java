package eu.lasersenigma.component.mirrorsupport.task;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.component.AArmorStandComponent;
import eu.lasersenigma.component.ArmorStandItemOffset;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * Task executed when a player places a mirror on a reflecting sphere.
 */
public class MirrorPlacementOnSupportAnimationTask extends BukkitRunnable {

    // The mirror support component
    private final MirrorSupport mirrorSupport;

    // Should this action be persistent?
    private final boolean save;

    // The number of times this task will currently be executed
    private int nbTimesRemaining;

    // Vectors for animation
    private final Vector clipVector;
    private final Vector enterVector;

    // Base locations for armor stands
    private Location mirrorASBaseLocation;
    private Location mirrorSupportASBaseLocation;

    /**
     * Constructor.
     *
     * @param mirrorSupport Mirror support component
     * @param save          Should this action be persistent?
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public MirrorPlacementOnSupportAnimationTask(MirrorSupport mirrorSupport, boolean save) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().fine("PlaceMirrorOnSupportTask.constructor");

        this.mirrorSupport = mirrorSupport;
        this.save = save;
        nbTimesRemaining = 30;

        // Calculate vectors for animation
        clipVector = mirrorSupport.getAnimationClipVector().multiply(-1d / 5);
        enterVector = mirrorSupport.getAnimationVector().multiply(-1d / 25);

        // Calculate base locations for armor stands
        mirrorASBaseLocation = AArmorStandComponent
                .getDefaultArmorStandBaseLocation(mirrorSupport.getComponentLocation())
                .add(mirrorSupport.getAnimationClipVector())
                .add(mirrorSupport.getAnimationVector());
        mirrorSupportASBaseLocation = AArmorStandComponent
                .getDefaultArmorStandBaseLocation(mirrorSupport.getComponentLocation(), mirrorSupport.getComponentFace());

        // Play sound when mirror is inserted into support
        SoundLauncher.playSound(mirrorSupport.getComponentLocation(), PlaySoundCause.MIRROR_INSERT_SUPPORT);

        // Run the task periodically
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 1, 0);
    }

    /**
     * The method called on each execution of the task.
     */
    @Override
    public void run() {
        // Check if component has not been removed
        if (mirrorSupport.isRemoved()) {
            this.cancel();
            return;
        }

        // Check if the animation is complete
        if (nbTimesRemaining <= 0) {
            mirrorSupport.setOnGoingAnimation(false);
            mirrorSupport.setHasMirror(true, save);
            this.cancel();
            return;
        }

        // Perform animation based on the remaining execution times
        if (nbTimesRemaining > 25) {
            mirrorASBaseLocation = mirrorASBaseLocation.add(clipVector);
            Location newLocation = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotationCurrent());
            mirrorSupport.getArmorStandMirror().teleport(newLocation);
        } else {
            mirrorASBaseLocation = mirrorASBaseLocation.add(enterVector);
            Location newLocationMirror = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotationCurrent());
            mirrorSupport.getArmorStandMirror().teleport(newLocationMirror);
            mirrorSupportASBaseLocation = mirrorSupportASBaseLocation.add(enterVector);
            Location newLocationMirrorSupport = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorSupportASBaseLocation,
                    ArmorStandItemOffset.HEAD,
                    mirrorSupport.getComponentFace().getDefaultRotation(mirrorSupport.getComponentType()));
            mirrorSupport.getArmorStandMirrorContainer().teleport(newLocationMirrorSupport);
        }

        nbTimesRemaining--;
    }

}
