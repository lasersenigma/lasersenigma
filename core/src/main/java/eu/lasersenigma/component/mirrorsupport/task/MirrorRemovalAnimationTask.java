package eu.lasersenigma.component.mirrorsupport.task;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.component.AArmorStandComponent;
import eu.lasersenigma.component.ArmorStandItemOffset;
import eu.lasersenigma.component.IMirrorContainer;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * The task runned when a players retrieves a mirror from a mirror container
 */
public class MirrorRemovalAnimationTask extends BukkitRunnable {

    /**
     * the mirror container
     */
    private final IMirrorContainer mirrorContainer;

    private final Vector unclipVector;

    private final Vector exitSphere;

    private final Item headItemAfterAnimation;

    /**
     * the number of times this task will currently be executed
     */
    private int nbTimesRemaining;

    private Location mirrorASBaseLocation;

    private boolean save;

    /**
     * Constructor
     *
     * @param mirrorContainer        the IMirrorContainer
     * @param headItemAfterAnimation the item used for the ArmorStand head
     * @param save should the lack of mirror be saved in database
     */
    public MirrorRemovalAnimationTask(IMirrorContainer mirrorContainer, Item headItemAfterAnimation, boolean save) {
        this.mirrorContainer = mirrorContainer;
        this.headItemAfterAnimation = headItemAfterAnimation;
        this.nbTimesRemaining = 30;
        this.save = save;
        Location mirrorContainerLoc = mirrorContainer.getComponentLocation();
        SoundLauncher.playSound(mirrorContainerLoc, PlaySoundCause.MIRROR_EXTRACT_SPHERE);
        unclipVector = mirrorContainer.getAnimationClipVector().multiply(1d / 5);
        exitSphere = mirrorContainer.getAnimationVector().multiply(1d / 25);
        mirrorASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorContainerLoc, mirrorContainer.getComponentFace());
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 1, 0);
    }

    /**
     * the method called on each execution of the task
     */
    @Override
    public void run() {
        if (mirrorContainer.isRemoved()) {
            this.cancel();
            return;
        }
        if (nbTimesRemaining <= 0) {
            mirrorContainer.setOnGoingAnimation(false);
            mirrorContainer.setHasMirror(false, save);
            this.cancel();
            return;
        }
        if (nbTimesRemaining > 5) {
            mirrorASBaseLocation = mirrorASBaseLocation.add(exitSphere);
            Location newLocation = mirrorContainer.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorContainer.getComponentFace().getDefaultRotation(mirrorContainer.getComponentType()));
            mirrorContainer.getArmorStandMirror().teleport(newLocation);
        } else {
            if (nbTimesRemaining == 5) {
                mirrorContainer.getArmorStandMirrorContainer().setHelmet(ItemsFactory.getInstance().getItemStack(headItemAfterAnimation));
            }
            mirrorASBaseLocation = mirrorASBaseLocation.add(unclipVector);
            Location newLocation = mirrorContainer.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorContainer.getComponentFace().getDefaultRotation(mirrorContainer.getComponentType()));
            mirrorContainer.getArmorStandMirror().teleport(newLocation);
        }
        nbTimesRemaining--;
    }

}
