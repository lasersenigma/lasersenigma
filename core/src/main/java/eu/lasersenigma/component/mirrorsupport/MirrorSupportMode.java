package eu.lasersenigma.component.mirrorsupport;

import eu.lasersenigma.common.items.Item;

/**
 * Enumeration representing mirror support modes.
 */
public enum MirrorSupportMode {

    FREE("messages.mirror_support_mode_free", Item.MIRROR_SUPPORT, false),
    BLOCKED("messages.mirror_support_mode_blocked", Item.MIRROR_SUPPORT_BLOCKED, true);

    private final String messageCode;
    private final boolean mirrorBlocked;
    private final Item correspondingSupportSkin;

    MirrorSupportMode(String messageCode, Item correspondingSupportSkin, boolean mirrorBlocked) {
        this.messageCode = messageCode;
        this.correspondingSupportSkin = correspondingSupportSkin;
        this.mirrorBlocked = mirrorBlocked;
    }

    /**
     * Gets the corresponding message for the mirror support mode
     *
     * @return The corresponding message
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Gets the next mirror support mode
     *
     * @return The next mirror support mode
     */
    public MirrorSupportMode next() {
        int nextModeIndex = this.ordinal() + 1;
        return MirrorSupportMode.values()[nextModeIndex % MirrorSupportMode.values().length];
    }

    /**
     * Checks if the mirror is rotatable in this support mode
     *
     * @return True if the mirror is rotatable, false otherwise
     */
    public boolean isMirrorRotatable() {
        return !mirrorBlocked;
    }

    /**
     * Checks if the mirror is retrievable in this support mode
     *
     * @return True if the mirror is retrievable, false otherwise
     */
    public boolean isMirrorRetrievable() {
        return !mirrorBlocked;
    }

    /**
     * Gets the corresponding support skin item
     *
     * @return The corresponding support skin item
     */
    public Item getCorrespondingSupportSkin() {
        return correspondingSupportSkin;
    }

    /**
     * Retrieves the MirrorSupportMode from a string
     *
     * @param modeStr The string representation of the mode
     * @return The MirrorSupportMode, or FREE if the input is invalid
     */
    public static MirrorSupportMode from(String modeStr) {
        try {
            return MirrorSupportMode.valueOf(modeStr);
        } catch (IllegalArgumentException iae) {
            return FREE;
        }
    }
}
