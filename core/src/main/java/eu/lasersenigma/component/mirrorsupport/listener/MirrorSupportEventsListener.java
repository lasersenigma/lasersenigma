package eu.lasersenigma.component.mirrorsupport.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

public class MirrorSupportEventsListener implements Listener {

    public MirrorSupportEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(ignoreCancelled = true)
    public void onComponentRotation(ComponentRotationEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onComponentRotation");

        if (!event.getActionCause().equals(ActionCause.PLAYER)) return;
        if (!(event.getComponent() instanceof MirrorSupport)) return;

        SoundLauncher.playSound(event.getComponent().getComponentLocation(), PlaySoundCause.MIRROR_SUPPORT_ROTATE);
    }
}
