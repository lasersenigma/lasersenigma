package eu.lasersenigma.component.mirrorsupport;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.common.util.MirrorReflectionUtils;
import eu.lasersenigma.component.event.ComponentRotationEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.mirrorsupport.task.MirrorPlacementOnSupportAnimationTask;
import eu.lasersenigma.component.mirrorsupport.task.MirrorRemovalFromSupportAnimationTask;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.particles.ReflectionData;
import org.apache.commons.lang.NotImplementedException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Mirror support component
 */
public final class MirrorSupport extends AArmorStandComponent implements IColorableComponent, IMirrorContainer, IRotatableComponent, IPlayerModifiableComponent {

    public static final double MIRROR_ANIMATION_LOCATION_DIFF = 0.275d;
    public static final double MIRROR_ANIMATION_CLIP_LOCATION_DIFF = 0.6d;

    private final LasersColor DEFAULT_COLOR = LasersColor.WHITE;

    // Cache for lasers reflexion calculation
    private final HashMap<ReflectionData, ReflectionData> reflectionCache = new HashMap<>();

    // Saved has mirror state used for activation purpose
    private boolean hasMirrorCurrent = true;

    // The color of this mirror support
    private LasersColor color = null;

    // Saved color used for activation purpose
    private LasersColor colorCurrent = DEFAULT_COLOR;

    private MirrorSupportMode mirrorSupportMode = MirrorSupportMode.FREE;

    // Is this mirror support currently showing an animation
    private boolean onGoingAnimation = false;

    private MirrorPlacementOnSupportAnimationTask mirrorPlacementAnimationTask = null;

    private MirrorRemovalFromSupportAnimationTask mirrorRemovalAnimationTask = null;

    // Component's armor stands
    private final ComponentArmorStand MIRROR_SUPPORT_AS = new ComponentArmorStand(this, "mirror_support");
    private final ComponentArmorStand MIRROR_SUPPORT_MIRROR_AS = new ComponentArmorStand(this, "mirror_support_mirror");

    // Component's options
    // Empty

    // Component's events listener
    // private final Listener eventsListener = new MirrorSupportEventsListener(this);

    /**
     * Constructor used for creation from the database
     *
     * @param area        The area containing this mirror support
     * @param componentId The id of the component inside the database
     * @param location    The location of this mirror support
     * @param face        The face the component is on
     * @param color       The color of this component on reset
     * @param rotation    The rotation of this component on reset
     */
    public MirrorSupport(Area area, int componentId, Location location, ComponentFace face, LasersColor color, Rotation rotation, MirrorSupportMode mirrorSupportMode) {
        super(area, componentId, location, ComponentType.MIRROR_SUPPORT, face, rotation);
        hasMirrorCurrent = color != null;
        this.color = color;
        colorCurrent = this.color;
        this.mirrorSupportMode = mirrorSupportMode;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this mirror support
     * @param location The location of this mirror support
     * @param face     The face the component is on
     */
    public MirrorSupport(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.MIRROR_SUPPORT, face);
        showOrUpdateComponent();
        dbCreate();
    }

    public HashMap<ReflectionData, ReflectionData> getReflectionCache() {
        return reflectionCache;
    }

    @Override
    public boolean hasMirror() {
        return color != null;
    }

    @Override
    public boolean hasMirrorCurrent() {
        return hasMirrorCurrent;
    }

    @Override
    public void setHasMirror(boolean hasMirror, boolean save) {
        // Check if there is no ongoing animation
        if (onGoingAnimation) return;

        hasMirrorCurrent = hasMirror;
        showOrUpdateComponent();

        // Perform persistent save
        if (save) {
            color = hasMirrorCurrent ? colorCurrent : null;
            dbUpdate();
        }
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean placeMirror(LasersColor color, boolean save) {
        // Check if there is no ongoing animation
        if (onGoingAnimation) return false;

        // Check if there is no mirror
        if (hasMirrorCurrent) return false;

        // Check if component has not been removed
        if (isRemoved()) return false;

        onGoingAnimation = true;
        colorCurrent = color;
        reflectionCache.clear();

        Location mirrorLocation = getDefaultArmorStandBaseLocation(componentLocation);
        mirrorLocation = mirrorLocation.add(getAnimationClipVector()).add(getAnimationVector());
        mirrorLocation = getArmorStandLocationWithOffsets(mirrorLocation, ArmorStandItemOffset.GLASS_PANE, rotationCurrent);
        newCreateArmorStand(MIRROR_SUPPORT_MIRROR_AS, mirrorLocation, rotationCurrent, Item.getMirror(colorCurrent));
        mirrorPlacementAnimationTask = new MirrorPlacementOnSupportAnimationTask(this, save);

        return true;
    }

    @Override
    public boolean removeMirror(boolean save) {
        // Check if there is no ongoing animation
        if (onGoingAnimation) return false;

        // Check if there is a mirror
        if (!hasMirrorCurrent) return false;

        // Check if component has not been removed
        if (isRemoved()) return false;

        onGoingAnimation = true;
        mirrorRemovalAnimationTask = new MirrorRemovalFromSupportAnimationTask(this, save);

        return true;
    }

    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getColorCurrent() {
        return colorCurrent;
    }

    @Override
    public void changeColor() {
        changeColor(false);
    }

    @Override
    public void changeColor(boolean save) {
        setColor(colorCurrent.getNextColor(true), save);
    }

    @Override
    public void setColor(LasersColor colorCurrent) {
        setColor(colorCurrent, false);
    }

    @Override
    public void setColor(LasersColor color, boolean save) {
        // Check if there is no ongoing animation
        if (onGoingAnimation) return;

        // Check if there is a mirror
        if (!hasMirrorCurrent) return;

        colorCurrent = color;
        showOrUpdateComponent();

        // Perform persistent save
        if (save) {
            this.color = colorCurrent;
            dbUpdate();
        }

    }

    @Override
    public void rotate(RotationType rotationType, boolean save, ActionCause actionCause) {
        // Check if there is no ongoing animation
        if (onGoingAnimation) return;

        // Check if there is mirror
        if (!hasMirrorCurrent) return;

        reflectionCache.clear();

        Rotation nextRotation = rotationCurrent.getNextRotation(rotationType);
        rotationCurrent = nextRotation;

        showOrUpdateComponent();

        ComponentRotationEvent componentRotationEvent = new ComponentRotationEvent(this, actionCause);
        Bukkit.getServer().getPluginManager().callEvent(componentRotationEvent);

        // Perform persistent save
        if (save) {
            rotation = nextRotation;
            dbUpdate();
        }
    }

    /**
     * Retrieves mirror support mode actually used
     */
    public MirrorSupportMode getMirrorSupportMode() {
        return mirrorSupportMode;
    }

    /**
     * Changes the mode of this MirrorSupport
     *
     * @param mode The new mode
     */
    public void changeMode(MirrorSupportMode mode) {
        this.mirrorSupportMode = mode;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public boolean isOnGoingAnimation() {
        return onGoingAnimation;
    }

    @Override
    public void setOnGoingAnimation(boolean onGoingAnimation) {
        this.onGoingAnimation = onGoingAnimation;
    }

    @Override
    public boolean canBeDeleted() {
        return !onGoingAnimation;
    }

    @Override
    public Vector getAnimationClipVector() {
        return componentFace.getVector().multiply(MIRROR_ANIMATION_CLIP_LOCATION_DIFF);
    }

    @Override
    public Vector getAnimationVector() {
        return componentFace.getVector().multiply(MIRROR_ANIMATION_LOCATION_DIFF);
    }

    @Override
    public ArmorStand getArmorStandMirror() {
        return MIRROR_SUPPORT_MIRROR_AS.getArmorStand();
    }

    @Override
    public ArmorStand getArmorStandMirrorContainer() {
        return MIRROR_SUPPORT_AS.getArmorStand();
    }

    @Override
    public void activateComponent() {
        // Reset component's animation tasks
        mirrorPlacementAnimationTask = null;
        mirrorRemovalAnimationTask = null;
        onGoingAnimation = false;

        // Restore cached states
        hasMirrorCurrent = color != null;
        colorCurrent = color != null ? color : DEFAULT_COLOR;
        rotationCurrent = rotation != null ? rotation : componentFace.getDefaultRotation(componentType);

        reflectionCache.clear();
    }

    @Override
    public void deactivateComponent() {
        // Cancel component's animation tasks
        if (mirrorPlacementAnimationTask != null) mirrorPlacementAnimationTask.cancel();
        if (mirrorRemovalAnimationTask != null) mirrorRemovalAnimationTask.cancel();
        onGoingAnimation = false;

        // Reset default state
        hasMirrorCurrent = color != null;
        colorCurrent = color != null ? color : DEFAULT_COLOR;
        rotationCurrent = rotation != null ? rotation : componentFace.getDefaultRotation(componentType);

        reflectionCache.clear();
    }

    @Override
    public void showOrUpdateComponent() {
        // Mirror support armor stand
        Location baseLocation = getDefaultArmorStandBaseLocation(componentLocation, componentFace)
                .add(hasMirrorCurrent ? getAnimationVector().multiply(-1d) : new Vector());

        Rotation supportRotation = componentFace.getDefaultRotation(ComponentType.MIRROR_SUPPORT);
        baseLocation = getArmorStandLocationWithOffsets(baseLocation, ArmorStandItemOffset.HEAD, supportRotation);
        updateComponentArmorStand(MIRROR_SUPPORT_AS, baseLocation, supportRotation, mirrorSupportMode.getCorrespondingSupportSkin(), false, false, null);

        // Mirror support mirror armor stand
        baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation), ArmorStandItemOffset.GLASS_PANE, rotationCurrent);
        updateComponentArmorStand(MIRROR_SUPPORT_MIRROR_AS, baseLocation, rotationCurrent, Item.getMirror(colorCurrent), true, hasMirrorCurrent, null);
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();

        // Cancel component's animation tasks
        if (mirrorPlacementAnimationTask != null) mirrorPlacementAnimationTask.cancel();
        if (mirrorRemovalAnimationTask != null) mirrorRemovalAnimationTask.cancel();
        onGoingAnimation = false;
        // hasMirrorCurrent = false;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }

        Set<LaserParticle> newParticles = new HashSet<>();

        // It has no mirror inside, it is an opaque component
        if (hasMirrorCurrent && !onGoingAnimation) {
            ReflectionData inputRD = new ReflectionData(laserParticle.getDirection(), laserParticle.getLocation());

            //Compute reflected direction (using a reflection cache to avoid recomputing the same reflection two times)
            ReflectionData outputRD = getReflectionCache().get(inputRD);
            if (outputRD == null) {
                outputRD = MirrorReflectionUtils.reflect(this, inputRD);
                getReflectionCache().put(inputRD, outputRD);
            }

            //Compute reflected color
            HashMap<LasersColor.FilterResult, LasersColor> filterResult;
            LasersColor reflectedColor = null;
            LasersColor passThroughColor = null;
            switch (outputRD.getReflexionResult()) {
                case NO_INTERSECTION:
                    newParticles.add(new LaserParticle(this, laserParticle.getLocation(), laserParticle.getDirection(), laserParticle.getColor(), area, laserParticle.getLightLevel()));
                    break;
                case ORTHOGONAL:
                    filterResult = laserParticle.getColor().filterBy(colorCurrent);
                    passThroughColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                    if (colorCurrent == LasersColor.WHITE) {
                        passThroughColor = filterResult.get(LasersColor.FilterResult.PASS_THROUGH);
                    }
                    break;
                case REFLECTED:
                    filterResult = laserParticle.getColor().filterBy(colorCurrent);
                    passThroughColor = filterResult.get(LasersColor.FilterResult.PASS_THROUGH);
                    reflectedColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                    if (colorCurrent == LasersColor.WHITE) {
                        reflectedColor = passThroughColor;
                        passThroughColor = null;
                    }
                    break;
                default:
                    throw new NotImplementedException("Unknown reflexion type");
            }
            if (passThroughColor != null) {
                newParticles.add(new LaserParticle(this, outputRD.getLocation(), laserParticle.getDirection(), passThroughColor, area, laserParticle.getLightLevel()));
            }
            if (reflectedColor != null) {
                newParticles.add(new LaserParticle(this, outputRD.getLocation(), outputRD.getDirection(), reflectedColor, area, laserParticle.getLightLevel()));
            }
        }
        return new LaserReceptionResult(true, newParticles);
    }

}
