package eu.lasersenigma.component.mirrorsupport.event;

import eu.lasersenigma.common.event.ABeforeActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IMirrorContainer;
import eu.lasersenigma.component.event.IPlayerEvent;
import org.bukkit.entity.Player;

public class PlayerTryToPlaceMirrorEvent extends ABeforeActionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final IMirrorContainer component;

    private boolean bypassPermissions;

    public PlayerTryToPlaceMirrorEvent(Player player, IMirrorContainer component) {
        super();
        this.player = player;
        this.component = component;
        this.bypassPermissions = false;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IMirrorContainer getMirrorContainer() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public boolean getBypassPermissions() {
        return bypassPermissions;
    }

    public void setBypassPermissions(boolean bypassPermissions) {
        this.bypassPermissions = bypassPermissions;
    }

}
