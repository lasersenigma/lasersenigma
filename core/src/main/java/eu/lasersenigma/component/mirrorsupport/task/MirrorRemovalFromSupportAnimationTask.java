package eu.lasersenigma.component.mirrorsupport.task;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.component.AArmorStandComponent;
import eu.lasersenigma.component.ArmorStandItemOffset;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * Task executed when a player retrieves a mirror from a reflecting sphere.
 */
public class MirrorRemovalFromSupportAnimationTask extends BukkitRunnable {

    // The reflecting sphere
    private final MirrorSupport mirrorSupport;

    // Should this action be persistent?
    private final boolean save;

    // The number of times this task will currently be executed
    private int nbTimesRemaining;

    // Vectors for animation
    private final Vector unclipVector;
    private final Vector exitVector;

    // Base locations for armor stands
    private Location mirrorASBaseLocation;
    private Location mirrorSupportASBaseLocation;

    /**
     * Constructor.
     *
     * @param mirrorSupport The MirrorSupport.
     * @param save          Should this action be persistent?
     */
    public MirrorRemovalFromSupportAnimationTask(MirrorSupport mirrorSupport, boolean save) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().fine("GetMirrorFromSupportTask.constructor");

        this.mirrorSupport = mirrorSupport;
        this.nbTimesRemaining = 30;
        this.save = save;

        // Play sound when mirror is extracted from support
        Location mirrorSupportLoc = mirrorSupport.getComponentLocation();
        SoundLauncher.playSound(mirrorSupportLoc, PlaySoundCause.MIRROR_EXTRACT_SUPPORT);

        // Calculate vectors for animation
        unclipVector = mirrorSupport.getAnimationClipVector().multiply(1d / 5);
        exitVector = mirrorSupport.getAnimationVector().multiply(1d / 25);

        // Calculate base locations for armor stands
        mirrorASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorSupportLoc);
        mirrorSupportASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorSupportLoc, mirrorSupport.getComponentFace())
                .add(mirrorSupport.getAnimationVector().multiply(-1d));

        // Run the task periodically
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 1, 0);
    }

    /**
     * The method called on each execution of the task.
     */
    @Override
    public void run() {
        // Check if the component has not been removed
        if (mirrorSupport.isRemoved()) {
            this.cancel();
            return;
        }

        // Check if the animation is complete
        if (nbTimesRemaining <= 0) {
            mirrorSupport.setOnGoingAnimation(false);
            mirrorSupport.setHasMirror(false, save);
            this.cancel();
            return;
        }

        // Perform animation based on the remaining execution times
        if (nbTimesRemaining > 5) {
            mirrorASBaseLocation = mirrorASBaseLocation.add(exitVector);
            Location newLocationMirror = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotationCurrent());
            mirrorSupport.getArmorStandMirror().teleport(newLocationMirror);

            mirrorSupportASBaseLocation = mirrorSupportASBaseLocation.add(exitVector);
            Location newLocationMirrorSupport = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorSupportASBaseLocation,
                    ArmorStandItemOffset.HEAD,
                    mirrorSupport.getComponentFace().getDefaultRotation(mirrorSupport.getComponentType()));
            mirrorSupport.getArmorStandMirrorContainer().teleport(newLocationMirrorSupport);
        } else {
            mirrorASBaseLocation = mirrorASBaseLocation.add(unclipVector);
            Location newLocation = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotationCurrent());
            mirrorSupport.getArmorStandMirror().teleport(newLocation);
        }

        nbTimesRemaining--;
    }
}
