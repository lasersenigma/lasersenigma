package eu.lasersenigma.component;

/**
 * Represents an interface for components that can be colored
 */
public interface ILightComponent extends IComponent {

    /**
     * Get the light level of this component
     *
     * @return the light level
     */
    int getLightLevel();

    /**
     * Set the current light level of this component
     *
     * @param lightLevel the light level to be set
     */
    void setLightLevel(int lightLevel);

}
