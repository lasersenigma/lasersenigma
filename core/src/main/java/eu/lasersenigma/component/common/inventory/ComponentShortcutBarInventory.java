package eu.lasersenigma.component.common.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.area.exception.NotSameAreaException;
import eu.lasersenigma.common.inventory.AShortcutBarInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.inventory.MainShortcutBarInventory;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.elevator.Elevator;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.component.lock.inventory.KeyChestCreationShortcutBarInventory;
import eu.lasersenigma.component.lock.inventory.KeyChestSelectorMenuInventory;
import eu.lasersenigma.component.lock.inventory.KeyChestTeleportationMenuInventory;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.musicblock.MusicBlock;
import eu.lasersenigma.component.prism.Prism;
import eu.lasersenigma.component.redstonesensor.RedstoneSensor;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.CrossableMaterials;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

/**
 * Component edition shortcut bar
 */
public class ComponentShortcutBarInventory extends AShortcutBarInventory {

    private final IComponent component;
    private final ComponentType componentType;

    public ComponentShortcutBarInventory(LEPlayer player, IComponent component) {
        super(player);
        componentType = component.getComponentType();
        this.component = component;
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        switch (componentType) {
            case LASER_SENDER:
                shortcutBar.addAll(getRotations());
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case LASER_RECEIVER:
                shortcutBar.addAll(getRotations());
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ANY_COLOR_TOGGLE);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_LET_PASS_LASER_TOGGLE);
                break;
            case MIRROR_SUPPORT:
                IMirrorContainer mirrorSupport = (IMirrorContainer) component;
                if (mirrorSupport.hasMirrorCurrent()) {
                    shortcutBar.addAll(getRotations());
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR);
                } else {
                    shortcutBar = addEmpty(shortcutBar, 5);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_PLACE_MIRROR);
                }
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case CONCENTRATOR:
                shortcutBar.addAll(getRotations());
                break;
            case REFLECTING_SPHERE:
            case FILTERING_SPHERE:
                IMirrorContainer reflectingSphere = (IMirrorContainer) component;
                if (reflectingSphere.hasMirrorCurrent()) {
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR);
                } else {
                    shortcutBar.add(Item.EMPTY);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_PLACE_MIRROR);
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case BURNABLE_BLOCK:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                shortcutBar.add(Item.SELECT_MATERIAL_MENU);
                break;
            case MIRROR_CHEST:
                int nbMirrors = ((MirrorChest) component).getNbMirrors();
                shortcutBar.add(nbMirrors <= 1 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB);
                shortcutBar.add(nbMirrors >= 99 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                break;
            case REDSTONE_WINNER_BLOCK:
            case APPEARING_WINNER_BLOCK:
            case DISAPPEARING_WINNER_BLOCK:
                shortcutBar.addAll(getRanges(component));
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case MUSIC_BLOCK:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_NEXT_SONG);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_PREV_SONG);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_SONG_LOOP);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case LOCK:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ADD_KEYCHEST);
                shortcutBar.add(Item.EMPTY);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU);
                break;
            case KEY_CHEST:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER);
                shortcutBar.add(Item.EMPTY);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK);
                break;
            case ELEVATOR:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CREATE_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case REDSTONE_SENSOR:
            case PRISM:
            case CALL_BUTTON:
                break;
            default:
                throw new UnsupportedOperationException();
        }

        shortcutBar = addEmptyUntil(shortcutBar, 7);
        if (component instanceof Prism || component instanceof RedstoneSensor) {
            shortcutBar.add(Item.EMPTY);
        } else {
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MENU);
        }
        shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    private ArrayList<Item> getRotations() {
        return new ArrayList<>(Arrays.asList(
                Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_UP,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN
        ));
    }

    private ArrayList<Item> getRanges(IComponent component) {
        IDetectionComponent detectionComponent = (IDetectionComponent) component;
        ArrayList<Item> ranges = new ArrayList<>();
        int min = detectionComponent.getMin();
        int max = detectionComponent.getMax();
        ranges.add((min <= 0 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN));
        ranges.add((min >= 99 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN));
        ranges.add((max <= 0 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX));
        ranges.add((max >= 99 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX));
        return ranges;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEComponentShortcutBarInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        Area area = component.getArea();

        if (!area.containsLocation(player.getBukkitPlayer().getLocation())) {
            item = Item.COMPONENT_SHORTCUTBAR_CLOSE;
        }
        MirrorChest mc;
        switch (item) {
            case COMPONENT_SHORTCUTBAR_COLOR_LOOPER:
                ComponentController.changeColor(component.getArea(), (IColorableComponent) component, player);
                break;
            case COMPONENT_SHORTCUTBAR_ANY_COLOR_TOGGLE:
                LaserReceiver receiver = (LaserReceiver) component;
                ComponentController.setAnyColorAccepted(component.getArea(), receiver, player, !receiver.isAnyColorAccepted());
                break;
            case COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR:
                ComponentController.removeMirror((IMirrorContainer) component, player);
                break;
            case COMPONENT_SHORTCUTBAR_PLACE_MIRROR:
                ComponentController.placeMirror((IMirrorContainer) component, player, LasersColor.WHITE);
                break;
            case COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, (GravitationalSphere) component, true);
                break;
            case COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, (GravitationalSphere) component, false);
                break;
            case COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB:
                mc = (MirrorChest) component;
                ComponentController.increaseMirrorChest(mc, player);
                break;
            case COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB:
                ComponentController.decreaseMirrorChest((MirrorChest) component, player);
                break;
            case COMPONENT_SHORTCUTBAR_NEXT_SONG:
                ComponentController.nextSong(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_PREV_SONG:
                ComponentController.prevSong(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_SONG_LOOP:
                ComponentController.toogleLoop(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT:
                ComponentController.toogleStopOnExit(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_MOD_LOOPER:
                if (component instanceof IDetectionComponent) {
                    ComponentController.changeMode(player, (IDetectionComponent) component);
                } else if (component instanceof GravitationalSphere) {
                    ComponentController.changeMode(player, (GravitationalSphere) component);
                } else if (component instanceof MirrorSupport) {
                    ComponentController.changeMirrorSupportMode(player, (MirrorSupport) component);
                }
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX:
                ComponentController.increaseMax(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX:
                ComponentController.decreaseMax(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN:
                ComponentController.increaseMin(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN:
                ComponentController.decreaseMin(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_LEFT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.LEFT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_RIGHT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.RIGHT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_UP:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.UP);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_DOWN:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.DOWN);
                break;
            case COMPONENT_SHORTCUTBAR_MENU:
                player.getInventoryManager().openLEInventory(new ComponentMenuInventory(player, component));
                break;
            case COMPONENT_SHORTCUTBAR_CLOSE:
                player.getInventoryManager().openLEInventory(new MainShortcutBarInventory(player));
                break;
            case COMPONENT_SHORTCUTBAR_ADD_KEYCHEST:
                player.getInventoryManager().openLEInventory(new KeyChestCreationShortcutBarInventory(player, (Lock) component));
                break;
            case COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU:
                player.getInventoryManager().openLEInventory(new KeyChestTeleportationMenuInventory(player, (Lock) component));
                break;
            case COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER:
                player.getInventoryManager().openLEInventory(new KeyChestSelectorMenuInventory(player, (LockKeyChest) component));
                break;
            case COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK:
                player.teleportToNearestSafeLocation(((LockKeyChest) component).getLock().getComponentLocation());
                break;
            case COMPONENT_SHORTCUTBAR_CREATE_FLOOR:
                ComponentController.elevatorCreateFloor(player, (Elevator) component);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR:
                Elevator elevator1 = (Elevator) component;
                int index1 = elevator1.getGroundFloorIndex();
                ComponentController.goToFloor(player, elevator1, index1);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR:
                Elevator elevator2 = (Elevator) component;
                int index2 = elevator2.getPrevFloorIndex();
                ComponentController.goToFloor(player, elevator2, index2);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR:
                Elevator elevator3 = (Elevator) component;
                int index3 = elevator3.getNextFloorIndex();
                ComponentController.goToFloor(player, elevator3, index3);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR:
                Elevator elevator4 = (Elevator) component;
                int index4 = elevator4.getTopFloorIndex();
                ComponentController.goToFloor(player, elevator4, index4);
                break;
            case COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS:
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
                    return;
                }
                Elevator elevator5 = (Elevator) component;
                Area area2 = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area2 == null || !area.isActivated()) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                if (area2 != elevator5.getArea()) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NotSameAreaException());
                    return;
                }
                List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20);
                if (blocks.size() < 2) {
                    return;
                }
                ComponentController.addComponentFromBlockFace(ComponentType.CALL_BUTTON, blocks.get(1), Objects.requireNonNull(blocks.get(1).getFace(blocks.get(0))), player, elevator5);
                break;
            case SELECT_MATERIAL_MENU:
                player.getInventoryManager().openLEInventory(new SelectMaterialInventory(player, component));
                break;
            case COMPONENT_SHORTCUTBAR_LET_PASS_LASER_TOGGLE:
                LaserReceiver receiver1 = (LaserReceiver) component;
                ComponentController.toggleLetPassLaser(player, receiver1);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER,
                Item.COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR,
                Item.COMPONENT_SHORTCUTBAR_PLACE_MIRROR,
                Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB,
                Item.COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB,
                Item.COMPONENT_SHORTCUTBAR_NEXT_SONG,
                Item.COMPONENT_SHORTCUTBAR_PREV_SONG,
                Item.COMPONENT_SHORTCUTBAR_SONG_LOOP,
                Item.COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT,
                Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER,
                Item.COMPONENT_SHORTCUTBAR_MENU,
                Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN,
                Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN,
                Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX,
                Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_UP,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN,
                Item.COMPONENT_SHORTCUTBAR_CLOSE,
                Item.COMPONENT_SHORTCUTBAR_ADD_KEYCHEST,
                Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU,
                Item.COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER,
                Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK,
                Item.COMPONENT_SHORTCUTBAR_CREATE_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS,
                Item.COMPONENT_SHORTCUTBAR_ANY_COLOR_TOGGLE,
                Item.COMPONENT_SHORTCUTBAR_LET_PASS_LASER_TOGGLE,
                Item.SELECT_MATERIAL_MENU
        )).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return component.getArea();
    }

    @Override
    public InventoryType getType() {
        return InventoryType.COMPONENT_SHORTCUTBAR;
    }

}
