package eu.lasersenigma.component.common.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.common.inventory.AHorizontallyPaginableOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.burnableblock.BurnableBlock;
import eu.lasersenigma.player.LEPlayer;
import fr.skytale.itemlib.item.utils.ItemStackTrait;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public final class SelectMaterialInventory extends AHorizontallyPaginableOpenableInventory {

    private final Area area;

    private final IComponent component;

    public SelectMaterialInventory(LEPlayer player, IComponent component) {
        super(player, "messages.select_material_menu_title");
        this.area = component.getArea();
        this.component = component;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.SELECT_MATERIAL_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public boolean contains(ItemStack itemStack) {
        return getColumnsItemStacks().stream()
                .flatMap(List::stream)
                .anyMatch(itemStackInMenu -> ItemStackUtils.compare(itemStackInMenu, itemStack, ItemStackTrait.TYPE));
    }

    @Override
    public void onClick(ItemStack itemStack) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "SelectMaterialInventory.onClick(ItemStack)");

        Material material = itemStack.getType();

        if (component instanceof BurnableBlock burnableBlock) {
            ComponentController.setMaterial(player, burnableBlock, material);
            player.getInventoryManager().closeOpenedInventory();
        } else {
            throw new IllegalStateException("Component is not a BurnableBlock. SelectMaterialInventory can only be used for BurnableBlock.");
        }
    }

    @Override
    protected List<List<ItemStack>> getColumnsItemStacks() {
        List<List<ItemStack>> columns = new ArrayList<>();

        List<Material> materials = Arrays.stream(Material.values())
                .filter(material -> !material.isAir() && material.isBlock())
                .toList();

        List<ItemStack> currentColumn = new ArrayList<>();

        for (Material currentMaterial : materials) {
            ItemStack itemStack = new ItemStack(currentMaterial);
            if (currentColumn.size() >= 4) {
                currentColumn = new ArrayList<>();
                columns.add(currentColumn);
            }
            currentColumn.add(itemStack);
        }

        return columns;
    }
}
