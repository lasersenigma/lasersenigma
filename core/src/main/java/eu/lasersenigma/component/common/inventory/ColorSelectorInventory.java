package eu.lasersenigma.component.common.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class ColorSelectorInventory extends AOpenableInventory {

    private final boolean isEmptyMirrorIncluded;
    private final boolean isBlackIncluded;
    private final IComponent component;
    private final Area area;

    public ColorSelectorInventory(LEPlayer player, boolean isEmptyMirrorIncluded, boolean isBlackIncluded,
                                  IComponent component, Area area) {
        super(player, "messages.component_select_color");
        this.isEmptyMirrorIncluded = isEmptyMirrorIncluded;
        this.isBlackIncluded = isBlackIncluded;
        this.component = component;
        this.area = area;
    }

    public ColorSelectorInventory(LEPlayer player, boolean isBlackIncluded,
                                  IComponent component, Area area) {
        super(player, "messages.component_select_color");
        isEmptyMirrorIncluded = false;
        this.isBlackIncluded = isBlackIncluded;
        this.component = component;
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.EMPTY, Item.BLUE)));
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.MAGENTA, Item.WHITE, Item.LIGHT_BLUE)));
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.RED, Item.YELLOW, Item.GREEN)));
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY)));
        if (isEmptyMirrorIncluded) {
            inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.EMPTY, Item.EMPTY_MIRROR)));
        }
        if (isBlackIncluded) {
            inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.EMPTY, Item.BLACK)));
        }
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEColorSelectorInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        LasersColor color = item.getColor();
        player.getInventoryManager().onColorSelected(color);
    }

    @Override
    public boolean contains(Item item) {
        ArrayList<Item> content = new ArrayList<>(Arrays.asList(Item.WHITE,
                Item.RED,
                Item.GREEN,
                Item.BLUE,
                Item.YELLOW,
                Item.MAGENTA,
                Item.LIGHT_BLUE
        ));
        if (isEmptyMirrorIncluded) {
            content.add(Item.EMPTY_MIRROR);
        }
        if (isBlackIncluded) {
            content.add(Item.BLACK);
        }
        return content.contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.COLOR_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
