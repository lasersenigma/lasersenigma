package eu.lasersenigma.component.common.util;

import eu.lasersenigma.component.AArmorStandComponent;
import eu.lasersenigma.component.Direction;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.particles.ReflectionData;
import eu.lasersenigma.particles.ReflectionResultType;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Utility class for performing reflection calculations with MirrorSupport
 */
public class MirrorReflectionUtils {

    private static final int MAX_CACHE_SIZE = 1000;

    private static final double MIRROR_DEPTH = 0.42;

    private static final double MIRROR_SIZE = 0.23;


    // Create a cache to store previously calculated reflections
    private static final Map<String, ReflectionData> reflectionCache = new LinkedHashMap<>(MAX_CACHE_SIZE, 0.75f, true) {
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, ReflectionData> eldest) {
            return size() > MAX_CACHE_SIZE;
        }
    };

    /**
     * Performs reflection calculations for the given MirrorSupport and laser particle data
     *
     * @param mirrorSupport     The MirrorSupport instance
     * @param laserParticleData The data of the laser particle
     * @return The result of the reflection
     */
    public static ReflectionData reflect(MirrorSupport mirrorSupport, ReflectionData laserParticleData) {
        /*// Generate a unique key for the cache based on input data
        String cacheKey = generateCacheKey(mirrorSupport, laserParticleData);

        // Check if the result is already in the cache
        if (reflectionCache.containsKey(cacheKey)) {
            return reflectionCache.get(cacheKey);
        }

        // If not in the cache, perform the reflection calculation
        ReflectionData reflectionResult = performReflection(mirrorSupport, laserParticleData);

        // Store the result in the cache
        reflectionCache.put(cacheKey, reflectionResult);

        return reflectionResult;*/

        return performReflection(mirrorSupport, laserParticleData);
    }

    // Helper method to generate a unique key for the cache
    private static String generateCacheKey(MirrorSupport mirrorSupport, ReflectionData laserParticleData) {
        // You can modify this method based on the characteristics of your input data
        return mirrorSupport.toString() + "_" + laserParticleData.toString();
    }

    private static ReflectionData performReflection(AArmorStandComponent armorStandComponent, ReflectionData laserParticleData) {
        // Extract necessary vectors and location from MirrorSupport
        Vector mirrorPlaneNormal = armorStandComponent.getRotationCurrent().toEyeDirection();
        Vector mirrorHeadDirection = armorStandComponent.getRotationCurrent().toHeadDirection();
        Vector mirrorSideDirection = armorStandComponent.getRotationCurrent().toSideDirection();
        Location mirrorCenterLocation = getMirrorCenterLocation(armorStandComponent);

        // Calculate dot product of laser direction and mirror plane normal
        double dotProd = laserParticleData.getDirection().dot(mirrorPlaneNormal);

        // Check if there is no intersection
        if (dotProd == 0) {
            return handleNoIntersection(mirrorPlaneNormal, laserParticleData, mirrorCenterLocation);
        } else {
            // Handle intersection
            return handleIntersection(mirrorPlaneNormal, laserParticleData, mirrorCenterLocation, mirrorHeadDirection, mirrorSideDirection, dotProd);
        }
    }

    // Helper method to get the center location of the mirror
    private static Location getMirrorCenterLocation(AArmorStandComponent armorStandComponent) {
        return new Location(armorStandComponent.getComponentLocation().getWorld(),
                armorStandComponent.getComponentLocation().getBlockX() + 0.5,
                armorStandComponent.getComponentLocation().getBlockY() + 0.5,
                armorStandComponent.getComponentLocation().getBlockZ() + 0.5);
    }

    // Handle the case when there is no intersection with the mirror
    private static ReflectionData handleNoIntersection(Vector mirrorPlaneNormal, ReflectionData laserParticleData, Location mirrorCenterLocation) {
        double distanceBetweenLaserAndMirror = calculateDistanceOnAxis(mirrorPlaneNormal, laserParticleData.getLocation(), mirrorCenterLocation);
        if (distanceBetweenLaserAndMirror < MIRROR_DEPTH) {
            // Laser is touching the mirror orthogonally
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.ORTHOGONAL);
        } else {
            // No intersection with the mirror
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.NO_INTERSECTION);
        }
    }

    // Handle the case when there is an intersection with the mirror
    private static ReflectionData handleIntersection(Vector mirrorPlaneNormal, ReflectionData laserParticleData, Location mirrorCenterLocation, Vector mirrorHeadDirection, Vector mirrorSideDirection, double dotProd) {
        // Calculate intersection parameter and location
        double param = calculateIntersectionParameter(mirrorPlaneNormal, laserParticleData.getLocation(), mirrorCenterLocation, laserParticleData.getDirection());
        Location intersection = calculateIntersectionLocation(laserParticleData.getLocation(), laserParticleData.getDirection(), param);

        // Calculate distances on mirror head and side axes
        double distanceOnHeadAxis = calculateDistanceOnAxis(mirrorHeadDirection, intersection, mirrorCenterLocation);
        double distanceOnSideAxis = calculateDistanceOnAxis(mirrorSideDirection, intersection, mirrorCenterLocation);

        // Check if the intersection point is within the mirror size
        if (distanceOnHeadAxis < MIRROR_SIZE && distanceOnSideAxis < MIRROR_SIZE) {
            // Calculate the reflected direction
            Direction resultDirection = calculateReflectionDirection(dotProd, mirrorPlaneNormal, laserParticleData.getDirection());
            return new ReflectionData(resultDirection, intersection, ReflectionResultType.REFLECTED);
        } else {
            // No intersection with the mirror
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.NO_INTERSECTION);
        }
    }

    // Helper method to calculate the distance between two points along a specified axis
    private static double calculateDistanceOnAxis(Vector axis, Location point1, Location point2) {
        return Math.abs((point1.getX() - point2.getX()) * axis.getX())
                + Math.abs((point1.getY() - point2.getY()) * axis.getY())
                + Math.abs((point1.getZ() - point2.getZ()) * axis.getZ());
    }

    // Helper method to calculate the intersection parameter for the laser and mirror
    private static double calculateIntersectionParameter(Vector mirrorPlaneNormal, Location laserLocation, Location mirrorCenterLocation, Vector laserDirection) {
        return (-mirrorPlaneNormal.getX() * (laserLocation.getX() - mirrorCenterLocation.getX())
                - mirrorPlaneNormal.getY() * (laserLocation.getY() - mirrorCenterLocation.getY())
                - mirrorPlaneNormal.getZ() * (laserLocation.getZ() - mirrorCenterLocation.getZ())) / (mirrorPlaneNormal.getX() * laserDirection.getX()
                + mirrorPlaneNormal.getY() * laserDirection.getY()
                + mirrorPlaneNormal.getZ() * laserDirection.getZ());
    }

    // Helper method to calculate the intersection location based on the laser parameters
    private static Location calculateIntersectionLocation(Location laserLocation, Vector laserDirection, double param) {
        return new Location(
                laserLocation.getWorld(),
                laserLocation.getX() + laserDirection.getX() * param,
                laserLocation.getY() + laserDirection.getY() * param,
                laserLocation.getZ() + laserDirection.getZ() * param);
    }

    // Helper method to calculate the reflection direction based on the mirror and laser parameters
    private static Direction calculateReflectionDirection(double dotProd, Vector mirrorPlaneNormal, Vector laserDirection) {
        return new Direction(
                laserDirection.getX() - (2 * dotProd * mirrorPlaneNormal.getX()),
                laserDirection.getY() - (2 * dotProd * mirrorPlaneNormal.getY()),
                laserDirection.getZ() - (2 * dotProd * mirrorPlaneNormal.getZ()));
    }

}
