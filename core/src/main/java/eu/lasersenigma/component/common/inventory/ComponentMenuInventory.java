package eu.lasersenigma.component.common.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.conditionnalwinnerblock.AppearingWinnerBlock;
import eu.lasersenigma.component.conditionnalwinnerblock.DisappearingWinnerBlock;
import eu.lasersenigma.component.elevator.Elevator;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphereMode;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.lasersender.LaserSender;
import eu.lasersenigma.component.lasersender.inventory.LightLevelEditInventory;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.component.lock.event.DeleteLockKeyChestsConfirmationInventory;
import eu.lasersenigma.component.lock.event.DeleteLockPlayersKeysConfirmationInventory;
import eu.lasersenigma.component.lock.inventory.KeyChestCreationShortcutBarInventory;
import eu.lasersenigma.component.lock.inventory.KeyChestSelectorMenuInventory;
import eu.lasersenigma.component.lock.inventory.KeyChestTeleportationMenuInventory;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.mirrorsupport.MirrorSupportMode;
import eu.lasersenigma.component.musicblock.MusicBlock;
import eu.lasersenigma.component.scheduledactions.inventory.ScheduledActionsMainInventory;
import eu.lasersenigma.component.winnerblock.WinnerBlock;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.PlayerController;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

public class ComponentMenuInventory extends AOpenableInventory {

    private final IComponent component;
    private final ComponentType componentType;

    public ComponentMenuInventory(LEPlayer player, IComponent component) {
        super(player, "messages.component_menu_title");
        componentType = component.getComponentType();
        this.component = component;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> content = new ArrayList<>();
        switch (componentType) {
            case LASER_SENDER:
                LaserSender ls = (LaserSender) component;
                // rotations
                addToInnerLists(content, getRotations());
                content.get(0).add(0, Item.EMPTY);
                content.get(1).add(0, Item.EMPTY);
                content.get(2).add(0, Item.EMPTY);
                // color
                content.get(0).add(Item.COMPONENT_MENU_COLOR);
                content.get(0).add(Item.COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU);
                content.get(0).add(Item.COMPONENT_MENU_LIGHT_LEVEL_OPEN_MENU);
                // ranges
                ArrayList<ArrayList<Item>> rangesLS = getRanges(ls);
                if (rangesLS != null) {
                    for (int i = 0; i < 3; i++) {
                        rangesLS.add(0, null);
                    }
                    addToInnerLists(content, (rangesLS));
                }
                // modes
                addToInnerLists(content, getDetectionMods(ls));
                break;
            case LASER_RECEIVER:
                final LaserReceiver laserReceiver = (LaserReceiver) component;
                // rotations
                addToInnerLists(content, addBorders(getRotations()));
                // color
                content.get(0).add(Item.COMPONENT_MENU_COLOR);
                content.get(0).add(Item.EMPTY);
                content.get(0).add(Item.COMPONENT_MENU_LIGHT_LEVEL_OPEN_MENU);

                addEmptyUntil(content.get(2), 7);
                content.get(2).add(Item.COMPONENT_MENU_LET_PASS_LASER);
                content.get(2).add(laserReceiver.isLetPassLaserThrough() ? Item.COMPONENT_MENU_LET_PASS_LASER_CHECKED : Item.COMPONENT_MENU_LET_PASS_LASER_UNCHECKED);

                addEmptyUntil(content.get(3), 7);
                content.get(3).add(Item.COMPONENT_MENU_ANY_COLOR);
                content.get(3).add(laserReceiver.isAnyColorAccepted() ? Item.COMPONENT_MENU_ANY_COLOR_CHECKED : Item.COMPONENT_MENU_ANY_COLOR_UNCHECKED);
                break;
            case MIRROR_SUPPORT:
                MirrorSupport mirrorSupport = (MirrorSupport) component;
                if (mirrorSupport.hasMirrorCurrent()) {
                    // retrieve mirror
                    content.add(Collections.singletonList(Item.COMPONENT_MENU_RETRIEVE_MIRROR));
                    content.add(Collections.singletonList(Item.EMPTY));
                    content.add(Collections.singletonList(Item.COMPONENT_MENU_COLOR));
                    // rotations
                    addToInnerLists(content, getRotations());
                } else {
                    content.add(Collections.singletonList(Item.COMPONENT_MENU_PLACE_MIRROR));
                    content.add(new ArrayList<>());
                    content.add(new ArrayList<>());
                }

                content.set(0, addEmptyUntil(content.get(0), 6));

                content.get(0).add(Item.COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU);
                content.get(0).add(Item.COMPONENT_MENU_MIRROR_FREE);
                content.get(0).add(mirrorSupport.getMirrorSupportMode() == MirrorSupportMode.FREE ? Item.COMPONENT_MENU_MIRROR_FREE_CHECKED : Item.COMPONENT_MENU_MIRROR_FREE_UNCHECKED);

                content.set(1, addEmptyUntil(content.get(1), 7));
                content.get(1).add(Item.COMPONENT_MENU_MIRROR_BLOCKED);
                content.get(1).add(mirrorSupport.getMirrorSupportMode() == MirrorSupportMode.BLOCKED ? Item.COMPONENT_MENU_MIRROR_BLOCKED_CHECKED : Item.COMPONENT_MENU_MIRROR_BLOCKED_UNCHECKED);
                break;
            case MIRROR_CHEST:
                // color
                ArrayList<Item> firstLine = new ArrayList<>(Arrays.asList(
                        Item.COMPONENT_MENU_COLOR,
                        Item.EMPTY,
                        Item.EMPTY
                ));
                // increase and decrease nbMirrors
                int nbMirrors = ((MirrorChest) component).getNbMirrors();
                if (nbMirrors > 1) {
                    firstLine.add(Item.COMPONENT_MENU_DECREASE_MIRROR_CHEST);
                } else {
                    firstLine.add(Item.EMPTY);
                }
                firstLine.addAll(getNumberAsItems(nbMirrors, true));
                if (nbMirrors < 99) {
                    firstLine.add(Item.COMPONENT_MENU_INCREASE_MIRROR_CHEST);
                } else {
                    firstLine.add(Item.EMPTY);
                }
                content.add(firstLine);
                break;
            case PRISM:
            case REDSTONE_SENSOR:
                break;
            case CONCENTRATOR:
                // rotations
                addToInnerLists(content, addBorders(getRotations()));
                content.get(0).add(Item.COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU);
                break;
            case FILTERING_SPHERE:
            case REFLECTING_SPHERE:
                if (((IMirrorContainer) component).hasMirrorCurrent()) {
                    // retrieve mirror
                    content.add(new ArrayList<>(Collections.singletonList(Item.COMPONENT_MENU_RETRIEVE_MIRROR)));
                    // color
                    content.add(new ArrayList<>());
                    content.add(Collections.singletonList(Item.COMPONENT_MENU_COLOR));
                } else {
                    content.add(new ArrayList<>(Collections.singletonList(Item.COMPONENT_MENU_PLACE_MIRROR)));
                    content.add(new ArrayList<>());
                    content.add(new ArrayList<>());
                }
                content.set(0, addEmptyUntil(content.get(0), 6));
                content.get(0).add(Item.COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU);
                break;
            // place mirror
            case ATTRACTION_REPULSION_SPHERE:
                GravitationalSphere sphere = (GravitationalSphere) component;
                // Increase & Reduce size
                int size = sphere.getCurrentGravityStrength();
                ArrayList<Item> sizeLine = new ArrayList<>();
                if (size > 1) {
                    sizeLine.add(Item.COMPONENT_MENU_DECREASE_SPHERE_SIZE);
                } else {
                    sizeLine.add(Item.EMPTY);
                }
                sizeLine.add(getNumberAsItems(sphere.getCurrentGravityStrength(), false).get(0));
                if (size < 8) {
                    sizeLine.add(Item.COMPONENT_MENU_INCREASE_SPHERE_SIZE);
                } else {
                    sizeLine.add(Item.EMPTY);
                }
                sizeLine.add(Item.EMPTY);
                sizeLine.add(Item.COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU);
                content.add(sizeLine);
                // modes
                List<Item> modLabels = Arrays.asList(
                        Item.COMPONENT_MENU_MOD_ATTRACTION,
                        Item.EMPTY,
                        Item.COMPONENT_MENU_MOD_REPULSION
                );
                content.add(modLabels);

                ArrayList<Item> modCheckboxes = new ArrayList<>();
                if (sphere.getGravitationalSphereMode() == GravitationalSphereMode.ATTRACTION) {
                    modCheckboxes.addAll(Arrays.asList(
                            Item.COMPONENT_MENU_MOD_ATTRACTION_CHECKED,
                            Item.EMPTY,
                            Item.COMPONENT_MENU_MOD_REPULSION_UNCHECKED
                    ));
                } else {
                    modCheckboxes.addAll(Arrays.asList(
                            Item.COMPONENT_MENU_MOD_ATTRACTION_UNCHECKED,
                            Item.EMPTY,
                            Item.COMPONENT_MENU_MOD_REPULSION_CHECKED
                    ));
                }
                content.add(modCheckboxes);
                break;
            case BURNABLE_BLOCK:
                content.add(new ArrayList<>(List.of(
                        Item.COMPONENT_MENU_COLOR, Item.EMPTY, Item.SELECT_MATERIAL_MENU
                )));
                break;
            case MUSIC_BLOCK:
                MusicBlock mb = (MusicBlock) component;
                // ranges
                ArrayList<ArrayList<Item>> rangesMB = getRanges(mb);
                if (rangesMB != null) {
                    rangesMB = addBorders(rangesMB);
                    int nbEmptyToAdd = 7 - rangesMB.get(0).size();
                    rangesMB.set(0, addEmpty(rangesMB.get(0), nbEmptyToAdd));
                    addToInnerLists(content, rangesMB);
                }

                // Gestion du son
                ArrayList<ArrayList<Item>> songManagement = new ArrayList<>();
                for (int i = 0; i < 4; i++) {
                    songManagement.add(new ArrayList<>());
                }
                ArrayList<Item> fifthLine = new ArrayList<>();
                fifthLine.add(Item.COMPONENT_MENU_PREV_SONG);
                fifthLine.add(Item.COMPONENT_MENU_NEXT_SONG);
                fifthLine.add(Item.EMPTY);
                fifthLine.add(Item.COMPONENT_MENU_SONG_LOOP);
                fifthLine.add(Item.COMPONENT_MENU_SONG_STOP_ON_EXIT);
                songManagement.add(fifthLine);

                ArrayList<Item> sixthLine = new ArrayList<>();
                sixthLine = addEmpty(sixthLine, 3);
                if (mb.isLoop()) {
                    sixthLine.add(Item.COMPONENT_MENU_SONG_LOOP_CHECKED);
                } else {
                    sixthLine.add(Item.COMPONENT_MENU_SONG_LOOP_UNCHECKED);
                }
                if (mb.isStopOnExit()) {
                    sixthLine.add(Item.COMPONENT_MENU_SONG_STOP_ON_EXIT_CHECKED);
                } else {
                    sixthLine.add(Item.COMPONENT_MENU_SONG_STOP_ON_EXIT_UNCHECKED);
                }
                songManagement.add(sixthLine);
                addToInnerLists(content, songManagement);

                // modes
                addToInnerLists(content, getDetectionMods(mb));
                break;
            case REDSTONE_WINNER_BLOCK:
                WinnerBlock wb = (WinnerBlock) component;
                // ranges
                ArrayList<ArrayList<Item>> rangesWB = getRanges(wb);
                if (rangesWB != null) {
                    rangesWB = addBorders(rangesWB);
                    int nbEmptyToAdd = 7 - rangesWB.get(0).size();
                    rangesWB.set(0, addEmpty(rangesWB.get(0), nbEmptyToAdd));
                    addToInnerLists(content, rangesWB);
                }
                // modes
                addToInnerLists(content, getDetectionMods(wb));
                break;
            case APPEARING_WINNER_BLOCK:
                AppearingWinnerBlock awb = (AppearingWinnerBlock) component;
                // ranges
                ArrayList<ArrayList<Item>> rangesAWB = getRanges(awb);
                if (rangesAWB != null) {
                    rangesAWB = addBorders(rangesAWB);
                    int nbEmptyToAdd = 7 - rangesAWB.get(0).size();
                    rangesAWB.set(0, addEmpty(rangesAWB.get(0), nbEmptyToAdd));
                    addToInnerLists(content, rangesAWB);
                }
                // modes
                addToInnerLists(content, getDetectionMods(awb));
                break;
            case DISAPPEARING_WINNER_BLOCK:
                DisappearingWinnerBlock dwb = (DisappearingWinnerBlock) component;
                // ranges
                ArrayList<ArrayList<Item>> rangesDWB = getRanges(dwb);
                if (rangesDWB != null) {
                    rangesDWB = addBorders(rangesDWB);
                    int nbEmptyToAdd = 7 - rangesDWB.get(0).size();
                    rangesDWB.set(0, addEmpty(rangesDWB.get(0), nbEmptyToAdd));
                    addToInnerLists(content, rangesDWB);
                }
                // modes
                addToInnerLists(content, getDetectionMods(dwb));
                break;
            case LOCK:
                ArrayList<Item> addAndTp = new ArrayList<>(Arrays.asList(Item.COMPONENT_MENU_ADD_KEY_CHEST, Item.EMPTY, Item.COMPONENT_MENU_TELEPORT_TO_KEY_CHESTS_MENU));
                addAndTp = addEmpty(addAndTp, 5);
                addAndTp.add(Item.COMPONENT_MENU_DELETE_ALL_KEY_CHESTS);
                content.add(addAndTp);
                content.add(new ArrayList<>());
                content.add(Arrays.asList(Item.COMPONENT_MENU_DELETE_MY_KEYS, Item.EMPTY, Item.COMPONENT_MENU_DELETE_ALL_PLAYERS_KEYS));
                break;
            case KEY_CHEST:
                content.add(Arrays.asList(Item.COMPONENT_MENU_SELECT_KEY_NUMBER, Item.EMPTY, Item.COMPONENT_MENU_TELEPORT_TO_LOCK));
                break;
            case ELEVATOR:
                Elevator e = (Elevator) component;
                content.add(Arrays.asList(Item.COMPONENT_MENU_GO_TO_GROUND_FLOOR, Item.COMPONENT_MENU_GO_TO_PREVIOUS_FLOOR, Item.COMPONENT_MENU_GO_TO_NEXT_FLOOR, Item.COMPONENT_MENU_GO_TO_TOP_FLOOR, Item.COMPONENT_MENU_AS_HIGH_AS_POSSIBLE, Item.EMPTY, Item.EMPTY));
                content.add(Arrays.asList(Item.COMPONENT_MENU_TELEPORT_INSIDE_ELEVATOR, Item.EMPTY, Item.COMPONENT_MENU_CREATE_FLOOR, Item.EMPTY, Item.COMPONENT_MENU_DELETE_ALL_FLOORS, Item.COMPONENT_MENU_DELETE_ALL_CALL_BUTTONS));
                // ranges
                ArrayList<ArrayList<Item>> rangesE = getRanges(e);
                if (rangesE != null) {
                    for (int i = 0; i < 2; i++) {
                        rangesE.add(0, null);
                    }
                    addToInnerLists(content, rangesE);
                }
                // modes
                addToInnerLists(content, getDetectionMods(e));
                break;
            case CALL_BUTTON:
                content.add(Collections.singletonList(Item.COMPONENT_MENU_TELEPORT_INSIDE_ELEVATOR));
                break;
            default:
                throw new UnsupportedOperationException("Unknown component type");
        }
        return content;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEComponentMenuInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        Area area = component.getArea();

        if (!area.containsLocation(player.getBukkitPlayer().getLocation())) {
            item = Item.COMPONENT_SHORTCUTBAR_CLOSE;
        }
        Location loc = component.getComponentLocation();
        if (Areas.getInstance().getAreaFromLocation(loc) != component.getArea()) {
            player.getInventoryManager().closeOpenedInventory();
            return;
        }
        if (component.getArea().getComponentFromLocation(component.getComponentLocation()) != component) {
            player.getInventoryManager().closeOpenedInventory();
            return;
        }

        switch (item) {
            case COMPONENT_MENU_COLOR:
                player.getInventoryManager().onChangeComponentColor(component);
                break;
            case COMPONENT_MENU_RETRIEVE_MIRROR:
                ComponentController.removeMirror((IMirrorContainer) component, player);
                break;
            case COMPONENT_MENU_PLACE_MIRROR:
                ComponentController.placeMirror((IMirrorContainer) component, player, LasersColor.WHITE);
                break;
            case COMPONENT_MENU_INCREASE_MIRROR_CHEST:
                ComponentController.increaseMirrorChest((MirrorChest) component, player);
                break;
            case COMPONENT_MENU_DECREASE_MIRROR_CHEST:
                ComponentController.decreaseMirrorChest((MirrorChest) component, player);
                break;
            case COMPONENT_MENU_DECREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, ((GravitationalSphere) component), false);
                break;
            case COMPONENT_MENU_INCREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, ((GravitationalSphere) component), true);
                break;
            case COMPONENT_MENU_MOD_ATTRACTION:
            case COMPONENT_MENU_MOD_LOCKS:
            case COMPONENT_MENU_MOD_LOCKS_CHECKED:
            case COMPONENT_MENU_MOD_REDSTONE_SENSORS_CHECKED:
            case COMPONENT_MENU_MOD_REDSTONE_SENSORS:
            case COMPONENT_MENU_MOD_PLAYERS_CHECKED:
            case COMPONENT_MENU_MOD_PLAYERS:
            case COMPONENT_MENU_MOD_REDSTONE:
            case COMPONENT_MENU_MOD_REDSTONE_CHECKED:
            case COMPONENT_MENU_MOD_PERMANENTLY_ENABLED:
            case COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_CHECKED:
            case COMPONENT_MENU_MOD_LASER_RECEIVERS:
            case COMPONENT_MENU_MOD_LASER_RECEIVERS_CHECKED:
            case COMPONENT_MENU_SONG_LOOP:
            case COMPONENT_MENU_SONG_STOP_ON_EXIT:
            case COMPONENT_MENU_MOD_REPULSION:
            case COMPONENT_MENU_MIRROR_BLOCKED:
            case COMPONENT_MENU_MIRROR_FREE:
                break;
            case COMPONENT_MENU_MIRROR_BLOCKED_CHECKED:
            case COMPONENT_MENU_MIRROR_FREE_UNCHECKED:
                ComponentController.changeMirrorSupportMode(player, (MirrorSupport) component, MirrorSupportMode.FREE);
                break;
            case COMPONENT_MENU_MIRROR_FREE_CHECKED:
            case COMPONENT_MENU_MIRROR_BLOCKED_UNCHECKED:
                ComponentController.changeMirrorSupportMode(player, (MirrorSupport) component, MirrorSupportMode.BLOCKED);
                break;
            case COMPONENT_MENU_MOD_ATTRACTION_CHECKED:
            case COMPONENT_MENU_MOD_REPULSION_UNCHECKED:
                ComponentController.changeMode(player, (GravitationalSphere) component, GravitationalSphereMode.REPULSION);
                break;
            case COMPONENT_MENU_MOD_ATTRACTION_UNCHECKED:
            case COMPONENT_MENU_MOD_REPULSION_CHECKED:
                ComponentController.changeMode(player, (GravitationalSphere) component, GravitationalSphereMode.ATTRACTION);
                break;
            case COMPONENT_MENU_PREV_SONG:
                ComponentController.prevSong(player, (MusicBlock) component);
                break;
            case COMPONENT_MENU_NEXT_SONG:
                ComponentController.nextSong(player, (MusicBlock) component);
                break;
            case COMPONENT_MENU_SONG_LOOP_CHECKED:
                ComponentController.setLoop(player, (MusicBlock) component, false);
                break;
            case COMPONENT_MENU_SONG_LOOP_UNCHECKED:
                ComponentController.setLoop(player, (MusicBlock) component, true);
                break;
            case COMPONENT_MENU_SONG_STOP_ON_EXIT_CHECKED:
                ComponentController.setStopOnExit(player, (MusicBlock) component, false);
                break;
            case COMPONENT_MENU_SONG_STOP_ON_EXIT_UNCHECKED:
                ComponentController.setStopOnExit(player, (MusicBlock) component, true);
                break;
            case COMPONENT_MENU_RANGE_INCREASE_MIN:
                ComponentController.increaseMin(player, (IDetectionComponent) component);
                break;
            case COMPONENT_MENU_RANGE_INCREASE_MAX:
                ComponentController.increaseMax(player, (IDetectionComponent) component);
                break;
            case COMPONENT_MENU_RANGE_DECREASE_MIN:
                ComponentController.decreaseMin(player, (IDetectionComponent) component);
                break;
            case COMPONENT_MENU_RANGE_DECREASE_MAX:
                ComponentController.decreaseMax(player, (IDetectionComponent) component);
                break;
            case COMPONENT_MENU_UP:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.UP);
                break;
            case COMPONENT_MENU_DOWN:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.DOWN);
                break;
            case COMPONENT_MENU_LEFT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.LEFT);
                break;
            case COMPONENT_MENU_RIGHT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.RIGHT);
                break;
            case COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_UNCHECKED:
                ComponentController.changeMode(player, (IDetectionComponent) component, DetectionMode.PERMANENTLY_ENABLED);
                break;
            case COMPONENT_MENU_MOD_LASER_RECEIVERS_UNCHECKED:
                ComponentController.changeMode(player, (IDetectionComponent) component, DetectionMode.DETECTION_LASER_RECEIVERS);
                break;
            case COMPONENT_MENU_MOD_REDSTONE_UNCHECKED:
                ComponentController.changeMode(player, (IDetectionComponent) component, DetectionMode.DETECTION_REDSTONE);
                break;
            case COMPONENT_MENU_MOD_PLAYERS_UNCHECKED:
                ComponentController.changeMode(player, (IDetectionComponent) component, DetectionMode.DETECTION_PLAYERS);
                break;
            case COMPONENT_MENU_MOD_REDSTONE_SENSORS_UNCHECKED:
                ComponentController.changeMode(player, (IDetectionComponent) component, DetectionMode.DETECTION_REDSTONE_SENSORS);
                break;
            case COMPONENT_MENU_MOD_LOCKS_UNCHECKED:
                ComponentController.changeMode(player, (IDetectionComponent) component, DetectionMode.DETECTION_LOCKS);
                break;
            case COMPONENT_MENU_ADD_KEY_CHEST:
                player.getInventoryManager().openLEInventory(new KeyChestCreationShortcutBarInventory(player, (Lock) component));
                break;
            case COMPONENT_MENU_TELEPORT_TO_KEY_CHESTS_MENU:
                player.getInventoryManager().openLEInventory(new KeyChestTeleportationMenuInventory(player, (Lock) component));
                break;
            case COMPONENT_MENU_DELETE_ALL_KEY_CHESTS:
                player.getInventoryManager().openLEInventory(new DeleteLockKeyChestsConfirmationInventory(player, (Lock) component));
                break;
            case COMPONENT_MENU_DELETE_ALL_PLAYERS_KEYS:
                player.getInventoryManager().openLEInventory(new DeleteLockPlayersKeysConfirmationInventory(player, (Lock) component));
                break;
            case COMPONENT_MENU_DELETE_MY_KEYS:
                PlayerController.removePlayerLockKeys(player, (Lock) component);
                player.getInventoryManager().closeOpenedInventory();
                break;
            case COMPONENT_MENU_SELECT_KEY_NUMBER:
                player.getInventoryManager().openLEInventory(new KeyChestSelectorMenuInventory(player, (LockKeyChest) component));
                break;
            case COMPONENT_MENU_TELEPORT_TO_LOCK:
                player.teleportToNearestSafeLocation(((LockKeyChest) component).getLock().getComponentLocation());
                player.getInventoryManager().closeOpenedInventory();
                break;
            case COMPONENT_MENU_GO_TO_GROUND_FLOOR:
                Elevator elevator1 = (Elevator) component;
                int index1 = elevator1.getGroundFloorIndex();
                ComponentController.goToFloor(player, elevator1, index1);
                break;
            case COMPONENT_MENU_GO_TO_PREVIOUS_FLOOR:
                Elevator elevator2 = (Elevator) component;
                int index2 = elevator2.getPrevFloorIndex();
                ComponentController.goToFloor(player, elevator2, index2);
                break;
            case COMPONENT_MENU_GO_TO_NEXT_FLOOR:
                Elevator elevator3 = (Elevator) component;
                int index3 = elevator3.getNextFloorIndex();
                ComponentController.goToFloor(player, elevator3, index3);
                break;
            case COMPONENT_MENU_GO_TO_TOP_FLOOR:
                Elevator elevator4 = (Elevator) component;
                int index4 = elevator4.getTopFloorIndex();
                ComponentController.goToFloor(player, elevator4, index4);
                break;
            case COMPONENT_MENU_AS_HIGH_AS_POSSIBLE:
                ComponentController.elevatorGoAsHighAsPossible(player, (Elevator) component);
                player.getInventoryManager().closeOpenedInventory();
                break;
            case COMPONENT_MENU_CREATE_FLOOR:
                ComponentController.elevatorCreateFloor(player, (Elevator) component);
                break;
            case COMPONENT_MENU_DELETE_ALL_FLOORS:
                ComponentController.elevatorDeleteFloors(player, (Elevator) component);
                break;
            case COMPONENT_MENU_DELETE_ALL_CALL_BUTTONS:
                ComponentController.elevatorDeleteCallButtons(player, (Elevator) component);
                break;
            case COMPONENT_MENU_TELEPORT_INSIDE_ELEVATOR:
                ComponentController.elevatorTeleportInside(player, (Elevator) component);
                player.getInventoryManager().closeOpenedInventory();
                break;
            case COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU:
                player.getInventoryManager().openLEInventory(new ScheduledActionsMainInventory(player, component));
                break;
            case COMPONENT_MENU_LIGHT_LEVEL_OPEN_MENU:
                player.getInventoryManager().openLEInventory(new LightLevelEditInventory(player, (ILightComponent) component));
                break;
            case COMPONENT_MENU_ANY_COLOR_CHECKED:
                ComponentController.setAnyColorAccepted(component.getArea(), (LaserReceiver) component, player, false);
                break;
            case COMPONENT_MENU_ANY_COLOR_UNCHECKED:
                ComponentController.setAnyColorAccepted(component.getArea(), (LaserReceiver) component, player, true);
                break;
            case SELECT_MATERIAL_MENU:
                player.getInventoryManager().openLEInventory(new SelectMaterialInventory(player, component));
                break;
            case COMPONENT_MENU_LET_PASS_LASER_CHECKED, COMPONENT_MENU_LET_PASS_LASER_UNCHECKED:
                ComponentController.toggleLetPassLaser(player, (LaserReceiver) component);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.COMPONENT_MENU_COLOR,
                Item.COMPONENT_MENU_RETRIEVE_MIRROR,
                Item.COMPONENT_MENU_PLACE_MIRROR,
                Item.COMPONENT_MENU_MIRROR_FREE,
                Item.COMPONENT_MENU_MIRROR_FREE_CHECKED,
                Item.COMPONENT_MENU_MIRROR_FREE_UNCHECKED,
                Item.COMPONENT_MENU_MIRROR_BLOCKED,
                Item.COMPONENT_MENU_MIRROR_BLOCKED_CHECKED,
                Item.COMPONENT_MENU_MIRROR_BLOCKED_UNCHECKED,
                Item.COMPONENT_MENU_INCREASE_MIRROR_CHEST,
                Item.COMPONENT_MENU_DECREASE_MIRROR_CHEST,
                Item.COMPONENT_MENU_DECREASE_SPHERE_SIZE,
                Item.COMPONENT_MENU_INCREASE_SPHERE_SIZE,
                Item.COMPONENT_MENU_MOD_ATTRACTION,
                Item.COMPONENT_MENU_MOD_ATTRACTION_CHECKED,
                Item.COMPONENT_MENU_MOD_ATTRACTION_UNCHECKED,
                Item.COMPONENT_MENU_MOD_REPULSION,
                Item.COMPONENT_MENU_MOD_REPULSION_CHECKED,
                Item.COMPONENT_MENU_MOD_REPULSION_UNCHECKED,
                Item.COMPONENT_MENU_PREV_SONG,
                Item.COMPONENT_MENU_NEXT_SONG,
                Item.COMPONENT_MENU_SONG_STOP_ON_EXIT,
                Item.COMPONENT_MENU_SONG_LOOP,
                Item.COMPONENT_MENU_SONG_LOOP_CHECKED,
                Item.COMPONENT_MENU_SONG_LOOP_UNCHECKED,
                Item.COMPONENT_MENU_SONG_STOP_ON_EXIT_CHECKED,
                Item.COMPONENT_MENU_SONG_STOP_ON_EXIT_UNCHECKED,
                Item.COMPONENT_MENU_RANGE_INCREASE_MIN,
                Item.COMPONENT_MENU_RANGE_INCREASE_MAX,
                Item.COMPONENT_MENU_RANGE_DECREASE_MIN,
                Item.COMPONENT_MENU_RANGE_DECREASE_MAX,
                Item.COMPONENT_MENU_UP,
                Item.COMPONENT_MENU_DOWN,
                Item.COMPONENT_MENU_LEFT,
                Item.COMPONENT_MENU_RIGHT,
                Item.COMPONENT_MENU_MOD_PERMANENTLY_ENABLED,
                Item.COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_CHECKED,
                Item.COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_UNCHECKED,
                Item.COMPONENT_MENU_MOD_LASER_RECEIVERS,
                Item.COMPONENT_MENU_MOD_LASER_RECEIVERS_CHECKED,
                Item.COMPONENT_MENU_MOD_LASER_RECEIVERS_UNCHECKED,
                Item.COMPONENT_MENU_MOD_REDSTONE,
                Item.COMPONENT_MENU_MOD_REDSTONE_CHECKED,
                Item.COMPONENT_MENU_MOD_REDSTONE_UNCHECKED,
                Item.COMPONENT_MENU_MOD_PLAYERS,
                Item.COMPONENT_MENU_MOD_PLAYERS_CHECKED,
                Item.COMPONENT_MENU_MOD_PLAYERS_UNCHECKED,
                Item.COMPONENT_MENU_MOD_REDSTONE_SENSORS,
                Item.COMPONENT_MENU_MOD_REDSTONE_SENSORS_CHECKED,
                Item.COMPONENT_MENU_MOD_REDSTONE_SENSORS_UNCHECKED,
                Item.COMPONENT_MENU_MOD_LOCKS,
                Item.COMPONENT_MENU_MOD_LOCKS_CHECKED,
                Item.COMPONENT_MENU_MOD_LOCKS_UNCHECKED,
                Item.COMPONENT_MENU_ADD_KEY_CHEST,
                Item.COMPONENT_MENU_DELETE_ALL_KEY_CHESTS,
                Item.COMPONENT_MENU_DELETE_MY_KEYS,
                Item.COMPONENT_MENU_DELETE_ALL_PLAYERS_KEYS,
                Item.COMPONENT_MENU_TELEPORT_TO_KEY_CHESTS_MENU,
                Item.COMPONENT_MENU_TELEPORT_TO_LOCK,
                Item.COMPONENT_MENU_SELECT_KEY_NUMBER,
                Item.COMPONENT_MENU_GO_TO_GROUND_FLOOR,
                Item.COMPONENT_MENU_GO_TO_PREVIOUS_FLOOR,
                Item.COMPONENT_MENU_GO_TO_NEXT_FLOOR,
                Item.COMPONENT_MENU_GO_TO_TOP_FLOOR,
                Item.COMPONENT_MENU_AS_HIGH_AS_POSSIBLE,
                Item.COMPONENT_MENU_TELEPORT_INSIDE_ELEVATOR,
                Item.COMPONENT_MENU_CREATE_FLOOR,
                Item.COMPONENT_MENU_DELETE_ALL_FLOORS,
                Item.COMPONENT_MENU_DELETE_ALL_CALL_BUTTONS,
                Item.COMPONENT_MENU_TELEPORT_INSIDE_ELEVATOR,
                Item.COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU,
                Item.COMPONENT_MENU_LIGHT_LEVEL_OPEN_MENU,
                Item.COMPONENT_MENU_ANY_COLOR,
                Item.COMPONENT_MENU_ANY_COLOR_CHECKED,
                Item.COMPONENT_MENU_ANY_COLOR_UNCHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_AREA_EXIT_CHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_AREA_EXIT_UNCHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_DEATH_CHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_DEATH_UNCHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_SERVER_QUIT_CHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_SERVER_QUIT_UNCHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_WORLD_CHANGE_CHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_ON_WORLD_CHANGE_UNCHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_WHEN_LOCK_IS_OPENED_CHECKED,
                Item.COMPONENT_MENU_CLEAR_KEYS_WHEN_LOCK_IS_OPENED_UNCHECKED,
                Item.COMPONENT_MENU_ALL_KEY_CHESTS_MUST_BE_FOUND_CHECKED,
                Item.COMPONENT_MENU_ALL_KEY_CHESTS_MUST_BE_FOUND_UNCHECKED,
                Item.SELECT_MATERIAL_MENU,
                Item.COMPONENT_MENU_LET_PASS_LASER,
                Item.COMPONENT_MENU_LET_PASS_LASER_CHECKED,
                Item.COMPONENT_MENU_LET_PASS_LASER_UNCHECKED
        )).contains(item);
    }

    private ArrayList<ArrayList<Item>> getRanges(IDetectionComponent detectionComponent) {
        ArrayList<ArrayList<Item>> ranges = new ArrayList<>();
        if (!detectionComponent.getDetectionMode().isRangeModificationCompatible()) {
            return null;
        }
        ArrayList<Item> firstLine = new ArrayList<>();
        int max = detectionComponent.getMax();
        int min = detectionComponent.getMin();
        if (min > 98) {
            firstLine.add(Item.EMPTY);
        } else {
            firstLine.add(Item.COMPONENT_MENU_RANGE_INCREASE_MIN);
        }
        firstLine = addEmpty(firstLine, 3);
        if (max > 98) {
            firstLine.add(Item.EMPTY);
        } else {
            firstLine.add(Item.COMPONENT_MENU_RANGE_INCREASE_MAX);
        }
        ranges.add(firstLine);

        ArrayList<Item> secondLine = new ArrayList<>(getNumberAsItems(detectionComponent.getMin(), true));
        secondLine.add(Item.EMPTY);
        secondLine.addAll(getNumberAsItems(detectionComponent.getMax(), true));
        ranges.add(secondLine);

        ArrayList<Item> thirdLine = new ArrayList<>();
        if (min < 1) {
            thirdLine.add(Item.EMPTY);
        } else {
            thirdLine.add(Item.COMPONENT_MENU_RANGE_DECREASE_MIN);
        }
        thirdLine = addEmpty(thirdLine, 3);
        if (max < 1) {
            thirdLine.add(Item.EMPTY);
        } else {
            thirdLine.add(Item.COMPONENT_MENU_RANGE_DECREASE_MAX);
        }
        ranges.add(thirdLine);
        return ranges;
    }

    private ArrayList<ArrayList<Item>> getRotations() {
        ArrayList<ArrayList<Item>> rotations = new ArrayList<>();
        rotations.add(new ArrayList<>(Arrays.asList(
                Item.EMPTY, Item.COMPONENT_MENU_UP, Item.EMPTY
        )));
        rotations.add(new ArrayList<>(Arrays.asList(
                Item.COMPONENT_MENU_LEFT, Item.EMPTY, Item.COMPONENT_MENU_RIGHT
        )));
        rotations.add(new ArrayList<>(Arrays.asList(
                Item.EMPTY, Item.COMPONENT_MENU_DOWN, Item.EMPTY
        )));
        return rotations;
    }

    private ArrayList<ArrayList<Item>> getDetectionMods(IDetectionComponent detectionComponent) {
        ArrayList<ArrayList<Item>> mods = new ArrayList<>();

        DetectionMode currentMod = detectionComponent.getDetectionMode();

        boolean isLaserSender = false;
        if (detectionComponent instanceof LaserSender) {
            isLaserSender = true;
        }

        // DetectionMode.PERMANENTLY_ENABLED
        if (isLaserSender) {
            ArrayList<Item> modPermanentlyEnabled = new ArrayList<>();
            modPermanentlyEnabled.add(Item.COMPONENT_MENU_MOD_PERMANENTLY_ENABLED);
            if (currentMod == DetectionMode.PERMANENTLY_ENABLED) {
                modPermanentlyEnabled.add(Item.COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_CHECKED);
            } else {
                modPermanentlyEnabled.add(Item.COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_UNCHECKED);
            }
            mods.add(modPermanentlyEnabled);
        }

        // DetectionMode.DETECTION_LASER_RECEIVERS
        ArrayList<Item> modLaserReceivers = new ArrayList<>();
        modLaserReceivers.add(Item.COMPONENT_MENU_MOD_LASER_RECEIVERS);
        if (currentMod == DetectionMode.DETECTION_LASER_RECEIVERS) {
            modLaserReceivers.add(Item.COMPONENT_MENU_MOD_LASER_RECEIVERS_CHECKED);
        } else {
            modLaserReceivers.add(Item.COMPONENT_MENU_MOD_LASER_RECEIVERS_UNCHECKED);
        }
        mods.add(modLaserReceivers);

        // DetectionMode.DETECTION_REDSTONE
        if (isLaserSender) {
            ArrayList<Item> modRedstone = new ArrayList<>();
            modRedstone.add(Item.COMPONENT_MENU_MOD_REDSTONE);
            if (currentMod == DetectionMode.DETECTION_REDSTONE) {
                modRedstone.add(Item.COMPONENT_MENU_MOD_REDSTONE_CHECKED);
            } else {
                modRedstone.add(Item.COMPONENT_MENU_MOD_REDSTONE_UNCHECKED);
            }
            mods.add(modRedstone);
        }

        // DetectionMode.DETECTION_PLAYERS
        ArrayList<Item> modPlayers = new ArrayList<>();
        modPlayers.add(Item.COMPONENT_MENU_MOD_PLAYERS);
        if (currentMod == DetectionMode.DETECTION_PLAYERS) {
            modPlayers.add(Item.COMPONENT_MENU_MOD_PLAYERS_CHECKED);
        } else {
            modPlayers.add(Item.COMPONENT_MENU_MOD_PLAYERS_UNCHECKED);
        }
        mods.add(modPlayers);

        // DetectionMode.DETECTION_REDSTONE_SENSORS
        ArrayList<Item> modRedstoneSensors = new ArrayList<>();
        modRedstoneSensors.add(Item.COMPONENT_MENU_MOD_REDSTONE_SENSORS);
        if (currentMod == DetectionMode.DETECTION_REDSTONE_SENSORS) {
            modRedstoneSensors.add(Item.COMPONENT_MENU_MOD_REDSTONE_SENSORS_CHECKED);
        } else {
            modRedstoneSensors.add(Item.COMPONENT_MENU_MOD_REDSTONE_SENSORS_UNCHECKED);
        }
        mods.add(modRedstoneSensors);

        // DetectionMode.LOCKS
        ArrayList<Item> modLocks = new ArrayList<>();
        modLocks.add(Item.COMPONENT_MENU_MOD_LOCKS);
        if (currentMod == DetectionMode.DETECTION_LOCKS) {
            modLocks.add(Item.COMPONENT_MENU_MOD_LOCKS_CHECKED);
        } else {
            modLocks.add(Item.COMPONENT_MENU_MOD_LOCKS_UNCHECKED);
        }
        mods.add(modLocks);
        return mods;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.COMPONENT_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return component.getArea();
    }

}
