package eu.lasersenigma.component;

/**
 * Enumeration representing different offsets for items on an armor stand
 */
public enum ArmorStandItemOffset {

    GLASS_PANE(0.275, 0.755),
    HEAD(0, 0.3);

    private final double offsetItemToHead;
    private final double offsetNeckToHead;

    /**
     * Constructor for ArmorStandItemOffset enum.
     *
     * @param offsetItemToHead The offset from the item to the head
     * @param offsetNeckToHead The offset from the neck to the head
     */
    ArmorStandItemOffset(double offsetItemToHead, double offsetNeckToHead) {
        this.offsetNeckToHead = offsetNeckToHead;
        this.offsetItemToHead = offsetItemToHead;
    }

    /**
     * Gets the offset from the item to the head
     *
     * @return The offset value
     */
    public double getOffsetItemToHead() {
        return offsetItemToHead;
    }

    /**
     * Gets the offset from the neck to the head
     *
     * @return The offset value
     */
    public double getOffsetNeckToHead() {
        return offsetNeckToHead;
    }
}
