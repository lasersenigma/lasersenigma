package eu.lasersenigma.component.elevator;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;

/**
 * Call Button
 */
public final class ElevatorCallButton extends AArmorStandComponent {

    // Call button related elevator
    private final Elevator elevator;

    // Component's armor stand
    private final ComponentArmorStand ELEVATOR_CALL_BUTTON_AS = new ComponentArmorStand(this, "elevator_call_button");

    /**
     * Constructor used for creation from the database
     *
     * @param area        The area containing this call button
     * @param componentId The id of the component inside the database
     * @param location    The location of the call button
     * @param face        The blockface of the component
     * @param rotation    The rotation of the component
     * @param elevator    The corresponding elevator
     */
    public ElevatorCallButton(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, Elevator elevator) {
        super(area, componentId, location, ComponentType.CALL_BUTTON, face, rotation);
        this.elevator = elevator;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this call button
     * @param location The location of the component
     * @param face     The blockface of the component
     * @param elevator The corresponding elevator
     */
    public ElevatorCallButton(Area area, Location location, ComponentFace face, Elevator elevator) {
        super(area, location, ComponentType.CALL_BUTTON, face);
        this.elevator = elevator;
        dbCreate();
    }

    /**
     * Retrieves call button's related elevator
     *
     * @return Related elevator
     */
    public Elevator getElevator() {
        return this.elevator;
    }


    public boolean onCallButton(boolean isEditingPuzzle) {
        return elevator.onCallButtonPushed(this, isEditingPuzzle);
    }

    @Override
    public void activateComponent() {
    }

    @Override
    public void deactivateComponent() {
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(ELEVATOR_CALL_BUTTON_AS, baseLocation, rotation, Item.CALL_BUTTON, false, false, null);
        /*updateComponentArmorStand(componentArmorStand -> {
            ArmorStand armorStand = findArmorStandBack(componentArmorStand.getArmorStand());

            if (armorStand == null) {
                newCreateArmorStand(this, componentArmorStand, Item.CALL_BUTTON);
            } else {
                Location nextLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getComponentLocation(), face), ArmorStandItemOffset.HEAD, rotation);
                componentArmorStand.getArmorStand().setHeadPose(rotation);
                componentArmorStand.getArmorStand().teleport(nextLocation);
            }
        });*/
        /*armorStandCallButton = findArmorStandBack(armorStandCallButton);
        if (armorStandCallButton == null) {
            createCallButtonArmorStand();
        } else {
            armorStandCallButton.setHeadPose(rotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getComponentLocation(), face), ArmorStandItemOffset.HEAD, rotation);
            armorStandCallButton.teleport(nextLoc);
        }
        Block b = getComponentLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }*/
    }

    /**
     * Deletes the component
     */
    /*@Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();
        if (armorStandCallButton != null) {
            Areas.getInstance().removeEntity(armorStandCallButton.getUniqueId());
            for (Entity entity : getComponentLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandCallButton.getUniqueId())) {
                    armorStandCallButton = (ArmorStand) entity;
                }
            }
            armorStandCallButton.remove();
            armorStandCallButton = null;
        }
    }*/

    /**
     * Creates the call button armorstand
     */
    /*private void createCallButtonArmorStand() {
        Location armorStandCallButtonLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getComponentLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandCallButton = createArmorStand(this, "elevatorCallButton", armorStandCallButtonLoc, rotation,
                Item.CALL_BUTTON);
    }*/

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
