package eu.lasersenigma.component.elevator.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AShortcutBarInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.inventory.MainShortcutBarInventory;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IAreaComponent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.common.inventory.ComponentShortcutBarInventory;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.CrossableMaterials;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Select the second location of the elevator cage during Elevator creation shortcut bar
 */
public class ElevatorSecondLocationShortcutBarInventory extends AShortcutBarInventory {

    private final Location firstLocation;
    private final Area area;

    public ElevatorSecondLocationShortcutBarInventory(LEPlayer player, Area area, Location firstLocation) {
        super(player);
        this.area = area;
        this.firstLocation = firstLocation;
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.ELEVATOR_SECOND_LOCATION);
        while (shortcutBar.size() < 8) {
            shortcutBar.add(Item.EMPTY);
        }
        shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEComponentShortcutBarInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        Location secondLocation = player.getBukkitPlayer().getTargetBlock(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20).getLocation();
        if (secondLocation == null) {
            return;
        }
        if (!area.containsLocation(secondLocation)) {
            item = Item.COMPONENT_SHORTCUTBAR_CLOSE;
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
        }
        switch (item) {
            case ELEVATOR_SECOND_LOCATION:
                IAreaComponent component = ComponentController.addIAreaComponent(player, firstLocation, secondLocation, ComponentType.ELEVATOR);
                player.getInventoryManager().openLEInventory(new ComponentShortcutBarInventory(player, component));
                break;
            case COMPONENT_SHORTCUTBAR_CLOSE:
                player.getInventoryManager().openLEInventory(new MainShortcutBarInventory(player));
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER,
                Item.ELEVATOR_SECOND_LOCATION,
                Item.COMPONENT_SHORTCUTBAR_CLOSE
        )).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.ELEVATOR_SECOND_LOCATION_SHORTCUTBAR;
    }

}
