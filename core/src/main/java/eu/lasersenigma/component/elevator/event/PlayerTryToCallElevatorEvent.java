package eu.lasersenigma.component.elevator.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.elevator.ElevatorCallButton;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToCallElevatorEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final ElevatorCallButton component;

    public PlayerTryToCallElevatorEvent(Player player, ElevatorCallButton component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public ElevatorCallButton getCallButton() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
