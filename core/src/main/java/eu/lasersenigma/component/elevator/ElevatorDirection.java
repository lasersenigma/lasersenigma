package eu.lasersenigma.component.elevator;

import org.bukkit.util.Vector;

/**
 * Enumeration representing elevator directions
 */
public enum ElevatorDirection {

    UP(new Vector(0, 1, 0)),
    DOWN(new Vector(0, -1, 0));

    private final Vector vector;

    /**
     * Constructs an elevator direction with the specified vector
     *
     * @param vector The vector associated with the elevator direction
     */
    ElevatorDirection(Vector vector) {
        this.vector = vector;
    }

    /**
     * Gets the vector associated with the elevator direction
     *
     * @return The vector representing the elevator direction
     */
    public Vector getVector() {
        return this.vector.clone();
    }
}
