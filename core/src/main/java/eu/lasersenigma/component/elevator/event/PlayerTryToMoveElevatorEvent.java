package eu.lasersenigma.component.elevator.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.elevator.Elevator;
import eu.lasersenigma.component.elevator.ElevatorDirection;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToMoveElevatorEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final Elevator component;

    private final ElevatorDirection direction;

    public PlayerTryToMoveElevatorEvent(Player player, Elevator component, ElevatorDirection direction) {
        super();
        this.player = player;
        this.component = component;
        this.direction = direction;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public Elevator getElevator() {
        return component;
    }

    public ElevatorDirection getDirection() {
        return direction;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
