package eu.lasersenigma.component;

import java.util.Objects;

public class ComponentOption {

    /**
     * Option's component
     */
    private final IComponent component;

    // Option's name
    private final String id;

    /**
     * Group component option by group number.
     * Multiple component options having the same group number can not be activated together.
     */
    private int groupNumber;

    /**
     * Option's status
     */
    private boolean isEnabled;

    public ComponentOption(IComponent component, String id, int groupNumber, boolean enabled) {
        this.component = component;
        this.id = id;
        this.groupNumber = groupNumber;
        this.isEnabled = enabled;

        component.addOption(this);
    }

    /**
     * Retrieves option's name
     *
     * @return Option's name
     */
    public String getId() {
        return id;
    }

    /**
     * Retrieves option's group number
     *
     * @return Option's group number
     */
    public int getGroupNumber() {
        return groupNumber;
    }

    /**
     * Define a new group number, this can be useful if you want to implement something dynamically
     *
     * @param groupNumber New group number
     */
    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    /**
     * Verifies if option is enabled
     *
     * @return True is option is enabled, false otherwise
     */
    public boolean isEnabled() {
        return isEnabled;
    }

    /**
     * Define option's status
     *
     * @param enabled Wanted status
     */
    public void setEnabled(boolean enabled) {
        this.isEnabled = enabled;
        if (enabled) disableOtherComponentsInSameGroup();
    }

    private void disableOtherComponentsInSameGroup() {
        component.getComponentOptions().stream()
                .filter(option -> !option.equals(this))
                .filter(option -> option.getGroupNumber() == groupNumber)
                .forEach(enabled -> setEnabled(false));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponentOption that = (ComponentOption) o;
        return Objects.equals(component, that.component) && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component, id);
    }
}
