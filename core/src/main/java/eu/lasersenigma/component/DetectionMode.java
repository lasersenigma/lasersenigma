package eu.lasersenigma.component;

/**
 * Enumeration representing detection modes
 */
public enum DetectionMode {

    PERMANENTLY_ENABLED(true, false, "messages.detection_mode_always_enabled"),
    DETECTION_LASER_RECEIVERS(false, false, "messages.detection_mode_laser_receivers"),
    DETECTION_REDSTONE(true, false, "messages.detection_mode_redstone"),
    DETECTION_PLAYERS(false, false, "messages.detection_mode_players"),
    DETECTION_REDSTONE_SENSORS(false, false, "messages.detection_mode_redstone_sensor"),
    DETECTION_LOCKS(false, false, "messages.detection_mode_locks"),
    DETECTION_VICTORY_AREA(false, true, "messages.detection_mode_victory_area");

    private final boolean specificToLaserSenders;
    private final boolean specificToAreas;
    private final String messageCode;

    /**
     * Constructs a detection mode with the specified parameters
     *
     * @param specificToLaserSenders True if the mode is specific to laser senders, false otherwise
     * @param specificToAreas        True if the mode is specific to areas, false otherwise
     * @param messageCode            The corresponding message for the detection mode
     */
    DetectionMode(boolean specificToLaserSenders, boolean specificToAreas, String messageCode) {
        this.specificToLaserSenders = specificToLaserSenders;
        this.messageCode = messageCode;
        this.specificToAreas = specificToAreas;
    }

    /**
     * Gets the next detection mode based on the current mode, considering specific conditions
     *
     * @param isLaserSender True if the mode is for a laser sender, false otherwise
     * @param isArea        True if the mode is for an area, false otherwise
     * @return The next detection mode
     */
    public DetectionMode next(boolean isLaserSender, boolean isArea) {
        DetectionMode tmpMode = this;
        while (true) {
            tmpMode = tmpMode.next();
            if (!isLaserSender && tmpMode.specificToLaserSenders) {
                continue;
            }
            if (!isArea && tmpMode.specificToAreas) {
                continue;
            }
            return tmpMode;
        }
    }

    private DetectionMode next() {
        int nextModeIndex = (this.ordinal() + 1) % DetectionMode.values().length;
        return DetectionMode.values()[nextModeIndex];
    }

    /**
     * Gets the corresponding message for the detection mode
     *
     * @return The corresponding message
     */
    public String getMessageCode() {
        return this.messageCode;
    }

    /**
     * Checks if the detection mode is compatible with range modification
     *
     * @return True if the mode is compatible, false otherwise
     */
    public boolean isRangeModificationCompatible() {
        return !specificToLaserSenders && !specificToAreas;
    }

    /**
     * Checks if the detection mode is specific to laser senders
     *
     * @return True if the mode is specific to laser senders, false otherwise
     */
    public boolean isSpecificToLaserSender() {
        return specificToLaserSenders;
    }

    /**
     * Checks if the detection mode is specific to areas
     *
     * @return True if the mode is specific to areas, false otherwise
     */
    public boolean isSpecificToAreas() {
        return specificToAreas;
    }
}
