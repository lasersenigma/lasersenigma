package eu.lasersenigma.component.schematic;

import java.io.Serializable;

public class ComponentSchematic implements Serializable {

    public static final long serialVersionUID = 1;

    private String type = null;
    private double locDiffX = 0;
    private double locDiffY = 0;
    private double locDiffZ = 0;
    private String face = null;
    private double rotationX = 0;
    private double rotationY = 0;
    private int min = 0;
    private int max = 0;
    private int color = 0;
    private String songFileName = null;
    private boolean loop = false;
    private boolean stopOnExit = false;
    private String mode = null;

    private int lightLevel = 0;

    private String material = null;

    private boolean letPassLasers = false;

    public ComponentSchematic() {
    }

    public double getLocDiffX() {
        return locDiffX;
    }

    public void setLocDiffX(double locDiffX) {
        this.locDiffX = locDiffX;
    }

    public double getLocDiffY() {
        return locDiffY;
    }

    public void setLocDiffY(double locDiffY) {
        this.locDiffY = locDiffY;
    }

    public double getLocDiffZ() {
        return locDiffZ;
    }

    public void setLocDiffZ(double locDiffZ) {
        this.locDiffZ = locDiffZ;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public double getRotationX() {
        return rotationX;
    }

    public void setRotationX(double rotationX) {
        this.rotationX = rotationX;
    }

    public double getRotationY() {
        return rotationY;
    }

    public void setRotationY(double rotationY) {
        this.rotationY = rotationY;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getSongFileName() {
        return songFileName;
    }

    public void setSongFileName(String songFileName) {
        this.songFileName = songFileName;
    }

    public boolean getLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public boolean getStopOnExit() {
        return stopOnExit;
    }

    public void setStopOnExit(boolean stopOnExit) {
        this.stopOnExit = stopOnExit;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public int getLightLevel() {
        return lightLevel;
    }

    public void setLightLevel(int lightLevel) {
        this.lightLevel = lightLevel;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public boolean getLetPassLasers() {
        return letPassLasers;
    }

    public void setLetPassLasers(boolean letPassLasers) {
        this.letPassLasers = letPassLasers;
    }
}
