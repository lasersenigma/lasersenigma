package eu.lasersenigma.component.schematic;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentFactory;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.burnableblock.BurnableBlock;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.musicblock.MusicBlock;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.util.Vector;

import java.util.LinkedList;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class ComponentSchematicManager {

    private ComponentSchematicManager() {

    }

    public static ComponentSchematic createSchematic(IComponent component, Location playerLocation) {
        ComponentSchematic componentSchematic = new ComponentSchematic();
        componentSchematic.setType(component.getComponentType().toString());
        Vector locDiff = component.getComponentLocation().toVector().subtract(playerLocation.toVector());
        componentSchematic.setLocDiffX(locDiff.getX());
        componentSchematic.setLocDiffY(locDiff.getY());
        componentSchematic.setLocDiffZ(locDiff.getZ());
        if (component instanceof AArmorStandComponent) {
            AArmorStandComponent rotatableComponent = ((AArmorStandComponent) component);
            componentSchematic.setRotationX(rotatableComponent.getRotationCurrent().getX());
            componentSchematic.setRotationY(rotatableComponent.getRotationCurrent().getY());
            componentSchematic.setFace(rotatableComponent.getComponentFace().toString());
        } else if (component instanceof MirrorChest) {
            componentSchematic.setFace(((MirrorChest) component).getFace().toString());
        }
        if (component.getComponentType() == ComponentType.MIRROR_CHEST) {
            componentSchematic.setMin(((MirrorChest) component).getNbMirrors());
        }
        if (component instanceof IColorableComponent) {
            LasersColor lasersColor = ((IColorableComponent) component).getColorCurrent();

            if (lasersColor != null) {
                int color = lasersColor.getInt();
                if (component instanceof LaserReceiver) {
                    if (!((LaserReceiver) component).isAnyColorAccepted()) {
                        color = 16;
                    }
                }
                componentSchematic.setColor(color);
            }
        }
        if (component instanceof MusicBlock) {
            MusicBlock musicBlock = (MusicBlock) component;
            componentSchematic.setSongFileName(musicBlock.getSongFileName());
            componentSchematic.setLoop(musicBlock.isLoop());
            componentSchematic.setStopOnExit(musicBlock.isStopOnExit());
        }
        if (component instanceof IDetectionComponent) {
            componentSchematic.setMode(((IDetectionComponent) component).getDetectionMode().toString());
            componentSchematic.setMin(((IDetectionComponent) component).getMin());
            componentSchematic.setMax(((IDetectionComponent) component).getMax());
        }
        if (component instanceof ILightComponent) {
            componentSchematic.setLightLevel(((ILightComponent) component).getLightLevel());
        }
        if (component instanceof GravitationalSphere) {
            componentSchematic.setMode(((GravitationalSphere) component).getGravitationalSphereMode().toString());
            componentSchematic.setMin(((GravitationalSphere) component).getGravityStrength());
        }
        if (component instanceof MirrorSupport) {
            componentSchematic.setMode(((MirrorSupport) component).getMirrorSupportMode().toString());
        }
        if (component instanceof BurnableBlock) {
            componentSchematic.setMaterial(((BurnableBlock) component).getMaterial().name());
        }
        if (component instanceof LaserReceiver laserReceiver) {
            componentSchematic.setLetPassLasers(laserReceiver.isLetPassLaserThrough());
        }
        return componentSchematic;
    }

    public static IComponent createComponent(ComponentSchematic componentSchematic, Area area, Location playerLocation) {
        IComponent component = ComponentFactory.createComponentFromDatabase(
                -1,
                area,
                ComponentType.valueOf(componentSchematic.getType()),
                playerLocation.clone().add(new Vector(
                        componentSchematic.getLocDiffX(),
                        componentSchematic.getLocDiffY(),
                        componentSchematic.getLocDiffZ()
                )),
                componentSchematic.getFace(),
                new Rotation(componentSchematic.getRotationX(), componentSchematic.getRotationY()),
                componentSchematic.getMin(),
                componentSchematic.getMax(),
                componentSchematic.getColor() == 16 ? null : LasersColor.fromInt(componentSchematic.getColor()),
                componentSchematic.getSongFileName(),
                componentSchematic.getLoop(),
                componentSchematic.getStopOnExit(),
                componentSchematic.getMode(),
                null,
                new LinkedList<>(),
                null,
                componentSchematic.getLightLevel(),
                componentSchematic.getMaterial() == null ? null : Material.valueOf(componentSchematic.getMaterial()),
                componentSchematic.getLetPassLasers()
        );
        area.addComponent(component, false);
        component.dbCreate();
        return component;
    }
}
