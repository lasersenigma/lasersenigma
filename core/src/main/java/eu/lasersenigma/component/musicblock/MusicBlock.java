package eu.lasersenigma.component.musicblock;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.songs.PlayersListSongManager;
import eu.lasersenigma.songs.SongsListManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Music Block
 */
public final class MusicBlock extends AArmorStandComponent implements IDetectionComponent {

    private static final SongsListManager SONG_LIST_MANAGER = SongsListManager.getInstance();

    private static final PlayersListSongManager PLAYERS_LIST_SONG_MANAGER = PlayersListSongManager.getInstance();

    private boolean loop = false;

    private boolean stopOnExit = true;

    private String songFileName = SONG_LIST_MANAGER.getSongFileName(0);

    // The minimum number of [activated laser receivers | players inside the area] needed to activate this component
    private int min = 1;

    // The maximum number of [activated laser receivers | players inside thearea] needed to activate this component
    private int max = 10;

    // The actual number of (laser receivers activated | players inside the area)
    private int nbActivated = -1;

    // The detection mode of the component (number of activated laser receivers or number of players inside the area)
    private DetectionMode detectionMode = DetectionMode.DETECTION_LASER_RECEIVERS;

    // Component's armor stand
    private final ComponentArmorStand MUSIC_BLOCK_AS = new ComponentArmorStand(this, "music_block_as");

    // Component's options
    // Empty

    // Component's events listener
    // Empty

    /**
     * Constructor used for creation from the database
     *
     * @param area          The area containing this laser receiver
     * @param componentId   The id of the component inside the database
     * @param location      The location of the laser receiver
     * @param face          The blockface of the component
     * @param rotation      The rotation of the component
     * @param min           The minimun number of activated laser receivers | players needed for this block to be activated
     * @param max           The maximum number of activated laser receivers | players needed for this block to be activated
     * @param songFileName  The name of the song file.
     * @param loop          If the loop mode is activated or not
     * @param stopOnExit    If the stop on exit mode is activated or not
     * @param detectionMode The detection mode
     */
    public MusicBlock(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, int min, int max, String songFileName, boolean loop, boolean stopOnExit, DetectionMode detectionMode) {
        super(area, componentId, location, ComponentType.MUSIC_BLOCK, face, rotation);
        this.loop = loop;
        this.stopOnExit = stopOnExit;
        this.songFileName = songFileName;
        this.min = min;
        this.max = max;
        this.detectionMode = detectionMode;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this
     * @param location The location of the component
     * @param face     The blockface of the component
     */
    public MusicBlock(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.MUSIC_BLOCK, face);
        dbCreate();
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
        showOrUpdateComponent();
        startSong();
        dbUpdate();
    }

    public boolean isStopOnExit() {
        return stopOnExit;
    }

    public void setStopOnExit(boolean stopOnExit) {
        this.stopOnExit = stopOnExit;
        showOrUpdateComponent();
        startSong();
        dbUpdate();
    }

    public String getSongFileName() {
        return songFileName;
    }

    public void nextSong(Player player) {
        if (!SONG_LIST_MANAGER.isActivated()) return;

        int index = SONG_LIST_MANAGER.getIndex(songFileName);
        int newIndex = SONG_LIST_MANAGER.getNextIndex(index);
        songFileName = SONG_LIST_MANAGER.getSongFileName(newIndex);

        SONG_LIST_MANAGER.showScoreboard(newIndex, player);
        startSong();
        dbUpdate();
    }

    public void prevSong(Player player) {
        if (!SONG_LIST_MANAGER.isActivated()) return;

        int index = SONG_LIST_MANAGER.getIndex(songFileName);
        int newIndex = SONG_LIST_MANAGER.getPrevIndex(index);
        songFileName = SONG_LIST_MANAGER.getSongFileName(newIndex);

        SONG_LIST_MANAGER.showScoreboard(newIndex, player);
        startSong();
        dbUpdate();
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        dbUpdate();
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        dbUpdate();
    }

    @Override
    public boolean isActivated() {
        return (this.min <= this.nbActivated && this.nbActivated <= this.max);
    }

    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated == this.nbActivated) return;
        this.nbActivated = nbActivated;
        if (isActivated()) startSong();
    }

    @Override
    public DetectionMode getDetectionMode() {
        return detectionMode;
    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToLaserSender() || mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.detectionMode = mode;
        dbUpdate();
        showOrUpdateComponent();
    }

    @Override
    public void activateComponent() {
        nbActivated = 0;
    }

    @Override
    public void deactivateComponent() {
        nbActivated = 0;
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation;

        // Music block armor stand
        baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(MUSIC_BLOCK_AS, baseLocation, componentFace.getDefaultRotation(componentType), getArmorStandItem(), false, false, null);
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();
        area.getPlayersInsideArea().forEach(p -> PLAYERS_LIST_SONG_MANAGER.fadeOut(p, 50));
    }

    private Item getArmorStandItem() {
        if (loop && !stopOnExit) {
            return Item.MUSIC_BLOCK_LOOP_N_KEEP_PLAYING_ON_EXIT;
        }
        if (loop) {
            return Item.MUSIC_BLOCK_LOOP;
        }
        if (!stopOnExit) {
            return Item.MUSIC_BLOCK_KEEP_PLAYING_ON_EXIT;
        }
        return Item.MUSIC_BLOCK;
    }

    public void onPlayerEnteredArea(Player p) {
        if (isActivated()) startSong(p);
    }

    private void startSong(Player player, boolean loop, boolean stopOnExit) {
        PLAYERS_LIST_SONG_MANAGER.startSong(player, songFileName, loop, stopOnExit);
    }

    private void startSong(Player player) {
        startSong(player, loop, stopOnExit);
    }

    private void startSong(boolean loop, boolean stopOnExit) {
        area.getPlayersInsideArea().forEach(player -> startSong(player, loop, stopOnExit));
    }

    private void startSong() {
        area.getPlayersInsideArea().forEach(this::startSong);
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
