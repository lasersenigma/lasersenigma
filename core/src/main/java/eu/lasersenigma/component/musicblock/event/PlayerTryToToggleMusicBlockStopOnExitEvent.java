package eu.lasersenigma.component.musicblock.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.component.musicblock.MusicBlock;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToToggleMusicBlockStopOnExitEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final MusicBlock component;

    public PlayerTryToToggleMusicBlockStopOnExitEvent(Player player, MusicBlock component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public MusicBlock getMusicBlock() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
