package eu.lasersenigma.component.mirrorchest.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToDecreaseMirrorChestEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final MirrorChest mirrorChest;

    public PlayerTryToDecreaseMirrorChestEvent(Player player, MirrorChest mirrorChest) {
        super();
        this.player = player;
        this.mirrorChest = mirrorChest;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public MirrorChest getMirrorChest() {
        return mirrorChest;
    }

    @Override
    public IComponent getComponent() {
        return mirrorChest;
    }

}
