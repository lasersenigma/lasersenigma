package eu.lasersenigma.component.mirrorchest;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.AComponent;
import eu.lasersenigma.component.ComponentFace;
import eu.lasersenigma.component.IColorableComponent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * A chest containing (colored) mirror(s)
 */
public final class MirrorChest extends AComponent implements IColorableComponent {

    /**
     * the facing of the chest
     */
    private final ComponentFace face;
    /**
     * the number of mirrors this component contains
     */
    private int nbMirrors;
    /**
     * the color of this component as saved in database
     */
    private LasersColor color;
    /**
     * current color of this component (not saved in the database)
     */
    private LasersColor currentColor;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this mirror chest
     * @param componentId the id of the component inside the database
     * @param location    the location of this mirror chest
     * @param nbMirrors   the number of mirrors this mirror chest contains
     * @param color       the color of this mirror chest
     * @param face        the component facing
     */
    public MirrorChest(Area area, int componentId, Location location, int nbMirrors, LasersColor color, ComponentFace face) {
        super(area, componentId, location, ComponentType.MIRROR_CHEST);
        this.nbMirrors = nbMirrors;
        this.color = color;
        currentColor = color;
        this.face = face;
    }

    /**
     * Constructor
     *
     * @param area      the area containing this mirror chest
     * @param location  the location of this mirror chest
     * @param nbMirrors the number of mirrors this mirror chest contains
     * @param color     the color of this mirror chest
     * @param face      the component facing
     */
    public MirrorChest(Area area, Location location, int nbMirrors, LasersColor color, ComponentFace face) {
        super(area, location, ComponentType.MIRROR_CHEST);
        this.nbMirrors = nbMirrors;
        this.color = color;
        currentColor = color;
        this.face = face;
        dbCreate();
    }

    /**
     * Constructor
     *
     * @param area      the area containing this mirror chest
     * @param location  the location of this mirror chest
     * @param nbMirrors the number of mirrors this mirror chest contains
     * @param face      the component facing
     */
    public MirrorChest(Area area, Location location, int nbMirrors, ComponentFace face) {
        this(area, location, nbMirrors, LasersColor.WHITE, face);
    }

    public ComponentFace getFace() {
        return face;
    }

    /**
     * gets the number of mirrors this chest contains
     *
     * @return the number of mirrors this chest contains
     */
    public int getNbMirrors() {
        return nbMirrors;
    }

    /**
     * sets the number of mirrors this chest will contain
     *
     * @param nbMirrors the number of mirrors this chest will contain
     */
    public void setNbMirrors(int nbMirrors) {
        this.nbMirrors = nbMirrors;
        showOrUpdateComponent();
        dbUpdate();
    }

    @Override
    public void activateComponent() {
        this.currentColor = this.color;
    }

    @Override
    public void deactivateComponent() {
        this.currentColor = this.color;
    }

    /**
     * updates this mirror chest
     */
    @Override
    public void showOrUpdateComponent() {
        Block b = getComponentLocation().getBlock();
        ItemStack item = ItemsFactory.getInstance().getItemStack(Item.getMirror(currentColor));
        if (b.getType() != Material.CHEST) {
            b.setType(Material.CHEST);
        }
        b.getState().update(true);
        Chest chest = (Chest) b.getState();
        chest.setData(new org.bukkit.material.Chest(face.getBlockFace()));
        //chest.setCustomName(Item.MIRROR_CHEST.getTranslatedName());
        chest.update();
        Chest chestTmp = (Chest) b.getState();
        Inventory inv = chestTmp.getInventory();
        inv.clear();
        for (int i = 0; i < nbMirrors; i++) {
            inv.addItem(item.clone());

        }
        b.getState().update();
    }

    /**
     * deletes this mirror chest
     */
    /*@Override
    public void hideComponent() {
        Block b = getComponentLocation().getBlock();
        BlockState state = b.getState();
        if (state instanceof Chest) {
            Chest chest = (Chest) state;
            chest.getBlockInventory().clear();
            b.getState().update();
        }
        b.setType(Material.AIR);
        b.getState().update();
    }*/

    /**
     * Resets the component (when the last player leaves the area)
     */
    /*@Override
    public void resetComponent() {
        this.currentColor = this.color;
    }*/

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        changeColor(true);
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of FilteringSpheres is defined by
     *             its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(currentColor.getNextColor(true), save);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is ignored since the color of FilteringSpheres is defined by
     *              its mirror so it's never saved in database.
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        this.currentColor = color;
        if (save) {
            this.color = color;
        }
        showOrUpdateComponent();
        if (save) {
            dbUpdate();
        }
    }

    @Override
    public LasersColor getColorCurrent() {
        return this.currentColor;
    }

    @Override
    public LasersColor getColor() {
        return color;
    }

    /**
     * Set the color of this component
     *
     * @param colorCurrent The new color of this component
     */
    @Override
    public void setColor(LasersColor colorCurrent) {
        setColor(colorCurrent, true);
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
