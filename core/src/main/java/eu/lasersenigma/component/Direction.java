package eu.lasersenigma.component;

import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.util.Vector;

/**
 * Represents a direction in 3D space, extending the {@link Vector} class.
 * This class is designed for handling directional calculations for laser particles.
 */
public class Direction extends Vector implements Cloneable {

    /**
     * Constructs a Direction instance from the specified radial yaw and pitch angles
     *
     * @param radialYaw   The yaw angle in radians
     * @param radialPitch The pitch angle in radians
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Direction(double radialYaw, double radialPitch) {
        super(Math.sin(radialPitch) * Math.cos(radialYaw), Math.sin(radialPitch) * Math.sin(radialYaw), Math.cos(radialPitch));
        normalize();
        multiply(LaserParticle.LASERS_SPEED);
    }

    /**
     * Constructs a Direction instance from the specified x, y, and z components
     *
     * @param x The x-component
     * @param y The y-component
     * @param z The z-component
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Direction(double x, double y, double z) {
        super(x, y, z);
        normalize();
        multiply(LaserParticle.LASERS_SPEED);
    }

    /**
     * Constructs a Direction instance from a Vector
     *
     * @param v The input Vector
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Direction(Vector v) {
        super(v.getX(), v.getY(), v.getZ());
        normalize();
        multiply(LaserParticle.LASERS_SPEED);
    }

    /**
     * Checks if the input angle is within an acceptable range based on a maximum radial angle
     *
     * @param inputAngle     The input angle to compare
     * @param maxRadialAngle The maximum allowed radial angle
     * @return True if the input angle is accepted, false otherwise
     */
    public boolean isAcceptedInputAngleFor(Vector inputAngle, double maxRadialAngle) {
        float angle = this.clone().multiply(-1).angle(inputAngle);
        return (Float.isNaN(angle) || angle < maxRadialAngle);
    }

    @Override
    public Direction clone() {
        Object o = super.clone();
        return (Direction) o;
    }
}
