package eu.lasersenigma.component.gravitationalsphere.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;

public class GravitationalSphereGravityStrengthChangeEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final GravitationalSphere gravitationalSphere;

    public GravitationalSphereGravityStrengthChangeEvent(GravitationalSphere gravitationalSphere) {
        this.gravitationalSphere = gravitationalSphere;
    }

    @Override
    public GravitationalSphere getComponent() {
        return gravitationalSphere;
    }
}
