package eu.lasersenigma.component.gravitationalsphere.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeAttractionRepulsionSphereModeEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final GravitationalSphere component;

    public PlayerTryToChangeAttractionRepulsionSphereModeEvent(Player player, GravitationalSphere component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public GravitationalSphere getAttractionRepulsionSphere() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
