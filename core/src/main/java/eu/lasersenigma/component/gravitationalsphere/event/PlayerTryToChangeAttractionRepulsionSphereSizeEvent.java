package eu.lasersenigma.component.gravitationalsphere.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeAttractionRepulsionSphereSizeEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final GravitationalSphere component;

    private final int oldSize;

    private final int newSize;

    public PlayerTryToChangeAttractionRepulsionSphereSizeEvent(Player player, GravitationalSphere component, int oldSize, int newSize) {
        super();
        this.player = player;
        this.component = component;
        this.oldSize = oldSize;
        this.newSize = newSize;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public GravitationalSphere getAttractionRepulsionSphereComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public int getOldSize() {
        return oldSize;
    }

    public int getNewSize() {
        return newSize;
    }

}
