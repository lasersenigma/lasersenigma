package eu.lasersenigma.component.gravitationalsphere;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.gravitationalsphere.event.GravitationalSphereGravityStrengthChangeEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Gravitational Sphere
 */
public final class GravitationalSphere extends AArmorStandComponent implements IPlayerModifiableComponent {

    private final HashMap<Location, HashMap<Direction, Vector>> cache;

    private int gravityStrength;
    private int currentGravityStrength;
    private GravitationalSphereMode gravitationalSphereMode;

    // Component's armor stand
    private final ComponentArmorStand GRAVITATIONAL_SPHERE_AS = new ComponentArmorStand(this, "gravitational_sphere");

    // Component's options
    // Empty

    // Component's events listener
    // private final Listener eventsListener = new GravitationalSphereEventsListener(this);

    /**
     * Constructor used for creation from the database
     *
     * @param area        The area containing this laser receiver
     * @param componentId The id of the component inside the database
     * @param location    The location of the laser receiver
     * @param face        The blockface of the component
     * @param rotation    The rotation of the component
     * @param gravitationalSphereMode        The mode (attraction or repulsion)
     * @param gravityStrength        The size of the sphere where laser particles will be affected
     */
    public GravitationalSphere(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, GravitationalSphereMode gravitationalSphereMode, int gravityStrength) {
        super(area, componentId, location, ComponentType.ATTRACTION_REPULSION_SPHERE, face, rotation);
        cache = new HashMap<>();
        this.gravityStrength = gravityStrength;
        this.currentGravityStrength = gravityStrength;
        this.gravitationalSphereMode = gravitationalSphereMode;
        getArea().clearAttractionRepulsionCache();
    }

    /**
     * Constructor
     *
     * @param area     The area containing this
     * @param location The location of the component
     * @param face     The blockface of the component
     */
    public GravitationalSphere(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.ATTRACTION_REPULSION_SPHERE, face);
        cache = new HashMap<>();
        gravitationalSphereMode = GravitationalSphereMode.ATTRACTION;
        gravityStrength = 4;
        this.currentGravityStrength = gravityStrength;
        getArea().clearAttractionRepulsionCache();
        dbCreate();
    }

    /**
     * Retrieves gravity strength
     *
     * @return Gravity strength
     */
    public int getGravityStrength() {
        return gravityStrength;
    }

    /**
     * Retrieves current gravity strength
     *
     * @return Current gravity strength
     */
    public int getCurrentGravityStrength() {
        return currentGravityStrength;
    }

    /**
     * Changes gravity strength
     *
     * @param gravityStrength Desired new gravity strength
     * @param save Should this action be saved on database?
     */
    public void setGravityStrength(int gravityStrength, boolean save) {
        this.currentGravityStrength = gravityStrength;
        showSphere();
        cache.clear();
        getArea().clearAttractionRepulsionCache();

        GravitationalSphereGravityStrengthChangeEvent gravitationalSphereGravityStrengthChangeEvent = new GravitationalSphereGravityStrengthChangeEvent(this);
        Bukkit.getPluginManager().callEvent(gravitationalSphereGravityStrengthChangeEvent);

        if (save) {
            this.gravityStrength = currentGravityStrength;
            dbUpdate();
        }
    }

    /**
     * Retrieves gravitational sphere mode
     *
     * @return Gravitational sphere mode
     */
    public GravitationalSphereMode getGravitationalSphereMode() {
        return this.gravitationalSphereMode;
    }

    /**
     * Changes gravitational sphere mode
     *
     * @param gravitationalSphereMode Gravitational sphere mode
     */
    public void changeMode(GravitationalSphereMode gravitationalSphereMode) {
        this.gravitationalSphereMode = gravitationalSphereMode;
        cache.clear();
        getArea().clearAttractionRepulsionCache();
        showOrUpdateComponent();
        showSphere();
        dbUpdate();
    }

    @Override
    public void activateComponent() {
        currentGravityStrength = gravityStrength;
        cache.clear();
        getArea().clearAttractionRepulsionCache();
    }

    @Override
    public void deactivateComponent() {
        currentGravityStrength = gravityStrength;
        cache.clear();
        getArea().clearAttractionRepulsionCache();
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(GRAVITATIONAL_SPHERE_AS, baseLocation, rotation, getItem(), false, false, null);
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();
        currentGravityStrength = gravityStrength;
        cache.clear();
        getArea().clearAttractionRepulsionCache();
    }

    private Item getItem() {
        return switch (gravitationalSphereMode) {
            case ATTRACTION -> Item.ATTRACTION_SPHERE;
            case REPULSION -> Item.REPULSION_SPHERE;
            default -> null;
        };
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }

    public Vector getTrajectoryModicationVector(Location location, Direction direction) {

        Vector trajectoryModicationVector;

        double cx = getASHeadCenterLocation().getX();
        double cy = getASHeadCenterLocation().getY();
        double cz = getASHeadCenterLocation().getZ();
        double px = location.getX();
        double py = location.getY();
        double pz = location.getZ();

        double distance = getASHeadCenterLocation().distance(location);
        if (gravitationalSphereMode == GravitationalSphereMode.REPULSION) {
            trajectoryModicationVector = new Vector(
                    (px - cx),
                    (py - cy),
                    (pz - cz)
            );
        } else {
            trajectoryModicationVector = new Vector(
                    (cx - px),
                    (cy - py),
                    (cz - pz)
            );
        }
        double gravityMultiplier = LaserParticle.LASERS_SPEED * 0.5 * ((currentGravityStrength - distance) / currentGravityStrength);
        return trajectoryModicationVector.normalize().multiply(gravityMultiplier);
    }

    public void showSphere() {
        Location baseLocation = getASHeadCenterLocation();
        double nbParticlesSquared = 50;
        double TAU = 2 * Math.PI;
        double increment = TAU / nbParticlesSquared;

        for (double angle1 = 0; angle1 < TAU; angle1 += increment) {
            double y = (Math.cos(angle1) * currentGravityStrength);
            double tmpRadius = Math.sin(angle1);

            for (double angle2 = 0; angle2 < TAU; angle2 += increment) {
                double x = Math.cos(angle2) * tmpRadius * currentGravityStrength;
                double z = Math.sin(angle2) * tmpRadius * currentGravityStrength;

                LaserParticle.playEffect(
                        getArea().getPlayersInsideArea(),
                        new Location(baseLocation.getWorld(), baseLocation.getX() + x, baseLocation.getY() + y, baseLocation.getZ() + z),
                        Particle.REDSTONE,
                        LasersColor.BLACK.getBukkitColor());
            }
        }
    }
}
