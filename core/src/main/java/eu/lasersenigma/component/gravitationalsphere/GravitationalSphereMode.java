package eu.lasersenigma.component.gravitationalsphere;

/**
 * Enumeration representing modes for attraction and repulsion in a sphere
 */
public enum GravitationalSphereMode {

    ATTRACTION("messages.attraction_mode"),
    REPULSION("messages.repulsion_mode");

    private final String messageCode;

    /**
     * Constructor for AttractionRepulsionSphereMode enum
     *
     * @param messageCode The corresponding message for the mode
     */
    GravitationalSphereMode(String messageCode) {
        this.messageCode = messageCode;
    }

    /**
     * Gets the corresponding message for the mode
     *
     * @return The corresponding message
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Gets the next mode in the enum sequence
     *
     * @return The next mode
     */
    public GravitationalSphereMode next() {
        int modesCount = GravitationalSphereMode.values().length;
        int nextModeIndex = (this.ordinal() + 1) % modesCount;
        return GravitationalSphereMode.values()[nextModeIndex];
    }

}
