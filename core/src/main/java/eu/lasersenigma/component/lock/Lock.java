package eu.lasersenigma.component.lock;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.lock.event.LockPlayerOpenEvent;
import eu.lasersenigma.component.lock.task.KeyChestObtainAnimationTask;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Represents a lock component.
 */
public final class Lock extends AArmorStandComponent {

    private final boolean DEFAULT_IS_OPENED = false;
    private final Rotation DEFAULT_ROTATION = componentFace.getDefaultRotation(componentType);

    /**
     * All key chests associated with the lock
     */
    private final HashSet<LockKeyChest> lockKeyChests = new HashSet<>();

    /**
     * All key chests found by players
     */
    private final HashMap<LEPlayer, HashSet<LockKeyChest>> playerFoundKeyChests = new HashMap<>();

    /**
     * The lock state (opened or closed) as it was previously saved
     */
    private boolean isOpened = DEFAULT_IS_OPENED;

    /**
     * The lock state (opened or closed)
     */
    private boolean isOpenedCurrent = DEFAULT_IS_OPENED;

    /**
     * Component's armor stands
     */
    private final ComponentArmorStand LOCK_AS = new ComponentArmorStand(this, "lock");

    /**
     * Component option : Clear keys on death
     */
    public ComponentOption CLEAR_KEYS_ON_DEATH = new ComponentOption(this, "CLEAR_KEYS_ON_DEATH", 1, LasersEnigmaPlugin.getInstance().getConfig().getBoolean("clear_keys_on_death"));

    /**
     * Component option : Clear keys on server quit
     */
    public ComponentOption CLEAR_KEYS_ON_QUIT = new ComponentOption(this, "CLEAR_KEYS_ON_SERVER_QUIT", 2, LasersEnigmaPlugin.getInstance().getConfig().getBoolean("clear_keys_on_quit"));

    /**
     * Component option : Clear keys on world change
     */
    public ComponentOption CLEAR_KEYS_ON_WORLD_CHANGE = new ComponentOption(this, "CLEAR_KEYS_ON_WORLD_CHANGE", 3, LasersEnigmaPlugin.getInstance().getConfig().getBoolean("clear_keys_on_change_world"));

    /**
     * Component option : Clear keys on unlock
     */
    public ComponentOption CLEAR_KEYS_ON_UNLOCK = new ComponentOption(this, "CLEAR_KEYS_ON_UNLOCK", 4, LasersEnigmaPlugin.getInstance().getConfig().getBoolean("opening_locks_consumes_keys"));

    /**
     * Constructor used for creation from the database
     *
     * @param area              The area containing this component
     * @param componentId       The id of the component inside the database
     * @param componentLocation The location of this component
     * @param componentFace     The face the component is on
     */
    public Lock(Area area, int componentId, Location componentLocation, ComponentFace componentFace) {
        super(area, componentId, componentLocation, ComponentType.LOCK, componentFace, componentFace.getDefaultRotation(ComponentType.LOCK));
        rotationCurrent = DEFAULT_ROTATION;
    }

    /**
     * Constructor
     *
     * @param area              The area containing this component
     * @param componentLocation The location of this component
     * @param componentFace     The face the component is on
     */
    public Lock(Area area, Location componentLocation, ComponentFace componentFace) {
        super(area, componentLocation, ComponentType.LOCK, componentFace);
        rotationCurrent = DEFAULT_ROTATION;
        dbCreate();
    }

    /**
     * Add a new key chest to the current lock
     *
     * @param lockKeyChest The key chest to add
     */
    public void addKeyChest(LockKeyChest lockKeyChest) {
        this.lockKeyChests.add(lockKeyChest);
    }

    /**
     * Remove a key chest from the current lock
     *
     * @param lockKeyChest The key chest to remove
     */
    public void removeKeyChest(LockKeyChest lockKeyChest) {
        // Remove key chest from player's found key chests
        playerFoundKeyChests.forEach((p, kSet) -> kSet.removeIf(k -> k == lockKeyChest));

        this.lockKeyChests.remove(lockKeyChest);
    }

    /**
     * Get all associated key chests of the current lock
     *
     * @return Associated key chests
     */
    public HashSet<LockKeyChest> getKeyChests() {
        return lockKeyChests;
    }

    /**
     * Get number of required key chests
     *
     * @return Number of required key chest
     */
    public int getRequiredKeyChestsCount() {
        return lockKeyChests.size();
    }

    /**
     * Attempts to open the lock by checking if the player has found all required key chests
     *
     * @param player The player attempting to open the lock
     */
    public void tryOpenLock(LEPlayer player) {
        // Check if the lock has already been isOpened
        if (isOpenedCurrent) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.lock_already_opened");
            return;
        }

        // Check if map contains player
        playerFoundKeyChests.putIfAbsent(player, new HashSet<>());

        // Check if all key chests has been found (at least one of each colour)
        if (!canUnlock(player)) {
            String playerFoundKeyChests = String.valueOf(getFoundKeyChestsCount(player));
            String requiredKeyChests = String.valueOf(getRequiredKeyChestsCount());

            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.lock_need_more_key", playerFoundKeyChests, requiredKeyChests);
            return;
        }

        isOpenedCurrent = true;

        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.lock_opened");
        startLockOpeningAnimations(player);
        showOrUpdateComponent();

        // Fire LockPlayerOpenEvent
        LockPlayerOpenEvent lockPlayerOpenEvent = new LockPlayerOpenEvent(this, player.getBukkitPlayer());
        Bukkit.getPluginManager().callEvent(lockPlayerOpenEvent);
    }

    private boolean canUnlock(LEPlayer player) {
        return hasAtLeastOneKeyChestOfEachColour(player);
    }

    /**
     * Marks a key chest as found by the player and performs related actions.
     *
     * @param player       the player marking the key chest as found
     * @param lockKeyChest the key chest to be marked as found
     */
    public void markKeyChestAsFound(LEPlayer player, LockKeyChest lockKeyChest) {
        // Check if the lock has already been isOpened
        if (isOpenedCurrent) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.lock_already_opened");
            return;
        }

        // Check if map contains player
        playerFoundKeyChests.putIfAbsent(player, new HashSet<>());

        // Check if key chest has already been found
        if (playerFoundKeyChests.get(player).contains(lockKeyChest)) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.key_already_possed");

            if (getRemainingKeyChestsCount(player) == 0) {
                TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.key_s_lock_needs_no_more_keys");
            }

            return;
        }

        // Add key chest to player's found key chest
        playerFoundKeyChests.get(player).add(lockKeyChest);

        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.key_obtained");
        lockKeyChest.startKeyObtainingAnimations(player);

        String playerFoundKeyChests = String.valueOf(getFoundKeyChestsCount(player));
        int leftKeyChests = getRemainingKeyChestsCount(player);

        if (leftKeyChests > 0) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.key_s_lock_needs_more_keys", playerFoundKeyChests, leftKeyChests);
        } else {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.key_s_lock_needs_no_more_keys");
        }
    }

    /**
     * Unmarks a key chest as not obtained by the player.
     *
     * @param player       the player unmarking the key chest
     * @param lockKeyChest the key chest to be unmarked
     * @return true if the key chest was successfully unmarked, false otherwise
     */
    public boolean unmarkKeyChestAsFound(LEPlayer player, LockKeyChest lockKeyChest) {
        return playerFoundKeyChests.get(player).remove(lockKeyChest);
    }

    /**
     * Clears the found key chests of a player.
     *
     * @param player the player whose found key chests to clear
     */
    public void clearFoundKeyChestsForPlayer(LEPlayer player) {
        Optional<Set<LockKeyChest>> foundChests = getFoundKeyChestsForPlayer(player);

        if (foundChests.isEmpty()) return;

        if (foundChests.get().size() == 1 && isOpenedCurrent) {
            // resetComponent();
            isOpenedCurrent = false;
            getArea().setPower(componentLocation, false);
        }
        foundChests.get().clear();
    }

    /**
     * Get found key chests of a player.
     *
     * @param player player
     * @return set of all found key chest
     */
    public Optional<Set<LockKeyChest>> getFoundKeyChestsForPlayer(LEPlayer player) {
        return Optional.ofNullable(playerFoundKeyChests.get(player));
    }

    /**
     * Get number of found key chests.
     *
     * @param player player
     * @return number of found key chests
     */
    public int getFoundKeyChestsCount(LEPlayer player) {
        return playerFoundKeyChests.get(player).size();
    }

    /**
     * Gets the number of remaining key chests for a player.
     *
     * @param player the player
     * @return the number of remaining key chests
     */
    public int getRemainingKeyChestsCount(LEPlayer player) {
        return lockKeyChests.size() - playerFoundKeyChests.get(player).size();
    }

    public Set<Integer> getRequiredColours() {
        Set<Integer> requiredColours = new HashSet<>();

        lockKeyChests.forEach(keyChest -> requiredColours.add(keyChest.getKeyNumber()));

        return requiredColours;
    }

    public Set<Integer> getFoundColoursForPlayer(LEPlayer player) {
        Set<Integer> playerColours = new HashSet<>();
        Set<LockKeyChest> playerLockKeyChests = playerFoundKeyChests.get(player);

        playerLockKeyChests.forEach(keyChest -> playerColours.add(keyChest.getKeyNumber()));

        return playerColours;
    }

    public boolean hasAtLeastOneKeyChestOfEachColour(LEPlayer player) {
        Set<Integer> requiredColours = getRequiredColours();
        Set<Integer> foundColours = getFoundColoursForPlayer(player);

        return foundColours.containsAll(requiredColours);
    }

    /**
     * Counts the number of key chests with a specific key number.
     *
     * @param keyNumber the key number to count
     * @return the number of key chests with the specified key number
     */
    public int countKeyChestsByNumber(int keyNumber) {
        return (int) lockKeyChests.stream()
                .filter(keyChest -> keyChest.getKeyNumber() == keyNumber)
                .count();
    }

    /**
     * Check if the current lock is open.
     *
     * @return true if the lock is open, false if not
     */
    public boolean isOpenedCurrent() {
        return this.isOpenedCurrent;
    }

    @Override
    public void activateComponent() {
        // Refresh redstone state if lock was previously unlocked
        if (!isOpened) return;

        isOpenedCurrent = true;
        isOpened = false;
        getArea().setPower(componentLocation, true);
    }

    @Override
    public void deactivateComponent() {
        if (isOpenedCurrent) isOpened = true;
        isOpenedCurrent = false;
        getArea().setPower(componentLocation, false);
    }

    @Override
    public void deleteComponent() {
        super.deleteComponent();

        // Remove all related key chests
        lockKeyChests.forEach(component -> area.removeComponent(component));

        // Remove related armor stand
        removeArmorStandDisplay();

        // Remove lock from database
        dbRemove();

        // Remove redstone power
        getArea().setPower(componentLocation, false);

        /*new HashSet<>(lockKeyChests).forEach(keyChest -> keyChest.getArea().removeComponent(keyChest));
        this.removeArmorStandDisplay();
        dbRemove();
        getArea().setPower(componentLocation, false);*/
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(LOCK_AS, baseLocation, rotationCurrent, getHelmetItem(), false, false, componentArmorStand -> {
            getArea().setPower(componentLocation, isOpenedCurrent);
        });
    }

    private Item getHelmetItem() {
        return isOpenedCurrent ? Item.LOCK_ACTIVATED : Item.LOCK_DEACTIVATED;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }

    /**
     * Starts lock opening animations.
     *
     * @param player the player opening the lock
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void startLockOpeningAnimations(LEPlayer player) {
        SoundLauncher.playSound(componentLocation, PlaySoundCause.OPEN_LOCK);
        new KeyChestObtainAnimationTask(player.getBukkitPlayer());
    }
}
