package eu.lasersenigma.component.lock.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Optional;
import java.util.Set;

public class LockEventsListener implements Listener {

    public LockEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Area area = Areas.getInstance().getAreaFromLocation(event.getEntity().getLocation());

        if (area == null) return;

        Set<Lock> locks = area.getComponents(Lock.class);

        locks.forEach(lock -> {

            LEPlayer player = LEPlayers.getInstance().findLEPlayer(event.getEntity());

            if (lock.CLEAR_KEYS_ON_UNLOCK.isEnabled()) {

                if (lock.CLEAR_KEYS_ON_DEATH.isEnabled()) {

                    if (lock.isOpenedCurrent()) {
                        removeWorldKeys(lock, player);
                    }
                }
            } else {

                if (lock.CLEAR_KEYS_ON_DEATH.isEnabled()) {
                    removeWorldKeys(lock, player);
                }
            }
        });
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Area area = Areas.getInstance().getAreaFromLocation(event.getPlayer().getLocation());

        if (area == null) return;

        Set<Lock> locks = area.getComponents(Lock.class);

        locks.forEach(lock -> {
            LEPlayer player = LEPlayers.getInstance().findLEPlayer(event.getPlayer());

            if (lock.CLEAR_KEYS_ON_UNLOCK.isEnabled()) {

                if (lock.CLEAR_KEYS_ON_QUIT.isEnabled()) {

                    if (lock.isOpenedCurrent()) {
                        removeAllKeys(lock, player);
                    }
                }
            } else {

                if (lock.CLEAR_KEYS_ON_QUIT.isEnabled()) {
                    removeAllKeys(lock, player);
                }
            }
        });
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerWorldChange(PlayerChangedWorldEvent event) {
        Area area = Areas.getInstance().getAreaFromLocation(event.getPlayer().getLocation());

        if (area == null) return;

        Set<Lock> locks = area.getComponents(Lock.class);

        locks.forEach(lock -> {

            LEPlayer player = LEPlayers.getInstance().findLEPlayer(event.getPlayer());

            if (lock.CLEAR_KEYS_ON_UNLOCK.isEnabled()) {

                if (lock.CLEAR_KEYS_ON_WORLD_CHANGE.isEnabled()) {

                    if (lock.isOpenedCurrent()) {
                        removeWorldKeys(lock, player);
                    }
                }
            } else {

                if (lock.CLEAR_KEYS_ON_WORLD_CHANGE.isEnabled()) {
                    removeWorldKeys(lock, player);
                }
            }
        });
    }

    private void removeAllKeys(Lock lock, LEPlayer player) {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(player.getUniqueId());
        lock.clearFoundKeyChestsForPlayer(player);
    }

    private void removeWorldKeys(Lock lock, LEPlayer player) {
        Optional<Set<LockKeyChest>> foundKeyChests = lock.getFoundKeyChestsForPlayer(player);

        if (foundKeyChests.isEmpty()) return;

        LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(player.getUniqueId(), foundKeyChests.get());
        lock.clearFoundKeyChestsForPlayer(player);
    }
}
