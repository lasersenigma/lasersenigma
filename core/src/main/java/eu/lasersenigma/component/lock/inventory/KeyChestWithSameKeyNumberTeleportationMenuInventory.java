package eu.lasersenigma.component.lock.inventory;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.common.inventory.ComponentMenuInventory;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * @author Benjamin
 */
public class KeyChestWithSameKeyNumberTeleportationMenuInventory extends AOpenableInventory {

    private final Lock lock;

    private final TreeMap<Item, LockKeyChest> keyChestsItems;

    public KeyChestWithSameKeyNumberTeleportationMenuInventory(LEPlayer player, Lock lock, ArrayList<LockKeyChest> lockKeyChestsClicked) {
        super(player, "messages.teleportation_menu_title");
        this.lock = lock;
        keyChestsItems = new TreeMap<>();
        for (int i = 0; i < lockKeyChestsClicked.size(); i++) {
            keyChestsItems.put(ComponentMenuInventory.getNumberAsItems(i, false).get(0), lockKeyChestsClicked.get(i));
            if (i > 9) {
                break;
            }
        }
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> res = new ArrayList<>();
        if (keyChestsItems.size() < 9) {
            res.add(new ArrayList<>(keyChestsItems.keySet()));
        } else {
            res.add(new ArrayList<>(keyChestsItems.subMap(Item.COMPONENT_MENU_ZERO, Item.COMPONENT_MENU_FIVE).keySet()));
            res.add(new ArrayList<>(keyChestsItems.subMap(Item.COMPONENT_MENU_SIX, Item.COMPONENT_MENU_NINE).keySet()));
        }
        return res;
    }

    @Override
    public void onClick(Item item) {
        Entry<Item, LockKeyChest> res = keyChestsItems.entrySet().stream()
                .filter(e -> e.getKey() == item)
                .findAny().orElse(null);
        if (res == null) {
            return;
        }
        player.teleportToNearestSafeLocation(res.getValue().getComponentLocation());
        player.getInventoryManager().closeOpenedInventory();
    }

    @Override
    public boolean contains(Item item) {
        return keyChestsItems.containsKey(item);
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

    @Override
    public InventoryType getType() {
        return InventoryType.KEY_CHEST_WITH_SAME_KEY_NUMBER_TELEPORTATION_MENU;
    }

}
