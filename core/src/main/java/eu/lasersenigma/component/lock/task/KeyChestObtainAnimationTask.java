package eu.lasersenigma.component.lock.task;

import eu.lasersenigma.LasersEnigmaPlugin;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.Random;

/**
 * Task used when a player obtain a new key
 */
public class KeyChestObtainAnimationTask extends BukkitRunnable {

    private static final int HALF_TICK_DURATION = 30;
    private static final double RADIUS_INITIAL = 2;
    private static final double RADIUS_END = 0;
    private static final double RADIUS_DIFF = (RADIUS_END - RADIUS_INITIAL) / HALF_TICK_DURATION;
    private static final double LOC_Y_INITIAL = 0;
    private static final double LOC_Y_END = 2.5;
    private static final double LOC_Y_DIFF = (LOC_Y_END - LOC_Y_INITIAL) / HALF_TICK_DURATION;

    private final Player player;

    private int nbTimesRemaining = HALF_TICK_DURATION * 2;

    private int degree = 0;
    private double radius = RADIUS_INITIAL;
    private double locY = LOC_Y_INITIAL;

    public KeyChestObtainAnimationTask(Player player) {
        this.player = player;
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 0);
    }

    /**
     * the method called on each execution of the task
     */
    @Override
    public void run() {
        if (nbTimesRemaining < 1) {
            this.cancel();
            return;
        }
        Location particleloc = player.getLocation().clone();
        particleloc.add(0, locY, 0);
        double radian1 = Math.toRadians(degree % 360);
        double radian2 = Math.toRadians((degree + 120) % 360);
        double radian3 = Math.toRadians((degree + 240) % 360);
        HashSet<Location> particleLocations = new HashSet<>();
        particleLocations.add(particleloc.clone().add(Math.cos(radian1) * radius, 0, Math.sin(radian1) * radius));
        particleLocations.add(particleloc.clone().add(Math.cos(radian2) * radius, 0, Math.sin(radian2) * radius));
        particleLocations.add(particleloc.clone().add(Math.cos(radian3) * radius, 0, Math.sin(radian3) * radius));

        Random rand = new Random();
        particleLocations.forEach(loc -> {
            Particle.DustOptions dustOptions = new Particle.DustOptions(Color.fromRGB(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)), 2);
            loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 1, dustOptions);
        });

        if (nbTimesRemaining < 40) {
            locY = locY - LOC_Y_DIFF;
            radius = radius - RADIUS_DIFF;
        } else {
            locY = locY + LOC_Y_DIFF;
            radius = radius + RADIUS_DIFF;
        }
        degree = degree + 5;
        nbTimesRemaining--;
    }
}