package eu.lasersenigma.component.lock.inventory;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class KeyChestTeleportationMenuInventory extends AOpenableInventory {

    private final Lock lock;

    private final TreeMap<Integer, Item> keyChestsItemByKeyNumber;

    public KeyChestTeleportationMenuInventory(LEPlayer player, Lock lock) {
        super(player, "messages.teleportation_menu_title");
        this.lock = lock;
        keyChestsItemByKeyNumber = new TreeMap<>();
        lock.getKeyChests().forEach(keyChest -> keyChestsItemByKeyNumber.put(keyChest.getKeyNumber(), keyChest.getHelmetItem()));
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> res = new ArrayList<>();
        List<Item> line = new ArrayList<>();
        for (Item item : keyChestsItemByKeyNumber.values()) {
            line.add(item);
            if (line.size() > 8) {
                res.add(line);
                line = new ArrayList<>();
            }
        }
        if (!line.isEmpty()) {
            res.add(line);
        }
        return res;
    }

    @Override
    public void onClick(Item item) {
        ArrayList<LockKeyChest> lockKeyChestsClicked = lock.getKeyChests().stream()
                .filter(keyChest -> keyChest.getHelmetItem() == item)
                .collect(Collectors.toCollection(ArrayList::new));
        if (lockKeyChestsClicked.size() == 1) {
            player.teleportToNearestSafeLocation(lockKeyChestsClicked.get(0).getComponentLocation());
            player.getInventoryManager().closeOpenedInventory();
        } else {
            player.getInventoryManager().openLEInventory(new KeyChestWithSameKeyNumberTeleportationMenuInventory(player, lock, lockKeyChestsClicked));
        }
    }

    @Override
    public boolean contains(Item item) {
        return keyChestsItemByKeyNumber.containsValue(item);
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

    @Override
    public InventoryType getType() {
        return InventoryType.KEY_CHEST_TELEPORTATION_MENU;
    }

}
