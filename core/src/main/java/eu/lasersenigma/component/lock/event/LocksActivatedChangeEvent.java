package eu.lasersenigma.component.lock.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;

public class LocksActivatedChangeEvent extends AAfterActionEvent implements IAreaEvent {

    private final Area area;

    private final int oldNbActivatedLocks;

    private final int newNbActivatedLocks;

    public LocksActivatedChangeEvent(Area area, int oldNbActivatedLocks, int newNbActivatedLocks) {
        super();
        this.area = area;
        this.oldNbActivatedLocks = oldNbActivatedLocks;
        this.newNbActivatedLocks = newNbActivatedLocks;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public int getOldNbActivatedLocks() {
        return oldNbActivatedLocks;
    }

    public int getNewNbActivatedLocks() {
        return newNbActivatedLocks;
    }

}
