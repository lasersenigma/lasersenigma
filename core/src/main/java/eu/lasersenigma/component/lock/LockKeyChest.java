package eu.lasersenigma.component.lock;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.lock.task.KeyChestObtainAnimationTask;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Location;

/**
 * A chest containing a key relative to a specific lock
 */
public final class LockKeyChest extends AArmorStandComponent {

    private final Rotation DEFAULT_ROTATION = componentFace.getDefaultRotation(componentType);

    // The associated lock
    private final Lock lock;

    // The number of key
    private int keyNumber = 1;

    // Component's armor stands
    private ComponentArmorStand KEY_CHEST_AS = new ComponentArmorStand(this, "keyChest");

    /**
     * Constructor used for creation from database
     *
     * @param componentId the id of the component inside the database
     * @param location    the location of this mirror chest
     * @param keyNumber   the number of the key
     * @param lock        the corresponding lock
     * @param face        the component facing
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public LockKeyChest(Area area, Lock lock, int componentId, Location location, int keyNumber, ComponentFace face) {
        super(area, componentId, location, ComponentType.KEY_CHEST, face, face.getDefaultRotation(ComponentType.KEY_CHEST));
        this.lock = lock;
        this.keyNumber = keyNumber;
        this.lock.addKeyChest(this);
        rotationCurrent = DEFAULT_ROTATION;
    }

    /**
     * Constructor.
     *
     * @param lock     Corresponding lock
     * @param location Location of this mirror chest
     * @param face     Component facing
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public LockKeyChest(Area area, Lock lock, Location location, ComponentFace face) {
        super(area, location, ComponentType.KEY_CHEST, face);
        this.lock = lock;
        this.lock.addKeyChest(this);
        rotationCurrent = DEFAULT_ROTATION;
        dbCreate();
    }

    /**
     * Retrieves key chest's lock
     *
     * @return Key chest's lock
     */
    public Lock getLock() {
        return this.lock;
    }

    /**
     * Retrieves key chest's number
     *
     * @return Key chest's number
     */
    public int getKeyNumber() {
        return keyNumber;
    }

    /**
     * Defines key chest's number
     *
     * @param keyNumber Key chest's number
     */
    public void setKeyNumber(int keyNumber) {
        if (keyNumber > 25) {
            keyNumber = 25;
        } else if (keyNumber < 1) {
            keyNumber = 1;
        }
        this.keyNumber = keyNumber;
        // this.lock.resetComponent();
        dbUpdate();
        showOrUpdateComponent();
    }

    @Override
    public void activateComponent() {}

    @Override
    public void deactivateComponent() {}

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(KEY_CHEST_AS, baseLocation, rotation, getHelmetItem(), false, false, null);
    }

    public Item getHelmetItem() {
        return switch (keyNumber) {
            case 1 -> Item.KEY_CHEST_1;
            case 2 -> Item.KEY_CHEST_2;
            case 3 -> Item.KEY_CHEST_3;
            case 4 -> Item.KEY_CHEST_4;
            case 5 -> Item.KEY_CHEST_5;
            case 6 -> Item.KEY_CHEST_6;
            case 7 -> Item.KEY_CHEST_7;
            case 8 -> Item.KEY_CHEST_8;
            case 9 -> Item.KEY_CHEST_9;
            case 10 -> Item.KEY_CHEST_10;
            case 11 -> Item.KEY_CHEST_11;
            case 12 -> Item.KEY_CHEST_12;
            case 13 -> Item.KEY_CHEST_13;
            case 14 -> Item.KEY_CHEST_14;
            case 15 -> Item.KEY_CHEST_15;
            case 16 -> Item.KEY_CHEST_16;
            case 17 -> Item.KEY_CHEST_17;
            case 18 -> Item.KEY_CHEST_18;
            case 19 -> Item.KEY_CHEST_19;
            case 20 -> Item.KEY_CHEST_20;
            case 21 -> Item.KEY_CHEST_21;
            case 22 -> Item.KEY_CHEST_22;
            case 23 -> Item.KEY_CHEST_23;
            case 24 -> Item.KEY_CHEST_24;
            default -> Item.KEY_CHEST_25;
        };
    }

    /*@Override
    public void resetComponent() {}*/

    /**
     * Removes key chest from database
     */
    @Override
    public void deleteComponent() {
        super.deleteComponent();

        // Remove key chest from it's related lock
        lock.removeKeyChest(this);

        // Remove related armor stand
        removeArmorStandDisplay();

        // Remove key chest from player's database
        LEPlayers.getInstance().removeKeyChest(this);

        // Remove key chest from database
        dbRemove();
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void startKeyObtainingAnimations(LEPlayer player) {
        SoundLauncher.playSound(getComponentLocation(), PlaySoundCause.OBTAIN_KEY);
        new KeyChestObtainAnimationTask(player.getBukkitPlayer());
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
