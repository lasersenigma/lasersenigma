package eu.lasersenigma.component.lock.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.lock.Lock;
import org.bukkit.entity.Player;

public class LockPlayerOpenEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final Lock lock;

    private final Player player;

    public LockPlayerOpenEvent(Lock lock, Player player) {
        this.lock = lock;
        this.player = player;
    }

    @Override
    public Lock getComponent() {
        return lock;
    }

    public Player getPlayer() {
        return player;
    }
}
