package eu.lasersenigma.component.lock.inventory;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AShortcutBarInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.inventory.MainShortcutBarInventory;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.CrossableMaterials;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class KeyChestCreationShortcutBarInventory extends AShortcutBarInventory {

    private final Lock lock;

    public KeyChestCreationShortcutBarInventory(LEPlayer player, Lock lock) {
        super(player);
        this.lock = lock;
    }

    @Override
    protected ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.PLACE_KEY_CHEST_SHORTCUTBAR_PLACE);
        shortcutBar = addEmpty(shortcutBar, 7);
        shortcutBar.add(Item.PLACE_KEY_CHEST_SHORTCUTBAR_EXIT);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case PLACE_KEY_CHEST_SHORTCUTBAR_EXIT:
                player.getInventoryManager().openLEInventory(new MainShortcutBarInventory(player));
                break;
            case PLACE_KEY_CHEST_SHORTCUTBAR_PLACE:
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
                    return;
                }
                Area area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null || !area.isActivated()) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20);
                if (blocks.size() < 2) {
                    return;
                }
                ComponentController.addComponentFromBlockFace(ComponentType.KEY_CHEST, blocks.get(1), blocks.get(1).getFace(blocks.get(0)), player, lock);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new HashSet<>(Arrays.asList(
                Item.PLACE_KEY_CHEST_SHORTCUTBAR_PLACE,
                Item.PLACE_KEY_CHEST_SHORTCUTBAR_EXIT
        )).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

    @Override
    public InventoryType getType() {
        return InventoryType.KEY_CHEST_CREATION_SHORTCUTBAR;
    }

}
