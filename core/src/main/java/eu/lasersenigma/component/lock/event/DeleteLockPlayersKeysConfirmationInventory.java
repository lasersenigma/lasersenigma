package eu.lasersenigma.component.lock.event;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.PlayerController;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class DeleteLockPlayersKeysConfirmationInventory extends AOpenableInventory {

    private final Lock lock;

    public DeleteLockPlayersKeysConfirmationInventory(LEPlayer player, Lock lock) {
        super(player, "messages.lock_s_players_keys_deletion");
        this.lock = lock;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.HELP_LOCK_S_PLAYERS_KEYS_DELETE_CONFIRM)));
        inv.add(new ArrayList<>(Arrays.asList(Item.CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS, Item.EMPTY, Item.CANCEL_DELETE_LOCK_S_PLAYERS_KEYS)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEDeleteLockPlayersKeysConfirmationInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != lock.getArea()) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().closeOpenedInventory();
            }, 1);
            return;
        }
        switch (item) {
            case CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                PlayerController.removeLockKeysFromEveryPlayers(player, lock);
                break;
            case CANCEL_DELETE_LOCK_S_PLAYERS_KEYS:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.HELP_LOCK_S_PLAYERS_KEYS_DELETE_CONFIRM,
                Item.CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS,
                Item.CANCEL_DELETE_LOCK_S_PLAYERS_KEYS
        )).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.DELETE_LOCK_S_PLAYERS_KEYS_CONFIRM_MENU;
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

}
