package eu.lasersenigma.component.lock.inventory;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KeyChestSelectorMenuInventory extends AOpenableInventory {

    private final LockKeyChest lockKeyChest;

    public KeyChestSelectorMenuInventory(LEPlayer player, LockKeyChest lockKeyChest) {
        super(player, "messages.key_chest_selector_menu_title");
        this.lockKeyChest = lockKeyChest;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> res = new ArrayList<>();
        res.add(new ArrayList<>(Arrays.asList(Item.KEY_CHEST_1, Item.KEY_CHEST_2, Item.KEY_CHEST_3, Item.KEY_CHEST_4, Item.KEY_CHEST_5, Item.KEY_CHEST_6, Item.KEY_CHEST_7, Item.KEY_CHEST_8, Item.KEY_CHEST_9)));
        res.add(new ArrayList<>(Arrays.asList(Item.KEY_CHEST_10, Item.KEY_CHEST_11, Item.KEY_CHEST_12, Item.KEY_CHEST_13, Item.KEY_CHEST_14, Item.KEY_CHEST_15, Item.KEY_CHEST_16, Item.KEY_CHEST_17, Item.KEY_CHEST_18)));
        res.add(new ArrayList<>(Arrays.asList(Item.KEY_CHEST_19, Item.KEY_CHEST_20, Item.KEY_CHEST_21, Item.KEY_CHEST_22, Item.KEY_CHEST_23, Item.KEY_CHEST_24, Item.KEY_CHEST_25)));
        return res;
    }

    @Override
    public void onClick(Item item) {
        int keyNb;
        switch (item) {
            case KEY_CHEST_1:
                keyNb = 1;
                break;
            case KEY_CHEST_2:
                keyNb = 2;
                break;
            case KEY_CHEST_3:
                keyNb = 3;
                break;
            case KEY_CHEST_4:
                keyNb = 4;
                break;
            case KEY_CHEST_5:
                keyNb = 5;
                break;
            case KEY_CHEST_6:
                keyNb = 6;
                break;
            case KEY_CHEST_7:
                keyNb = 7;
                break;
            case KEY_CHEST_8:
                keyNb = 8;
                break;
            case KEY_CHEST_9:
                keyNb = 9;
                break;
            case KEY_CHEST_10:
                keyNb = 10;
                break;
            case KEY_CHEST_11:
                keyNb = 11;
                break;
            case KEY_CHEST_12:
                keyNb = 12;
                break;
            case KEY_CHEST_13:
                keyNb = 13;
                break;
            case KEY_CHEST_14:
                keyNb = 14;
                break;
            case KEY_CHEST_15:
                keyNb = 15;
                break;
            case KEY_CHEST_16:
                keyNb = 16;
                break;
            case KEY_CHEST_17:
                keyNb = 17;
                break;
            case KEY_CHEST_18:
                keyNb = 18;
                break;
            case KEY_CHEST_19:
                keyNb = 19;
                break;
            case KEY_CHEST_20:
                keyNb = 20;
                break;
            case KEY_CHEST_22:
                keyNb = 22;
                break;
            case KEY_CHEST_23:
                keyNb = 23;
                break;
            case KEY_CHEST_24:
                keyNb = 24;
                break;
            default:
                keyNb = 25;
                break;
        }
        ComponentController.setKeyNumber(player, lockKeyChest, keyNb);
        player.getInventoryManager().closeOpenedInventory();
    }

    @Override
    public boolean contains(Item item) {
        return Arrays.asList(
                Item.KEY_CHEST_1, Item.KEY_CHEST_2, Item.KEY_CHEST_3, Item.KEY_CHEST_4, Item.KEY_CHEST_5, Item.KEY_CHEST_6, Item.KEY_CHEST_7, Item.KEY_CHEST_8, Item.KEY_CHEST_9,
                Item.KEY_CHEST_10, Item.KEY_CHEST_11, Item.KEY_CHEST_12, Item.KEY_CHEST_13, Item.KEY_CHEST_14, Item.KEY_CHEST_15, Item.KEY_CHEST_16, Item.KEY_CHEST_17, Item.KEY_CHEST_18,
                Item.KEY_CHEST_19, Item.KEY_CHEST_20, Item.KEY_CHEST_21, Item.KEY_CHEST_22, Item.KEY_CHEST_23, Item.KEY_CHEST_24, Item.KEY_CHEST_25).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return lockKeyChest;
    }

    @Override
    public Area getArea() {
        return lockKeyChest.getArea();
    }

    @Override
    public InventoryType getType() {
        return InventoryType.KEY_CHEST_SELECTOR;
    }

}
