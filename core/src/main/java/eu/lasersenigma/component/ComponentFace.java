package eu.lasersenigma.component;

import eu.lasersenigma.common.items.ComponentType;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import java.util.Arrays;

/**
 * Enumeration representing component faces
 */
public enum ComponentFace {

    UP(BlockFace.UP, new Rotation(0, 0), new Vector(0, 1, 0)),
    DOWN(BlockFace.DOWN, new Rotation(Math.toRadians(90), 0), new Vector(0, -1, 0)),
    SOUTH(BlockFace.SOUTH, new Rotation(0, 0), new Vector(0, 0, 1)),
    NORTH(BlockFace.NORTH, new Rotation(0, Math.toRadians(180)), new Vector(0, 0, -1)),
    EAST(BlockFace.EAST, new Rotation(0, Math.toRadians(-90)), new Vector(1, 0, 0)),
    WEST(BlockFace.WEST, new Rotation(0, Math.toRadians(90)), new Vector(-1, 0, 0));

    private final BlockFace blockFace;
    private final Rotation rotation;
    private final Vector vector;

    /**
     * Constructs a component face with the specified parameters
     *
     * @param blockFace The associated BlockFace
     * @param rotation  The rotation associated with the face
     * @param vector    The vector representing the face direction
     */
    ComponentFace(BlockFace blockFace, Rotation rotation, Vector vector) {
        this.blockFace = blockFace;
        this.rotation = rotation;
        this.vector = vector.normalize();
    }

    /**
     * Retrieves the ComponentFace corresponding to a BlockFace
     *
     * @param blockFace The BlockFace to find
     * @return The corresponding ComponentFace, or null if not found
     */
    public static ComponentFace from(BlockFace blockFace) {
        return Arrays.stream(values())
                .filter(face -> blockFace == face.blockFace)
                .findFirst()
                .orElse(null);
    }

    /**
     * Gets the rotation associated with the face
     *
     * @return The rotation associated with the face
     */
    public Rotation getRotation() {
        return rotation;
    }

    /**
     * Gets the vector representing the face direction
     *
     * @return The vector representing the face direction
     */
    public Vector getVector() {
        return vector.clone();
    }

    /**
     * Gets the associated BlockFace
     *
     * @return The associated BlockFace
     */
    public BlockFace getBlockFace() {
        return blockFace;
    }

    /**
     * Gets the default rotation based on the component type
     *
     * @param type The component type
     * @return The default rotation
     */
    public Rotation getDefaultRotation(ComponentType type) {
        switch (type) {
            case FILTERING_SPHERE:
            case REFLECTING_SPHERE:
            case MIRROR_SUPPORT:
            case PRISM:
            case CALL_BUTTON:
                switch (this) {
                    case DOWN:
                        return new Rotation(Math.toRadians(180), 0);
                    case SOUTH:
                        return new Rotation(Math.toRadians(90), 0);
                    case NORTH:
                        return new Rotation(Math.toRadians(90), Math.toRadians(180));
                    case EAST:
                        return new Rotation(Math.toRadians(90), Math.toRadians(-90));
                    case WEST:
                        return new Rotation(Math.toRadians(90), Math.toRadians(90));
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        return rotation;
    }
}
