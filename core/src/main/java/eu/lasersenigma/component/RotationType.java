package eu.lasersenigma.component;

/**
 * Enumeration representing rotation types.
 */
public enum RotationType {
    LEFT,
    RIGHT,
    UP,
    DOWN;
}
