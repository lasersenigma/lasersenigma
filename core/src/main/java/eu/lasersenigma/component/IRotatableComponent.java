package eu.lasersenigma.component;

import eu.lasersenigma.component.scheduledactions.ActionCause;

/**
 * Represents an interface for components that can be rotated
 */
public interface IRotatableComponent extends IComponent {

    /**
     * Gets the rotation of this component
     *
     * @return The rotation of this component
     */
    Rotation getRotation();

    /**
     * Rotates the component based on the specified rotation type
     *
     * @param rotationType  The type of rotation to apply
     * @param save          True if the rotation should be saved, false otherwise
     * @param actionCause   The cause of the action
     */
    void rotate(RotationType rotationType, boolean save, ActionCause actionCause);
}
