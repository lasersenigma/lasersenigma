package eu.lasersenigma.component.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IDetectionComponent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeRangeEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final IDetectionComponent component;

    private final int minDiff;

    private final int maxDiff;

    public PlayerTryToChangeRangeEvent(Player player, IDetectionComponent component, int minDiff, int maxDiff) {
        super();
        this.player = player;
        this.component = component;
        this.minDiff = minDiff;
        this.maxDiff = maxDiff;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IDetectionComponent getRangeDetectionComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public int getMinDiff() {
        return minDiff;
    }

    public int getMaxDiff() {
        return maxDiff;
    }

}
