package eu.lasersenigma.component.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IColorableComponent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeColorEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final IColorableComponent component;

    public PlayerTryToChangeColorEvent(Player player, IColorableComponent component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IColorableComponent getColorableComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
