package eu.lasersenigma.component.event;

import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IRotatableComponent;
import eu.lasersenigma.component.scheduledactions.ActionCause;

public class ComponentRotationEvent extends AAfterActionEvent implements IComponentLEEvent {

    private final IRotatableComponent component;
    private final ActionCause actionCause;

    public ComponentRotationEvent(IRotatableComponent component, ActionCause actionCause) {
        super();
        this.component = component;
        this.actionCause = actionCause;
    }

    @Override
    public IRotatableComponent getComponent() {
        return component;
    }

    public ActionCause getActionCause() {
        return actionCause;
    }

}
