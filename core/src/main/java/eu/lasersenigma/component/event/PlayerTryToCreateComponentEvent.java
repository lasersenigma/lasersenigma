package eu.lasersenigma.component.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.component.ComponentFace;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerTryToCreateComponentEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IAreaEvent {

    private final Player player;

    private final Area area;

    private final ComponentType componentType;

    private final Location location;

    private final ComponentFace face;

    public PlayerTryToCreateComponentEvent(Player player, Area area, ComponentType componentType, Location location, ComponentFace face) {
        super();
        this.player = player;
        this.area = area;
        this.componentType = componentType;
        this.location = location;
        this.face = face;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public Location getLocation() {
        return location;
    }

    public ComponentFace getClickedBlockFace() {
        return face;
    }

}
