package eu.lasersenigma.component.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IDetectionComponent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeComponentDetectionModeEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final IDetectionComponent component;

    public PlayerTryToChangeComponentDetectionModeEvent(Player player, IDetectionComponent component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IDetectionComponent getDetectionComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
