package eu.lasersenigma.component.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IRotatableComponent;
import eu.lasersenigma.component.RotationType;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToRotateComponentEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final IRotatableComponent component;

    private final RotationType rotationType;

    public PlayerTryToRotateComponentEvent(Player player, IRotatableComponent component, RotationType rotationType) {
        super();
        this.player = player;
        this.component = component;
        this.rotationType = rotationType;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IRotatableComponent getRotatableComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public RotationType getRotationType() {
        return rotationType;
    }

}
