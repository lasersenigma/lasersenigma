package eu.lasersenigma.component.event;

import org.bukkit.entity.Player;

public interface IPlayerEvent {

    public Player getPlayer();

}
