package eu.lasersenigma.component.event;

import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.ILightComponent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeLightLevelEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IComponentLEEvent {

    private final Player player;

    private final ILightComponent component;

    private final int newValue;

    public PlayerTryToChangeLightLevelEvent(Player player, ILightComponent component, int newValue) {
        super();
        this.player = player;
        this.component = component;
        this.newValue = newValue;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public ILightComponent getRangeDetectionComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public int getNewValue() {
        return newValue;
    }

}
