package eu.lasersenigma.component.filteringsphere;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.mirrorsupport.task.MirrorPlacementAnimationTask;
import eu.lasersenigma.component.mirrorsupport.task.MirrorRemovalAnimationTask;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Filtering Sphere
 */
public final class FilteringSphere extends AArmorStandComponent implements IColorableComponent, IMirrorContainer {

    // The difference between the location where the mirror appears and the location where it starts entering inside the sphere
    public static final double MIRROR_ANIMATION_CLIP_LOCATION_DIFF = 0.6d;

    // The difference between the location where the mirror starts entering the sphere and the location where it stops and disappears
    public static final double MIRROR_ANIMATION_LOCATION_DIFF = 0.7d;

    // Does this filtering sphere has a mirror inside
    private boolean hasMirrorCurrent = true;

    // The saved color of this component's mirror. Used only when resetting.
    private LasersColor color = null;

    // The color of this filtering sphere
    private LasersColor colorCurrent = LasersColor.WHITE;

    // Is this filtering sphere currently showing an animation?
    private boolean onGoingAnimation = false;

    // Filtering sphere placement animation task
    private MirrorPlacementAnimationTask mirrorPlacementAnimationTask = null;

    // Filtering sphere removal animation task
    private MirrorRemovalAnimationTask mirrorRemovalAnimationTask = null;

    // Component's armor stands
    private final ComponentArmorStand FILTERING_SPHERE_AS = new ComponentArmorStand(this, "filtering_sphere");
    private final ComponentArmorStand FILTERING_SPHERE_MIRROR_AS = new ComponentArmorStand(this, "filtering_sphere_mirror");

    // Component's options
    // Empty

    // Component's events listener
    // Empty

    /**
     * Constructor used for creation from database
     *
     * @param area        The area containing this component
     * @param componentId The id of the component inside the database
     * @param location    The location of this component
     * @param face        The face the component is on
     * @param color       The color of the sphere on reset
     */
    public FilteringSphere(Area area, int componentId, Location location, ComponentFace face, LasersColor color) {
        super(area, componentId, location, ComponentType.FILTERING_SPHERE, face, face.getDefaultRotation(ComponentType.FILTERING_SPHERE));
        this.color = color;
        hasMirrorCurrent = color != null;
        colorCurrent = color != null ? color : LasersColor.BLACK;
    }

    /**
     * Constructor
     *
     * @param area     The area containing this component
     * @param location The location of this component
     * @param face     The face the component is on
     */
    public FilteringSphere(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.FILTERING_SPHERE, face);
        showOrUpdateComponent();
        dbCreate();
    }

    @Override
    public boolean hasMirror() {
        return color != null;
    }

    @Override
    public boolean hasMirrorCurrent() {
        return hasMirrorCurrent;
    }

    @Override
    public void setHasMirror(boolean hasMirror, boolean save) {
        if (onGoingAnimation) return;

        hasMirrorCurrent = hasMirror;
        if (!hasMirrorCurrent) colorCurrent = LasersColor.BLACK;

        showOrUpdateComponent();

        if (save) {
            color = hasMirrorCurrent ? colorCurrent : null;
            dbUpdate();
        }
    }

    @Override
    public boolean placeMirror(LasersColor color, boolean save) {
        if (onGoingAnimation) return false;

        if (hasMirrorCurrent) return false;

        if (isRemoved()) return false;

        if (FILTERING_SPHERE_MIRROR_AS.getArmorStand() != null) {
            removeArmorStandDisplay(FILTERING_SPHERE_MIRROR_AS);
        }

        onGoingAnimation = true;
        colorCurrent = color;

        Location mirrorLocation = getDefaultArmorStandBaseLocation(componentLocation);
        mirrorLocation = mirrorLocation.add(getAnimationClipVector()).add(getAnimationVector());
        mirrorLocation = getArmorStandLocationWithOffsets(mirrorLocation, ArmorStandItemOffset.GLASS_PANE, rotationCurrent);
        newCreateArmorStand(FILTERING_SPHERE_MIRROR_AS, mirrorLocation, rotationCurrent, Item.getMirror(colorCurrent));
        mirrorPlacementAnimationTask = new MirrorPlacementAnimationTask(this, Item.getFilteringSphereItem(colorCurrent), save);

        return true;
    }

    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getColorCurrent() {
        return colorCurrent;
    }

    @Override
    public void changeColor() {
        setColor(colorCurrent.getNextColor(false));
    }

    @Override
    public void changeColor(boolean save) {
        setColor(colorCurrent.getNextColor(false), save);
    }

    @Override
    public void setColor(LasersColor color) {
        setColor(color, false);
    }

    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }

        if (!hasMirrorCurrent || onGoingAnimation) {
            return;
        }

        colorCurrent = color;
        showOrUpdateComponent();

        if (save) {
            this.color = colorCurrent;
            dbUpdate();
        }
    }

    @Override
    public boolean removeMirror(boolean save) {
        if (onGoingAnimation) return false;

        if (!hasMirrorCurrent) return false;

        if (isRemoved()) return false;

        onGoingAnimation = true;
        mirrorRemovalAnimationTask = new MirrorRemovalAnimationTask(this, Item.FILTERING_SPHERE, save);

        return true;
    }

    @Override
    public boolean isOnGoingAnimation() {
        return onGoingAnimation;
    }

    @Override
    public void setOnGoingAnimation(boolean onGoingAnimation) {
        this.onGoingAnimation = onGoingAnimation;
    }

    @Override
    public boolean canBeDeleted() {
        return !onGoingAnimation;
    }

    @Override
    public Vector getAnimationClipVector() {
        return componentFace.getVector().multiply(MIRROR_ANIMATION_CLIP_LOCATION_DIFF);
    }

    @Override
    public Vector getAnimationVector() {
        return componentFace.getVector().multiply(MIRROR_ANIMATION_LOCATION_DIFF);
    }

    @Override
    public ArmorStand getArmorStandMirror() {
        return FILTERING_SPHERE_MIRROR_AS.getArmorStand();
    }

    @Override
    public ArmorStand getArmorStandMirrorContainer() {
        return FILTERING_SPHERE_AS.getArmorStand();
    }

    @Override
    public void activateComponent() {
        // Reset component's animation tasks
        mirrorRemovalAnimationTask = null;
        mirrorPlacementAnimationTask = null;
        onGoingAnimation = false;

        // Restore cached states
        hasMirrorCurrent = color != null;
        colorCurrent = color != null ? color : LasersColor.BLACK;
        rotationCurrent = rotation != null ? rotation : componentFace.getDefaultRotation(componentType);
    }

    @Override
    public void deactivateComponent() {
        // Cancel component's animation tasks
        if (mirrorRemovalAnimationTask != null) {
            mirrorRemovalAnimationTask.cancel();
            mirrorRemovalAnimationTask = null;
        }
        if (mirrorPlacementAnimationTask != null) {
            mirrorPlacementAnimationTask.cancel();
            mirrorPlacementAnimationTask = null;
        }
        onGoingAnimation = false;

        // Reset default states
        hasMirrorCurrent = color != null; // DEFAULT_HAS_MIRROR;
        colorCurrent = color != null ? color : LasersColor.BLACK; // DEFAULT_COLOR;
        rotationCurrent = rotation != null ? rotation : componentFace.getDefaultRotation(componentType); // componentFace.getDefaultRotation(componentType);
    }

    @Override
    public void showOrUpdateComponent() {
        Location baseLocation;

        // Filtering sphere armor stand
        baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.HEAD, rotationCurrent);
        updateComponentArmorStand(FILTERING_SPHERE_AS, baseLocation, rotationCurrent, Item.getFilteringSphereItem(colorCurrent), false, false, null);

        // Filtering sphere mirror armor stand
        baseLocation = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(componentLocation, componentFace), ArmorStandItemOffset.GLASS_PANE, rotationCurrent);
        updateComponentArmorStand(FILTERING_SPHERE_MIRROR_AS, baseLocation, rotationCurrent, Item.getMirror(colorCurrent), true, hasMirrorCurrent, null);
    }

    @Override
    public void removeArmorStandDisplay() {
        super.removeArmorStandDisplay();

        // Cancel component's animation tasks
        if (mirrorRemovalAnimationTask != null) {
            mirrorRemovalAnimationTask.cancel();
            mirrorRemovalAnimationTask = null;
        }
        if (mirrorPlacementAnimationTask != null) {
            mirrorPlacementAnimationTask.cancel();
            mirrorPlacementAnimationTask = null;
        }
        onGoingAnimation = false;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }
        // It has no mirror inside, it is an opaque component
        if (hasMirrorCurrent() && !isOnGoingAnimation()) {
            LasersColor resColor;
            if (colorCurrent == LasersColor.WHITE) {
                resColor = laserParticle.getColor();
            } else {
                HashMap<LasersColor.FilterResult, LasersColor> filterResult = laserParticle.getColor().filterBy(colorCurrent);
                resColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
            }
            if (resColor != null) {
                return new LaserReceptionResult(
                        true,
                        new LaserParticle(
                                this,
                                laserParticle.getLocation(),
                                laserParticle.getDirection(),
                                resColor,
                                getArea(),
                                laserParticle.getLightLevel()
                        ));
            }
        }
        return new LaserReceptionResult(true);
    }
}
