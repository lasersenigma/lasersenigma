package eu.lasersenigma.component.scheduledactions;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.scheduledactions.exception.ActionSavingDisabledBecauseDifferentAreaException;
import eu.lasersenigma.component.scheduledactions.exception.ActionSavingDisabledBecauseDifferentComponentException;
import eu.lasersenigma.component.scheduledactions.exception.ActionSavingDisabledException;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.HashSet;

public class PlayerActionsSaver {

    private final static HashSet<PlayerActionsSaver> TICK_COUNTING_INSTANCES = new HashSet<>();
    protected static Long CURRENT_SERVER_TICK = 0L;
    private static Integer TICK_COUNTING_TASK_ID = null;
    private final Area area;
    private final IComponent component;
    private final boolean multipleComponentsAllowed;
    private final ArrayList<ScheduledAction> scheduledActions;
    private Long previousActionTime;

    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerActionsSaver(Area area, IComponent component) {
        this.area = area;
        this.multipleComponentsAllowed = (component != null);
        this.component = component;
        this.previousActionTime = startCountingTicks(this);
        this.scheduledActions = new ArrayList<>();
    }

    private static Long startCountingTicks(PlayerActionsSaver aThis) {
        TICK_COUNTING_INSTANCES.add(aThis);
        if (TICK_COUNTING_TASK_ID == null) {
            CURRENT_SERVER_TICK = 0L;
            TICK_COUNTING_TASK_ID = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), (new Runnable() {
                @Override
                public void run() {
                    CURRENT_SERVER_TICK++;
                }
            }), 0, 1).getTaskId();
        }
        return CURRENT_SERVER_TICK;
    }

    private static Long stopCountingTicks(PlayerActionsSaver aThis) throws ActionSavingDisabledException {
        if (!TICK_COUNTING_INSTANCES.contains(aThis)) {
            throw new ActionSavingDisabledException();
        }
        TICK_COUNTING_INSTANCES.remove(aThis);
        if (TICK_COUNTING_INSTANCES.isEmpty()) {
            Bukkit.getScheduler().cancelTask(TICK_COUNTING_TASK_ID);
            TICK_COUNTING_TASK_ID = null;
        }
        return CURRENT_SERVER_TICK;
    }

    public void saveAction(IComponent component, ActionType action) throws ActionSavingDisabledException, ActionSavingDisabledBecauseDifferentAreaException, ActionSavingDisabledBecauseDifferentComponentException {
        if (component.getArea() != area) {
            stopCountingTicks(this);
            throw new ActionSavingDisabledBecauseDifferentAreaException();
        }
        if (!multipleComponentsAllowed && component != this.component) {
            stopCountingTicks(this);
            throw new ActionSavingDisabledBecauseDifferentComponentException();
        }
        int timeDiff = (int) (CURRENT_SERVER_TICK - previousActionTime);
        if (timeDiff > 0) {
            while (timeDiff > 99) {
                scheduledActions.add(new ScheduledAction(component, ActionType.WAIT, 99));
                timeDiff -= 99;
            }
            scheduledActions.add(new ScheduledAction(component, ActionType.WAIT, timeDiff));
        }
        if (action != null) {
            scheduledActions.add(new ScheduledAction(component, action, 0));
        }
        previousActionTime = CURRENT_SERVER_TICK;
    }

    public ArrayList<ScheduledAction> stopSavingActions() throws ActionSavingDisabledException, ActionSavingDisabledBecauseDifferentAreaException, ActionSavingDisabledBecauseDifferentComponentException {
        saveAction(component, null);
        stopCountingTicks(this);
        return scheduledActions;
    }

}
