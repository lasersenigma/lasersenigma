package eu.lasersenigma.component.scheduledactions.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class ActionSavingDisabledException extends AbstractLasersException {

    public ActionSavingDisabledException() {
        super("errors.scheduled_actions.messages.action_saving_disabled");
    }

}
