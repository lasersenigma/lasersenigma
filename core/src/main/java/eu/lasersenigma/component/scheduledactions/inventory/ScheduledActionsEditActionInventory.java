package eu.lasersenigma.component.scheduledactions.inventory;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.scheduledactions.ActionType;
import eu.lasersenigma.component.scheduledactions.ScheduledAction;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class ScheduledActionsEditActionInventory extends AOpenableInventory {

    private final IComponent component;
    private final Area area;
    private final ScheduledAction action;

    public ScheduledActionsEditActionInventory(LEPlayer player, IComponent component, ScheduledAction action) {
        super(player, "messages.scheduled_actions_action_edit_menu_title");
        this.action = action;
        this.component = component;
        this.area = component.getArea();
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(
                ActionType.COLOR_WHITE.getItem(),
                ActionType.COLOR_RED.getItem(),
                ActionType.COLOR_GREEN.getItem(),
                ActionType.COLOR_BLUE.getItem(),
                ActionType.COLOR_MAGENTA.getItem(),
                ActionType.COLOR_YELLOW.getItem(),
                ActionType.COLOR_LIGHT_BLUE.getItem(),
                ActionType.COLOR_BLACK.getItem(),
                ActionType.COLOR_LOOP.getItem()
        )));
        inv.add(new ArrayList<>(Arrays.asList(
                ActionType.ROTATE_LEFT.getItem(),
                ActionType.ROTATE_RIGHT.getItem(),
                ActionType.ROTATE_UP.getItem(),
                ActionType.ROTATE_DOWN.getItem(),
                Item.EMPTY,
                ActionType.SPHERE_DECREASE.getItem(),
                ActionType.SPHERE_INCREASE.getItem(),
                Item.EMPTY,
                ActionType.WAIT.getItem()
        )));

        inv.add(new ArrayList<>());
        inv.add(new ArrayList<>());
        if (action.getType() == ActionType.WAIT) {
            List<Item> delayLine = inv.get(3);
            delayLine.addAll(Arrays.asList(
                    Item.SCHEDULED_ACTION_EDIT_VERY_LESS_DELAY,
                    Item.SCHEDULED_ACTION_EDIT_LESS_DELAY
            ));
            delayLine.addAll(getNumberAsItems(action.getDelay(), true));
            delayLine.addAll(Arrays.asList(
                    Item.SCHEDULED_ACTION_EDIT_MORE_DELAY,
                    Item.SCHEDULED_ACTION_EDIT_VERY_MORE_DELAY
            ));
        }
        inv.add(new ArrayList<>());
        inv.add(new ArrayList<>());
        inv.get(5).add(Item.SCHEDULED_ACTION_EDIT_DELETE);
        inv.set(5, addEmpty(inv.get(5), 7));
        inv.get(5).add(Item.SCHEDULED_ACTION_EDIT_BACK);
        return inv;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case SCHEDULED_ACTION_EDIT_VERY_LESS_DELAY:
                ComponentController.modifyScheduledActionDelay(player, component, action, -10);
                break;
            case SCHEDULED_ACTION_EDIT_LESS_DELAY:
                ComponentController.modifyScheduledActionDelay(player, component, action, -1);
                break;
            case SCHEDULED_ACTION_EDIT_MORE_DELAY:
                ComponentController.modifyScheduledActionDelay(player, component, action, 1);
                break;
            case SCHEDULED_ACTION_EDIT_VERY_MORE_DELAY:
                ComponentController.modifyScheduledActionDelay(player, component, action, 10);
                break;
            case SCHEDULED_ACTION_EDIT_DELETE:
                ComponentController.deleteScheduledAction(player, component, action);
                player.getInventoryManager().openLEInventory(new ScheduledActionsEditionInventory(player, component));
                break;
            case SCHEDULED_ACTION_EDIT_BACK:
                player.getInventoryManager().openLEInventory(new ScheduledActionsEditionInventory(player, component));
                break;
            default:
                ActionType actionType = ActionType.getFromItem(item);
                if (actionType == null) {
                    return;
                }
                ComponentController.changeScheduledActionType(player, component, action, actionType);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        List<Item> items = getAvailableActionTypes().stream()
                .map(ActionType::getItem)
                .collect(Collectors.toList());
        if (action.getType() == ActionType.WAIT) {
            items.addAll(Arrays.asList(
                    Item.SCHEDULED_ACTION_EDIT_MORE_DELAY,
                    Item.SCHEDULED_ACTION_EDIT_VERY_MORE_DELAY,
                    Item.SCHEDULED_ACTION_EDIT_LESS_DELAY,
                    Item.SCHEDULED_ACTION_EDIT_VERY_LESS_DELAY,
                    Item.COMPONENT_MENU_ZERO,
                    Item.COMPONENT_MENU_ONE,
                    Item.COMPONENT_MENU_TWO,
                    Item.COMPONENT_MENU_THREE,
                    Item.COMPONENT_MENU_FOUR,
                    Item.COMPONENT_MENU_FIVE,
                    Item.COMPONENT_MENU_SIX,
                    Item.COMPONENT_MENU_EIGHT,
                    Item.COMPONENT_MENU_NINE));
        }
        items.addAll(Arrays.asList(
                Item.SCHEDULED_ACTION_EDIT_DELETE,
                Item.SCHEDULED_ACTION_EDIT_BACK
        ));
        return items.contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.SCHEDULED_ACTIONS_ACTION_EDIT_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

    private List<ActionType> getAvailableActionTypes() {
        return Arrays.stream(ActionType.values())
                .filter(actionType -> actionType.isAvailable(component))
                .collect(Collectors.toList());
    }

}
