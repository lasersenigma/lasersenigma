package eu.lasersenigma.component.scheduledactions;

import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;

import java.util.Arrays;

/**
 * Enumeration representing different types of actions
 */
public enum ActionType {

    WAIT(Item.SCHEDULED_ACTIONS_WAIT),
    ROTATE_LEFT(Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT),
    ROTATE_RIGHT(Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT),
    ROTATE_UP(Item.COMPONENT_SHORTCUTBAR_ROTATION_UP),
    ROTATE_DOWN(Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN),
    COLOR_LOOP(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER),
    COLOR_RED(Item.RED),
    COLOR_GREEN(Item.GREEN),
    COLOR_BLUE(Item.BLUE),
    COLOR_MAGENTA(Item.MAGENTA),
    COLOR_YELLOW(Item.YELLOW),
    COLOR_LIGHT_BLUE(Item.LIGHT_BLUE),
    COLOR_WHITE(Item.WHITE),
    COLOR_BLACK(Item.BLACK),
    SPHERE_DECREASE(Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE),
    SPHERE_INCREASE(Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE);

    private final Item item;

    ActionType(Item item) {
        this.item = item;
    }

    /**
     * Get ActionType based on the specified color
     *
     * @param color The color to match
     * @return Corresponding ActionType or null if not found
     */
    public static ActionType getFromColor(LasersColor color) {
        return switch (color) {
            case WHITE -> COLOR_WHITE;
            case RED -> COLOR_RED;
            case GREEN -> COLOR_GREEN;
            case BLUE -> COLOR_BLUE;
            case MAGENTA -> COLOR_MAGENTA;
            case YELLOW -> COLOR_YELLOW;
            case CYAN -> COLOR_LIGHT_BLUE;
            case BLACK -> COLOR_BLACK;
            default -> null;
        };
    }

    /**
     * Get ActionType based on the specified rotation type
     *
     * @param rotationType The rotation type to match
     * @return Corresponding ActionType or null if not found
     */
    public static ActionType getFromRotation(RotationType rotationType) {
        return switch (rotationType) {
            case DOWN -> ROTATE_DOWN;
            case UP -> ROTATE_UP;
            case LEFT -> ROTATE_LEFT;
            case RIGHT -> ROTATE_RIGHT;
            default -> null;
        };
    }

    /**
     * Get ActionType based on the specified item
     *
     * @param item The item to match
     * @return Corresponding ActionType or null if not found
     */
    public static ActionType getFromItem(Item item) {
        return Arrays.stream(ActionType.values())
                .filter(actionType -> actionType.getItem() == item)
                .findAny()
                .orElse(null);
    }

    /**
     * Get the associated Item with this ActionType
     *
     * @return The associated Item
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Check if delay is available for this ActionType
     *
     * @return True if delay is available, false otherwise
     */
    public boolean isDelayAvailable() {
        return (this == WAIT);
    }

    /**
     * Check if the action is available for the specified component
     *
     * @param component The component to check against
     * @return True if the action is available, false otherwise
     */
    public boolean isAvailable(IComponent component) {
        return switch (this) {
            case ROTATE_DOWN, ROTATE_LEFT, ROTATE_RIGHT, ROTATE_UP -> (component instanceof IRotatableComponent);
            case COLOR_RED, COLOR_GREEN, COLOR_BLUE, COLOR_MAGENTA, COLOR_YELLOW, COLOR_LIGHT_BLUE, COLOR_WHITE, COLOR_LOOP ->
                    (component instanceof IColorableComponent);
            case COLOR_BLACK -> (component instanceof MirrorSupport);
            case SPHERE_DECREASE, SPHERE_INCREASE -> (component instanceof GravitationalSphere);
            case WAIT -> true;
            default -> throw new IllegalStateException("Unsupported action");
        };
    }

    /**
     * Check if the action is doable now for the specified component
     *
     * @param component The component to check against
     * @return True if the action is doable now, false otherwise
     */
    public boolean isDoableNow(IComponent component) {
        if (!component.getArea().isActivated()) {
            return false;
        }
        return switch (this) {
            case ROTATE_DOWN, ROTATE_LEFT, ROTATE_RIGHT, ROTATE_UP -> {
                if (component instanceof IRotatableComponent) {
                    if (component instanceof IMirrorContainer) {
                        yield ((IMirrorContainer) component).hasMirrorCurrent();
                    }
                    yield true;
                }
                yield false;
            }
            case COLOR_RED, COLOR_GREEN, COLOR_BLUE, COLOR_MAGENTA, COLOR_YELLOW, COLOR_LIGHT_BLUE, COLOR_WHITE, COLOR_LOOP -> {
                if (component instanceof IColorableComponent) {
                    if (component instanceof IMirrorContainer) {
                        yield ((IMirrorContainer) component).hasMirrorCurrent();
                    }
                    yield true;
                }
                yield false;
            }
            case COLOR_BLACK -> {
                if (!(component instanceof MirrorSupport)) {
                    yield false;
                }
                yield ((MirrorSupport) component).hasMirrorCurrent();
            }
            case SPHERE_DECREASE, SPHERE_INCREASE -> (component instanceof GravitationalSphere);
            case WAIT -> true;
            default -> throw new IllegalStateException("Unsupported action");
        };
    }

    /**
     * Get the color associated with this ActionType
     *
     * @return The associated LasersColor
     */
    public LasersColor getColor() {
        return switch (this) {
            case COLOR_RED -> LasersColor.RED;
            case COLOR_GREEN -> LasersColor.GREEN;
            case COLOR_BLUE -> LasersColor.BLUE;
            case COLOR_MAGENTA -> LasersColor.MAGENTA;
            case COLOR_YELLOW -> LasersColor.YELLOW;
            case COLOR_LIGHT_BLUE -> LasersColor.CYAN;
            case COLOR_WHITE -> LasersColor.WHITE;
            case COLOR_BLACK -> LasersColor.BLACK;
            default -> null;
        };
    }

    /**
     * Get the rotation type associated with this ActionType
     *
     * @return The associated RotationType
     */
    public RotationType getRotationType() {
        return switch (this) {
            case ROTATE_UP -> RotationType.UP;
            case ROTATE_DOWN -> RotationType.DOWN;
            case ROTATE_LEFT -> RotationType.LEFT;
            case ROTATE_RIGHT -> RotationType.RIGHT;
            default -> null;
        };
    }
}
