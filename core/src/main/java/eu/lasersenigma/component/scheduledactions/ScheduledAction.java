package eu.lasersenigma.component.scheduledactions;

import eu.lasersenigma.component.IColorableComponent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IRotatableComponent;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;

/**
 * Represents a saved action associated with a component.
 */
public class ScheduledAction implements Comparable {

    private final IComponent component;
    private int scheduledActionId;
    private ActionType type;
    private Integer delay;

    /**
     * Constructs a SavedAction with the specified component, action type, and delay
     *
     * @param component The associated component
     * @param type      The action type
     * @param delay     The delay for the action
     */
    public ScheduledAction(IComponent component, ActionType type, Integer delay) {
        this.component = component;
        this.type = type;
        this.delay = delay;
    }

    /**
     * Constructs a default SavedAction with the specified component, using the WAIT action type and a default delay
     *
     * @param component The associated component
     */
    public ScheduledAction(IComponent component) {
        this.component = component;
        this.type = ActionType.WAIT;
        this.delay = 20;
    }

    /**
     * Retrieves the scheduled action ID associated with this SavedAction
     *
     * @return The scheduled action ID
     */
    public int getScheduledActionId() {
        return scheduledActionId;
    }

    /**
     * Retrieves the associated component of this SavedAction
     *
     * @return The associated component
     */
    public IComponent getComponent() {
        return component;
    }

    /**
     * Retrieves the delay for the action in ticks
     *
     * @return The delay for the action
     */
    public Integer getDelay() {
        return this.delay;
    }

    /**
     * Sets the delay for the action in ticks
     *
     * @param delay The delay to set
     */
    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    /**
     * Retrieves the index of this SavedAction within its associated component's scheduled actions
     *
     * @return The index of this SavedAction
     */
    public int getIndex() {
        return this.component.getScheduledActions().indexOf(this);
    }

    /**
     * Retrieves the ActionType associated with this SavedAction
     *
     * @return The ActionType of this SavedAction
     */
    public ActionType getType() {
        return type;
    }

    /**
     * Sets the ActionType for this SavedAction
     *
     * @param type The ActionType to set
     */
    public void setType(ActionType type) {
        this.type = type;
    }

    /**
     * Performs the saved action based on its type
     */
    public void doAction() {
        switch (type) {
            case COLOR_BLACK, COLOR_WHITE, COLOR_RED, COLOR_GREEN, COLOR_BLUE, COLOR_MAGENTA, COLOR_YELLOW, COLOR_LIGHT_BLUE ->
                    ((IColorableComponent) component).setColor(type.getColor(), false);
            case COLOR_LOOP ->
                    ((IColorableComponent) component).changeColor(false);
            case ROTATE_DOWN, ROTATE_LEFT, ROTATE_RIGHT, ROTATE_UP ->
                    ((IRotatableComponent) component).rotate(type.getRotationType(), false, ActionCause.SCHEDULED_ACTION);
            case SPHERE_DECREASE, SPHERE_INCREASE -> {
                GravitationalSphere sphere = ((GravitationalSphere) component);
                int size = sphere.getCurrentGravityStrength();
                int newSize = (type == ActionType.SPHERE_DECREASE) ? Math.max(size - 1, 1) : Math.min(size + 1, 9);
                sphere.setGravityStrength(newSize, false);
            }
            default ->
                    throw new UnsupportedOperationException("ActionType not supported yet.");
        }
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof ScheduledAction secondScheduledAction)) {
            return -1;
        }
        if (this.getComponent() != secondScheduledAction.getComponent()) {
            return -1;
        }
        return Integer.compare(this.getIndex(), secondScheduledAction.getIndex());
    }
}
