package eu.lasersenigma.component.scheduledactions.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.common.inventory.ComponentMenuInventory;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class ScheduledActionsMainInventory extends AOpenableInventory {

    private final IComponent component;

    public ScheduledActionsMainInventory(LEPlayer player, IComponent component) {
        super(player, "messages.scheduled_actions_main_menu");
        this.component = component;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        ArrayList<Item> firstLine = new ArrayList<>();
        if (player.isSavingActions()) {
            firstLine.add(Item.SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE);
        } else {
            firstLine.add(Item.SCHEDULED_ACTIONS_START_SAVING_ACTIONS);
        }
        firstLine.addAll(Arrays.asList(Item.EMPTY, Item.SCHEDULED_ACTIONS_OPEN_EDITION_MENU));
        if (component.getScheduledActions().size() > 0) {
            firstLine.addAll(Arrays.asList(Item.EMPTY, Item.SCHEDULED_ACTIONS_CLEAR));
        }
        firstLine = addEmptyUntil(firstLine, 8);
        firstLine.add(Item.SCHEDULED_ACTION_EDIT_BACK);
        inv.add(firstLine);
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEScheduledActionsMainInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != component.getArea()) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> player.getInventoryManager().closeOpenedInventory(), 1);
            return;
        }
        switch (item) {
            case SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE:
                if (player.isSavingActions()) {
                    ComponentController.saveActionsDone(player, component);
                }
                break;
            case SCHEDULED_ACTIONS_START_SAVING_ACTIONS:
                player.startSavingActions(component.getArea(), component);
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> player.getInventoryManager().closeOpenedInventory(), 1);
                break;
            case SCHEDULED_ACTIONS_OPEN_EDITION_MENU:
                player.stopSavingActions();
                player.getInventoryManager().openLEInventory(new ScheduledActionsEditionInventory(player, component));
                break;
            case SCHEDULED_ACTIONS_CLEAR:
                ComponentController.clearScheduledActions(player, component);
                break;
            case SCHEDULED_ACTION_EDIT_BACK:
                player.getInventoryManager().openLEInventory(new ComponentMenuInventory(player, component));
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE,
                Item.SCHEDULED_ACTIONS_START_SAVING_ACTIONS,
                Item.SCHEDULED_ACTIONS_OPEN_EDITION_MENU,
                Item.SCHEDULED_ACTIONS_CLEAR,
                Item.SCHEDULED_ACTION_EDIT_BACK
        )).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.SCHEDULED_ACTIONS_MAIN;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return component.getArea();
    }

}
