package eu.lasersenigma.component.scheduledactions.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.common.inventory.AHorizontallyPaginableOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.scheduledactions.ActionType;
import eu.lasersenigma.component.scheduledactions.ScheduledAction;
import eu.lasersenigma.player.LEPlayer;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;

public final class ScheduledActionsEditionInventory extends AHorizontallyPaginableOpenableInventory {

    private final Area area;

    private final IComponent component;

    public ScheduledActionsEditionInventory(LEPlayer player, IComponent component) {
        super(player, "messages.scheduled_actions_edition_menu_title");
        this.area = component.getArea();
        this.component = component;
    }

    private TreeMap<ScheduledAction, ArrayList<ItemStack>> getSavedActionColumns() {
        TreeMap<ScheduledAction, ArrayList<ItemStack>> savedActionsColumns = new TreeMap<>();
        ItemsFactory itemsFactory = ItemsFactory.getInstance();
        ArrayList<ScheduledAction> scheduledActions = component.getScheduledActions();
        int lastColumnIndex = scheduledActions.size() - 1;

        for (int i = 0; i <= lastColumnIndex; i++) {
            ScheduledAction scheduledAction = scheduledActions.get(i);
            ArrayList<ItemStack> column = new ArrayList<>();

            ItemStack actionEdit = itemsFactory.getItemStack(scheduledAction.getType().getItem());
            ItemStack actionDelete = itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_DELETE);
            boolean shouldShowActionMoveLeft = i != 0;
            boolean shouldShowActionMoveRight = i != lastColumnIndex;
            ItemStack actionMoveLeft = shouldShowActionMoveLeft ? itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_MOVE_LEFT) : itemsFactory.getItemStack(Item.EMPTY);
            ItemStack actionMoveRight = shouldShowActionMoveRight ? itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_MOVE_RIGHT) : itemsFactory.getItemStack(Item.EMPTY);

            // Modify itemStacks name to make them unique (necessary for letting contains and onClick identify the specific column users clics)
            String txtToAdd = "#" + i;
            ItemStackUtils.addLore(actionEdit,scheduledAction.getType() == ActionType.WAIT ? "(" + scheduledAction.getDelay() + " ticks) " + txtToAdd : txtToAdd);
            ItemStackUtils.addLore(actionDelete, txtToAdd);
            if (shouldShowActionMoveLeft) {
                ItemStackUtils.addLore(actionMoveLeft, txtToAdd);
            }
            if (shouldShowActionMoveRight) {
                ItemStackUtils.addLore(actionMoveRight, txtToAdd);
            }

            column.add(actionEdit);
            column.add(actionDelete);
            column.add(actionMoveLeft);
            column.add(actionMoveRight);

            savedActionsColumns.put(scheduledAction, column);
        }
        return savedActionsColumns;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.SCHEDULED_ACTIONS_EDITION_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case SCHEDULED_ACTIONS_ACTION_NEW_LEFT:
                ComponentController.addNewScheduledActionBefore(player, component);
                updateInventory();
                break;
            case SCHEDULED_ACTIONS_ACTION_NEW_RIGHT:
                ComponentController.addNewScheduledActionAfter(player, component);
                updateInventory();
                break;
            case BLACK_EMPTY:
                break;
            case SCHEDULED_ACTION_EDIT_BACK:
                player.getInventoryManager().openLEInventory(new ScheduledActionsMainInventory(player, component));
                break;
            default:
                super.onClick(item);
                break;
        }
    }

    @Override
    public boolean contains(ItemStack itemStack) {
        return getColumnsItemStacks().stream()
                .anyMatch(
                        column -> column.stream().anyMatch(
                                curItemStack -> ItemsFactory.isSimilar(itemStack, curItemStack)
                        )
                );
    }

    @Override
    public void onClick(ItemStack itemStack) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack)");
        TreeMap<ScheduledAction, ArrayList<ItemStack>> savedActionsColumns = getSavedActionColumns();
        for (Entry<ScheduledAction, ArrayList<ItemStack>> savedActionColumn : savedActionsColumns.entrySet()) {
            ArrayList<ItemStack> column = savedActionColumn.getValue();
            for (int i = 0; i < column.size(); i++) {
                ItemStack curItemStack = column.get(i);

                if (ItemsFactory.isSimilar(itemStack, curItemStack)) {
                    switch (i) {
                        case 0 -> {
                            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => edit");
                            player.getInventoryManager().openLEInventory(new ScheduledActionsEditActionInventory(player, component, savedActionColumn.getKey()));
                            return;
                        }
                        case 1 -> {
                            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => delete");
                            ComponentController.deleteScheduledAction(player, component, savedActionColumn.getKey());
                            return;
                        }
                        case 2 -> {
                            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => moveBefore");
                            ComponentController.moveScheduledActionBefore(player, component, savedActionColumn.getKey());
                            return;
                        }
                        case 3 -> {
                            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => moveAfter");
                            ComponentController.moveScheduledActionAfter(player, component, savedActionColumn.getKey());
                            return;
                        }
                    }
                }
            }
        }
    }

    @Override
    public final boolean contains(Item item) {
        if (Arrays.asList(
                Item.SCHEDULED_ACTIONS_ACTION_NEW_LEFT,
                Item.SCHEDULED_ACTIONS_ACTION_NEW_RIGHT,
                Item.BLACK_EMPTY,
                Item.SCHEDULED_ACTION_EDIT_BACK
        ).contains(item)) {
            return true;
        }
        return super.contains(item);
    }

    @Override
    protected List<List<ItemStack>> getColumnsItemStacks() {
        ItemsFactory itemsFactory = ItemsFactory.getInstance();
        List<List<ItemStack>> columns = new ArrayList<>();
        TreeMap<ScheduledAction, ArrayList<ItemStack>> savedActionsColumns = getSavedActionColumns();
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_NEW_LEFT),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.SCHEDULED_ACTION_EDIT_BACK)
        )));
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY)
        )));
        savedActionsColumns.forEach((action, column) -> columns.add(column));
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY)
        )));
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_NEW_RIGHT),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.SCHEDULED_ACTION_EDIT_BACK)
        )));
        return columns;
    }
}
