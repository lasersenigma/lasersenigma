package eu.lasersenigma.component.scheduledactions.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class ActionSavingDisabledBecauseDifferentComponentException extends AbstractLasersException {

    public ActionSavingDisabledBecauseDifferentComponentException() {
        super("errors.scheduled_actions.messages.action_saving_disabled_because_different_component");
    }
}
