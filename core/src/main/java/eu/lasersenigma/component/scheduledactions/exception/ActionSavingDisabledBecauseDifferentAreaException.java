package eu.lasersenigma.component.scheduledactions.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class ActionSavingDisabledBecauseDifferentAreaException extends AbstractLasersException {

    public ActionSavingDisabledBecauseDifferentAreaException() {
        super("errors.scheduled_actions.messages.action_saving_disabled_because_different_area");
    }

}
