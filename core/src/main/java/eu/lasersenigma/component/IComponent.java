package eu.lasersenigma.component;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.component.scheduledactions.ScheduledAction;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 * Represents an interface for laser components
 */
public interface IComponent {

    /**
     * Retrieves the component's area
     *
     * @return The component's area
     */
    Area getArea();

    /**
     * Retrieves the component's ID
     *
     * @return The component's ID
     */
    int getComponentId();

    /**
     * Retrieves the component's type
     *
     * @return The component's type
     */
    ComponentType getComponentType();

    /**
     * Retrieves the component's location
     *
     * @return The component's location
     */
    Location getComponentLocation();

    /**
     * Receives a laser particle
     *
     * @param laserParticle The received laser particle
     * @return The result of the particle processing
     */
    LaserReceptionResult receiveLaser(LaserParticle laserParticle);

    /**
     * Retrieves the current component's options bitmap
     *
     * @return The component's options bitmap
     */
    int getComponentOptionsBitmap();

    /**
     * Defines the component's options from a bitmap
     *
     * @param bitmap The bitmap
     */
    void setComponentOptionsFromBitmap(int bitmap);

    /**
     * Gets the component's options
     *
     * @return A set of options
     */
    LinkedHashSet<ComponentOption> getComponentOptions();

    /**
     * Adds an option to the current component
     *
     * @param option The option to add
     * @return True if this component did not already contain the option
     */
    boolean addOption(ComponentOption option);

    /**
     * Retrieves the component's events listener
     *
     * @return The component's events listener
     */
    Listener getComponentEventsListener();

    /**
     * Defines the component's events listener
     *
     * @param componentEventsListener The listener to add
     */
    void setComponentEventsListener(Listener componentEventsListener);

    /**
     * Activates the component when the component's area is activated
     */
    void activateComponent();

    /**
     * Deactivates the component when the component's area is deactivated
     */
    void deactivateComponent();

    /**
     * Hides the component by removing visible elements
     */
    // void hideComponent();

    /**
     * Shows or updates the visible elements of the component
     */
    void showOrUpdateComponent();

    /**
     * Resets the component when the last player leaves the area (should be only for resetting states)
     */
    // void resetComponent();

    /**
     * Removes the component
     */
    void deleteComponent();

    /**
     * Removes the component from the database
     */
    void dbRemove();

    /**
     * Creates the component in the database
     */
    void dbCreate();

    /**
     * Updates the component in the database
     */
    void dbUpdate();

    /**
     * Checks if the component has been removed
     *
     * @return True if the component still exists, false otherwise
     */
    boolean isRemoved();

    /**
     * Checks if the component can currently be deleted
     *
     * @return True if the component can be deleted now
     */
    boolean canBeDeleted();

    /**
     * Retrieves the scheduled actions that will be executed in a loop on this component
     *
     * @return The scheduled actions that will be executed in a loop on this component
     */
    ArrayList<ScheduledAction> getScheduledActions();

    /**
     * Notifies the system that this component's scheduled actions have been modified
     */
    void updateActions();

    /**
     * Starts the scheduled actions
     */
    void startScheduledActions();

    /**
     * Stops the scheduled actions
     */
    void stopScheduledActions();
}
