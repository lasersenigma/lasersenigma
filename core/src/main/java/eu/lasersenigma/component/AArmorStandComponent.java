package eu.lasersenigma.component;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Used for components using armor stand as representation
 */
public abstract class AArmorStandComponent extends AComponent {

    public static final String ARMOR_STAND_CUSTOM_NAME = "lasersenigma-as";

    // Component's face
    protected final ComponentFace componentFace;

    // Component's rotation (head's rotation)
    protected Rotation rotation = null;

    // Component's rotation current (head's rotation)
    protected Rotation rotationCurrent;

    private final Set<ComponentArmorStand> armorStands = new HashSet<>();

    /**
     * Constructor
     *
     * @param area          The area containing the component
     * @param componentId   The id of the component
     * @param location      The location of the component
     * @param type          The type of the component
     * @param componentFace The direction the component is facing (up/down/north/south/east/west)
     * @param rotation      The current rotation of the component
     */
    public AArmorStandComponent(Area area, int componentId, Location location, ComponentType type, ComponentFace componentFace, Rotation rotation) {
        super(area, componentId, location, type);
        this.componentFace = componentFace;
        this.rotation = rotation != null ? rotation : this.componentFace.getDefaultRotation(type);
        this.rotationCurrent = this.componentFace.getDefaultRotation(type);
    }

    /**
     * Constructor
     *
     * @param area          The area containing the component
     * @param location      The location of the component
     * @param type          The type of the component
     * @param componentFace The direction the component is facing (up/down/north/south/east/west)
     */
    public AArmorStandComponent(Area area, Location location, ComponentType type, ComponentFace componentFace) {
        super(area, location, type);
        this.componentFace = componentFace;
        this.rotation = this.componentFace.getDefaultRotation(type);
        this.rotationCurrent = this.componentFace.getDefaultRotation(type);
    }

    /**
     * Retrieves component's facing direction (up/down/north/south/east/west)
     *
     * @return Component's facing direction (up/down/north/south/east/west)
     */
    public final ComponentFace getComponentFace() {
        return componentFace;
    }

    /**
     * Retrieves component's rotation (not affected by player's edits)
     *
     * @return Saved rotation
     */
    public final Rotation getRotation() {
        return rotation;
    }

    /**
     * Retrieves component's rotation (affected by player's edits)
     *
     * @return Component's rotation
     */
    public final Rotation getRotationCurrent() {
        return rotationCurrent;
    }

    public final void rotate(RotationType rotationType, boolean save) {
        rotate(rotationType);
    }

    /**
     * Rotates the component
     *
     * @param rotationType the type of the rotation (vertical/horizontal)
     */
    protected final void rotate(RotationType rotationType) {
        if (canRotate()) {
            rotation = rotation.getNextRotation(rotationType);
            showOrUpdateComponent();
        }
    }

    /**
     * Retrieves all component's related armor stands
     *
     * @return Al component's related armor stands
     */
    public final Set<ComponentArmorStand> getComponentArmorStands() {
        return armorStands;
    }

    /**
     * Adds a component armor stand
     *
     * @param componentArmorStand Component armor stand to add
     */
    public final void addComponentArmorStand(ComponentArmorStand componentArmorStand) {
        armorStands.add(componentArmorStand);
    }

    /**
     * Updates component armor stand: If there is no related armor stand to this component show it, update it or remove it elsewhere
     *
     * @param componentArmorStand Desired component armor stand
     * @param rotation            Desired head rotation
     * @param headItem            Desired head item
     * @param isAMirror           Is the component armor stand a mirror?
     * @param mirrorState         Should the component armor stand has a mirror?
     * @param action              Action that should be added after armor stand creation
     */
    public final void updateComponentArmorStand(@Nonnull ComponentArmorStand componentArmorStand, @Nonnull Location location, @Nonnull Rotation rotation, @Nullable Item headItem, boolean isAMirror, boolean mirrorState, @Nullable Consumer<ComponentArmorStand> action) {
        // Create armor stands if necessary
        ArmorStand armorStand = findArmorStandBack(componentArmorStand.getArmorStand());

        if (!isAMirror) {
            if (armorStand == null) {
                newCreateArmorStand(componentArmorStand, location, rotation, headItem);
            } else {
                updateArmorStand(armorStand, location, rotation, headItem);
            }
        } else {
            if (mirrorState) {
                if (armorStand == null) {
                    newCreateArmorStand(componentArmorStand, location, rotation, headItem);
                } else {
                    updateArmorStand(armorStand, location, rotation, headItem);
                }
            } else if (armorStand != null) {
                removeArmorStandDisplay(componentArmorStand);
            }
        }

        // Then doing
        if (action != null) action.accept(componentArmorStand);

        // If block at component's location was not a barrier block, place one
        Block block = componentLocation.getBlock();
        if (block.getType() == Material.BARRIER) return;
        block.setType(Material.BARRIER);
        block.getState().update();
    }

    private void updateArmorStand(ArmorStand armorStand, Location location, Rotation rotation, Item headItem) {
        armorStand.setHeadPose(rotation);
        armorStand.setHelmet(ItemsFactory.getInstance().getItemStack(headItem));
        armorStand.teleport(location);
    }

    @Override
    public void deleteComponent() {
        super.deleteComponent();
        removeArmorStandDisplay();
    }

    /**
     * Removes component's armor stand's related stuff
     */
    public void removeArmorStandDisplay() {
        armorStands.forEach(this::removeArmorStandDisplay);
    }

    /**
     * Removes component's armor stand's related stuff
     */
    public void removeArmorStandDisplay(ComponentArmorStand componentArmorStand) {
        armorStands.stream()
                .filter(entry -> entry.getArmorStand() != null)
                .filter(armorStand -> armorStand.equals(componentArmorStand))
                .forEach(this::removeArmorStand);

        // Remove block at component's location and replace it by air
        Block block = componentLocation.getBlock();
        block.setType(Material.AIR);
        block.getState().update();
    }

    private void removeArmorStand(ComponentArmorStand componentArmorStand) {
        ArmorStand armorStand = componentArmorStand.getArmorStand();

        // Remove armor stand from all areas
        Areas.getInstance().removeEntity(armorStand.getUniqueId());

        // Remove armor stand from the chunk's entities
        Arrays.stream(componentLocation.getChunk().getEntities())
                .filter(entity -> entity.getUniqueId().equals(armorStand.getUniqueId()))
                .findFirst()
                .ifPresent(entity -> {
                    entity.remove();
                    componentArmorStand.setArmorStand(null);
                });
    }

    public static Location getDefaultArmorStandBaseLocation(Location location, ComponentFace face) {
        return getDefaultArmorStandBaseLocation(location).add(face.getVector().multiply(-0.16));
    }

    public static Location getDefaultArmorStandBaseLocation(Location location) {
        return location.clone().add(0.5, -1, 0.5);
    }

    public final Location getASHeadCenterLocation() {
        return componentLocation.clone().add(0.5, 0.5, 0.5).add(componentFace.getVector().multiply(-0.19));
    }

    protected final ArmorStand findArmorStandBack(ArmorStand armorStand) {
        if (armorStand != null) {
            if (armorStand.isDead()) {
                armorStand = null;
            } else {
                boolean found = false;
                Chunk asChunk = armorStand.getLocation().getChunk();
                Chunk componentChunk = componentLocation.getChunk();
                Entity[] entities = asChunk.getEntities();
                HashSet<Entity> entitiesSet = new HashSet<>(Arrays.asList(entities));
                if (asChunk != componentChunk) {
                    entitiesSet.addAll(Arrays.asList(componentChunk.getEntities()));
                }
                for (Entity entity : entitiesSet) {
                    if (entity.getUniqueId().equals(armorStand.getUniqueId())) {
                        armorStand = (ArmorStand) entity;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    armorStand = null;
                }
            }
        }
        return armorStand;
    }

    protected final void newCreateArmorStand(ComponentArmorStand componentArmorStand, Location location, Rotation rotation, @Nonnull Item item) {
        ArmorStand armorStand = area.getAreaWorld().spawn(location, ArmorStand.class, entity -> {
            entity.teleport(location);
            entity.setVisible(false);
            entity.setGravity(false);
            entity.setAI(false);
            entity.setCollidable(false);
            entity.setSilent(true);
            entity.setPersistent(false);
            entity.setCustomNameVisible(false);
            entity.setCustomName(ARMOR_STAND_CUSTOM_NAME);

            entity.setHeadPose(rotation);
            entity.setHelmet(ItemsFactory.getInstance().getItemStack(item));
        });

        // Define the armor stand of component's armor stand
        componentArmorStand.setArmorStand(armorStand);

        // Add the armor stand to areas' entities
        Areas.getInstance().addEntity(armorStand.getUniqueId(), this);
    }

    /**
     * Checks if the component can be rotated
     *
     * @return True if the component can be rotated, false elsewhere
     */
    public final boolean canRotate() {
        return true;
    }

    /**
     * Adds an offset on a location corresponding to its rotation (used for armorstand with decentered head block)
     *
     * @param baseLocation The origin location of the armorstand
     * @param offset       The offset between the item on the armorstand head and the armorstand head center
     * @param rot          The current rotation of the armorstand
     * @return The new armorstand location where the item is well centered on the baseLocation
     */
    public Location getArmorStandLocationWithOffsets(Location baseLocation, ArmorStandItemOffset offset, Rotation rot) {
        double x = baseLocation.getX();
        double y = baseLocation.getY();
        double z = baseLocation.getZ();

        // Calculate radial form or yaw and pitch

        // head item offset
        double offsetValue = offset.getOffsetItemToHead();
        if (offsetValue > 0) {
            Vector eye = rot.toEyeDirection().multiply(offsetValue);
            x += eye.getX();
            y += eye.getY();
            z += eye.getZ();
        }

        // head rotation offset
        Vector head = rot.toHeadDirection().multiply(offset.getOffsetNeckToHead());
        x -= head.getX();
        y -= head.getY();
        z -= head.getZ();

        // normalization
        x = Math.round(x * 100000d) / 100000d;
        y = Math.round(y * 100000d) / 100000d;
        z = Math.round(z * 100000d) / 100000d;

        return new Location(baseLocation.getWorld(), x, y, z, baseLocation.getYaw(), baseLocation.getPitch());
    }

    /**
     * Sets all armor stand entities to null
     */
    public final void clearEntitiesAttribute() {
        armorStands.forEach(componentArmorStand -> componentArmorStand.setArmorStand(null));
    }
}
