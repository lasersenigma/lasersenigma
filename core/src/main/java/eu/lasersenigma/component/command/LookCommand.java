package eu.lasersenigma.component.command;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public class LookCommand extends LasersCommand {

    private static final int DEFAULT_RANGE = 5;

    public LookCommand() {
        super("look", "component.command.look.description");
        super.setPermission("lasers.edit");
        super.addArgument("range", false, ArgumentType.integerOnly());
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {

        Optional<Area> area = Optional.ofNullable(Areas.getInstance().getAreaFromLocation(player.getLocation()));

        if (area.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(player, "player.error.must_be_inside_an_area");
            return false;
        }

        int range = DEFAULT_RANGE;

        if (super.hasArgumentValue(player, "range")) {
            range = super.getArgumentValue(player, "range", ArgumentType.integerOnly());
        }

        if (range < 0) {
            TranslationUtils.sendTranslatedMessage(player, "component.error.range_value_must_be_positive");
            return false;
        }

        Optional<Block> targetBlock = Optional.ofNullable(player.getTargetBlockExact(range));

        if (targetBlock.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(player, "component.error.no_targeted_block_within_the_range");
            return false;
        }

        Location targetBlockLocation = targetBlock.get().getLocation();
        Optional<IComponent> targetComponent = Optional.ofNullable(area.get().getComponentFromLocation(targetBlockLocation));

        if (targetComponent.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(player, "component.error.no_targeted_component_within_the_range");
            return false;
        }

        showComponentInformation(player, targetComponent.get());

        return true;
    }

    private void showComponentInformation(Player player, IComponent component) {
        TranslationUtils.sendTranslatedMessage(player, "component.command.look.header");
        TranslationUtils.sendTranslatedMessage(player, "component.command.look.area_id", component.getArea().getAreaId());
        TranslationUtils.sendTranslatedMessage(player, "component.command.look.component_id", component.getComponentId());
        TranslationUtils.sendTranslatedMessage(player, "component.command.look.component_type", component.getComponentType());
        TranslationUtils.sendTranslatedMessage(player, "component.command.look.coordinates",
                component.getComponentLocation().getBlockX(),
                component.getComponentLocation().getBlockY(),
                component.getComponentLocation().getBlockZ());

        String interfaces = Arrays.stream(component.getClass().getInterfaces())
                .map(Class::getSimpleName)
                // Extract relevant part
                .map(name -> name.replaceAll("^I(.*)$", "$1"))
                .map(name -> name.replaceAll("^(.*)Component$", "$1"))
                // In order to remove IComponent from the result
                .filter(name -> !name.equalsIgnoreCase(""))
                .collect(Collectors.joining(", "));

        TranslationUtils.sendTranslatedMessage(player, "component.command.look.interfaces", interfaces);
    }


}
