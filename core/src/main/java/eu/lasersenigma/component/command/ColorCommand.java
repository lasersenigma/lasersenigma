package eu.lasersenigma.component.command;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.command.LasersSenderCommand;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IColorableComponent;
import eu.lasersenigma.component.IComponent;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Optional;

public class ColorCommand extends LasersSenderCommand {

    public ColorCommand() {
        super("color", "component.command.color.description");
        super.setPermission("lasers.edit");
        super.addArgument("x", true, ArgumentType.doubleOnly());
        super.addArgument("y", true, ArgumentType.doubleOnly());
        super.addArgument("z", true, ArgumentType.doubleOnly());
        super.addArgument("color", true, ArgumentType.string());
    }

    @Override
    public void reloadAutoCompleteValues(CommandSender sender, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "x", "y", "z": {
                super.setAutoCompleteValuesArg(currentArgumentName, currentArgumentName);
                break;
            }
            case "color": {
                super.setAutoCompleteValuesArg(currentArgumentName,
                        Arrays.stream(LasersColor.values())
                                .map(Enum::name)
                                .toArray(String[]::new)
                );
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, CommandSender sender, String... strings) {
        if (!(sender instanceof Player || sender instanceof BlockCommandSender)) {
            TranslationUtils.sendTranslatedMessage(sender, "command.error.must_be_executed_by_a_player_or_from_command_block");
            return false;
        }

        World componentWorld;

        if (sender instanceof Player player) {
            componentWorld = player.getWorld();
        } else {
            componentWorld = ((BlockCommandSender) sender).getBlock().getWorld();
        }

        double componentX = super.getArgumentValue(sender, "x", ArgumentType.doubleOnly());
        double componentY = super.getArgumentValue(sender, "y", ArgumentType.doubleOnly());
        double componentZ = super.getArgumentValue(sender, "z", ArgumentType.doubleOnly());
        String color = super.getArgumentValue(sender, "color", ArgumentType.string());

        Location componentLocation = new Location(componentWorld, componentX, componentY, componentZ);
        Optional<Area> area = Optional.ofNullable(Areas.getInstance().getAreaFromLocation(componentLocation));

        if (area.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(sender, "component.error.no_area_for_this_location"); // TODO: translation
            return false;
        }

        // With the current architecture, this is legal, but it won't with a future caching system
        if (!area.get().isActivated()) {
            TranslationUtils.sendTranslatedMessage(sender, "player.error.must_be_inside_an_area");
            return false;
        }

        Optional<IComponent> component = Optional.ofNullable(area.get().getComponentFromLocation(componentLocation));

        if (component.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(sender, "component.error.component_not_found");
            return false;
        }

        if (!(component.get() instanceof IColorableComponent)) {
            TranslationUtils.sendTranslatedMessage(sender, "component.error.component_not_colorable");
            return false;
        }

        try {
            ((IColorableComponent) component.get()).setColor(LasersColor.valueOf(color), false);
        } catch (IllegalArgumentException e) {
            TranslationUtils.sendTranslatedMessage(sender, "component.error.color_does_not_exist");
        }

        return true;
    }
}
