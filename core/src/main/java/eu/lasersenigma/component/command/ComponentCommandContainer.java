package eu.lasersenigma.component.command;

import eu.lasersenigma.common.command.LasersSenderCommand;
import eu.lasersenigma.common.exception.AbstractLasersException;
import fr.skytale.commandlib.Commands;
import org.bukkit.command.CommandSender;

public class ComponentCommandContainer extends LasersSenderCommand {

    public ComponentCommandContainer() {
        super("component", "component.command.description");
        super.setPermission("lasers.edit");
        super.registerSubCommand(new LookCommand());
        super.registerSubCommand(new ColorCommand());
    }

    @Override
    protected boolean execute(Commands commands, CommandSender commandSender, String... strings) throws AbstractLasersException {
        return super.defaultProcessForContainer(commands, commandSender, strings);
    }
}
