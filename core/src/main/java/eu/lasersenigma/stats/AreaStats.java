package eu.lasersenigma.stats;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.exception.AreaStatsLinkedException;
import eu.lasersenigma.area.exception.AreaStatsNotLinkedException;
import eu.lasersenigma.area.exception.SameAreaException;
import eu.lasersenigma.common.database.Database;
import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.stats.exception.PlayerStatsNotFoundException;
import eu.lasersenigma.stats.listener.PlayerLeftAreaStatsAndVictoryDetectionListener;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class AreaStats {

    public static final String SEPARATOR = ": ";
    private final Area area;
    private final HashMap<UUID, PlayerLeftAreaStatsAndVictoryDetectionListener> listeners;
    private Area linkedArea;
    private LinkedHashMap<UUID, Integer> nbActionPlayersRecord;
    private LinkedHashMap<UUID, Integer> nbStepPlayersRecord;
    private LinkedHashMap<UUID, Duration> durationPlayersRecord;

    public AreaStats(Area area) {
        this.area = area;
        this.nbActionPlayersRecord = new LinkedHashMap<>();
        this.nbStepPlayersRecord = new LinkedHashMap<>();
        this.durationPlayersRecord = new LinkedHashMap<>();
        this.listeners = new HashMap<>();
        this.linkedArea = null;
    }

    public static String toStr(Duration duration) {
        return DurationFormatUtils.formatDurationHMS(duration.toMillis());
    }

    public static void showStats(Player player, String playerName, PlayerStats playerStats) {
        String[] messages = new String[4];
        messages[0] = ChatColor.UNDERLINE + ChatColor.BOLD.toString() + playerName;
        messages[1] = ChatColor.BOLD + TranslationUtils.getTranslation(player, "messages.duration") + SEPARATOR + ChatColor.BOLD + ChatColor.GOLD + toStr(playerStats.duration());
        messages[2] = ChatColor.BOLD + TranslationUtils.getTranslation(player, "messages.nbactions") + SEPARATOR + ChatColor.BOLD + ChatColor.GOLD + playerStats.nbAction();
        messages[3] = ChatColor.BOLD + TranslationUtils.getTranslation(player, "messages.nbsteps") + SEPARATOR + ChatColor.BOLD + ChatColor.GOLD + playerStats.nbStep();
        player.sendMessage(messages);
    }

    public Area getLinkedArea() {
        return this.linkedArea;
    }

    public void setLinkedArea(Area area) {
        this.linkedArea = area;
    }

    public boolean isLinked() {
        return linkedArea != null;
    }

    public void onActionDone(UUID playerUUID) {
        PlayerLeftAreaStatsAndVictoryDetectionListener listener = listeners.get(playerUUID);
        if (listener != null) {
            listener.incrementNbAction();
        }
    }

    public LinkedHashMap<UUID, Integer> getNbActionPlayersRecord() {
        return nbActionPlayersRecord;
    }

    public LinkedHashMap<UUID, Integer> getNbStepPlayersRecord() {
        return nbStepPlayersRecord;
    }

    public LinkedHashMap<UUID, Duration> getDurationPlayersRecord() {
        return durationPlayersRecord;
    }

    public void onActionDone(Player player) {
        PlayerLeftAreaStatsAndVictoryDetectionListener listener = listeners.get(player.getUniqueId());
        if (listener != null) {
            listener.incrementNbAction();
        }
    }

    public void addStatsFromDb(HashMap<UUID, PlayerStats> areaPlayerStats) {
        areaPlayerStats.forEach((key, value) -> {
            nbActionPlayersRecord.put(key, value.nbAction());
            nbStepPlayersRecord.put(key, value.nbStep());
            durationPlayersRecord.put(key, value.duration());
        });
        sortRecords();
    }

    public PlayerStats addStats(UUID playerUUID, Duration duration, int nbAction, int nbStep) {
        if (isLinked()) {
            return linkedArea.getStats().addStats(playerUUID, duration, nbAction, nbStep);
        }
        Duration oldDuration = durationPlayersRecord.get(playerUUID);
        Integer oldNbStep = nbStepPlayersRecord.get(playerUUID);
        Integer oldNbAction = nbActionPlayersRecord.get(playerUUID);
        boolean creationFlag = false;
        boolean updateFlag = false;
        if (oldDuration == null && oldNbStep == null && oldNbAction == null) {
            creationFlag = true;
        }
        if (oldDuration == null || oldDuration.getSeconds() > duration.getSeconds()) {
            updateFlag = true;
            durationPlayersRecord.put(playerUUID, duration);
        }
        if (oldNbStep == null || oldNbStep > nbStep) {
            updateFlag = true;
            nbStepPlayersRecord.put(playerUUID, nbStep);
        }
        if (oldNbAction == null || oldNbAction > nbAction) {
            updateFlag = true;
            nbActionPlayersRecord.put(playerUUID, nbAction);
        }
        if (updateFlag) {
            Database db = LasersEnigmaPlugin.getInstance().getPluginDatabase();
            PlayerStats newRecord = new PlayerStats(
                    durationPlayersRecord.get(playerUUID),
                    nbActionPlayersRecord.get(playerUUID),
                    nbStepPlayersRecord.get(playerUUID));
            sortRecords();
            if (creationFlag) {
                db.createStats(area.getAreaId(), playerUUID, newRecord);
            } else {
                db.updateStats(area.getAreaId(), playerUUID, newRecord);
            }
        }
        return new PlayerStats(duration, nbAction, nbStep);
    }

    public void removeListener(UUID playerUUID) {
        this.listeners.remove(playerUUID);
    }

    public void addListener(UUID playerUUID, PlayerLeftAreaStatsAndVictoryDetectionListener playerLeftAreaStatsAndVictoryDetectionListener) {
        this.listeners.put(playerUUID, playerLeftAreaStatsAndVictoryDetectionListener);
    }

    private void sortRecords() {
        durationPlayersRecord = durationPlayersRecord
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        nbActionPlayersRecord = nbActionPlayersRecord
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        nbStepPlayersRecord = nbStepPlayersRecord
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public void showStats(Player player) {
        showStats(player, player.getUniqueId());
    }

    public void showStats(Player player, UUID playerUUID) {
        if (isLinked()) {
            linkedArea.getStats().showStats(player, playerUUID);
            return;
        }
        try {
            PlayerStats playerStats = getStats(playerUUID);
            OfflinePlayer oPlayer = LasersEnigmaPlugin.getInstance().getServer().getOfflinePlayer(playerUUID);
            showStats(player, oPlayer.getName(), playerStats);
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player, ex);
        }
    }

    public PlayerStats getStats(UUID playerUUID) throws PlayerStatsNotFoundException {
        if (isLinked()) {
            return linkedArea.getStats().getStats(playerUUID);
        }
        Duration durationRecord = durationPlayersRecord.get(playerUUID);
        if (durationRecord == null) {
            throw new PlayerStatsNotFoundException();
        }
        Integer nbActionRecord = nbActionPlayersRecord.get(playerUUID);
        Integer nbStepRecord = nbStepPlayersRecord.get(playerUUID);
        return new PlayerStats(durationRecord, nbActionRecord, nbStepRecord);
    }

    public void clear() {
        if (isLinked()) {
            linkedArea.getStats().clear();
            return;
        }
        clearAllAreaStats();
    }

    private void clearAllAreaStats() {
        Database db = LasersEnigmaPlugin.getInstance().getPluginDatabase();
        db.clearStats(area);
        durationPlayersRecord.clear();
        nbActionPlayersRecord.clear();
        nbStepPlayersRecord.clear();
    }

    public void linkStats(Area area) throws SameAreaException, AreaStatsLinkedException {
        if (isLinked()) {
            throw new AreaStatsLinkedException();
        }
        if (this.area == area) {
            throw new SameAreaException();
        }
        this.linkedArea = area;
        mergeStats(area);
        clearAllAreaStats();
    }

    public void unlinkStats() throws AreaStatsNotLinkedException {
        if (!isLinked()) {
            throw new AreaStatsNotLinkedException();
        }
        copyStats(linkedArea);
        this.linkedArea = null;
    }

    private void mergeStats(Area area) {
        durationPlayersRecord.forEach((key, value) -> area.getStats().addStats(
                key,
                value,
                nbActionPlayersRecord.get(key),
                nbStepPlayersRecord.get(key)
        ));
    }

    private void copyStats(Area linkedArea) {
        linkedArea.getStats().getDurationPlayersRecord().forEach((key, value) -> area.getStats().addStats(
                key,
                value,
                linkedArea.getStats().getNbActionPlayersRecord().get(key),
                linkedArea.getStats().getNbStepPlayersRecord().get(key)
        ));
    }
}