package eu.lasersenigma.stats.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.VictoryArea;
import eu.lasersenigma.area.event.AreaPlayerWinAndLeaveEvent;
import eu.lasersenigma.area.event.AreaWinConditionsReachedEvent;
import eu.lasersenigma.area.event.PlayerLeftAreaEvent;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.stats.AreaStats;
import eu.lasersenigma.stats.PlayerStats;
import eu.lasersenigma.stats.exception.PlayerStatsNotFoundException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;

/**
 * EventListener for SongEndEvent
 */
public class PlayerLeftAreaStatsAndVictoryDetectionListener implements Listener {

    private static final String SAVE_VICTORY_EVEN_IF_PLAYER_CHANGED_WORLD_CONFIG_PATH = "save_victory_even_if_player_changed_world";

    private static final String SAVE_VICTORY_EVEN_IF_PLAYER_TELEPORTED_CONFIG_PATH = "save_victory_even_if_player_teleported";

    private final Area area;

    private final UUID playerUUID;
    private final LocalDateTime playerEnteredAreaDateTime;
    private int taskId;
    private int nbStep;
    private int nbAction;
    private Location currentLocation;
    private double lastDistance;

    /**
     * Constructor
     *
     * @param area   the area to follow
     * @param player the player to follow
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerLeftAreaStatsAndVictoryDetectionListener(Area area, Player player) {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        this.area = area;
        this.playerUUID = player.getUniqueId();
        this.nbStep = 0;
        this.nbAction = 0;
        this.playerEnteredAreaDateTime = LocalDateTime.now();
        this.currentLocation = player.getLocation();
        this.lastDistance = 0;
        this.taskId = -1;
        this.taskId = LasersEnigmaPlugin.getInstance().getServer().getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), () -> {
            Location playerNewLocation = player.getLocation();
            World previousWorld = currentLocation.getWorld();
            World newWorld = playerNewLocation.getWorld();
            if (previousWorld == null || newWorld == null || !previousWorld.getUID().equals(newWorld.getUID())) {
                if (this.taskId != -1) {
                    LasersEnigmaPlugin.getInstance().getServer().getScheduler().cancelTask(taskId);
                    taskId = -1;
                }
                return;
            }
            double distance = playerNewLocation.distance(currentLocation) + lastDistance;
            if (distance >= 1) {
                nbStep += Math.floor(distance);
                lastDistance = distance % 1;
                currentLocation = player.getLocation();
            }
        }, 0, 30).getTaskId();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed when a player leaves the area.
     *
     * @param e a PlayerLeftAreaLEEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerLeftAreaLEEvent(PlayerLeftAreaEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerLeftAreaLEEvent");
        final Player player = e.getPlayer();

        if (!player.getUniqueId().equals(playerUUID)) {
            return;
        }
        if (taskId != -1) {
            LasersEnigmaPlugin.getInstance().getServer().getScheduler().cancelTask(taskId);
            taskId = -1;
        }
        PlayerLeftAreaEvent.getHandlerList().unregister(this);
        checkVictory(e, player);
    }

    private void checkVictory(PlayerLeftAreaEvent e, Player player) {
        AreaStats stats = area.getStats();
        if (
                (e.getArea() != area)
                || (!player.isOnline())
                || (
                        LasersEnigmaPlugin.getInstance().getConfig().getBoolean(SAVE_VICTORY_EVEN_IF_PLAYER_CHANGED_WORLD_CONFIG_PATH) &&
                        !Objects.requireNonNull(area.getAreaMinLocation().getWorld()).getUID().equals(
                                Objects.requireNonNull(player.getLocation().getWorld()).getUID()
                        )
                )
                || (
                        LasersEnigmaPlugin.getInstance().getConfig().getBoolean(SAVE_VICTORY_EVEN_IF_PLAYER_TELEPORTED_CONFIG_PATH) &&
                        e.getLastPlayerLocationInsideArea().distance(player.getLocation()) > 50)
        ) {
            stats.removeListener(player.getUniqueId());
            return;
        }

        switch (area.getVictoryDetectionMode()) {
            case DETECTION_VICTORY_AREA:
                //Checking if the players exited by the area's end (one of the victory areas)
                VictoryArea vArea = null;
                for (VictoryArea tmpVArea : area.getVictoryAreas()) {
                    if (tmpVArea.containsLocation(e.getLastPlayerLocationInsideArea())) {
                        vArea = tmpVArea;
                        break;
                    }
                }
                if (vArea == null) {
                    //left without winning
                    stats.removeListener(player.getUniqueId());
                    return;
                }
                // call PlayerReachedAreaWinConditionsLEEvent for VictoryArea win condition only
                // the call of PlayerReachedAreaWinConditionsLEEvent for numeric conditions is done
                // in Area#computeAndCallAreaWinConditionsReachedEvent
                Bukkit.getPluginManager().callEvent(new AreaWinConditionsReachedEvent(player, area));

                break;
            case DETECTION_LASER_RECEIVERS:
                //Checking other victory conditions
                if (!area.checkVictoryConditionsReached()) {
                    stats.removeListener(player.getUniqueId());
                    return;
                }
                break;
            default:
                throw new IllegalStateException();
        }

        // At this point the player won the area


        //Compute new record
        LEPlayers.getInstance().findLEPlayer(player).setCurrentVictoryCheckpoint(e.getLastPlayerLocationInsideArea().clone());
        Duration duration = Duration.between(playerEnteredAreaDateTime, LocalDateTime.now());
        PlayerStats oldRecord;
        try {
            oldRecord = stats.getStats(playerUUID);
        } catch (PlayerStatsNotFoundException ex) {
            oldRecord = null;
        }
        PlayerStats playerStats = stats.addStats(playerUUID, duration, nbAction, nbStep);
        PlayerStats newRecord;
        try {
            newRecord = stats.getStats(playerUUID);
        } catch (PlayerStatsNotFoundException ex) {
            //should never happen since we just saved stats
            newRecord = null;
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, null, ex);
        }

        //send a PlayerWonAndLeftAreaLEEvent
        Bukkit.getServer().getPluginManager().callEvent(new AreaPlayerWinAndLeaveEvent(
                        player,
                        e.getArea(),
                        playerStats,
                        oldRecord,
                        newRecord,
                        e.getLastPlayerLocationInsideArea()
                )
        );

        stats.removeListener(player.getUniqueId());

        //show stats
        if (LasersEnigmaPlugin.getInstance().getConfig().getBoolean("show_stats_on_player_left_area")) {
            AreaStats.showStats(player, player.getName(), playerStats);
        }
    }

    public void incrementNbAction() {
        nbAction++;
    }

}
