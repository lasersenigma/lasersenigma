package eu.lasersenigma.stats.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class PlayerStatsNotFoundException extends AbstractLasersException {

    public PlayerStatsNotFoundException() {
        super("errors.player_stats_not_found");
    }

}
