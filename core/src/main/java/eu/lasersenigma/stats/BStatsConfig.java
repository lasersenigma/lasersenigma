package eu.lasersenigma.stats;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.config.ConfigData;
import eu.lasersenigma.player.LEPlayers;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.AdvancedPie;
import org.bstats.charts.SimplePie;
import org.bstats.charts.SingleLineChart;

import java.util.function.Function;
import java.util.stream.Collectors;

public class BStatsConfig {

    public static void addCustomCharts(Metrics metrics) {
        metrics.addCustomChart(new SimplePie("language", () -> LasersEnigmaPlugin.getInstance().getConfig().getString(ConfigData.LANGUAGE)));
        metrics.addCustomChart(new SingleLineChart("nb_areas", () -> Areas.getInstance().getAreas().size()));
        metrics.addCustomChart(new SingleLineChart("nb_components", () -> Areas.getInstance().getAreas().stream()
                .map(a -> a.getComponents().size())
                .reduce(Integer::sum).orElse(0))
        );
        metrics.addCustomChart(new AdvancedPie("component_types", () -> Areas.getInstance().getAreas().stream()
                .flatMap(area -> area.getComponents().stream())
                .map(c -> c.getComponentType().toString())
                .collect(Collectors.groupingBy(Function.identity(),Collectors.summingInt(a -> 1)))));
        metrics.addCustomChart(new SingleLineChart("nb_areas_solved", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> player.getPerAreaRecords(null).size())
                .reduce(0, Integer::sum)
        ));
        metrics.addCustomChart(new SingleLineChart("nb_actions_records", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> (int) player.getTotalNbActionDoneInSolvedAreasRecords(null))
                .reduce(0, Integer::sum)
        ));
        metrics.addCustomChart(new SingleLineChart("nb_steps_records", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> (int) player.getTotalNbStepsInSolvedAreasRecords(null))
                .reduce(0, Integer::sum)
        ));
        metrics.addCustomChart(new SingleLineChart("nb_minutes_records", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> (int) player.getTotalTimeSpentInSolvedAreasRecords(null).toMinutes())
                .reduce(0, Integer::sum)
        ));
    }
}
