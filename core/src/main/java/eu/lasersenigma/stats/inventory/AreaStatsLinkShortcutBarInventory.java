package eu.lasersenigma.stats.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AShortcutBarInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.inventory.MainShortcutBarInventory;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Benjamin
 */
public class AreaStatsLinkShortcutBarInventory extends AShortcutBarInventory {

    private final Area area;

    public AreaStatsLinkShortcutBarInventory(LEPlayer player, Area area) {
        super(player);
        this.area = area;
    }

    @Override
    protected ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA);
        shortcutBar = addEmpty(shortcutBar, 7);
        shortcutBar.add(Item.AREA_STATS_LINK_SHORTCUTBAR_EXIT);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case AREA_STATS_LINK_SHORTCUTBAR_EXIT:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new MainShortcutBarInventory(player));
                }, 1);
                break;
            case AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA:
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.ADMIN)) {
                    return;
                }
                Area area2 = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area2 == null) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new AreaStatsLinkConfirmationInventory(player, area, area2));
                }, 1);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA, Item.AREA_STATS_LINK_SHORTCUTBAR_EXIT)).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.AREA_STATS_LINK_SHORTCUTBAR;
    }

}
