package eu.lasersenigma.stats.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.inventory.APaginableStatsOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.stats.PlayerStats;
import eu.lasersenigma.stats.exception.PlayerStatsNotFoundException;
import org.bukkit.inventory.ItemStack;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.logging.Level;

public final class AreaStatsTimeListInventory extends APaginableStatsOpenableInventory {

    private final Area area;

    public AreaStatsTimeListInventory(LEPlayer player, Area area) {
        super(player, "messages.area_stats_time_menu_title", getItemList(area));
        this.area = area;
    }

    private static ArrayList<ItemStack> getItemList(Area area) {
        ArrayList<ItemStack> result = new ArrayList<>();
        ItemsFactory itemFactory = ItemsFactory.getInstance();
        Iterator<Entry<UUID, Duration>> it = area.getStats().getDurationPlayersRecord().entrySet().iterator();
        int rank = 0;
        Duration lastStats = null;
        while (it.hasNext()) {
            Entry<UUID, Duration> entry = it.next();
            try {
                if (!entry.getValue().equals(lastStats)) {
                    rank++;
                }
                lastStats = entry.getValue();
                PlayerStats playerStats = area.getStats().getStats(entry.getKey());
                result.add(itemFactory.getPlayerStatsItem(entry.getKey(), getRankAndStats(rank, playerStats, InventoryType.AREA_STATS_TIME_MENU)));
            } catch (PlayerStatsNotFoundException ex) {
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "Player's stats not found. Player UUID=%s", entry.getKey());
                TranslationUtils.sendExceptionMessage(ex);

                result.add(itemFactory.getPlayerStatsItem(entry.getKey(), new StringBuilder(getRank(rank)).append(NEW_LINE_STR).append("error retrieving stats").toString()));
            }
        }
        return result;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.AREA_STATS_TIME_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }
}
