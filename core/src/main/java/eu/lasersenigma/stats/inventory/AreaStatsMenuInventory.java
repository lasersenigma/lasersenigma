package eu.lasersenigma.stats.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public final class AreaStatsMenuInventory extends AOpenableInventory {

    Area area;

    public AreaStatsMenuInventory(LEPlayer player, Area area) {
        super(player, "messages.stats_menu_title");
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> content = new ArrayList<>();
        ArrayList<Item> firstLine = new ArrayList<>(Arrays.asList(
                Item.AREA_STATS_TIME_LIST_MENU,
                Item.AREA_STATS_ACTION_LIST_MENU,
                Item.AREA_STATS_STEP_LIST_MENU
        ));
        if (Permission.ADMIN.hasPermission(player.getBukkitPlayer())) {
            firstLine = addEmpty(firstLine, 4);
            if (area.getStats().isLinked()) {
                firstLine.add(Item.AREA_STATS_UNLINK);
            } else {
                firstLine.add(Item.AREA_STATS_LINK);
            }
            firstLine.add(Item.AREA_STATS_CLEAR);
        }
        content.add(firstLine);
        return content;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEAreaStatsMenuInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != area) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().closeOpenedInventory();
            }, 1);
            return;
        }
        switch (item) {
            case AREA_STATS_TIME_LIST_MENU:
                player.getInventoryManager().openLEInventory(new AreaStatsTimeListInventory(player, area));
                break;
            case AREA_STATS_ACTION_LIST_MENU:
                player.getInventoryManager().openLEInventory(new AreaStatsActionListInventory(player, area));
                break;
            case AREA_STATS_STEP_LIST_MENU:
                player.getInventoryManager().openLEInventory(new AreaStatsStepListInventory(player, area));
                break;
            case AREA_STATS_LINK:
                player.getInventoryManager().openLEInventory(new AreaStatsLinkShortcutBarInventory(player, area));
                break;
            case AREA_STATS_UNLINK:
                player.getInventoryManager().openLEInventory(new AreaStatsUnlinkConfirmationInventory(player, area));
                break;
            case AREA_STATS_CLEAR:
                player.getInventoryManager().openLEInventory(new AreaStatsClearConfirmationInventory(player, area));
                break;
            default:
                throw new UnsupportedOperationException("Unknown item type");
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.AREA_STATS_TIME_LIST_MENU,
                Item.AREA_STATS_ACTION_LIST_MENU,
                Item.AREA_STATS_STEP_LIST_MENU,
                Item.AREA_STATS_LINK,
                Item.AREA_STATS_UNLINK,
                Item.AREA_STATS_CLEAR
        )).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.AREA_STATS_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
