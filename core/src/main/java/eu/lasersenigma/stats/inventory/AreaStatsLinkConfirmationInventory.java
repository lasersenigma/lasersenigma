package eu.lasersenigma.stats.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class AreaStatsLinkConfirmationInventory extends AOpenableInventory {

    private final Area area;

    private final Area area2;

    public AreaStatsLinkConfirmationInventory(LEPlayer player, Area area, Area area2) {
        super(player, "messages.area_stats_link_confirm_menu_title");
        this.area = area;
        this.area2 = area2;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.HELP_AREA_STATS_LINK_CONFIRM)));
        inv.add(new ArrayList<>(Arrays.asList(Item.CONFIRM_AREA_STATS_LINK, Item.EMPTY, Item.CANCEL_AREA_STATS_LINK)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEAreaStatsLinkConfirmationInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.ADMIN)) {
            return;
        }
        switch (item) {
            case CONFIRM_AREA_STATS_LINK:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                AreaController.statsLink(player, area, area2);
                break;
            case CANCEL_AREA_STATS_LINK:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.HELP_AREA_STATS_LINK_CONFIRM,
                Item.CONFIRM_AREA_STATS_LINK,
                Item.CANCEL_AREA_STATS_LINK
        )).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.AREA_STATS_LINK_CONFIRM;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
