package eu.lasersenigma.stats.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class AreaStatsUnlinkConfirmationInventory extends AOpenableInventory {

    private final Area area;

    public AreaStatsUnlinkConfirmationInventory(LEPlayer player, Area area) {
        super(player, "messages.area_stats_unlink_confirm_menu_title");
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.HELP_AREA_STATS_UNLINK_CONFIRM)));
        inv.add(new ArrayList<>(Arrays.asList(Item.CONFIRM_AREA_STATS_UNLINK, Item.EMPTY, Item.CANCEL_AREA_STATS_UNLINK)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEAreaStatsUnlinkConfirmationInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.ADMIN)) {
            return;
        }
        switch (item) {
            case CONFIRM_AREA_STATS_UNLINK:
                if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != area) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                        player.getInventoryManager().closeOpenedInventory();
                    }, 1);
                    return;
                }
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                AreaController.statsUnlink(player.getBukkitPlayer(), area);
                break;
            case CANCEL_AREA_STATS_UNLINK:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.HELP_AREA_STATS_UNLINK_CONFIRM,
                Item.CONFIRM_AREA_STATS_UNLINK,
                Item.CANCEL_AREA_STATS_UNLINK
        )).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.DELETE_AREA_CONFIRM_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
