package eu.lasersenigma.stats;

import java.time.Duration;

public record PlayerStats(Duration duration, int nbAction, int nbStep) {}
