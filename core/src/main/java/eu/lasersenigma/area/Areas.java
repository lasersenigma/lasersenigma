package eu.lasersenigma.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.exception.AreaCrossWorldsException;
import eu.lasersenigma.area.exception.AreaNoDepthException;
import eu.lasersenigma.area.exception.AreaOverlapException;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.database.Database;
import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.AArmorStandComponent;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import javax.annotation.Nullable;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Singleton class responsible for managing all areas
 */
public class Areas {

    // The static instance of this class (only one instance will be created)
    private static Areas instance;

    // Contains all the areas
    private final LinkedHashSet<Area> areas = new LinkedHashSet<>();

    // A map linking entities UUID to components for quicker access
    private final LinkedHashMap<UUID, IComponent> componentByEntityUUID = new LinkedHashMap<>();
    private final HashMap<Location, Area> areaByLocationCache = new HashMap<>();

    /**
     * Private constructor.
     */
    private Areas() {
    }

    /**
     * Gets the instance of this class
     *
     * @return an instance of this class
     */
    public static Areas getInstance() {
        if (instance == null) {
            instance = new Areas();
        }
        return instance;
    }

    /**
     * Creates a new area with specified parameters.
     *
     * @param a                             First location (not minimal location yet, simply a corner location).
     * @param b                             Second location (not maximal location yet, simply the opposite corner location).
     * @param areaId                        The ID of the area in the database or -1 if created in-game.
     * @param vRotationAllowed              Defines if the vertical rotation of mirrors is allowed in this area.
     * @param hRotationAllowed              Defines if the horizontal rotation of mirrors is allowed in this area.
     * @param concentratorsvRotationAllowed Defines if the vertical rotation of concentrators is allowed in this area.
     * @param concentratorshRotationAllowed Defines if the horizontal rotation of concentrators is allowed in this area.
     * @param lsVRotationAllowed            Defines if the vertical rotation of laser senders is allowed in this area.
     * @param lsHRotationAllowed            Defines if the horizontal rotation of laser senders is allowed in this area.
     * @param lrVRotationAllowed            Defines if the vertical rotation of laser receivers is allowed in this area.
     * @param lrHRotationAllowed            Defines if the horizontal rotation of laser receivers is allowed in this area.
     * @param arspheresizing                Defines if the resizing of attraction/repulsion spheres area of effect is allowed.
     * @param checkpoint                    The respawn point.
     * @param victoryCriteria               The victory criteria for the area.
     * @param minRange                      The minimum number of elements required for victory.
     * @param maxRange                      The maximum number of elements required for victory.
     * @return the created area.
     * @throws AreaOverlapException     if the new area overlaps another area.
     * @throws AreaCrossWorldsException if the 2 locations are in different worlds.
     * @throws AreaNoDepthException     if the area has no depth/width/height.
     * @throws NoAreaFoundException     if the checkpoint is not within the area.
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public Area createArea(Location a, Location b, int areaId, boolean vRotationAllowed, boolean hRotationAllowed, boolean concentratorsvRotationAllowed, boolean concentratorshRotationAllowed, boolean lsVRotationAllowed, boolean lsHRotationAllowed, boolean lrVRotationAllowed, boolean lrHRotationAllowed, boolean arspheresizing, Location checkpoint, DetectionMode victoryCriteria, int minRange, int maxRange) throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, NoAreaFoundException {
        // Create a new area
        Area newArea = new Area(a, b);

        // Check for overlapping with existing areas
        Area overlappingArea = getOverlappingOtherAreas(newArea);
        if (overlappingArea != null) {
            throw new AreaOverlapException(overlappingArea);
        }

        // Database creation and sound notification based on the areaId
        if (areaId == -1) {
            newArea.dbCreate();
            SoundLauncher.playSound(newArea.getAreaCenterLocation(), PlaySoundCause.CREATE_AREA);
        } else {
            newArea.setAreaId(areaId);
        }

        // Set rotation and other properties
        newArea.setVMirrorsRotationAllowed(vRotationAllowed);
        newArea.setHMirrorsRotationAllowed(hRotationAllowed);
        newArea.setHConcentratorsRotationAllowed(concentratorshRotationAllowed);
        newArea.setVConcentratorsRotationAllowed(concentratorsvRotationAllowed);
        newArea.setHLaserSendersRotationAllowed(lsHRotationAllowed);
        newArea.setVLaserSendersRotationAllowed(lsVRotationAllowed);
        newArea.setHLaserReceiversRotationAllowed(lrHRotationAllowed);
        newArea.setVLaserReceiversRotationAllowed(lrVRotationAllowed);
        newArea.setAttractionRepulsionSpheresResizingAllowed(arspheresizing);
        newArea.setAreaCheckpointLocation(checkpoint);
        newArea.setVictoryDetectionMode(victoryCriteria);
        newArea.setMinimumRange(minRange);
        newArea.setMaximumRange(maxRange);

        // Add the new area to the set
        areas.add(newArea);

        // Return the created area
        return newArea;
    }

    /**
     * Gets the set of all areas.
     *
     * @return The set containing all areas.
     */
    public LinkedHashSet<Area> getAreas() {
        return areas;
    }

    /**
     * Deletes the specified area and performs necessary cleanup.
     *
     * @param area The area to delete.
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void deleteArea(Area area) {
        // Remove the area's components and database entry
        area.deleteArea();
        area.dbRemove();

        // Remove the area from the location cache
        areaByLocationCache.entrySet().removeIf(e -> e.getValue().getAreaId() == area.getAreaId());

        // Play a sound to indicate the deletion
        SoundLauncher.playSound(area.getAreaCenterLocation(), PlaySoundCause.DELETE_AREA);

        // Remove the area from the set
        areas.remove(area);
    }

    /**
     * Gets a component from an entity
     *
     * @param entityUUID the entity UUID used to find a component
     * @return the component corresponding to the given entity UUID
     */
    public IComponent getComponentFromEntity(UUID entityUUID) {
        return componentByEntityUUID.get(entityUUID);
    }

    /**
     * Adds a pair entityUUID / component to the set used to have quicker access to components
     *
     * @param entityUUID the entity UUID
     * @param component  the component corresponding to this entity
     */
    public void addEntity(UUID entityUUID, IComponent component) {
        componentByEntityUUID.put(entityUUID, component);
    }

    /**
     * Removes an entity from the entityUUID / component set
     *
     * @param entityUUID the entity UUID
     */
    public void removeEntity(UUID entityUUID) {
        componentByEntityUUID.remove(entityUUID);
    }

    /**
     * Retrieves the area containing the specified location.
     *
     * @param location A location potentially inside an area.
     * @return The area containing the location, or null if the location is not inside any area.
     */
    public Area getAreaFromLocation(Location location) {
        // Check if the area is cached for the given location
        Area cachedArea = areaByLocationCache.get(location);

        if (cachedArea != null) return cachedArea;

        // Use stream to find the area that contains the location
        Area foundArea = areas.stream()
                .filter(area -> area.containsLocation(location))
                .findFirst()
                .orElse(null);

        if (foundArea != null) {
            // Cache the area for future reference
            areaByLocationCache.put(location, foundArea);
            return foundArea;
        }

        // Return null if no area contains the location
        return null;

        /*Area cachedArea = areaByLocationCache.get(location);
        if (cachedArea == null) {
            for (Area area : areas) {
                if (area.containsLocation(location)) {
                    areaByLocationCache.put(location, area);
                    return area;
                }
            }
            return null;
        } else {
            return cachedArea;
        }*/

    }

    public void clearAreaByLocationCache() {
        areaByLocationCache.clear();
    }

    public Area getArea(int correspondingLockAreaId) {
        return this.areas.stream()
                .filter(area -> area.getAreaId() != correspondingLockAreaId)
                .findAny()
                .orElse(null);
    }

    public Optional<Area> getAreaFromId(int areaId) {
        return areas.stream()
                .filter(area -> area.getAreaId() == areaId)
                .findFirst();
    }

    public List<String> getIDs() {
        return Areas.getInstance()
                .getAreas()
                .stream()
                .map(area -> Integer.toString(area.getAreaId()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Checks if the area overlaps another area
     *
     * @param area the area to test
     * @return true if the area is overlapping another existing area
     */
    public Area getOverlappingOtherAreas(Area area) {
        for (Area curArea : areas) {
            if (curArea.isOverlapping(area)) {
                return curArea;
            }
        }
        return null;
    }

    public Area getOverlappingOtherAreas(World world, double minX, double minY, double minZ, double maxX, double maxY, double maxZ, Set<Area> exceptions) {
        for (Area curArea : areas) {
            if (exceptions.contains(curArea)) {
                continue;
            }
            if (curArea.isOverlapping(world, minX, minY, minZ, maxX, maxY, maxZ)) {
                return curArea;
            }
        }
        return null;
    }

    /**
     * Loads areas from the database
     */
    public void loadAreas() {
        removeAllASEntities();
        try {
            LasersEnigmaPlugin.getInstance().getPluginDatabase().loadData();
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(ex);
        }
    }

    /**
     * Activates all areas.
     */
    @SuppressWarnings("Not implemented")
    public void activateAreas() {
    }

    /**
     * Deactivates all areas. This should be called on plugin deactivation.
     */
    public void deactivateAreas() {
        // Deactivate every area
        areas.forEach(area -> {
            area.deactivateArea();
            area.getComponents().forEach(IComponent::deactivateComponent);
        });

        // Remove all armor stands entities
        removeAllASEntities();

        // Clear all existing areas from cache
        areas.clear();
    }

    /**
     * Clears areas (to do on plugin disable)
     */
    /*public void clearAreas() {
        areas.forEach(area -> {
            area.reset();
            area.hideComponents();
        });
        removeAllASEntities();
        areas.clear();
    }*/

    /**
     * Updates every area. Used by AreaTask to update every area states.
     */
    public void updateAreas() {
        areas.forEach(Area::updateArea);
    }

    /**
     * Updates lasers particles
     */
    public void updateLasers() {
        areas.stream()
                .filter(AArea::isActivated)
                .forEach(Area::updateLasers);
    }

    protected void removeAllASEntities() {
        List<World> worldList = areas.stream()
                .map((area) -> area.getAreaMinLocation().getWorld())
                .distinct()
                .toList();

        worldList.forEach(world -> world.getEntities().stream()
                .filter(entity -> (entity instanceof ArmorStand))
                .map(entity -> (ArmorStand) entity)
                .filter(as -> (as.getCustomName() != null && as.getCustomName().equals(AArmorStandComponent.ARMOR_STAND_CUSTOM_NAME)))
                .forEach(Entity::remove)
        );
    }

    public void onWorldLoad(WorldLoadEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "Areas.onWorldLoad({0})", e.getWorld().getName());
        try {
            LasersEnigmaPlugin.getInstance().getPluginDatabase().loadData(e.getWorld().getName());
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(ex);
        }
    }

    public void onWorldUnload(WorldUnloadEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "Areas.onWorldUnload({0})", e.getWorld().getName());
        areas.removeIf(a -> Objects.requireNonNull(a.getAreaMinLocation().getWorld()).getUID().equals(e.getWorld().getUID()));
    }

    /**
     * This method copies the plugin's data from originWorldName to destinationWorldName
     * In order for this to work, the world destinationWorldName must not be loaded yet OR we need to call
     * Database#loadData(destinationWorldName) after.
     *
     * @param originWorldName      the name of the world we want to copy
     * @param destinationWorldName the name of the world where we want to paste all data
     * @param clearAndReplace      should we delete this world data before
     * @throws IllegalStateException
     */
    public void copyWorldData(String originWorldName, String destinationWorldName, boolean clearAndReplace) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(
                Level.FINEST,
                "copyWorldData(originWorldName: {0}, destinationWorldName: {1}, clearAndReplace: {2})",
                originWorldName,
                destinationWorldName,
                String.valueOf(clearAndReplace));

        if (Bukkit.getWorld(destinationWorldName) != null) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger()
                    .log(Level.SEVERE, "A world must be unloaded to paste data into it.");
            return;
        }

        Database db = LasersEnigmaPlugin.getInstance().getPluginDatabase();

        if (clearAndReplace) {
            db.clearWorldData(destinationWorldName);
        }
        db.copyWorldData(originWorldName, destinationWorldName);
    }

    public void removeWorldData(String worldName) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger()
                .log(Level.FINEST, "removeWorldData(worldName: {0})", worldName);

        if (Bukkit.getWorld(worldName) != null) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger()
                    .log(Level.SEVERE, "A world must be unloaded to delete all its data.");
            return;
        }
        LasersEnigmaPlugin.getInstance().getPluginDatabase().clearWorldData(worldName);
    }
}
