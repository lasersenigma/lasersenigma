package eu.lasersenigma.area.schematic;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.VictoryArea;
import eu.lasersenigma.area.exception.*;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class VictoryAreaSchematicManager {

    private VictoryAreaSchematicManager() {

    }

    public static VictoryAreaSchematic createSchematic(VictoryArea va, Location playerLocation) {
        VictoryAreaSchematic areaSchematic = new VictoryAreaSchematic();
        Vector vMin = va.getMin().toVector().subtract(playerLocation.toVector());
        Vector vMax = va.getMax().toVector().subtract(playerLocation.toVector());
        areaSchematic.setMinLocDiffX(vMin.getX());
        areaSchematic.setMinLocDiffY(vMin.getY());
        areaSchematic.setMinLocDiffZ(vMin.getZ());
        areaSchematic.setMaxLocDiffX(vMax.getX());
        areaSchematic.setMaxLocDiffY(vMax.getY());
        areaSchematic.setMaxLocDiffZ(vMax.getZ());
        return areaSchematic;
    }

    public static void createVictoryArea(Area area, VictoryAreaSchematic vaSchematic, Location playerLocation) throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, NoAreaFoundException {
        area.addVictoryArea(new VictoryArea(
                area,
                playerLocation.clone().add(new Vector(
                        vaSchematic.getMinLocDiffX(),
                        vaSchematic.getMinLocDiffY(),
                        vaSchematic.getMinLocDiffZ()
                )),
                playerLocation.clone().add(new Vector(
                        vaSchematic.getMaxLocDiffX(),
                        vaSchematic.getMaxLocDiffY(),
                        vaSchematic.getMaxLocDiffZ()
                ))
        ));
    }
}
