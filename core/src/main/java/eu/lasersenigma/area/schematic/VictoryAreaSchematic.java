package eu.lasersenigma.area.schematic;

import java.io.Serializable;

public class VictoryAreaSchematic implements Serializable {

    public static final long serialVersionUID = 1;

    private double minLocDiffX = 0;
    private double minLocDiffY = 0;
    private double minLocDiffZ = 0;
    private double maxLocDiffX = 0;
    private double maxLocDiffY = 0;
    private double maxLocDiffZ = 0;

    public double getMinLocDiffX() {
        return minLocDiffX;
    }

    public void setMinLocDiffX(double minLocDiffX) {
        this.minLocDiffX = minLocDiffX;
    }

    public double getMinLocDiffY() {
        return minLocDiffY;
    }

    public void setMinLocDiffY(double minLocDiffY) {
        this.minLocDiffY = minLocDiffY;
    }

    public double getMinLocDiffZ() {
        return minLocDiffZ;
    }

    public void setMinLocDiffZ(double minLocDiffZ) {
        this.minLocDiffZ = minLocDiffZ;
    }

    public double getMaxLocDiffX() {
        return maxLocDiffX;
    }

    public void setMaxLocDiffX(double maxLocDiffX) {
        this.maxLocDiffX = maxLocDiffX;
    }

    public double getMaxLocDiffY() {
        return maxLocDiffY;
    }

    public void setMaxLocDiffY(double maxLocDiffY) {
        this.maxLocDiffY = maxLocDiffY;
    }

    public double getMaxLocDiffZ() {
        return maxLocDiffZ;
    }

    public void setMaxLocDiffZ(double maxLocDiffZ) {
        this.maxLocDiffZ = maxLocDiffZ;
    }
}
