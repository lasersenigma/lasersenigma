package eu.lasersenigma.area.schematic;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.*;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.elevator.Elevator;
import eu.lasersenigma.component.elevator.ElevatorCallButton;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.component.schematic.ComponentSchematic;
import eu.lasersenigma.component.schematic.ComponentSchematicManager;
import eu.lasersenigma.schematic.task.CreateAreaComponentsFromSchematicTask;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class AreaSchematicManager {

    private AreaSchematicManager() {

    }

    public static AreaSchematic createSchematic(Area area, Location playerLocation) {
        AreaSchematic areaSchematic = new AreaSchematic();
        Vector vMin = area.getAreaMinLocation().toVector().subtract(playerLocation.toVector());
        Vector vMax = area.getAreaMaxLocation().toVector().subtract(playerLocation.toVector());
        areaSchematic.setMinLocDiffX(vMin.getX());
        areaSchematic.setMinLocDiffY(vMin.getY());
        areaSchematic.setMinLocDiffZ(vMin.getZ());
        areaSchematic.setMaxLocDiffX(vMax.getX());
        areaSchematic.setMaxLocDiffY(vMax.getY());
        areaSchematic.setMaxLocDiffZ(vMax.getZ());
        areaSchematic.setVrotation(area.isVMirrorsRotationAllowed());
        areaSchematic.setHrotation(area.isHMirrorsRotationAllowed());
        areaSchematic.setCvrotation(area.isVConcentratorsRotationAllowed());
        areaSchematic.setChrotation(area.isHConcentratorsRotationAllowed());
        areaSchematic.setLsvrotation(area.isVLaserSendersRotationAllowed());
        areaSchematic.setLshrotation(area.isHLaserSendersRotationAllowed());
        areaSchematic.setLrvrotation(area.isVLaserReceiversRotationAllowed());
        areaSchematic.setLrhrotation(area.isHLaserReceiversRotationAllowed());
        areaSchematic.setArspheresizing(area.isAttractionRepulsionSpheresResizingAllowed());
        areaSchematic.setMode(area.getVictoryDetectionMode().toString());
        areaSchematic.setMinRange(area.getMinimumRange());
        areaSchematic.setMaxRange(area.getMaximumRange());
        if (area.getAreaCheckpointLocation() != null) {
            Vector vCheckpoint = area.getAreaCheckpointLocation().toVector().subtract(playerLocation.toVector());
            areaSchematic.setCheckpointX(vCheckpoint.getX());
            areaSchematic.setCheckpointY(vCheckpoint.getY());
            areaSchematic.setCheckpointZ(vCheckpoint.getZ());
            areaSchematic.setCheckpointPitch(area.getAreaCheckpointLocation().getPitch());
            areaSchematic.setCheckpointYaw(area.getAreaCheckpointLocation().getYaw());
        }
        List<ComponentSchematic> componentSchematicList = new ArrayList<>();
        area.getComponents().stream()
                .filter(c -> (!(c instanceof LockKeyChest || c instanceof Lock || c instanceof Elevator || c instanceof ElevatorCallButton)))
                .forEach(c -> componentSchematicList.add(ComponentSchematicManager.createSchematic(c, playerLocation)));
        areaSchematic.setComponentSchematicList(componentSchematicList);
        List<VictoryAreaSchematic> victoryAreaSchematicList = new ArrayList<>();
        area.getVictoryAreas().forEach(va -> victoryAreaSchematicList.add(VictoryAreaSchematicManager.createSchematic(va, playerLocation)));
        areaSchematic.setVictoryAreaSchematicList(victoryAreaSchematicList);
        return areaSchematic;
    }

    public static CreateAreaComponentsFromSchematicTask createArea(AreaSchematic areaSchematic, Location playerLocation) throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, NoAreaFoundException {
        Location checkpointLocation = null;
        if (areaSchematic.getCheckpointPitch() != Float.MIN_VALUE) {
            checkpointLocation = playerLocation.clone().add(new Vector(
                    areaSchematic.getCheckpointX(),
                    areaSchematic.getCheckpointY(),
                    areaSchematic.getCheckpointZ()
            ));
            checkpointLocation.setPitch(areaSchematic.getCheckpointPitch());
            checkpointLocation.setYaw(areaSchematic.getCheckpointYaw());
        }
        final Area area = Areas.getInstance().createArea(
                playerLocation.clone().add(new Vector(
                        areaSchematic.getMinLocDiffX(),
                        areaSchematic.getMinLocDiffY(),
                        areaSchematic.getMinLocDiffZ()
                )),
                playerLocation.clone().add(new Vector(
                        areaSchematic.getMaxLocDiffX(),
                        areaSchematic.getMaxLocDiffY(),
                        areaSchematic.getMaxLocDiffZ()
                )),
                -1,
                areaSchematic.getVrotation(),
                areaSchematic.getHrotation(),
                areaSchematic.getCvrotation(),
                areaSchematic.getChrotation(),
                areaSchematic.getLsvrotation(),
                areaSchematic.getLshrotation(),
                areaSchematic.getLrvrotation(),
                areaSchematic.getLrhrotation(),
                areaSchematic.getArspheresizing(),
                checkpointLocation,
                DetectionMode.valueOf(areaSchematic.getMode()),
                areaSchematic.getMinRange(),
                areaSchematic.getMaxRange()
        );
        for (VictoryAreaSchematic vas : areaSchematic.getVictoryAreaSchematicList()) {
            VictoryAreaSchematicManager.createVictoryArea(area, vas, playerLocation);
        }
        return new CreateAreaComponentsFromSchematicTask(areaSchematic.getComponentSchematicList(), area, playerLocation);
    }
}
