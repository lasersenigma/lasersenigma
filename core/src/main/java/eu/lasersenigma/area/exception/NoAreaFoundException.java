package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class NoAreaFoundException extends AbstractLasersException {

    public NoAreaFoundException() {
        super("errors.no_area_found");
    }

}
