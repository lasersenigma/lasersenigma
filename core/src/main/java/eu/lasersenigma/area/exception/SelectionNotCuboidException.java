package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class SelectionNotCuboidException extends AbstractLasersException {
    public SelectionNotCuboidException() {
        super("errors.selection_not_cuboid");
    }

}
