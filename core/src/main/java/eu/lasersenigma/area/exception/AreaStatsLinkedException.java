package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class AreaStatsLinkedException extends AbstractLasersException {

    public AreaStatsLinkedException() {
        super("errors.stats_linked");
    }

}
