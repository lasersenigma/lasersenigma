package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class AreaCrossWorldsException extends AbstractLasersException {

    public AreaCrossWorldsException() {
        super("errors.area_cross_worlds");
    }

}
