package eu.lasersenigma.area.exception;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.exception.AbstractLasersException;

public class AreaOverlapException extends AbstractLasersException {

    private Area overlappedArea;
    public AreaOverlapException(Area overlappedArea) {
        super("errors.area.selection_overlap_with_another_area",
                overlappedArea.getAreaMinLocation().getBlockX(),
                overlappedArea.getAreaMinLocation().getBlockY(),
                overlappedArea.getAreaMinLocation().getBlockZ(),
                overlappedArea.getAreaMaxLocation().getBlockX(),
                overlappedArea.getAreaMaxLocation().getBlockY(),
                overlappedArea.getAreaMaxLocation().getBlockZ()
        );
        this.overlappedArea = overlappedArea;
    }

    public Area getOverlappedArea() {
        return overlappedArea;
    }
}
