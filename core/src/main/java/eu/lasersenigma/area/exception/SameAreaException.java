package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class SameAreaException extends AbstractLasersException {

    public SameAreaException() {
        super("errors.same_area");
    }

}
