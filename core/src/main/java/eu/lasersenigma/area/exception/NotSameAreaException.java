package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class NotSameAreaException extends AbstractLasersException {
    public NotSameAreaException() {
        super("errors.not_same_area");
    }

}
