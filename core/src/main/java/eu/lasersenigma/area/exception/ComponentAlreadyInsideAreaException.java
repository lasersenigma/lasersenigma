package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class ComponentAlreadyInsideAreaException extends AbstractLasersException {

    public ComponentAlreadyInsideAreaException() {
        super("errors.component_already_inside_area");
    }

}
