package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class AreaNoDepthException extends AbstractLasersException {
    public AreaNoDepthException() {
        super("errors.area_has_no_depth");
    }

}
