package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class InvalidSelectionException extends AbstractLasersException {

    public InvalidSelectionException() {
        super("errors.invalid_selection");
    }

}
