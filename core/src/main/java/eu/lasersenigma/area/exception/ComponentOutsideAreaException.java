package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.component.IComponent;

public class ComponentOutsideAreaException extends AbstractLasersException {

    private final IComponent component;

    /**
     * Constructor
     *
     * @param component the component that would be outside the area
     */
    public ComponentOutsideAreaException(IComponent component) {
        super("errors.component_outside_area");
        this.component = component;
    }

    public IComponent getComponent() {
        return component;
    }

}
