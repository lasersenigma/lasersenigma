package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class SelectionOverlapPartiallyAreaException extends AbstractLasersException {
    public SelectionOverlapPartiallyAreaException(Object... params) {
        super("errors.area.selection_overlap_with_another_area", params);
    }
}
