package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class AreaStatsNotLinkedException extends AbstractLasersException {

    public AreaStatsNotLinkedException() {
        super("errors.stats_not_linked");
    }

}
