package eu.lasersenigma.area.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class NoAreaFoundNearbyException extends AbstractLasersException {

    public NoAreaFoundNearbyException() {
        super("errors.no_areas_nearby");
    }

}
