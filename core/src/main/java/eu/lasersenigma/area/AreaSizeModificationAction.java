package eu.lasersenigma.area;

public enum AreaSizeModificationAction {
    CONTRACT, EXPAND
}
