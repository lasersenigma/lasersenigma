package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;

public class AreaDeactivatedEvent extends AAfterActionEvent implements IAreaEvent {

    private final Area area;

    public AreaDeactivatedEvent(Area area) {
        super();
        this.area = area;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
