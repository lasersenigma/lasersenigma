package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToSelectFirstLocDeleteAreaEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IAreaEvent {

    private final Player player;

    private final Area area;

    public PlayerTryToSelectFirstLocDeleteAreaEvent(Player player, Area area) {
        super();
        this.player = player;
        this.area = area;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
