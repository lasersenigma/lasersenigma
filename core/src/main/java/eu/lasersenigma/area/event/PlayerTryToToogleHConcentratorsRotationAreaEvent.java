package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToToogleHConcentratorsRotationAreaEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IAreaEvent {

    private final Player player;

    private final Area area;

    private final boolean newHRotationAuthorizationState;

    public PlayerTryToToogleHConcentratorsRotationAreaEvent(Player player, Area area, boolean newHRotationAuthorizationState) {
        super();
        this.player = player;
        this.area = area;
        this.newHRotationAuthorizationState = newHRotationAuthorizationState;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public boolean getNewHRotationAuthorizationState() {
        return newHRotationAuthorizationState;
    }

}
