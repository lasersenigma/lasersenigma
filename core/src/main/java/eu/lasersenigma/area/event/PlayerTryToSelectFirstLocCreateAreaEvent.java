package eu.lasersenigma.area.event;

import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerTryToSelectFirstLocCreateAreaEvent extends ABeforeActionPermissionEvent implements IPlayerEvent {

    private final Player player;

    private final Location firstLocation;

    public PlayerTryToSelectFirstLocCreateAreaEvent(Player player, Location firstLocation) {
        super();
        this.player = player;
        this.firstLocation = firstLocation;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public Location getFirstLocation() {
        return firstLocation;
    }

}
