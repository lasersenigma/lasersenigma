package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerLeftAreaEvent extends AAfterActionEvent implements IPlayerEvent, IAreaEvent {

    private final Player player;

    private final Area area;

    private final Location lastPlayerLocationInsideArea;

    public PlayerLeftAreaEvent(Player player, Area area, Location lastPlayerLocationInsideArea) {
        super();
        this.player = player;
        this.area = area;
        this.lastPlayerLocationInsideArea = lastPlayerLocationInsideArea;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public Location getLastPlayerLocationInsideArea() {
        return lastPlayerLocationInsideArea;
    }

}
