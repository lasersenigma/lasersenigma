package eu.lasersenigma.area.event;

import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerTryToCreateAreaEvent extends ABeforeActionPermissionEvent implements IPlayerEvent {

    private final Player player;

    private final Location firstLocation;

    private final Location secondLocation;

    public PlayerTryToCreateAreaEvent(Player player, Location firstLocation, Location secondLocation) {
        super();
        this.player = player;
        this.firstLocation = firstLocation;
        this.secondLocation = secondLocation;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public Location getFirstLocation() {
        return firstLocation;
    }

    public Location getSecondLocation() {
        return secondLocation;
    }

}
