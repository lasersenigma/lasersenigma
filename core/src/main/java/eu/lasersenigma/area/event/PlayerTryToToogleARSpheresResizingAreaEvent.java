package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.permission.event.ABeforeActionPermissionEvent;
import org.bukkit.entity.Player;

public class PlayerTryToToogleARSpheresResizingAreaEvent extends ABeforeActionPermissionEvent implements IPlayerEvent, IAreaEvent {

    private final Player player;

    private final Area area;

    private final boolean newARSpheresResizingState;

    public PlayerTryToToogleARSpheresResizingAreaEvent(Player player, Area area, boolean newARSpheresResizingState) {
        super();
        this.player = player;
        this.area = area;
        this.newARSpheresResizingState = newARSpheresResizingState;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public boolean getNewARSpheresResizingState() {
        return newARSpheresResizingState;
    }

}
