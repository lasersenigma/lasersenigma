package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;

public class PlayersCountChangeEvent extends AAfterActionEvent implements IAreaEvent {

    private final Area area;

    private final int oldNbPlayersInsideArea;

    private final int newNbPlayersInsideArea;

    public PlayersCountChangeEvent(Area area,
                                   int oldNbPlayersInsideArea, int newNbPlayersInsideArea) {
        super();
        this.area = area;
        this.oldNbPlayersInsideArea = oldNbPlayersInsideArea;
        this.newNbPlayersInsideArea = newNbPlayersInsideArea;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public int getOldNbPlayersInsideArea() {
        return oldNbPlayersInsideArea;
    }

    public int getNewNbPlayersInsideArea() {
        return newNbPlayersInsideArea;
    }

}
