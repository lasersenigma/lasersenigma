package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;
import org.bukkit.entity.Player;

public class AreaDeletedEvent extends AAfterActionEvent implements IAreaEvent {

    private final Area area;
    private final Player player;

    public AreaDeletedEvent(Area area, Player player) {
        super();
        this.area = area;
        this.player = player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public Player getPlayer() {
        return player;
    }
}
