package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import org.bukkit.entity.Player;

public class AreaWinConditionsReachedEvent extends AAfterActionEvent implements IPlayerEvent, IAreaEvent {

    private final Player player;

    private final Area area;

    public AreaWinConditionsReachedEvent(Player player, Area area) {
        super();
        this.player = player;
        this.area = area;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
