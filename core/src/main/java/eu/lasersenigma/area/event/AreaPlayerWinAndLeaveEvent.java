package eu.lasersenigma.area.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.AAfterActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import eu.lasersenigma.stats.PlayerStats;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class AreaPlayerWinAndLeaveEvent extends AAfterActionEvent implements IPlayerEvent, IAreaEvent {

    private final Player player;

    private final Area area;

    private final Location lastPlayerLocationInsideArea;

    private final PlayerStats stats;
    private final PlayerStats oldRecord;
    private final PlayerStats newRecord;

    public AreaPlayerWinAndLeaveEvent(Player player, Area area, PlayerStats stats, PlayerStats oldRecord, PlayerStats newRecord, Location lastPlayerLocationInsideArea) {
        super();
        this.player = player;
        this.area = area;
        this.stats = stats;
        this.oldRecord = oldRecord;
        this.newRecord = newRecord;
        this.lastPlayerLocationInsideArea = lastPlayerLocationInsideArea;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public PlayerStats getStats() {
        return stats;
    }

    public PlayerStats getOldRecord() {
        return oldRecord;
    }

    public PlayerStats getNewRecord() {
        return newRecord;
    }

    public Location getLastPlayerLocationInsideArea() {
        return lastPlayerLocationInsideArea;
    }

}
