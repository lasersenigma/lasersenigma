package eu.lasersenigma.area.command;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.common.message.TranslationUtils;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Optional;

public class TeleportCommand extends LasersCommand {

    public TeleportCommand() {
        super("teleport", "area.commands.teleport.description");
        super.setPermission("lasers.edit");
        super.addArgument("area_id", true, ArgumentType.integerOnly());
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "area_id": {
                super.setAutoCompleteValuesArg(currentArgumentName, Areas.getInstance().getIDs());
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        int areaId = super.getArgumentValue(player, "area_id", ArgumentType.integerOnly());

        Optional<Area> area = Areas.getInstance().getAreaFromId(areaId);

        if (area.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(player, "errors.area.not_exists");
            return false;
        }

        Location areaCenter = area.get().getAreaCenterLocation();
        player.teleport(areaCenter);

        return true;
    }
}
