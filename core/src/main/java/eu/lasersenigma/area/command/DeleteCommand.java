package eu.lasersenigma.area.command;

import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.player.LEPlayers;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class DeleteCommand extends LasersCommand {

    public DeleteCommand() {
        super("delete", "area.commands.delete.description");
        super.setPermission("lasers.edit");
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        AreaController.deleteArea(LEPlayers.getInstance().findLEPlayer(player), player.getLocation());
        return true;
    }
}
