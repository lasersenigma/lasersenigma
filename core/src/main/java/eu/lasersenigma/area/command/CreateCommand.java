package eu.lasersenigma.area.command;

import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.player.LEPlayers;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class CreateCommand extends LasersCommand {

    public CreateCommand() {
        super("create", "area.commands.create.description");
        super.setPermission("lasers.edit");
        super.addArgument("x1", true, ArgumentType.doubleOnly());
        super.addArgument("y1", true, ArgumentType.doubleOnly());
        super.addArgument("z1", true, ArgumentType.doubleOnly());
        super.addArgument("x2", true, ArgumentType.doubleOnly());
        super.addArgument("y2", true, ArgumentType.doubleOnly());
        super.addArgument("z2", true, ArgumentType.doubleOnly());
    }

    @Override
    public void reloadAutoCompleteValues(Player player, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "x1":
            case "x2": {
                super.setAutoCompleteValuesArg(currentArgumentName, Integer.toString(player.getEyeLocation().getBlockX()));
                break;
            }
            case "y1":
            case "y2": {
                super.setAutoCompleteValuesArg(currentArgumentName, Integer.toString(player.getEyeLocation().getBlockY()));
                break;
            }
            case "z1":
            case "z2": {
                super.setAutoCompleteValuesArg(currentArgumentName, Integer.toString(player.getEyeLocation().getBlockZ()));
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        double x1 = super.getArgumentValue(player, "x1", ArgumentType.doubleOnly());
        double y1 = super.getArgumentValue(player, "y1", ArgumentType.doubleOnly());
        double z1 = super.getArgumentValue(player, "z1", ArgumentType.doubleOnly());
        double x2 = super.getArgumentValue(player, "x2", ArgumentType.doubleOnly());
        double y2 = super.getArgumentValue(player, "y2", ArgumentType.doubleOnly());
        double z2 = super.getArgumentValue(player, "z2", ArgumentType.doubleOnly());
        Location firstLocation = new Location(player.getWorld(), x1, y1, z1);
        Location secondLocation = new Location(player.getWorld(), x2, y2, z2);

        AreaController.createArea(LEPlayers.getInstance().findLEPlayer(player), firstLocation);
        AreaController.createArea(LEPlayers.getInstance().findLEPlayer(player), secondLocation);

        return true;
    }
}
