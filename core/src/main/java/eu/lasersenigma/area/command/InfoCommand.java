package eu.lasersenigma.area.command;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.player.LEPlayers;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class InfoCommand extends LasersCommand {

    public InfoCommand() {
        super("info", "area.commands.info.description");
        super.setPermission("lasers.edit");
        super.addArgument("area_id", false, ArgumentType.integerOnly());
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "area_id": {
                super.setAutoCompleteValuesArg(currentArgumentName, Areas.getInstance().getIDs());
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        Optional<Area> area;
        if (!super.hasArgumentValue(player, "area_id")) {
            //Check if the player is in an area
            area = Optional.ofNullable(Areas.getInstance().getAreaFromLocation(player.getLocation()));

            //If the player is not in an area, find the nearest area
            if (area.isEmpty()) {
                area = AreaController.findNearestArea(LEPlayers.getInstance().findLEPlayer(player));
            }
        } else {
            int areaId = super.getArgumentValue(player, "area_id", ArgumentType.integerOnly());
            area = Areas.getInstance().getAreaFromId(areaId);
        }

        if (area.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(player, "errors.area.not_exists");
            return false;
        }

        showInformations(player, area.get());
        return true;
    }

    private void showInformations(Player player, Area area) {
        TranslationUtils.sendTranslatedMessage(player, "area.commands.info.line1",
                area.getAreaId());
        TranslationUtils.sendTranslatedMessage(player, "area.commands.info.line2",
                area.getAreaWorld().getName());
        TranslationUtils.sendTranslatedMessage(player, "area.commands.info.line3",
                area.getAreaMinLocation().getX(), area.getAreaMinLocation().getY(), area.getAreaMinLocation().getZ(),
                area.getAreaMaxLocation().getX(), area.getAreaMaxLocation().getY(), area.getAreaMaxLocation().getZ());

        //Number of components per component type
        String componentsCountPerComponentTypeStr = area.getComponents().stream()
                .map(IComponent::getComponentType)
                //group by component type and count
                .collect(Collectors.groupingBy(Function.identity(), Collectors.summingInt(e -> 1))).entrySet().stream()
                //sort by component type's usage
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                // format string
                .map(e -> StringUtils.capitalize(e.getKey().name().toLowerCase().replace("_", " ")) + ": " + e.getValue())
                .collect(Collectors.joining(", "));

        TranslationUtils.sendTranslatedMessage(player, "area.commands.info.line4",
                area.getComponents().size(),
                componentsCountPerComponentTypeStr
        );
    }
}
