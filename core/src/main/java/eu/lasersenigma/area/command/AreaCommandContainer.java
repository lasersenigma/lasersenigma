package eu.lasersenigma.area.command;

import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.common.exception.AbstractLasersException;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class AreaCommandContainer extends LasersCommand {

    public AreaCommandContainer() {
        super("area", "area.commands.description");
        super.setPermission("lasers.edit");
        super.registerSubCommand(new CreateCommand());
        super.registerSubCommand(new DeleteCommand());
        super.registerSubCommand(new ResizeCommand());
        super.registerSubCommand(new InfoCommand());
        super.registerSubCommand(new ShowCommand());
        super.registerSubCommand(new TeleportCommand());
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) throws AbstractLasersException {
        return super.defaultProcessForContainer(commands, player, strings);
    }
}
