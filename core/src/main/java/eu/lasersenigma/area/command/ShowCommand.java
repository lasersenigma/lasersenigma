package eu.lasersenigma.area.command;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.player.LEPlayers;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

import java.util.Optional;

public class ShowCommand extends LasersCommand {

    public ShowCommand() {
        super("show", "area.commands.show.description");
        super.setPermission("lasers.edit");
        super.addArgument("area_id", false, ArgumentType.integerOnly());
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "area_id": {
                super.setAutoCompleteValuesArg(currentArgumentName, Areas.getInstance().getIDs());
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        if (!super.hasArgumentValue(player, "area_id")) {
            AreaController.showNearestArea(LEPlayers.getInstance().findLEPlayer(player));
            return true;
        }

        int areaId = super.getArgumentValue(player, "area_id", ArgumentType.integerOnly());
        Optional<Area> area = Areas.getInstance().getAreaFromId(areaId);

        if (area.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(player, "errors.area.not_exists");
            return false;
        }

        area.get().show(player);

        return true;
    }
}
