package eu.lasersenigma.area.command;

import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.AreaSizeModificationAction;
import eu.lasersenigma.area.CardinalDirection;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.player.LEPlayers;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

public class ResizeCommand extends LasersCommand {

    public ResizeCommand() {
        super("resize", "area.commands.resize.description", "contract", "expand");
        super.setPermission("lasers.edit");
        super.addArgument("action", true, ArgumentType.fromEnum(AreaSizeModificationAction.class));
        super.addArgument("amount", true, ArgumentType.integerOnly());
        super.addArgument("direction", true, ArgumentType.fromEnum(CardinalDirection.class));
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        AreaSizeModificationAction action = super.getArgumentValue(player, "action", ArgumentType.fromEnum(AreaSizeModificationAction.class));
        int amount = super.getArgumentValue(player, "amount", ArgumentType.integerOnly());
        CardinalDirection direction = super.getArgumentValue(player, "direction", ArgumentType.fromEnum(CardinalDirection.class));

        AreaController.modifyArea(LEPlayers.getInstance().findLEPlayer(player), action, amount, direction);

        return true;
    }
}
