package eu.lasersenigma.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.event.*;
import eu.lasersenigma.area.exception.AreaOverlapException;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.area.exception.NoAreaFoundNearbyException;
import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

public final class AreaController {

    // Player's first location map
    private static final HashMap<UUID, Location> PLAYERS_CREATE_AREA_FIRST_LOCATION = new HashMap<>();

    // Player's second location map
    private static final HashMap<UUID, Location> PLAYERS_VICTORY_AREA_FIRST_LOCATION = new HashMap<>();

    private AreaController() {
    }

    public static void createArea(LEPlayer player, Location location) {
        UUID playerUUID = player.getUniqueId();
        Location firstAreaLocation = PLAYERS_CREATE_AREA_FIRST_LOCATION.get(playerUUID);

        if (firstAreaLocation == null) {
            PlayerTryToSelectFirstLocCreateAreaEvent firstLocEvent = new PlayerTryToSelectFirstLocCreateAreaEvent(player.getBukkitPlayer(), location);
            Bukkit.getServer().getPluginManager().callEvent(firstLocEvent);

            if (firstLocEvent.isCancelled()) {
                return;
            }

            if (!firstLocEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
                return;
            }

            PLAYERS_CREATE_AREA_FIRST_LOCATION.put(playerUUID, location);
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.create_area_first_location", TranslationUtils.fromLocation(player.getBukkitPlayer(), location));
        } else {
            PlayerTryToCreateAreaEvent createAreaEvent = new PlayerTryToCreateAreaEvent(player.getBukkitPlayer(), firstAreaLocation, location);
            Bukkit.getServer().getPluginManager().callEvent(createAreaEvent);

            if (createAreaEvent.isCancelled()) {
                return;
            }

            if (!createAreaEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
                return;
            }

            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.create_area_second_location", TranslationUtils.fromLocation(player.getBukkitPlayer(), location));

            try {
                Area createdArea = Areas.getInstance().createArea(firstAreaLocation, location, -1, true, true, true, true, false, false, false, false, false, null, DetectionMode.DETECTION_LASER_RECEIVERS, 1, 10);

                // Show the area to the player
                createdArea.show(player.getBukkitPlayer());

                // Send related message
                TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.create_area_success");

                // Fire area creation event
                AreaCreatedEvent areaCreatedEvent = new AreaCreatedEvent(createdArea, player.getBukkitPlayer());
                Bukkit.getServer().getPluginManager().callEvent(areaCreatedEvent);

            } catch (AbstractLasersException ex) {
                TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), ex);
                if (ex instanceof AreaOverlapException) {
                     ((AreaOverlapException) ex).getOverlappedArea().show(player.getBukkitPlayer());
                }
            }

            PLAYERS_CREATE_AREA_FIRST_LOCATION.remove(playerUUID);
        }
    }

    public static void deleteArea(LEPlayer player, Location location) {
        Area area = Areas.getInstance().getAreaFromLocation(location);

        PlayerTryToDeleteAreaEvent deleteAreaEvent = new PlayerTryToDeleteAreaEvent(player.getBukkitPlayer(), area);
        Bukkit.getServer().getPluginManager().callEvent(deleteAreaEvent);

        if (deleteAreaEvent.isCancelled()) {
            return;
        }

        if (!deleteAreaEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }

        if (area.getStats().isLinked()) {
            statsUnlink(player.getBukkitPlayer(), area);
        }

        Areas.getInstance().deleteArea(area);
        LEPlayers.getInstance().onAreaDeleted(area);

        // Send related message
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.delete_area");

        // Fire area deletion event
        AreaDeletedEvent areaDeletedEvent = new AreaDeletedEvent(area, player.getBukkitPlayer());
        Bukkit.getPluginManager().callEvent(areaDeletedEvent);
    }

    public static void toggleVMirrorsRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVMirrorsRotationAllowed();

        // Fire related event
        PlayerTryToToogleVMirrorsRotationAreaEvent mirrorVrotationEvent = new PlayerTryToToogleVMirrorsRotationAreaEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(mirrorVrotationEvent);

        if (mirrorVrotationEvent.isCancelled()) {
            return;
        }

        if (!mirrorVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }

        area.setVMirrorsRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();

        // Send dedicated message
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_area_vrotation", TranslationUtils.fromBoolean(player.getBukkitPlayer(), newVRotationAllowed));
    }

    public static void toggleHMirrorsRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHMirrorsRotationAllowed();
        PlayerTryToToogleHMirrorsRotationAreaEvent mirrorHrotationEvent = new PlayerTryToToogleHMirrorsRotationAreaEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(mirrorHrotationEvent);
        if (mirrorHrotationEvent.isCancelled()) {
            return;
        }
        if (!mirrorHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHMirrorsRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_area_hrotation", TranslationUtils.fromBoolean(player.getBukkitPlayer(), newHRotationAllowed));
    }

    public static void toggleVConcentratorsRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVConcentratorsRotationAllowed();
        PlayerTryToToogleVConcentratorsRotationAreaEvent concentratorsVrotationEvent = new PlayerTryToToogleVConcentratorsRotationAreaEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(concentratorsVrotationEvent);
        if (concentratorsVrotationEvent.isCancelled()) {
            return;
        }
        if (!concentratorsVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setVConcentratorsRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_concentrators_vrotation",
                TranslationUtils.fromBoolean(player.getBukkitPlayer(), newVRotationAllowed));
        area.components.stream().filter(component -> component.getComponentType() == ComponentType.CONCENTRATOR).forEach(IComponent::showOrUpdateComponent);
    }

    public static void toggleHConcentratorsRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHConcentratorsRotationAllowed();
        PlayerTryToToogleHConcentratorsRotationAreaEvent concentratorsHrotationEvent = new PlayerTryToToogleHConcentratorsRotationAreaEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(concentratorsHrotationEvent);
        if (concentratorsHrotationEvent.isCancelled()) {
            return;
        }
        if (!concentratorsHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHConcentratorsRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_concentrators_hrotation",
                TranslationUtils.fromBoolean(player.getBukkitPlayer(), newHRotationAllowed));
        area.components.stream().filter(component -> component.getComponentType() == ComponentType.CONCENTRATOR).forEach(IComponent::showOrUpdateComponent);
    }

    public static void toggleVLaserSendersRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVLaserSendersRotationAllowed();
        PlayerTryToToogleVLaserSendersRotationAreaEvent lsVrotationEvent = new PlayerTryToToogleVLaserSendersRotationAreaEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lsVrotationEvent);
        if (lsVrotationEvent.isCancelled()) {
            return;
        }
        if (!lsVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setVLaserSendersRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_laser_senders_vrotation",
                TranslationUtils.fromBoolean(player.getBukkitPlayer(), newVRotationAllowed));
        area.components.stream().filter(component -> component.getComponentType() == ComponentType.LASER_SENDER).forEach(IComponent::showOrUpdateComponent);
    }

    public static void toggleHLaserSendersRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHLaserSendersRotationAllowed();
        PlayerTryToToogleHLaserSendersRotationAreaEvent lsHrotationEvent = new PlayerTryToToogleHLaserSendersRotationAreaEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lsHrotationEvent);
        if (lsHrotationEvent.isCancelled()) {
            return;
        }
        if (!lsHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHLaserSendersRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_laser_senders_hrotation",
                TranslationUtils.fromBoolean(player.getBukkitPlayer(), newHRotationAllowed));
        area.components.stream().filter(component -> component.getComponentType() == ComponentType.LASER_SENDER).forEach(IComponent::showOrUpdateComponent);
    }

    public static void toggleVLaserReceiversRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVLaserReceiversRotationAllowed();
        PlayerTryToToogleVLaserReceiversRotationAreaEvent lrVrotationEvent = new PlayerTryToToogleVLaserReceiversRotationAreaEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lrVrotationEvent);
        if (lrVrotationEvent.isCancelled()) {
            return;
        }
        if (!lrVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setVLaserReceiversRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_laser_receivers_vrotation",
                TranslationUtils.fromBoolean(player.getBukkitPlayer(), newVRotationAllowed));
        area.components.stream().filter(component -> component.getComponentType() == ComponentType.LASER_RECEIVER).forEach(IComponent::showOrUpdateComponent);
    }

    public static void toggleHLaserReceiversRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHLaserReceiversRotationAllowed();
        PlayerTryToToogleHLaserReceiversRotationAreaEvent lrHrotationEvent = new PlayerTryToToogleHLaserReceiversRotationAreaEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lrHrotationEvent);
        if (lrHrotationEvent.isCancelled()) {
            return;
        }
        if (!lrHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHLaserReceiversRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_laser_receivers_hrotation",
                TranslationUtils.fromBoolean(player.getBukkitPlayer(), newHRotationAllowed));
        area.components.stream().filter(component -> component.getComponentType() == ComponentType.LASER_RECEIVER).forEach(IComponent::showOrUpdateComponent);
    }

    public static void toggleARSpheresResizing(LEPlayer player, Area area) {
        boolean newARSpheresResizingAllowed = !area.isAttractionRepulsionSpheresResizingAllowed();
        PlayerTryToToogleARSpheresResizingAreaEvent arspheresResizingEvent = new PlayerTryToToogleARSpheresResizingAreaEvent(player.getBukkitPlayer(), area, newARSpheresResizingAllowed);
        Bukkit.getServer().getPluginManager().callEvent(arspheresResizingEvent);
        if (arspheresResizingEvent.isCancelled()) {
            return;
        }
        if (!arspheresResizingEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setAttractionRepulsionSpheresResizingAllowed(newARSpheresResizingAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.toggle_spheres_sizing",
                TranslationUtils.fromBoolean(player.getBukkitPlayer(), newARSpheresResizingAllowed));
    }

    public static void statsLink(LEPlayer player, Area area, Area area2) {
        if (!Permission.ADMIN.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        try {
            area.getStats().linkStats(area2);
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.stats_merged_and_linked");
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), ex);
        }

    }

    public static void statsUnlink(Player player, Area area) {
        if (!Permission.ADMIN.checkPermissionAndSendMsg(player)) {
            return;
        }
        try {
            area.getStats().unlinkStats();
            TranslationUtils.sendTranslatedMessage(player, "messages.stats_unlinked");
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player, ex);
        }
    }

    public static void statsClear(LEPlayer player, Area area) {
        if (!Permission.ADMIN.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.getStats().clear();
    }

    public static void defineCheckpoint(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        try {
            area.setAreaCheckpointLocation(player.getBukkitPlayer().getLocation().clone());
            area.dbUpdate();
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.checkpoint_selected",
                    TranslationUtils.fromLocation(player.getBukkitPlayer(), player.getBukkitPlayer().getLocation()));
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (NoAreaFoundException ex) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), ex);
        }
    }

    public static void deleteCheckpoint(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        try {
            area.setAreaCheckpointLocation(null);
            area.dbUpdate();
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.checkpoint_deleted");
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (NoAreaFoundException ex) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), ex);
        }
    }

    public static void deleteAllVictoryAreas(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        area.deleteVictoryAreas();
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.victory_areas_deleted");
        LEPlayers.getInstance().onAreaUpdated(area);
    }

    public static void createVictoryArea(LEPlayer player, Area area, Location location) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !player.getInventoryManager().isInEditionMode() || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        UUID playerUUID = player.getUniqueId();
        Location firstVAreaLocation = PLAYERS_VICTORY_AREA_FIRST_LOCATION.get(playerUUID);
        if (firstVAreaLocation == null) {
            PLAYERS_VICTORY_AREA_FIRST_LOCATION.put(playerUUID, location);
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.victory_area_first_location",
                    TranslationUtils.fromLocation(player.getBukkitPlayer(), location));
        } else {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.victory_area_second_location",
                    TranslationUtils.fromLocation(player.getBukkitPlayer(), location));
            PLAYERS_VICTORY_AREA_FIRST_LOCATION.remove(playerUUID);
            try {
                area.addVictoryArea(new VictoryArea(area, firstVAreaLocation, location));
                area.dbUpdate();
                TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.victory_area_created");
            } catch (AbstractLasersException ex) {
                TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), ex);
            }
        }
    }

    public static void setVictoryCriteria(LEPlayer player, Area area, DetectionMode victoryCriteria) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (victoryCriteria.isSpecificToLaserSender()) {
            throw new IllegalStateException();
        }
        area.setVictoryDetectionMode(victoryCriteria);
        area.dbUpdate();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), victoryCriteria.getMessageCode());
        LEPlayers.getInstance().onAreaUpdated(area);
    }

    public static void decreaseMin(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMinimumRange() < 1) {
            return;
        }
        if (!area.getVictoryDetectionMode().isRangeModificationCompatible()) {
            return;
        }
        area.setMinimumRange(area.getMinimumRange() - 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    public static void decreaseMax(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMaximumRange() < 1) {
            return;
        }
        if (!area.getVictoryDetectionMode().isRangeModificationCompatible()) {
            return;
        }
        if (area.getMaximumRange() == area.getMinimumRange()) {
            area.setMinimumRange(area.getMinimumRange() - 1);
        }
        area.setMaximumRange(area.getMaximumRange() - 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    public static void increaseMin(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMinimumRange() > 98) {
            return;
        }
        if (!area.getVictoryDetectionMode().isRangeModificationCompatible()) {
            return;
        }
        if (area.getMinimumRange() == area.getMaximumRange()) {
            area.setMaximumRange(area.getMaximumRange() + 1);
        }
        area.setMinimumRange(area.getMinimumRange() + 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    public static void increaseMax(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMaximumRange() > 98) {
            return;
        }
        if (!area.getVictoryDetectionMode().isRangeModificationCompatible()) {
            return;
        }
        area.setMaximumRange(area.getMaximumRange() + 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    private static void rangeChanged(LEPlayer player, int min, int max) {
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.range_changed", Integer.toString(min), Integer.toString(max));
    }

    public static void clearTemporaryLocations(LEPlayer player) {
        PLAYERS_CREATE_AREA_FIRST_LOCATION.remove(player.getBukkitPlayer().getUniqueId());
        PLAYERS_VICTORY_AREA_FIRST_LOCATION.remove(player.getBukkitPlayer().getUniqueId());
    }

    public static void modifyArea(LEPlayer player, AreaSizeModificationAction action, Integer amount, CardinalDirection direction) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        Area area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
        if (area == null) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            return;
        }
        int diff = 0;
        switch (action) {
            case CONTRACT:
                diff -= amount;
                break;
            case EXPAND:
                diff += amount;
                break;
        }

        double minX = area.getAreaMinLocation().getBlockX();
        double minY = area.getAreaMinLocation().getBlockY();
        double minZ = area.getAreaMinLocation().getBlockZ();
        double maxX = area.getAreaMaxLocation().getBlockX();
        double maxY = area.getAreaMaxLocation().getBlockY();
        double maxZ = area.getAreaMaxLocation().getBlockZ();

        if (CardinalDirection.ALL == direction) {
            minY -= diff;
            maxY += diff;
            minX -= diff;
            maxX += diff;
            minZ -= diff;
            maxZ += diff;
        }

        if (CardinalDirection.DOWN == direction) {
            minY -= diff;
        } else if (CardinalDirection.UP == direction) {
            maxY += diff;
        } else if (CardinalDirection.WEST == direction) {
            minX -= diff;
        } else if (CardinalDirection.EAST == direction) {
            maxX += diff;
        } else if (CardinalDirection.NORTH == direction) {
            minZ -= diff;
        } else if (CardinalDirection.SOUTH == direction) {
            maxZ += diff;
        }

        try {
            area.modify(minX, minY, minZ, maxX, maxY, maxZ);
            area.show(player.getBukkitPlayer());
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.area_size_updated");
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), ex);
            if (ex instanceof AreaOverlapException) {
                 ((AreaOverlapException) ex).getOverlappedArea().show(player.getBukkitPlayer());
            }
        }
    }

    public static Optional<Area> findNearestArea(LEPlayer player) {
        return Areas.getInstance().getAreas().stream()
                // Keep only areas in the same world as the player
                .filter(area -> area.getAreaWorld().getUID().equals(player.getBukkitPlayer().getWorld().getUID()))
                // Compute the distance between the player and the area
                .map(area -> Map.entry(area, area.getAreaCenterLocation().distance(player.getBukkitPlayer().getLocation())))
                // Keep only closest area
                .min(Comparator.comparingDouble(Map.Entry::getValue))
                .map(Map.Entry::getKey);
    }

    public static void showNearestArea(LEPlayer player) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }

        Optional<Area> foundArea = findNearestArea(player);

        if (foundArea.isEmpty()) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundNearbyException());
            return;
        }

        foundArea.get().show(player.getBukkitPlayer());

        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "area.commands.show.show_nearest_area",
                String.valueOf(foundArea.get().areaMinLocation.getBlockX()),
                String.valueOf(foundArea.get().areaMinLocation.getBlockY()),
                String.valueOf(foundArea.get().areaMinLocation.getBlockZ()),
                String.valueOf(foundArea.get().areaMaxLocation.getBlockX()),
                String.valueOf(foundArea.get().areaMaxLocation.getBlockY()),
                String.valueOf(foundArea.get().areaMaxLocation.getBlockZ())
        );
    }
}
