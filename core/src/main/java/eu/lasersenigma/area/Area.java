package eu.lasersenigma.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.event.*;
import eu.lasersenigma.area.exception.AreaCrossWorldsException;
import eu.lasersenigma.area.exception.AreaNoDepthException;
import eu.lasersenigma.area.exception.AreaOverlapException;
import eu.lasersenigma.area.exception.ComponentOutsideAreaException;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.task.ITaskComponent;
import eu.lasersenigma.common.util.BlockUtils;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IDetectionComponent;
import eu.lasersenigma.component.concentrator.Concentrator;
import eu.lasersenigma.component.conditionnalwinnerblock.event.ConditionalComponentActivationEvent;
import eu.lasersenigma.component.conditionnalwinnerblock.event.ConditionalComponentDeactivationEvent;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.laserreceiver.event.LaserReceiversActivatedChangeEvent;
import eu.lasersenigma.component.lasersender.LaserSender;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.event.LocksActivatedChangeEvent;
import eu.lasersenigma.component.musicblock.MusicBlock;
import eu.lasersenigma.component.redstonesensor.RedstoneSensor;
import eu.lasersenigma.component.redstonesensor.event.RedstoneSensorActivatedChangeEvent;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.particles.ReflectionData;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.songs.PlayersListSongManager;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import eu.lasersenigma.stats.AreaStats;
import eu.lasersenigma.stats.listener.PlayerLeftAreaStatsAndVictoryDetectionListener;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Light;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

public final class Area extends AArea {

    public static final boolean IS_LASER_EMITS_LIGHT = LasersEnigmaPlugin.getInstance().isLaserEmitsLight();

    // Number of players inside the area
    private int playersInsideAreaCount = 0;

    // Last number of players inside the area
    private int lastPlayersInsideAreaCount = 0;

    // Players currently inside the area
    private HashSet<Player> playersInsideArea = new HashSet<>();

    // Players currently inside the area (except players in spectator mode)
    private HashSet<Player> playersExceptSpectatorsInsideArea = new HashSet<>();

    // Last player known location
    private HashMap<UUID, Location> lastPlayersKnownLocation = new HashMap<>();


    // AttractionRepulsionSphere Cache
    private final HashMap<Location, ArrayList<GravitationalSphere>> gravitationalSphereCache = new HashMap<>();

    // Block reflection Cache
    private final HashMap<ReflectionData, ReflectionData> blocksReflectionCache = new HashMap<>();

    // Number of activated locks inside the area
    private int activatedLocksCount = 0;

    // Number of activated locks inside the area during the last check
    private int lastActivatedLocksCount = 0;

    // Used to know how often should we check entity impact within particles update process
    private int laserDamageLooper = 1;

    // Used to know how often show particles within particles update process
    private int laserShowLooper = 1;

    // Used to know how often update light within particles update process
    private int laserLightUpdateLooper = 1;

    // Was the light updated during the last execution of the particle update process
    private final boolean wasLightUpdatedLastRun = false;

    // Number of activated laser receivers
    private int activatedLaserReceiversCount = 0;

    // Number of laser receivers activated during the last check
    private int lastActivatedLaserReceiversCount = 0;

    // Number of activated redstone sensors
    private int activatedRedstoneSensorsCount = 0;

    // Number of activated redstone sensors during the last check
    private int lastActivatedRedstoneSensorCount = 0;

    // The number of time updateArea was called since the area was activated
    private int updateLasersCount = 0;

    // If the AreaWinConditionsReachedLEEvent has already been sent since the area activation
    private boolean areaWinConditionsReachedEventAlreadyCalled = false;

    // Area's statistics
    private final AreaStats stats = new AreaStats(this);

    /**
     * Constructor.
     *
     * @param a first location (not minimal location yet, simply a corner location)
     * @param b second location (not maximal location yet, simply the opposite corner location)
     * @throws AreaCrossWorldsException if the 2 locations are in different worlds
     * @throws AreaNoDepthException     if the area has no depth/width/height
     */
    public Area(Location a, Location b) throws AreaCrossWorldsException, AreaNoDepthException {
        super(a, b);
    }

    /**
     * Verifies if current area have any players inside it
     *
     * @return True if player's inside area count is greater than 0, false otherwise
     */
    public boolean hasPlayersInsideArea() {
        return playersInsideAreaCount > 0;
    }

    /**
     * Retrieves the number of players (except spectators) within the area
     *
     * @return the number of players (except spectators) within the area
     */
    public int getPlayersInsideAreaCount() {
        return playersInsideAreaCount;
    }

    /**
     * gets the players inside the area
     *
     * @return the players inside the area
     */
    public HashSet<Player> getPlayersInsideArea() {
        return playersInsideArea;
    }

    /**
     * gets the players inside the area except Spectators
     *
     * @return the players inside the area
     */
    public HashSet<Player> getPlayersExceptSpectatorsInsideArea() {
        return playersExceptSpectatorsInsideArea;
    }

    /**
     * Clears the cache for AttractionRepulsionSpheres.
     */
    public void clearAttractionRepulsionCache() {
        gravitationalSphereCache.clear();
    }

    /**
     * Retrieves the reflection cache for blocks within this area
     *
     * @return the block's reflection cache
     */
    public HashMap<ReflectionData, ReflectionData> getBlocksReflectionCache() {
        return blocksReflectionCache;
    }

    /**
     * Clears the cache for block reflections.
     */
    public void clearBlockReflectionCache() {
        blocksReflectionCache.clear();
    }

    /**
     * gets the number of activated laser receivers during the last check
     *
     * @return the number of activated laser receivers during the last check
     */
    public int getActivatedLaserReceiversCount() {
        return this.activatedLaserReceiversCount;
    }

    /**
     * gets the number of activated laser sensor activated
     *
     * @return the number of activated laser sensors
     */
    public int getActivatedRedstoneSensorsCount() {
        return this.activatedRedstoneSensorsCount;
    }

    /**
     * Retrieves the number of activated locks within the area
     *
     * @return the number of activated locks within the area
     */
    public int getActivatedLocksCount() {
        return activatedLocksCount;
    }

    /**
     * Gets the count of updates for lasers.
     *
     * @return The count of updates for lasers.
     */
    public int getUpdateLasersCount() {
        return updateLasersCount;
    }

    /**
     * Creates a database record for the area.
     */
    public void dbCreate() {
        // Create a database record for the area
        this.areaId = LasersEnigmaPlugin.getInstance().getPluginDatabase().createArea(this);
    }

    /**
     * Removes the area's database record.
     */
    public void dbRemove() {
        // Remove the area's database record
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removeArea(this);
    }

    /**
     * Updates the area's database record.
     */
    public void dbUpdate() {
        // Update the area's database record
        LasersEnigmaPlugin.getInstance().getPluginDatabase().updateArea(this);
    }

    /**
     * Hides all components associated with the area.
     */
    /*public void hideComponents() {
        // Hide each component in the area
        components.forEach(IComponent::hideComponent);
    }*/

    /**
     * Gets the list of AttractionRepulsionSpheres affecting the specified location.
     *
     * @param location The location for which to retrieve affecting AttractionRepulsionSpheres.
     * @return The list of affecting AttractionRepulsionSpheres.
     */
    public List<GravitationalSphere> getAffectingAttractionRepulsionSphere(Location location) {
        // Retrieve cached list of affecting AttractionRepulsionSpheres if available
        ArrayList<GravitationalSphere> affectingGravitationalSpheres = gravitationalSphereCache.get(location);

        // If not cached, compute and cache the list based on components
        if (affectingGravitationalSpheres == null) {
            affectingGravitationalSpheres = components.stream().filter(component -> component instanceof GravitationalSphere).map(component -> (GravitationalSphere) component).filter(sphere -> sphere.getASHeadCenterLocation().distance(location) <= sphere.getCurrentGravityStrength()).collect(Collectors.toCollection(ArrayList::new));

            // Cache the computed list for the specified location
            gravitationalSphereCache.put(location.clone(), affectingGravitationalSpheres);
        }

        return affectingGravitationalSpheres;
    }

    /**
     * Updates the area by performing various computations and calling associated events.
     */
    public void updateArea() {
        // Compute players in the area and call relevant events
        computeAreaPlayersCountAndCallEvents();

        // Activate or deactivate the area based on conditions and call associated events
        computeAreaActivationOrDeactivationAndCallEvents();

        // Compute and call the event when area win conditions are reached
        computeAreaWinConditionsAndCallEvent();
    }

    private void computeAreaPlayersCountAndCallEvents() {
        // Update the set of current players inside the area
        HashSet<Player> currentPlayersInsideArea = getCurrentPlayersInsideArea();

        // Handle players who left the area
        playersInsideArea.stream()
                .filter(p -> !currentPlayersInsideArea.contains(p) && !p.getGameMode().equals(GameMode.SPECTATOR))
                .forEach(this::computePlayerLeftArea);

        // Save last known player's location
        currentPlayersInsideArea
                .forEach(enteredPlayer -> lastPlayersKnownLocation.put(enteredPlayer.getUniqueId(), enteredPlayer.getLocation()));

        // Handle players who entered the area
        currentPlayersInsideArea.stream()
                .filter(p -> !playersInsideArea.contains(p))
                .forEachOrdered(enteredPlayer -> {
                    // Handle MusicBlocks for players entering the area
                    components.stream().filter(MusicBlock.class::isInstance).forEach(mb -> ((MusicBlock) mb).onPlayerEnteredArea(enteredPlayer));

                    // Notify the LEPlayer about entering the area
                    if (enteredPlayer.getGameMode().equals(GameMode.SPECTATOR)) return;

                    // Trigger PlayerEnteredAreaLEEvent
                    computePlayerEnteredArea(enteredPlayer);
                });

        // Update the set of players inside the area
        playersInsideArea = currentPlayersInsideArea;
        playersExceptSpectatorsInsideArea = filterSpectators(playersInsideArea);
    }

    private void computePlayerLeftArea(Player player) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerLeftAreaEvent");

        // Notify the LEPlayer about leaving the area
        PlayersListSongManager.getInstance().onPlayerLeftArea(player);
        LEPlayers.getInstance().findLEPlayer(player.getUniqueId()).onLeaveArea();

        // TODO: Send a message to the player about leaving the area
        // leftPlayer.sendMessage("You're leaving an area!");

        // Trigger PlayerLeftAreaEvent
        Location lastKnownLocation = lastPlayersKnownLocation.get(player.getUniqueId());
        Bukkit.getServer().getPluginManager().callEvent(new PlayerLeftAreaEvent(player, this, lastKnownLocation));
    }

    private void computePlayerEnteredArea(Player player) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerEnteredArea");

        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(player);
        LEPlayers.getInstance().findLEPlayer(player.getUniqueId()).onEnterArea();

        // TODO: Send a message to the player about entering the area
        // enteredPlayer.sendMessage("You entered into an area!");

        switch (player.getGameMode()) {
            case SPECTATOR:
            case CREATIVE:
                return;
            default:
                break;
        }
        if (lePlayer.getInventoryManager().isInEditionMode()) {
            return;
        }
        // Checking if the players enters by the area's end (one of the victory areas)
        VictoryArea vArea = null;
        for (VictoryArea tmpVArea : this.getVictoryAreas()) {
            if (tmpVArea.containsLocation(player.getLocation())) {
                vArea = tmpVArea;
                break;
            }
        }
        if (vArea != null) {
            lePlayer.setCurrentVictoryCheckpoint(player.getLocation().clone());
            return; // if a player enters an area by its end (one of the victory areas), we will not start statistics recording.
        }
        lePlayer.setCurrentCheckpoint(this.getAreaCheckpointLocation());
        this.getStats().addListener(player.getUniqueId(), new PlayerLeftAreaStatsAndVictoryDetectionListener(this, player));

        Bukkit.getServer().getPluginManager().callEvent(new PlayerEnteredAreaEvent(player, this));
    }

    /**
     * Activate or deactivate area and their components according to players entrance/exit
     */
    private void computeAreaActivationOrDeactivationAndCallEvents() {
        int playersCount = playersExceptSpectatorsInsideArea.size();

        if (isActivated() && playersCount == 0) {
            deactivateArea();
        } else if (!isActivated() && playersCount > 0) {
            activateArea();
        }
    }

    public void activateArea() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().finest("Area activation");

        // Activate the area
        this.setActivated(true);

        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), bukkitTask -> {

            // Start tasks and scheduled actions
            this.getComponents().forEach(component -> {
                if (component instanceof ITaskComponent taskComponent) taskComponent.startTask();
                component.startScheduledActions();
            });

            // Delete potential phantom entities
            this.removeAllAmorStands();

            // Activate components
            this.getComponents().stream().filter(component -> component.getComponentLocation().getChunk().isLoaded()).forEach(component -> {
                component.activateComponent();
                component.showOrUpdateComponent();
            });

            // Update light
            updateLight();

        }, 1L);

        // Fire area activated event
        AreaActivatedEvent areaActivatedEvent = new AreaActivatedEvent(this);
        Bukkit.getPluginManager().callEvent(areaActivatedEvent);
    }

    public void deactivateArea() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().finest("Area deactivation");

        // Deactivate the area
        this.setActivated(false);

        // Stop tasks and scheduled actions
        this.getComponents().forEach(component -> {
            if (component instanceof ITaskComponent taskComponent) (taskComponent).cancelTask();
            component.stopScheduledActions();
        });

        // Deactivate components
        this.getComponents().stream().filter(component -> component.getComponentLocation().getChunk().isLoaded()).forEach(component -> {
            component.deactivateComponent();
            component.showOrUpdateComponent();
        });

        this.clearLaserParticles();
        clearLight();

        // Fire area deactivation event
        AreaDeactivatedEvent areaDeactivatedEvent = new AreaDeactivatedEvent(this);
        Bukkit.getPluginManager().callEvent(areaDeactivatedEvent);
    }

    /**
     * Computes and calls the event when area win conditions are reached.
     * Calls PlayerReachedAreaWinConditionsLEEvent for numeric win conditions only.
     * The call of PlayerReachedAreaWinConditionsLEEvent for VictoryArea win condition is done
     * in PlayerLeftAreaStatsAndVictoryDetectionListener class.
     */
    private void computeAreaWinConditionsAndCallEvent() {
        // Check if victory conditions are reached and the event hasn't been called already
        if (checkVictoryConditionsReached() && !areaWinConditionsReachedEventAlreadyCalled) {
            // Iterate through players inside the area (excluding spectators) and call the event
            getPlayersExceptSpectatorsInsideArea().forEach(player -> {

                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerReachedAreaWinConditions");

                // When lasers senders are activated except if it's during area activation.
                SoundLauncher.playSound(player.getLocation(), PlaySoundCause.REACHED_AREA_WIN_CONDITIONS);

                Bukkit.getPluginManager().callEvent(new AreaWinConditionsReachedEvent(player, this));
            });

            areaWinConditionsReachedEventAlreadyCalled = true;
        }
    }

    /**
     * Retrieves the area statistics
     *
     * @return Area's statistics
     */
    public AreaStats getStats() {
        return this.stats;
    }

    /**
     * Resets the area, clearing various state and caches.
     */
    /*public void reset() {
        resetComponents();

        clearLaserParticles();
        if (LIGHT_API_AVAILABLE) clearLight();
        clearCaches();

        resetCounts();
        resetAreaWinConditionsReachedEvent();

        // Kill previous entities (hide components)
    }*/

    /*private void resetComponents() {
        components.forEach(IComponent::resetComponent);
    }*/

    // private void clearLaserParticles() {
    //     laserParticles.clear();
    //     laserParticlesToAdd.clear();
    // }
    //
    // private void resetCounts() {
    //     updateLasersCount = 0;
    //     activatedLaserReceiversCount = 0;
    //     lastActivatedLaserReceiversCount = 0;
    //     activatedRedstoneSensorsCount = 0;
    //     lastActivatedRedstoneSensorCount = 0;
    // }
    //
    // private void clearCaches() {
    //     componentByLocationCache.clear();
    //     locationsWithNoComponent.clear();
    //     clearAttractionRepulsionCache();
    //     clearBlockReflectionCache();
    // }
    //
    // private void resetAreaWinConditionsReachedEvent() {
    //     areaWinConditionsReachedEventAlreadyCalled = false;
    // }

    /**
     * Updates the laser particles and related components.
     */
    @SuppressWarnings({"ResultOfObjectAllocationIgnored"})
    public void updateLasers() {
        // Update the number of players
        updatePlayersInsideAreaCount();

        // Update the number of laser receivers
        updateLaserReceiversCount();

        // Update the number of Redstone sensors
        updateRedstoneSensorsCount();

        // Update the number of locks
        updateLocksCount();

        // Updating IDetection components
        updateDetectionComponents();

        // Update existing laser particles
        updateParticles();

        // Update Redstone Receivers (RedstoneSensor & Laser Sender)
        updateRedstoneReceivers();

        // When a change on a component takes place, this variable allows knowing if this change
        // comes from the activation of the area or if it comes from a real user action.
        updateLasersCount++;
    }

    /**
     * Updates the count of players inside the area and triggers an event if there is a change.
     */
    private void updatePlayersInsideAreaCount() {
        // Get the count of players inside the area, excluding spectators
        playersInsideAreaCount = getPlayersExceptSpectatorsInsideArea().size();

        // Check if there is a change in the players inside the area count
        if (playersInsideAreaCount != lastPlayersInsideAreaCount) {
            // Trigger an event with the old and new players inside the area count
            Bukkit.getPluginManager().callEvent(new PlayersCountChangeEvent(this, lastPlayersInsideAreaCount, playersInsideAreaCount));
        }

        // Update the last players inside the area count for future comparisons
        lastPlayersInsideAreaCount = playersInsideAreaCount;
    }

    /**
     * Updates the count of activated laser receivers and triggers an event if there is a change.
     */
    private void updateLaserReceiversCount() {
        // Count the number of activated laser receivers
        activatedLaserReceiversCount = (int) components.stream().filter(c -> (c instanceof LaserReceiver && ((LaserReceiver) c).isActivated())).count();

        // Check if there is a change in the activated laser receivers count
        if (activatedLaserReceiversCount != lastActivatedLaserReceiversCount) {
            // Trigger an event with the old and new activated laser receivers count
            Bukkit.getPluginManager().callEvent(new LaserReceiversActivatedChangeEvent(this, lastActivatedLaserReceiversCount, activatedLaserReceiversCount));
        }

        // Update the last activated laser receivers count for future comparisons
        lastActivatedLaserReceiversCount = activatedLaserReceiversCount;
    }

    /**
     * Updates the count of activated Redstone sensors and triggers an event if the count has changed.
     */
    private void updateRedstoneSensorsCount() {
        // Count the number of activated Redstone sensors
        activatedRedstoneSensorsCount = (int) components.stream().filter(component -> component instanceof RedstoneSensor && ((RedstoneSensor) component).isPowered()).count();

        // Check if the count has changed since the last update
        if (activatedRedstoneSensorsCount != lastActivatedRedstoneSensorCount) {
            // Trigger an event for Redstone sensor change
            Bukkit.getPluginManager().callEvent(new RedstoneSensorActivatedChangeEvent(this, lastActivatedRedstoneSensorCount, activatedRedstoneSensorsCount));
        }

        // Update the last activated count for the next comparison
        lastActivatedRedstoneSensorCount = activatedRedstoneSensorsCount;
    }

    /**
     * Updates the count of activated locks and triggers an event if the count has changed.
     */
    private void updateLocksCount() {
        // Count the number of activated locks
        activatedLocksCount = (int) components.stream().filter(component -> component instanceof Lock && ((Lock) component).isOpenedCurrent()).count();

        // Check if the count has changed since the last update
        if (activatedLocksCount != lastActivatedLocksCount) {
            // Trigger an event for activated locks change
            Bukkit.getPluginManager().callEvent(new LocksActivatedChangeEvent(this, lastActivatedLocksCount, activatedLocksCount));
        }

        // Update the last activated count for the next comparison
        lastActivatedLocksCount = activatedLocksCount;
    }

    /**
     * Updates the state of detection components based on various modes and triggers events if their activation state
     * changes.
     */
    private void updateDetectionComponents() {
        components.stream().filter(IDetectionComponent.class::isInstance).forEach(component -> {
            IDetectionComponent detectionComponent = ((IDetectionComponent) component);

            // Store the previous activation state
            boolean previousState = detectionComponent.isActivated();

            // Update the number of activated based on the detection component's mode
            switch (detectionComponent.getDetectionMode()) {
                case DETECTION_PLAYERS:
                    detectionComponent.setNbActivated(playersInsideAreaCount);
                    break;
                case DETECTION_REDSTONE_SENSORS:
                    detectionComponent.setNbActivated(activatedRedstoneSensorsCount);
                    break;
                case DETECTION_LASER_RECEIVERS:
                    detectionComponent.setNbActivated(activatedLaserReceiversCount);
                    break;
                case DETECTION_LOCKS:
                    detectionComponent.setNbActivated(activatedLocksCount);
                    break;
                default:
                    break;
            }

            // Get the new activation state after the update
            boolean newState = detectionComponent.isActivated();

            // Check if the activation state has changed
            if (previousState != newState) {
                // Trigger events based on the new activation state
                if (newState) {
                    Bukkit.getPluginManager().callEvent(new ConditionalComponentActivationEvent(detectionComponent));
                } else {
                    Bukkit.getPluginManager().callEvent(new ConditionalComponentDeactivationEvent(detectionComponent));
                }
            }
        });
    }

    /**
     * Updates the state of laser particles, including movement, display, and interaction.
     * Removes particles that return false during the update.
     */
    private void updateParticles() {
        // Increment loopers for different particle update frequencies
        laserDamageLooper = (laserDamageLooper + 1) % LASER_DAMAGE_FREQUENCY;
        laserShowLooper = (laserShowLooper + 1) % LASERS_SHOW_FREQUENCY;
        laserLightUpdateLooper = (laserLightUpdateLooper + 1) % LASERS_LIGHT_UPDATE_FREQUENCY;

        // Update each particle (move, show, interact) and remove particles that return false
        laserParticles.removeIf(particle -> !particle.update(laserDamageLooper == 0, laserShowLooper == 0));

        // Update light
        updateLight();

        // Add just created laser particles (via pass_through)
        laserParticles.addAll(laserParticlesToAdd);
        laserParticlesToAdd.clear();

        // Create laser particles from laser senders
        createLaserSendersParticles();

        // Create laser particles from concentrators
        createConcentratorsParticles();

        // Show LaserReceivers corners colors
        showLaserReceiversCornersParticles();
    }

    /**
     * Creates laser particles from activated laser senders.
     */
    private void createLaserSendersParticles() {
        // Filter and map laser senders, then add particles for activated ones
        components.stream().filter(component -> component.getComponentType().equals(ComponentType.LASER_SENDER)).map(LaserSender.class::cast).filter(LaserSender::isActivated).forEach(laserSender -> this.addLaserParticle(new LaserParticle(laserSender, this)));
    }

    /**
     * Creates laser particles from concentrators with resulting colors.
     */
    private void createConcentratorsParticles() {
        // Filter concentrators and add particles for those with resulting colors
        components.stream().filter(component -> component.getComponentType().equals(ComponentType.CONCENTRATOR)).forEach(component -> {
            Concentrator concentrator = ((Concentrator) component);
            LasersColor color = concentrator.getResultingColor();

            // Add a particle only if the concentrator has a resulting color
            if (color != null) {
                this.addLaserParticle(new LaserParticle(concentrator, color, this));
            }
        });
    }

    /**
     * Shows particles at the corners of non-activated laser receivers.
     */
    private void showLaserReceiversCornersParticles() {
        // Filter laser receivers, handle light level, and show particles at front corners
        components.stream().filter(component -> component.getComponentType().equals(ComponentType.LASER_RECEIVER)).map(component -> ((LaserReceiver) component)).filter(laserReceiver -> !laserReceiver.isActivated()).forEach(laserReceiver -> {
            // Handle light level if available
            // if (IS_LASER_EMITS_LIGHT && laserReceiver.getLightLevel() != 0) {
            //     lightLevelPerLocations.put(laserReceiver.getComponentLocation(), laserReceiver.getLightLevel());
            // }

            // Show particles at the front corners using the Bukkit color
            Color color = laserReceiver.getCornersColor().getBukkitColor();
            laserReceiver.getParticleDisplayLocations().forEach(location -> {
                LaserParticle.playEffect(getPlayersInsideArea(), location, Particle.REDSTONE, color);
            });
        });
    }

    /**
     * Updates the light levels based on the locations of particles.
     * Light updates occur if the light API is available and the update frequency allows.
     */
    public void updateLight() {

        // Only update light when the feature is activated
        if (!IS_LASER_EMITS_LIGHT) return;

        // Compute only when necessary
        if (laserLightUpdateLooper != 0) return;

        // Retrieve current light locations and previous light locations
        Set<Location> lightLocations = lightLevelPerLocations.keySet();
        Set<Location> previousLightLocations = previousLightLevelPerLocations.keySet();

        // Determine locations where light was created and removed
        Set<Location> createdLightLocations = new HashSet<>(lightLocations);
        createdLightLocations.removeAll(previousLightLocations);

        Set<Location> removedLightLocations = new HashSet<>(previousLightLocations);
        removedLightLocations.removeAll(lightLocations);

        // Check if there are changes in particle locations that should impact lighting
        boolean lightShouldChange = (!createdLightLocations.isEmpty() || !removedLightLocations.isEmpty());

        if (lightShouldChange) {
            // Delete light where there are no particles anymore
            removedLightLocations.forEach(this::removeLight);

            // Create light where new particles appeared
            createdLightLocations.forEach(location -> addLight(location, lightLevelPerLocations.get(location)));
        }

        // Prepare for the next execution
        previousLightLevelPerLocations.clear();
        previousLightLevelPerLocations.putAll(lightLevelPerLocations);
        lightLevelPerLocations.clear();
    }

    /**
     * Sets the light level at a specified location using the Light API.
     *
     * @param location   The location at which to set the light level.
     * @param lightLevel The light level to set.
     */
    private void addLight(Location location, int lightLevel) {
        // The following line place a block of air if light level is less or equal than 0 to
        // ensure that players can push / pull blocks using Piston next to lasers particles.
        // See issue #219 for more information.
        if (lightLevel <= 0) removeLight(location);

        // Ensure the location's world is not null
        World world = Objects.requireNonNull(location.getWorld());
        Block block = world.getBlockAt(location);

        // // Ensure to not delete existing block
        if (!BlockUtils.isLightOrVoid(block.getType())) return;

        block.setType(Material.LIGHT);
        BlockData blockData = Material.LIGHT.createBlockData();
        ((Light) blockData).setLevel(lightLevel);
        block.setBlockData(blockData);
        block.getState().update();
    }

    private void removeLight(Location location) {
        // Ensure the location's world is not null
        World world = Objects.requireNonNull(location.getWorld());
        Block lightBlock = world.getBlockAt(location);

        if (lightBlock.getType() != Material.LIGHT) return;

        lightBlock.setType(Material.AIR);
        lightBlock.getState().update();
    }

    /**
     * Clears the light levels at the locations of previous light levels.
     */
    private void clearLight() {
        lightLevelPerLocations.keySet().forEach(this::removeLight);
    }

    /**
     * Updates the state of laser senders and redstone sensors based on adjacent redstone power.
     */
    private void updateRedstoneReceivers() {
        // Update laser senders with detection mode DETECTION_REDSTONE
        this.getComponents().stream().filter(component -> component.getComponentType().equals(ComponentType.LASER_SENDER)).map(LaserSender.class::cast).filter(laserSender -> laserSender.getDetectionMode() == DetectionMode.DETECTION_REDSTONE).forEach(laserSender -> {
            final Block block = laserSender.getComponentLocation().getBlock();
            laserSender.setPowered(block.isBlockIndirectlyPowered() || block.isBlockPowered());
        });

        // Update redstone sensors based on adjacent redstone power
        this.getComponents().stream().filter(component -> component.getComponentType().equals(ComponentType.REDSTONE_SENSOR)).map(RedstoneSensor.class::cast).forEach(redstoneSensor -> {
            final Block block = redstoneSensor.getComponentLocation().getBlock();
            redstoneSensor.setPowered(block.isBlockIndirectlyPowered() || block.isBlockPowered());
        });
    }

    /**
     * Modifies the area boundaries and performs checks for validity.
     *
     * @param minX The new minimum X-coordinate.
     * @param minY The new minimum Y-coordinate.
     * @param minZ The new minimum Z-coordinate.
     * @param maxX The new maximum X-coordinate.
     * @param maxY The new maximum Y-coordinate.
     * @param maxZ The new maximum Z-coordinate.
     * @throws AreaNoDepthException          Thrown if the area has no depth (e.g., minX >= maxX).
     * @throws AreaOverlapException          Thrown if the area overlaps with another area.
     * @throws ComponentOutsideAreaException Thrown if any component is outside the modified area.
     */
    public void modify(double minX, double minY, double minZ, double maxX, double maxY, double maxZ) throws AreaNoDepthException, AreaOverlapException, ComponentOutsideAreaException {
        // Create new Location objects based on the provided coordinates
        Location newMin = new Location(areaMinLocation.getWorld(), minX, minY, minZ);
        Location newMax = new Location(areaMaxLocation.getWorld(), maxX, maxY, maxZ);

        // Check for valid area depth
        if (minX >= maxX || minY >= maxY || minZ >= maxZ) {
            throw new AreaNoDepthException();
        }

        // Check for overlapping areas
        Area overlappingArea = Areas.getInstance().getOverlappingOtherAreas(areaMinLocation.getWorld(), minX, minY, minZ, maxX, maxY, maxZ, Collections.singleton(this));
        if (overlappingArea != null) {
            throw new AreaOverlapException(overlappingArea);
        }

        // Check if any component is outside the modified area
        IComponent componentOutsideArea = components.stream().filter(c -> !Area.containsLocation(c.getComponentLocation(), newMin, newMax)).findAny().orElse(null);
        if (componentOutsideArea != null) {
            throw new ComponentOutsideAreaException(componentOutsideArea);
        }

        // Update the area boundaries and perform necessary cache clearing and database update
        this.areaMinLocation = newMin;
        this.areaMaxLocation = newMax;
        Areas.getInstance().clearAreaByLocationCache();
        clearBlockReflectionCache();
        clearAttractionRepulsionCache();
        dbUpdate();
    }

    /**
     * Checks if the victory conditions for the area are reached.
     *
     * @return True if the victory conditions are met, otherwise false.
     */
    public boolean checkVictoryConditionsReached() {
        boolean won = false;

        // Check victory conditions based on the selected victory criteria
        switch (getVictoryDetectionMode()) {
            case DETECTION_LASER_RECEIVERS:
                won = getMinimumRange() <= activatedLaserReceiversCount && activatedLaserReceiversCount <= getMaximumRange();
                break;
            case DETECTION_REDSTONE_SENSORS:
                won = getMinimumRange() <= activatedRedstoneSensorsCount && activatedRedstoneSensorsCount <= getMaximumRange();
                break;
            case DETECTION_PLAYERS:
                won = getMinimumRange() <= playersInsideAreaCount && playersInsideAreaCount <= getMaximumRange();
                break;
            case DETECTION_LOCKS:
                won = getMinimumRange() <= activatedLocksCount && activatedLocksCount <= getMaximumRange();
                break;
            case DETECTION_VICTORY_AREA:
                // This method does not undertake player locations
                break;
            default:
                throw new IllegalStateException();
        }

        return won;
    }

    @Override
    public String toString() {
        return "Area{" + "areaWorld=" + areaWorld + ", areaId=" + areaId + ", areaMinLocation=" + areaMinLocation + ", areaMaxLocation=" + areaMaxLocation + '}';
    }

    public void clearLaserParticles() {
        laserParticles.clear();
        laserParticlesToAdd.clear();
    }
}
