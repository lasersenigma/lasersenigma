package eu.lasersenigma.area.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import java.util.logging.Level;

public class AreaEventsListener implements Listener {

    public AreaEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a BlockBreakEvent
     *
     * @param event a BlockBreakEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onBlockBreakEvent(BlockBreakEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onBlockBreakEvent");
        Location loc = event.getBlock().getLocation();

        Area area = Areas.getInstance().getAreaFromLocation(loc);
        if (area == null) {
            return;
        }

        // area.onBlockBreakEvent(e);

        // Update redstone (commented out)
        // updateRedstone();

        IComponent component = area.getComponentFromLocation(loc);
        if (component != null) {
            event.setCancelled(true);
            return;
        }
        if (event.getPlayer() == null) {
            event.setCancelled(true);
            return;
        }
        if (!Permission.EDIT.hasPermission(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    /**
     * executed on each BlockPlaceEvent
     *
     * @param event a BlockPlaceEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onBlockPlaceEvent(BlockPlaceEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onBlockPlaceEvent");
        Block blockPlaced = event.getBlockPlaced();
        Area area = Areas.getInstance().getAreaFromLocation(blockPlaced.getLocation());
        Item item;
        if (area == null) {
            item = ItemsFactory.getInstance().getSimilarItem(event.getItemInHand());
            if (item != null && (!item.isPlacableOutsideAnArea() || !item.isPlacableAsBlock())) {
                event.setCancelled(true);
            }
            return;
        }

        // area.onBlockBreakEvent(e);

        // Check if the placed block is in the powered locations set
        if (!area.getPoweredLocations().contains(event.getBlock().getLocation())) {
            return;
        }
        // Update redstone when a block is placed in a powered location
        area.updateRedstone();

        if (!Permission.EDIT.hasPermission(event.getPlayer())) {
            event.setCancelled(true);
            return;
        }
        item = ItemsFactory.getInstance().getSimilarItem(event.getItemInHand());
        if (item == null) {
            return;
        }
        if (!item.isPlacableAsBlock()) {
            event.setCancelled(true);
        }
    }

    /**
     * executed on a ChunkUnloadEvent
     *
     * @param event a ChunkUnloadEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onBlockRedstoneEvent(BlockRedstoneEvent event) {
        Location loc = event.getBlock().getLocation();
        Area area = Areas.getInstance().getAreaFromLocation(loc);
        if (area != null) {
            // area.onBlockRedstoneEvent(e, loc);

            // Check if the location is in the powered locations set
            if (!area.getPoweredLocations().contains(loc)) return;

            // Set the new current to 15 for powered locations
            event.setNewCurrent(15);
        }
    }

    /**
     * executed on each PlayerInteractAtEntityEvent
     *
     * @param event a PlayerInteractAtEntityEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onEntityDamageByEntityEvent");
        if (event.getEntityType() != EntityType.ARMOR_STAND) {
            return;
        }

        Entity damager = event.getDamager();
        IComponent component = Areas.getInstance().getComponentFromEntity(event.getEntity().getUniqueId());
        if (component == null) {
            if (damager instanceof Player && Areas.getInstance().getAreaFromLocation(event.getEntity().getLocation()) != null && !Permission.EDIT.checkPermissionAndSendMsg((Player) damager)) {
                event.setCancelled(true);
            }
            return;
        }

        event.setCancelled(true);

        if (!(damager instanceof Player)) {
            return;
        }
        Player player = (Player) damager;
        LEPlayers.getInstance().findLEPlayer(player).onLeftClick(component);
    }

    /**
     * executed on each PlayerInteractAtEntityEvent
     *
     * @param event a PlayerInteractAtEntityEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerInteractAtEntityEvent(PlayerInteractAtEntityEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerInteractAtEntityEvent");
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInteractAtEntityEventLister");
        IComponent component = Areas.getInstance().getComponentFromEntity(event.getRightClicked().getUniqueId());
        Player p = event.getPlayer();
        if (component == null) {
            if (Areas.getInstance().getAreaFromLocation(event.getRightClicked().getLocation()) != null && !Permission.EDIT.checkPermissionAndSendMsg(p)) {
                event.setCancelled(true);
            }
            return;
        }
        event.setCancelled(true);
        LEPlayers.getInstance().findLEPlayer(p).onRightClick(component);
    }

    /**
     * executed on each WorldLoadEvent
     *
     * @param event a WorldLoadEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onInventoryClickEvent(WorldLoadEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onWorldLoadEvent");
        Areas.getInstance().onWorldLoad(event);
    }

    /**
     * executed on each WorldLoadEvent
     *
     * @param event a WorldLoadEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onInventoryClickEvent(WorldUnloadEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onWorldLoadEvent");
        Areas.getInstance().onWorldUnload(event);
    }
}
