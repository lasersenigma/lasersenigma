package eu.lasersenigma.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.exception.AreaCrossWorldsException;
import eu.lasersenigma.area.exception.AreaNoDepthException;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.task.ITaskComponent;
import eu.lasersenigma.common.util.RedstoneUtils;
import eu.lasersenigma.component.*;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockRedstoneEvent;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Abstract class containing basics of an area.
 * This abstract class was created to organize and clarify the code in the Area class (partial equivalent).
 */
public abstract class AArea {

    public static final int LIMIT_FOR_SHOWING_WHOLE_AREA = 70;
    public static final int VICTORY_DETECTION_DELAY = 40;
    public static final int LASER_DAMAGE_FREQUENCY = 5;
    public static final int LASERS_SHOW_FREQUENCY = 4;
    public static final int LASERS_LIGHT_UPDATE_FREQUENCY = 8;

    // The world containing the area
    protected final World areaWorld;

    // The id of the area in database
    protected int areaId;

    // The location of the corner of the area with the minimal coordinates
    protected Location areaMinLocation;

    // The location of the corner of the area with the maximal coordinates
    protected Location areaMaxLocation;

    // If the area is activated or not. An area is activated when players are inside it.
    private boolean isActivated = false;


    // The area victory criteria
    protected DetectionMode victoryDetectionMode = DetectionMode.DETECTION_LASER_RECEIVERS;

    // The minimum amount of things that must be activated for the area to be won
    private int minimumRange = 1;

    // The minimum amount of things that must be activated for the area to be won
    private int maximumRange = 10;


    // The laser particles
    protected HashSet<LaserParticle> laserParticles = new HashSet<>();

    // The new laser particles to add
    protected HashSet<LaserParticle> laserParticlesToAdd = new HashSet<>();

    // This will contain the locations where light needs to be added during the next update of the
    // laser particles aswell as the level of brightness to be applied.
    protected HashMap<Location, Integer> lightLevelPerLocations = new HashMap<>();

    // This will contain the locations where light was added during the previous laser particle
    // update as well as thebrightness level that was applied.
    protected HashMap<Location, Integer> previousLightLevelPerLocations = new HashMap<>();


    // Area's components
    protected final HashSet<IComponent> components = new HashSet<>();

    // Area's components cache (stored by their location)
    protected final HashMap<Location, IComponent> componentsCache = new HashMap<>();

    // Area's locations without component
    protected final HashSet<Location> locationsWithoutComponent = new HashSet<>();

    // Area's powered locations
    private final HashSet<Location> poweredLocations = new HashSet<>();

    // Area's faces to power
    public static final HashSet<BlockFace> FACES_TO_POWER = new HashSet<>(Arrays.asList(BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST));

    // Victory areas
    private final HashSet<VictoryArea> victoryAreas = new HashSet<>();

    // Checkpoints
    private Location areaCheckpointLocation = null;


    // Defines if the vertical rotation of mirrors is allowed in this area.
    private boolean vMirrorRotationAllowed = true;

    // Defines if the horizontal rotation of mirrors is allowed in this area
    private boolean hMirrorRotationAllowed = true;

    // Defines if the horizontal rotation of concentrator is allowed in this area or not
    private boolean vConcentratorRotationAllowed = true;

    // Defines if the verical rotation of concentrator is allowed in this area or not
    private boolean hConcentratorRotationAllowed = true;

    // Defines if the horizontal rotation of laser senders is allowed in this area or not
    private boolean vLaserSenderRotationAllowed = false;

    // Defines if the verical rotation of laser senders is allowed in this area or not
    private boolean hLaserSenderRotationAllowed = false;

    // Defines if the horizontal rotation of laser receivers is allowed in this area or not
    private boolean vLaserReceiverRotationAllowed = false;

    // Defines if the verical rotation of laser receivers is allowed in this area or not
    private boolean hLaserReceiverRotationAllowed = false;

    // Define if the resizing of attraction/repulsion spheres is allowed in this area or not
    private boolean attractionRepulsionSpheresResizingAllowed;


    /**
     * Constructor for initializing an area with specified locations.
     *
     * @param corner1 first location (not minimal location yet, simply a corner location)
     * @param corner2 second location (not maximal location yet, simply the opposite corner location)
     * @throws AreaCrossWorldsException if the 2 locations are in different worlds
     * @throws AreaNoDepthException     if the area has no depth/width/height
     */
    public AArea(Location corner1, Location corner2) throws AreaCrossWorldsException, AreaNoDepthException {
        this.areaWorld = corner1.getWorld();

        // Check if corner1 and corner2 are in the same world
        if (!Objects.equals(areaWorld, corner2.getWorld())) {
            throw new AreaCrossWorldsException();
        }

        double minX = Math.min(corner1.getBlockX(), corner2.getBlockX());
        double minY = Math.min(corner1.getBlockY(), corner2.getBlockY());
        double minZ = Math.min(corner1.getBlockZ(), corner2.getBlockZ());

        double maxX = Math.max(corner1.getBlockX(), corner2.getBlockX());
        double maxY = Math.max(corner1.getBlockY(), corner2.getBlockY());
        double maxZ = Math.max(corner1.getBlockZ(), corner2.getBlockZ());

        // Check if corner1 and corner2 are not the same
        if (minX == maxX || minY == maxY || minZ == maxZ) {
            throw new AreaNoDepthException();
        }

        this.areaMinLocation = new Location(areaWorld, minX, minY, minZ);
        this.areaMaxLocation = new Location(areaWorld, maxX, maxY, maxZ);
    }

    /**
     * Retrieves area's related world
     *
     * @return Area's world
     */
    public World getAreaWorld() {
        return areaWorld;
    }

    /**
     * Retrieves area's id
     *
     * @return Area's id
     */
    public int getAreaId() {
        return areaId;
    }

    /**
     * Defines area's id
     *
     * @param areaId Desired id
     */
    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    /**
     * Retrieves area's min location
     *
     * @return Area's min location
     */
    public Location getAreaMinLocation() {
        return areaMinLocation;
    }

    /**
     * Retrieves area's max location
     *
     * @return Area's max location
     */
    public Location getAreaMaxLocation() {
        return areaMaxLocation;
    }

    /**
     * Checks if the area is activated i.e. the area contains players
     *
     * @return True if the area contains player
     */
    public final boolean isActivated() {
        return this.isActivated;
    }

    /**
     * Defines if an area is activated or deactivated
     *
     * @param newState Should area be activated?
     */
    public final void setActivated(boolean newState) {
        this.isActivated = newState;
    }

    /**
     * Retrieves area's victory detection mode
     *
     * @return Area's victory detection mode
     */
    public DetectionMode getVictoryDetectionMode() {
        return victoryDetectionMode;
    }

    /**
     * Defines area's victory detection mode
     *
     * @param detectionMode Desired victory detection mode
     */
    public void setVictoryDetectionMode(DetectionMode detectionMode) {
        this.victoryDetectionMode = detectionMode;
    }

    /**
     * Retrieves area's minimum range
     *
     * @return Area's minimum range
     */
    public int getMinimumRange() {
        return minimumRange;
    }

    /**
     * Defines area's minimum range
     *
     * @param minimumRange Desired minimum range
     */
    public void setMinimumRange(int minimumRange) {
        this.minimumRange = minimumRange;
    }

    /**
     * Retrieves area's maximum range
     *
     * @return Area's maximum range
     */
    public int getMaximumRange() {
        return maximumRange;
    }

    /**
     * Defines area's maximum range
     *
     * @param maximumRange Desired maximum range
     */
    public void setMaximumRange(int maximumRange) {
        this.maximumRange = maximumRange;
    }

    /**
     * Retrieves the area's laser particles
     *
     * @return The area's laser particles
     */
    public final HashSet<LaserParticle> getLaserParticles() {
        return this.laserParticles;
    }

    /**
     * Adds a laser particle which will be added later in the main laserParticles HashSet
     *
     * @param laserParticle Laser particle
     */
    public final void addLaserParticle(LaserParticle laserParticle) {
        if (laserParticlesToAdd.stream().anyMatch(p -> p.isDuplicate(laserParticle))) {
            return;
        }
        laserParticlesToAdd.add(laserParticle);
    }

    public final HashMap<Location, Integer> getLightLevelPerLocations() {
        return lightLevelPerLocations;
    }

    public final void putLightLevelPerLocations(Location location, int lightLevel) {
        lightLevelPerLocations.put(location, lightLevel);
    }

    /**
     * Retrieves the players (not in spectator mode) inside the area without using or updating cached data
     *
     * @return The players inside the area
     */
    public final HashSet<Player> getCurrentPlayersInsideArea() {
        HashSet<Player> currentPlayersInsideArea = new HashSet<>();
        int pX, pY, pZ;
        int minX = areaMinLocation.getBlockX(), minY = areaMinLocation.getBlockY(), minZ = areaMinLocation.getBlockZ();
        int maxX = areaMaxLocation.getBlockX(), maxY = areaMaxLocation.getBlockY(), maxZ = areaMaxLocation.getBlockZ();

        for (Player player : Objects.requireNonNull(areaMinLocation.getWorld()).getPlayers()) {
            Location loc = player.getLocation();
            pX = loc.getBlockX();
            pY = loc.getBlockY();
            pZ = loc.getBlockZ();

            if (minX <= pX && pX <= maxX && minY <= pY && pY <= maxY && minZ <= pZ && pZ <= maxZ) {
                currentPlayersInsideArea.add(player);
            }
        }
        return currentPlayersInsideArea;
    }

    /**
     * Gets the players (not in spectator gamemode) inside the area without
     * using or updating cached data.
     *
     * @param players All players.
     * @return The players inside the area who are not in spectator mode.
     */
    protected final HashSet<Player> filterSpectators(HashSet<Player> players) {
        return players.stream()
                .filter(p -> p.getGameMode() != GameMode.SPECTATOR)
                .collect(Collectors.toCollection(HashSet::new));
    }

    /**
     * Retrieves components from it location
     *
     * @return Map of components
     */
    public final HashMap<Location, IComponent> getComponentsCache() {
        return componentsCache;
    }

    /**
     * Gets a component from its location
     *
     * @param location The location of the component
     * @return Null if no component was found, else the component
     */
    public final IComponent getComponentFromLocation(Location location) {
        Location blockLocation = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());

        if (locationsWithoutComponent.contains(blockLocation)) return null;

        IComponent cachedComponent = componentsCache.get(blockLocation);

        if (cachedComponent == null) {
            cachedComponent = findComponentFromLocation(location);

            if (cachedComponent == null) {
                locationsWithoutComponent.add(blockLocation);
            } else {
                componentsCache.put(blockLocation, cachedComponent);
            }
        }

        return cachedComponent;
    }

    private IComponent findComponentFromLocation(Location location) {
        return components.stream()
                .filter(c -> isMatchingLocation(c, location))
                .findAny()
                .orElse(null);
    }

    public void clearLocationsWithNoComponentCache() {
        locationsWithoutComponent.clear();
    }

    /**
     * Retrieves area's components
     *
     * @return Area's components
     */
    public final Set<IComponent> getComponents() {
        return components;
    }

    /**
     * Retrieves area's components of certain type and get it actual instance
     *
     * @param componentClass Desired component class
     * @return Area's components of desired type
     */
    public final <T extends IComponent> Set<T> getComponents(Class<T> componentClass) {
        return components.stream()
                .filter(component -> componentClass.isInstance(component))
                .map(componentClass::cast)
                .collect(Collectors.toSet());
    }

    /**
     * Retrieves area's components of certain type
     *
     * @param componentType Desired component type
     * @return Area's components of desired type
     */
    public final Set<IComponent> getComponents(ComponentType componentType) {
        return components.stream()
                .filter(component -> component.getComponentType() == componentType)
                .collect(Collectors.toSet());
    }

    /**
     * Retrieves a component from it id
     *
     * @param componentId Component's id to search for
     * @return Found component or null
     */
    public final IComponent getComponent(int componentId) {
        return components.stream()
                .filter(c -> c.getComponentId() == componentId)
                .findAny().orElse(null);
    }

    /**
     * Adds a new component to the area and update it
     *
     * @param component Component to add
     */
    public final void addComponent(IComponent component) {
        addComponent(component, true);
    }

    /**
     * Adds a new component to the area
     *
     * @param component Component to add
     * @param update    Should the component be updated?
     */
    public final void addComponent(IComponent component, boolean update) {
        // Add component to components list
        components.add(component);

        if (update) component.showOrUpdateComponent();
        if (component instanceof ITaskComponent taskComponent) taskComponent.startTask();

        locationsWithoutComponent.clear();
    }

    /**
     * Removes a component from the location
     *
     * @param component The component to remove
     */
    public final void removeComponent(IComponent component) {
        component.deleteComponent();
        components.remove(component);
        componentsCache.entrySet().removeIf(e -> e.getValue() == component);
    }

    /**
     * Removes the area and its components
     */
    public final void deleteArea() {
        components.forEach(IComponent::deleteComponent);
        components.clear();
    }

    /**
     * Retrieves area's powered locations
     *
     * @return Area's powered locations
     */
    public final HashSet<Location> getPoweredLocations() {
        return poweredLocations;
    }

    /**
     * Checks if a location is powered
     *
     * @param location Desired location
     * @return True if a location is powered, false elsewhere
     */
    public final boolean isPoweredAtLocation(Location location) {
        return poweredLocations.contains(location);
    }

    /**
     * Sets the power state at the given source location and triggers BlockRedstoneEvent for affected locations
     *
     * @param source  The source location to set the power state
     * @param powered The new power state
     */
    public final void setPower(Location source, boolean powered) {
        // Iterate through affected locations and update power state
        HashSet<Location> redstoneSignalUpdateLocations = getRedstoneSignalUpdateLocations(source);

        redstoneSignalUpdateLocations.forEach(location -> {
            Block block = location.getBlock();
            int newCurrent = 0, oldCurrent = 0;

            // Update power state and record current values
            if (powered) {
                poweredLocations.add(location);
                if (block.getType() == Material.REDSTONE_WIRE) {
                    RedstoneUtils.powerBlock(block, (byte) 15);
                    newCurrent = 15;
                }
            } else {
                poweredLocations.remove(location);
                if (block.getType() == Material.REDSTONE_WIRE) {
                    RedstoneUtils.powerBlock(block, (byte) 0);
                    oldCurrent = 0;
                }
            }

            // Trigger BlockRedstoneEvent for the updated block
            callLater(new BlockRedstoneEvent(block, oldCurrent, newCurrent));
        });
    }

    /**
     * Gets the locations to update redstone signals based on the given location
     *
     * @param location The reference location
     * @return The set of locations to update redstone signals
     */
    private static HashSet<Location> getRedstoneSignalUpdateLocations(Location location) {
        HashSet<Location> result = new HashSet<>();

        // Iterate through faces to power and their adjacent locations
        FACES_TO_POWER.forEach(faceToPower -> {
            Location faceLocation = location.getBlock().getRelative(faceToPower).getLocation();
            result.add(faceLocation);

            // Iterate through faces to power of the adjacent location
            result.addAll(FACES_TO_POWER.stream()
                    .map(face -> faceLocation.getBlock().getRelative(face).getLocation())
                    .collect(Collectors.toSet()));
        });

        return result;
    }

    /**
     * Calls a BlockRedstoneEvent after a delay
     *
     * @param event The BlockRedstoneEvent to call
     */
    private static void callLater(final BlockRedstoneEvent event) {
        // Check if the plugin is disabling, if so, do not proceed
        if (LasersEnigmaPlugin.getInstance().isDisabling()) return;

        // Schedule the event to be called after a short delay
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> Bukkit.getServer().getPluginManager().callEvent(event), 1);
    }

    /**
     * Update redstone for all powered locations
     */
    public final void updateRedstone() {
        poweredLocations.forEach(location -> {
            Block block = location.getBlock();

            int newCurrent = 0, oldCurrent = 0;

            // Update power state based on block type
            if (block.getType() == Material.REDSTONE_WIRE) {
                RedstoneUtils.powerBlock(block, (byte) 15);
                newCurrent = 15;
            }
        });
    }

    /**
     * Retrieves locations that not contains component
     *
     * @return Set of locations
     */
    public final HashSet<Location> getLocationsWithoutComponent() {
        return locationsWithoutComponent;
    }

    /**
     * Gets the set of stored victory areas
     *
     * @return Set of victory areas
     */
    public final HashSet<VictoryArea> getVictoryAreas() {
        return victoryAreas;
    }

    /**
     * Adds a victory area to the stored set
     *
     * @param victoryArea The victory area to add
     */
    public final void addVictoryArea(VictoryArea victoryArea) {
        victoryAreas.add(victoryArea);
    }

    /**
     * Deletes all stored victory areas
     */
    public final void deleteVictoryAreas() {
        victoryAreas.clear();
    }

    /**
     * Gets the stored checkpoint location
     *
     * @return The checkpoint location
     */
    public Location getAreaCheckpointLocation() {
        return areaCheckpointLocation;
    }

    /**
     * Sets the checkpoint location if it is within the area, otherwise throws NoAreaFoundException
     *
     * @param areaCheckpointLocation The checkpoint location to set
     * @throws NoAreaFoundException Thrown if the location is not within the area
     */
    public void setAreaCheckpointLocation(Location areaCheckpointLocation) throws NoAreaFoundException {
        // Check if the location is within the area, otherwise throw an exception
        if (areaCheckpointLocation != null && !this.containsLocation(areaCheckpointLocation)) {
            this.areaCheckpointLocation = null;
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().warning("Could not define the checkpoint location for the area " + this.getAreaId() + " because it is not within the area. The location was " + areaCheckpointLocation.toString());
        }
        this.areaCheckpointLocation = areaCheckpointLocation;
    }

    /**
     * Deletes the stored area checkpoint
     */
    public void deleteAreaCheckpointLocation() {
        this.areaCheckpointLocation = null;
    }

    public final boolean isHMirrorsRotationAllowed() {
        return hMirrorRotationAllowed;
    }

    public final void setHMirrorsRotationAllowed(boolean hRotationAllowed) {
        this.hMirrorRotationAllowed = hRotationAllowed;
    }

    public final boolean isVMirrorsRotationAllowed() {
        return vMirrorRotationAllowed;
    }

    public final void setVMirrorsRotationAllowed(boolean vRotationAllowed) {
        this.vMirrorRotationAllowed = vRotationAllowed;
    }

    public final boolean isVConcentratorsRotationAllowed() {
        return vConcentratorRotationAllowed;
    }

    public final void setVConcentratorsRotationAllowed(boolean vConcentratorRotationAllowed) {
        this.vConcentratorRotationAllowed = vConcentratorRotationAllowed;
    }

    public final boolean isHConcentratorsRotationAllowed() {
        return hConcentratorRotationAllowed;
    }

    public final void setHConcentratorsRotationAllowed(boolean hConcentratorRotationAllowed) {
        this.hConcentratorRotationAllowed = hConcentratorRotationAllowed;
    }

    public final boolean isHLaserSendersRotationAllowed() {
        return hLaserSenderRotationAllowed;
    }

    public final void setHLaserSendersRotationAllowed(boolean hLaserSenderRotationAllowed) {
        this.hLaserSenderRotationAllowed = hLaserSenderRotationAllowed;
    }

    public final boolean isHLaserReceiversRotationAllowed() {
        return hLaserReceiverRotationAllowed;
    }

    public final void setHLaserReceiversRotationAllowed(boolean hLaserReceiverRotationAllowed) {
        this.hLaserReceiverRotationAllowed = hLaserReceiverRotationAllowed;
    }

    public final boolean isVLaserSendersRotationAllowed() {
        return vLaserSenderRotationAllowed;
    }

    public final void setVLaserSendersRotationAllowed(boolean vLaserSenderRotationAllowed) {
        this.vLaserSenderRotationAllowed = vLaserSenderRotationAllowed;
    }

    public final boolean isVLaserReceiversRotationAllowed() {
        return vLaserReceiverRotationAllowed;
    }

    public final void setVLaserReceiversRotationAllowed(boolean vLaserReceiverRotationAllowed) {
        this.vLaserReceiverRotationAllowed = vLaserReceiverRotationAllowed;
    }

    public final boolean isAttractionRepulsionSpheresResizingAllowed() {
        return attractionRepulsionSpheresResizingAllowed;
    }

    public final void setAttractionRepulsionSpheresResizingAllowed(boolean attractionRepulsionSpheresResizingAllowed) {
        this.attractionRepulsionSpheresResizingAllowed = attractionRepulsionSpheresResizingAllowed;
    }

    public final boolean isRotationAllowedWithoutPermissions(ComponentType componentType, RotationType rotationType) {
        return switch (componentType) {
            case LASER_RECEIVER ->
                    isRotationAllowed(rotationType, isHLaserReceiversRotationAllowed(), isVLaserReceiversRotationAllowed());
            case LASER_SENDER ->
                    isRotationAllowed(rotationType, isHLaserSendersRotationAllowed(), isVLaserSendersRotationAllowed());
            case CONCENTRATOR ->
                    isRotationAllowed(rotationType, isHConcentratorsRotationAllowed(), isVConcentratorsRotationAllowed());
            case MIRROR_SUPPORT ->
                    isRotationAllowed(rotationType, isHMirrorsRotationAllowed(), isVMirrorsRotationAllowed());
            default -> throw new UnsupportedOperationException("Unsupported component type");
        };
    }

    private boolean isRotationAllowed(RotationType rotationType, boolean horizontalAllowed, boolean verticalAllowed) {
        return switch (rotationType) {
            case LEFT, RIGHT -> horizontalAllowed;
            case UP, DOWN -> verticalAllowed;
            default -> throw new UnsupportedOperationException("Rotation type should never be null");
        };
    }

    /**
     * Checks if the area contains a location
     *
     * @param location The location to test
     * @return True if the area contains the given location, false elsewhere
     */
    public final boolean containsLocation(Location location) {
        return containsLocation(location, areaMinLocation, areaMaxLocation);
    }

    /**
     * Checks if an area contains a location.
     *
     * @param location the location to test
     * @param min      the minimum location of the area
     * @param max      the maximum location of the area
     * @return true if the area contains the given location
     */
    public static boolean containsLocation(Location location, Location min, Location max) {
        return min.getBlockX() <= location.getBlockX() && location.getBlockX() <= max.getBlockX()
                && min.getBlockY() <= location.getBlockY() && location.getBlockY() <= max.getBlockY()
                && min.getBlockZ() <= location.getBlockZ() && location.getBlockZ() <= max.getBlockZ();
    }

    private boolean isMatchingLocation(IComponent component, Location location) {
        if (component instanceof IAreaComponent areaComponent) {
            return AArea.containsLocation(location, areaComponent.getMinLocation(), areaComponent.getMaxLocation());
        } else {
            Location componentLocation = component.getComponentLocation();
            return Objects.equals(componentLocation.getWorld(), location.getWorld())
                    && componentLocation.getBlockX() == location.getBlockX()
                    && componentLocation.getBlockY() == location.getBlockY()
                    && componentLocation.getBlockZ() == location.getBlockZ();
        }
    }

    /**
     * Checks if an area is overlapping this area.
     *
     * @param area The other area which could overlap the current area.
     * @return True if the areas overlap.
     */
    public final boolean isOverlapping(Area area) {
        return this.isOverlapping(area.getAreaWorld(), area.getAreaMinLocation().getBlockX(), area.getAreaMinLocation().getBlockY(), area.getAreaMinLocation().getBlockZ(), area.getAreaMaxLocation().getBlockX(), area.getAreaMaxLocation().getBlockY(), area.getAreaMaxLocation().getBlockZ());
    }

    /**
     * Checks if an area is overlapping with another area specified by coordinates.
     *
     * @param world The world of the other area.
     * @param minX  The minimal x-coordinate of the other area.
     * @param minY  The minimal y-coordinate of the other area.
     * @param minZ  The minimal z-coordinate of the other area.
     * @param maxX  The maximal x-coordinate of the other area.
     * @param maxY  The maximal y-coordinate of the other area.
     * @param maxZ  The maximal z-coordinate of the other area.
     * @return True if the areas overlap.
     */
    public final boolean isOverlapping(World world, double minX, double minY, double minZ, double maxX, double maxY, double maxZ) {
        if (!areaWorld.getUID().equals(world.getUID())) {
            return false;
        }
        if (!intersectsDimension(areaMinLocation.getBlockX(), areaMaxLocation.getBlockX(), minX, maxX)) {
            return false;
        }
        if (!intersectsDimension(areaMinLocation.getBlockY(), areaMaxLocation.getBlockY(), minY, maxY)) {
            return false;
        }
        return intersectsDimension(areaMinLocation.getBlockZ(), areaMaxLocation.getBlockZ(), minZ, maxZ);

    }


    /**
     * Checks if an area is overlapping with a chunk.
     *
     * @param chunk The chunk that could overlap the current area.
     * @return True if the areas overlap.
     */
    public final boolean isOverlapping(Chunk chunk) {
        if (!chunk.getWorld().getName().equals(Objects.requireNonNull(areaMinLocation.getWorld()).getName())) {
            return false;
        }
        Location cMinLoc = new Location(chunk.getWorld(), chunk.getX() * 16, 0, chunk.getZ() * 16);
        Location cMaxLoc = new Location(chunk.getWorld(), chunk.getX() * 16 + 15, chunk.getWorld().getMaxHeight(), chunk.getZ() * 16 + 15);
        if (!intersectsDimension(areaMinLocation.getBlockX(), areaMaxLocation.getBlockX(), cMinLoc.getBlockX(), cMaxLoc.getBlockX())) {
            return false;
        }
        if (!intersectsDimension(areaMinLocation.getBlockY(), areaMaxLocation.getBlockY(), cMinLoc.getBlockY(), cMaxLoc.getBlockY())) {
            return false;
        }
        return intersectsDimension(areaMinLocation.getBlockZ(), areaMaxLocation.getBlockZ(), cMinLoc.getBlockZ(), cMaxLoc.getBlockZ());

    }

    /**
     * Checks if dimensions intersect.
     *
     * @param aMin Minimal value of the first area on a particular axis.
     * @param aMax Maximal value of the first area on a particular axis.
     * @param bMin Minimal value of the second area on a particular axis.
     * @param bMax Maximal value of the second area on a particular axis.
     * @return True if the dimensions intersect.
     */
    protected boolean intersectsDimension(double aMin, double aMax, double bMin, double bMax) {
        return aMin <= bMax && aMax >= bMin;
    }

    /**
     * Gets the location of this area's center.
     *
     * @return The location of this area's center.
     */
    public Location getAreaCenterLocation() {
        return new Location(
                areaMinLocation.getWorld(),
                areaMinLocation.getBlockX() + ((areaMaxLocation.getBlockX() - areaMinLocation.getBlockX()) / 2.0D),
                areaMinLocation.getBlockY() + ((areaMaxLocation.getBlockY() - areaMinLocation.getBlockY()) / 2.0D),
                areaMinLocation.getBlockZ() + ((areaMaxLocation.getBlockZ() - areaMinLocation.getBlockZ()) / 2.0D)
        );
    }

    private boolean isLocationInChunk(Location loc, double minX, double maxX, double minZ, double maxZ) {
        if (loc.getBlockX() < minX || loc.getBlockX() > maxX) {
            return false;
        }

        return !(loc.getBlockZ() < minZ || loc.getBlockZ() > maxZ);
    }

    // int counter;

    /**
     * Removes all armor stands within the area
     */
    public final void removeAllAmorStands() {
        double minX = areaMinLocation.getX() - 1;
        double minY = areaMinLocation.getY() - 1;
        double minZ = areaMinLocation.getZ() - 1;
        double maxX = areaMaxLocation.getX() + 1;
        double maxY = areaMaxLocation.getY() + 1;
        double maxZ = areaMaxLocation.getZ() + 1;

        Objects.requireNonNull(areaMinLocation.getWorld()).getEntities().forEach(entity -> {
            if (entity.getType() == EntityType.ARMOR_STAND && hasCorrectCustomName(entity)) {
                Location location = entity.getLocation();
                double asX = location.getX();
                double asY = location.getY();
                double asZ = location.getZ();
                if (minX <= asX && asX <= maxX && minY <= asY && asY <= maxY && minZ <= asZ && asZ <= maxZ) {
                    entity.remove();
                }
            }
        });

    }

    private boolean hasCorrectCustomName(Entity entity) {
        return entity.getCustomName() != null
                && entity.getCustomName().equals(AArmorStandComponent.ARMOR_STAND_CUSTOM_NAME);
    }

    private boolean isEntityInsideBoundingBox(Location location,
                                              double minX, double minY, double minZ,
                                              double maxX, double maxY, double maxZ) {
        double asX = location.getX();
        double asY = location.getY();
        double asZ = location.getZ();

        return minX <= asX && asX <= maxX && minY <= asY && asY <= maxY && minZ <= asZ && asZ <= maxZ;
    }

    public final void show(Player player) {
        show(player, 20);
    }

    public void show(Player player, int nbShow) {
        final HashSet<Player> players = new HashSet<>(Collections.singleton(player));
        final HashSet<Location> particlesLocations = new HashSet<>();
        final double areaDiagonalSize = areaMaxLocation.distance(areaMinLocation);
        int minX = areaMinLocation.getBlockX();
        int maxX = areaMaxLocation.getBlockX() + 1;
        int minY = areaMinLocation.getBlockY();
        int maxY = areaMaxLocation.getBlockY() + 1;
        int minZ = areaMinLocation.getBlockZ();
        int maxZ = areaMaxLocation.getBlockZ() + 1;

        //Getting edges
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                for (int z = minZ; z <= maxZ; z++) {
                    boolean checkX = x == minX || x == maxX;
                    boolean checkY = y == minY || y == maxY;
                    boolean checkZ = z == minZ || z == maxZ;
                    boolean corner = false;

                    if (checkX && checkY && checkZ) {
                        corner = true;
                    }
                    if (!((checkX && checkY) || (checkX && checkZ) || (checkY && checkZ))) {
                        //not a edge
                        continue;
                    }
                    if (corner || areaDiagonalSize < LIMIT_FOR_SHOWING_WHOLE_AREA) { //we show only corners if the area is too big
                        particlesLocations.add(new Location(areaMinLocation.getWorld(), x, y, z));
                    }
                }
            }
        }

        //Getting diagonal
        for (int i = 0; i <= areaDiagonalSize; i++) {
            double x = minX + ((maxX - minX) / areaDiagonalSize) * i;
            double y = minY + ((maxY - minY) / areaDiagonalSize) * i;
            double z = minZ + ((maxZ - minZ) / areaDiagonalSize) * i;
            particlesLocations.add(new Location(areaMinLocation.getWorld(), x, y, z));
        }

        //Pour N, x = minX + ((maxX - minX) / areaDiagonalSize) * N
        //Pour N, x = minX + ((maxX - minX) / areaDiagonalSize) * N
        //Pour N, x = minX + ((maxX - minX) / areaDiagonalSize) * N


        particlesLocations.forEach(location -> {
            for (int delay = 1, i = 0; i < nbShow; i++) {
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> LaserParticle.playEffect(
                        players,
                        location,
                        Particle.REDSTONE,
                        Color.ORANGE), delay);
                delay += 5;
            }
        });

    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AArea aArea = (AArea) o;
        return areaId == aArea.areaId && Objects.equals(areaWorld, aArea.areaWorld) && Objects.equals(areaMinLocation, aArea.areaMinLocation) && Objects.equals(areaMaxLocation, aArea.areaMaxLocation);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(areaWorld, areaId, areaMinLocation, areaMaxLocation);
    }
}
