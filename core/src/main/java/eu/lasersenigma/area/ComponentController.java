package eu.lasersenigma.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.exception.ComponentAlreadyInsideAreaException;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.area.exception.NotSameAreaException;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.common.task.ColorChangeTask;
import eu.lasersenigma.component.*;
import eu.lasersenigma.component.burnableblock.BurnableBlock;
import eu.lasersenigma.component.burnableblock.event.PlayerTryToChangeBurnableBlockMaterialEvent;
import eu.lasersenigma.component.elevator.Elevator;
import eu.lasersenigma.component.elevator.ElevatorCallButton;
import eu.lasersenigma.component.elevator.ElevatorDirection;
import eu.lasersenigma.component.elevator.event.PlayerTryToCallElevatorEvent;
import eu.lasersenigma.component.elevator.event.PlayerTryToMoveElevatorEvent;
import eu.lasersenigma.component.event.*;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphereMode;
import eu.lasersenigma.component.gravitationalsphere.event.PlayerTryToChangeAttractionRepulsionSphereModeEvent;
import eu.lasersenigma.component.gravitationalsphere.event.PlayerTryToChangeAttractionRepulsionSphereSizeEvent;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.laserreceiver.event.PlayerTryToToggleLetPassLaserEvent;
import eu.lasersenigma.component.lasersender.LaserSender;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.component.mirrorchest.event.PlayerTryToDecreaseMirrorChestEvent;
import eu.lasersenigma.component.mirrorchest.event.PlayerTryToIncreaseMirrorChestEvent;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.mirrorsupport.MirrorSupportMode;
import eu.lasersenigma.component.mirrorsupport.event.PlayerTryToChangeMirrorSupportModeEvent;
import eu.lasersenigma.component.mirrorsupport.event.PlayerTryToPlaceMirrorEvent;
import eu.lasersenigma.component.mirrorsupport.event.PlayerTryToRetrieveMirrorEvent;
import eu.lasersenigma.component.musicblock.MusicBlock;
import eu.lasersenigma.component.musicblock.event.PlayerTryToChangeMusicBlockSongEvent;
import eu.lasersenigma.component.musicblock.event.PlayerTryToToggleMusicBlockLoopEvent;
import eu.lasersenigma.component.musicblock.event.PlayerTryToToggleMusicBlockStopOnExitEvent;
import eu.lasersenigma.component.scheduledactions.ActionCause;
import eu.lasersenigma.component.scheduledactions.ActionType;
import eu.lasersenigma.component.scheduledactions.ScheduledAction;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.player.PlayerInventoryManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class ComponentController {

    private ComponentController() {
    }

    /**
     * Create component from a player action
     *
     * @param componentType    the type of the component to create
     * @param clickedBlock     the clicked block
     * @param clickedBlockFace the face clicked
     * @param player           the player that started this action
     * @param linkedComponent  the component linked to this one (for example the
     *                         lock if this component is meant to be a chest)
     * @return the created component
     */
    public static IComponent addComponentFromBlockFace(ComponentType componentType, Block clickedBlock, BlockFace clickedBlockFace, LEPlayer player, IComponent linkedComponent) {
        Location newComponentLocation = clickedBlock.getLocation().add(clickedBlockFace.getModX(), clickedBlockFace.getModY(), clickedBlockFace.getModZ());
        Area area = Areas.getInstance().getAreaFromLocation(newComponentLocation);
        if (area == null) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            return null;
        }
        if (area.getComponentFromLocation(newComponentLocation) != null) {
            return null;
        }
        ComponentFace face = ComponentFace.from(clickedBlockFace);
        if (face == null) {
            return null;
        }
        PlayerTryToCreateComponentEvent compEvent = new PlayerTryToCreateComponentEvent(player.getBukkitPlayer(), area, componentType, newComponentLocation, face);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return null;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return null;
        }
        if (componentType == ComponentType.MIRROR_CHEST) {
            float dir = (float) Math.toDegrees(Math.atan2(player.getBukkitPlayer().getLocation().getBlockX() - newComponentLocation.getX(), newComponentLocation.getZ() - player.getBukkitPlayer().getLocation().getBlockZ()));
            dir = dir % 360;

            if (dir < 0) {
                dir += 360;
            }

            dir = Math.round(dir / 90);
            switch ((int) dir) {

                case 0:
                    face = ComponentFace.NORTH;
                    break;
                case 1:
                    face = ComponentFace.EAST;
                    break;
                case 2:
                    face = ComponentFace.SOUTH;
                    break;
                default:
                    face = ComponentFace.WEST;
                    break;
            }
        }
        IComponent component = ComponentFactory.createComponentFromBlockFace(area, componentType, newComponentLocation, face, linkedComponent, null);
        if (component == null) {
            return null;
        }

        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "addComponentFromBlockFace => addComponent(component: {0} location: {1},{2},{3})", component.getComponentType().name(), component.getComponentLocation().getBlockX(), component.getComponentLocation().getBlockY(), component.getComponentLocation().getBlockZ());
        area.addComponent(component);
        return component;
    }

    public static IAreaComponent addIAreaComponent(LEPlayer player, Location firstLocation, Location secondLocation, ComponentType componentType) {
        Area area = Areas.getInstance().getAreaFromLocation(firstLocation);
        Area area2 = Areas.getInstance().getAreaFromLocation(secondLocation);
        if (area != area2) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NotSameAreaException());
        }

        World w = firstLocation.getWorld();

        //Defining min and max location:
        double[] xCoordinates = new double[2];
        xCoordinates[0] = firstLocation.getBlockX();
        xCoordinates[1] = secondLocation.getBlockX();
        Arrays.sort(xCoordinates);

        double[] yCoordinates = new double[2];
        yCoordinates[0] = firstLocation.getBlockY();
        yCoordinates[1] = secondLocation.getBlockY();
        Arrays.sort(yCoordinates);

        double[] zCoordinates = new double[2];
        zCoordinates[0] = firstLocation.getBlockZ();
        zCoordinates[1] = secondLocation.getBlockZ();
        Arrays.sort(zCoordinates);
        Location minLoc = new Location(w, xCoordinates[0], yCoordinates[0], zCoordinates[0]);
        Location maxLoc = new Location(w, xCoordinates[1], yCoordinates[1], zCoordinates[1]);

        for (double x = xCoordinates[0]; x <= xCoordinates[1]; x++) {
            for (double y = yCoordinates[0]; y <= yCoordinates[1]; y++) {
                for (double z = zCoordinates[0]; z <= zCoordinates[1]; z++) {
                    if (area.getComponentFromLocation(new Location(w, x, y, z)) != null) {

                        TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new ComponentAlreadyInsideAreaException());
                        return null;
                    }
                }
            }
        }

        IComponent component = ComponentFactory.createComponentFromBlockFace(area, componentType, minLoc, null, null, maxLoc);
        if (component == null) {
            return null;
        }

        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "addIAreaComponent => addIAreaComponent(component: {0} minLoc: {1},{2},{3} minLoc: {4},{5},{6})", component.getComponentType().name(), xCoordinates[0], yCoordinates[0], zCoordinates[0], xCoordinates[1], yCoordinates[1], zCoordinates[1]);
        area.addComponent(component);
        return (IAreaComponent) component;

    }

    /**
     * Create component from a player action
     *
     * @param componentType    the type of the component to create
     * @param clickedBlock     the clicked block
     * @param clickedBlockFace the face clicked
     * @param player           the player that started this action
     * @return the created component
     */
    public static IComponent addComponentFromBlockFace(ComponentType componentType, Block clickedBlock, BlockFace clickedBlockFace, LEPlayer player) {
        return addComponentFromBlockFace(componentType, clickedBlock, clickedBlockFace, player, null);
    }

    public static void addColoredMaterialFromBlockFace(Material material, Block clickedBlock, BlockFace clickedBlockFace, LEPlayer player, LasersColor color) {
        Location newMaterialLocation = clickedBlock.getLocation().add(clickedBlockFace.getModX(), clickedBlockFace.getModY(), clickedBlockFace.getModZ());
        Area area = Areas.getInstance().getAreaFromLocation(newMaterialLocation);

        if (area == null) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            return;
        }
        if (area.getComponentFromLocation(newMaterialLocation) != null) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        Block block = newMaterialLocation.getBlock();

        Item item = null;
        switch (material) {
            case WHITE_STAINED_GLASS:
                item = Item.getStainedGlass(color);
                break;
            case WHITE_STAINED_GLASS_PANE:
                item = Item.getStainedGlassPane(color);
                break;
            case WHITE_CONCRETE_POWDER:
                item = Item.getMirrorBlock(color);
                break;
            default:
                break;
        }
        Material newMaterial = item != null ? item.getMaterial() : null;
        if (newMaterial != null) {
            block.setType(newMaterial);
            block.getState().update();
        }
    }

    /**
     * Removes a component
     *
     * @param area      the area containing the component
     * @param player    the player that started this action
     * @param component the component to hide
     * @return true if the component was deleted
     */
    public static boolean removeComponent(Area area, LEPlayer player, IComponent component) {
        if (!player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return true;
        }
        if (!component.canBeDeleted()) {
            return true;
        }
        PlayerTryToDeleteComponentEvent compEvent = new PlayerTryToDeleteComponentEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return true;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return true;
        }
        area.removeComponent(component);
        LEPlayers.getInstance().onComponentDeleted(component);
        return false;
    }

    /**
     * change the color of a component
     *
     * @param area      the area containing the component
     * @param component the component
     * @param player    the player that started this action
     * @param color     the color to set
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void setColor(Area area, IColorableComponent component, LEPlayer player, LasersColor color) {
        PlayerTryToChangeColorEvent compEvent = new PlayerTryToChangeColorEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);

        if (compEvent.isCancelled()) {
            return;
        }

        if (!compEvent.getBypassPermissions()
                && !player.getInventoryManager().isInEditionMode()
                && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }

        if (color == null) {
            throw new IllegalArgumentException("You cannot set null color!");
        }

        component.setColor(color, player.getInventoryManager().isInEditionMode());
        new ColorChangeTask(component.getComponentLocation(), component.getColor(), area.getPlayersInsideArea());
        player.saveActions(component, ActionType.getFromColor(color));

        LEPlayers.getInstance().onComponentUpdated(component);
    }

    /**
     * change the color of a component
     *
     * @param area      the area containing the component
     * @param component the component
     * @param player    the player that started this action
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void changeColor(Area area, IColorableComponent component, LEPlayer player) {
        PlayerTryToChangeColorEvent compEvent = new PlayerTryToChangeColorEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.changeColor(player.getInventoryManager().isInEditionMode());
        new ColorChangeTask(component.getComponentLocation(), component.getColor(), area.getPlayersInsideArea());
        player.saveActions(component, ActionType.COLOR_LOOP);
        LEPlayers.getInstance().onComponentUpdated(component);
    }

    /**
     * Defines if this laser sender accepts any colors or only specific one
     *
     * @param area     the area containing the component
     * @param receiver the component
     * @param player   the player that started this action
     */
    public static void setAnyColorAccepted(Area area, LaserReceiver receiver, LEPlayer player, boolean anyColorAccepted) {
        PlayerTryToChangeAnyColorAcceptedEvent receiverEvent = new PlayerTryToChangeAnyColorAcceptedEvent(player.getBukkitPlayer(), receiver);
        Bukkit.getServer().getPluginManager().callEvent(receiverEvent);
        if (receiverEvent.isCancelled()) {
            return;
        }
        if (!receiverEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        receiver.setAnyColorAccepted(anyColorAccepted);
        LEPlayers.getInstance().onComponentUpdated(receiver);
    }

    /**
     * place a mirror on a mirror support
     *
     * @param component the mirror support the mirror will be placed on
     * @param player    the player that is placing the mirror
     * @param color     the color of the mirror
     */
    public static void placeMirror(IMirrorContainer component, LEPlayer player, LasersColor color) {
        Area area = component.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        PlayerTryToPlaceMirrorEvent compEvent = new PlayerTryToPlaceMirrorEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        boolean inEditionMode = player.getInventoryManager().isInEditionMode();
        boolean authorized = true;
        if (component instanceof MirrorSupport) {
            boolean isPlacingAllowedWithoutPermissions = ((MirrorSupport) component).getMirrorSupportMode().isMirrorRetrievable();
            authorized = isPlacingAllowedWithoutPermissions || inEditionMode;
        }
        if (!compEvent.getBypassPermissions() && !authorized) {
            return;
        }
        boolean mirrorPlaced = component.placeMirror(color, inEditionMode);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> LEPlayers.getInstance().onComponentUpdated(component), 31);
        if (mirrorPlaced) {
            player.getBukkitPlayer().getInventory().removeItem(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
            component.getArea().getStats().onActionDone(player.getBukkitPlayer());
        }
    }

    /**
     * gets a mirror off its mirror support
     *
     * @param component the mirror support the mirror will be retrieved from
     * @param player    the player that is retrieving the mirror
     */
    public static void removeMirror(IMirrorContainer component, LEPlayer player) {
        Area area = component.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        PlayerTryToRetrieveMirrorEvent compEvent = new PlayerTryToRetrieveMirrorEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        boolean inEditionMode = player.getInventoryManager().isInEditionMode();
        boolean authorized = true;
        if (component instanceof MirrorSupport) {
            boolean isPlacingAllowedWithoutPermissions = ((MirrorSupport) component).getMirrorSupportMode().isMirrorRetrievable();
            authorized = isPlacingAllowedWithoutPermissions || inEditionMode;
        }
        if (!compEvent.getBypassPermissions() && !authorized) {
            return;
        }
        boolean mirrorRetrieved = component.removeMirror(inEditionMode);
        if (mirrorRetrieved) {
            final LasersColor color = component.getColorCurrent();
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> LEPlayers.getInstance().onComponentUpdated(component), 31);
            LEPlayers.getInstance().onMirrorRetrieved(component, player);
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                PlayerInventoryManager inventoryManager = player.getInventoryManager();
                if (inventoryManager.isRotationShortcutBarOpened()) {
                    inventoryManager.getInventorySaveManager().addToSavedInventory(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
                } else {
                    player.getBukkitPlayer().getInventory().addItem(
                            ItemsFactory.getInstance().getItemStack(Item.getMirror(color))
                    );
                }
            }, 38);
            component.getArea().getStats().onActionDone(player.getBukkitPlayer());
        }
    }

    public static void changeMirrorSupportMode(LEPlayer player, MirrorSupport mirrorSupport) {
        changeMirrorSupportMode(player, mirrorSupport, mirrorSupport.getMirrorSupportMode().next());
    }

    public static void changeMirrorSupportMode(LEPlayer player, MirrorSupport mirrorSupport, MirrorSupportMode newMode) {
        PlayerTryToChangeMirrorSupportModeEvent compEvent = new PlayerTryToChangeMirrorSupportModeEvent(player.getBukkitPlayer(), mirrorSupport);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        mirrorSupport.changeMode(newMode);
        LEPlayers.getInstance().onComponentUpdated(mirrorSupport);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), newMode.getMessageCode());
    }

    public static void decreaseMin(LEPlayer player, IDetectionComponent component) {
        if (component.getMin() < 1) {
            return;
        }
        PlayerTryToChangeRangeEvent compEvent = new PlayerTryToChangeRangeEvent(player.getBukkitPlayer(), component, -1, 0);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setMin(component.getMin() - 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    public static void decreaseMax(LEPlayer player, IDetectionComponent component) {
        if (component.getMax() < 1) {
            return;
        }
        PlayerTryToChangeRangeEvent compEvent = new PlayerTryToChangeRangeEvent(player.getBukkitPlayer(), component, 0, -1);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (component.getMax() == component.getMin()) {
            component.setMin(component.getMin() - 1);
        }
        component.setMax(component.getMax() - 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    public static void increaseMin(LEPlayer player, IDetectionComponent component) {
        if (component.getMin() > 98) {
            return;
        }
        PlayerTryToChangeRangeEvent compEvent = new PlayerTryToChangeRangeEvent(player.getBukkitPlayer(), component, 1, 0);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (component.getMin() == component.getMax()) {
            component.setMax(component.getMax() + 1);
        }
        component.setMin(component.getMin() + 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    public static void increaseMax(LEPlayer player, IDetectionComponent component) {
        if (component.getMax() > 98) {
            return;
        }
        PlayerTryToChangeRangeEvent compEvent = new PlayerTryToChangeRangeEvent(player.getBukkitPlayer(), component, 0, 1);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setMax(component.getMax() + 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    private static void rangeChanged(LEPlayer player, int min, int max) {
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.range_changed", Integer.toString(min),
                Integer.toString(max));
    }

    /**
     * increase the number of mirrors inside the chest
     *
     * @param component the component
     * @param player    the player that started this action
     */
    public static void increaseMirrorChest(MirrorChest component, LEPlayer player) {
        if (component.getNbMirrors() > 98) {
            return;
        }
        PlayerTryToIncreaseMirrorChestEvent compEvent = new PlayerTryToIncreaseMirrorChestEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setNbMirrors(component.getNbMirrors() + 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.mirror_chest_increase", component.getNbMirrors());
    }

    /**
     * decrease the number of mirrors inside the chest
     *
     * @param component the component
     * @param player    the player that started this action
     */
    public static void decreaseMirrorChest(MirrorChest component, LEPlayer player) {
        if (component.getNbMirrors() < 2) {
            return;
        }
        PlayerTryToDecreaseMirrorChestEvent compEvent = new PlayerTryToDecreaseMirrorChestEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setNbMirrors(component.getNbMirrors() - 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.mirror_chest_decrease", Integer.toString(component.getNbMirrors()));
    }

    public static void changeMode(LEPlayer player, IDetectionComponent detectionComponent) {
        boolean isLaserSender = detectionComponent instanceof LaserSender;
        changeMode(player, detectionComponent, detectionComponent.getDetectionMode().next(isLaserSender, false));
    }

    public static void changeMode(LEPlayer player, IDetectionComponent detectionComponent, DetectionMode newMode) {
        PlayerTryToChangeComponentDetectionModeEvent compEvent = new PlayerTryToChangeComponentDetectionModeEvent(player.getBukkitPlayer(), detectionComponent);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        detectionComponent.changeMode(newMode);
        LEPlayers.getInstance().onComponentUpdated(detectionComponent);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), newMode.getMessageCode());
    }

    public static void toogleLoop(LEPlayer player, MusicBlock musicBlock) {
        setLoop(player, musicBlock, !musicBlock.isLoop());
    }

    public static void setLoop(LEPlayer player, MusicBlock musicBlock, boolean newLoopState) {
        PlayerTryToToggleMusicBlockLoopEvent compEvent = new PlayerTryToToggleMusicBlockLoopEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (newLoopState) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.music_block_loop_true");
        } else {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.music_block_loop_false");
        }
        musicBlock.setLoop(newLoopState);
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static void toogleStopOnExit(LEPlayer player, MusicBlock musicBlock) {
        setStopOnExit(player, musicBlock, !musicBlock.isStopOnExit());
    }

    public static void setStopOnExit(LEPlayer player, MusicBlock musicBlock, boolean newStopOnExitState) {
        PlayerTryToToggleMusicBlockStopOnExitEvent compEvent = new PlayerTryToToggleMusicBlockStopOnExitEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (newStopOnExitState) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.music_block_stop_on_exit_area_true");
        } else {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.music_block_stop_on_exit_area_false");
        }
        musicBlock.setStopOnExit(newStopOnExitState);
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static void prevSong(LEPlayer player, MusicBlock musicBlock) {
        PlayerTryToChangeMusicBlockSongEvent compEvent = new PlayerTryToChangeMusicBlockSongEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        musicBlock.prevSong(player.getBukkitPlayer());
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static void nextSong(LEPlayer player, MusicBlock musicBlock) {
        PlayerTryToChangeMusicBlockSongEvent compEvent = new PlayerTryToChangeMusicBlockSongEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        musicBlock.nextSong(player.getBukkitPlayer());
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static boolean canModify(LEPlayer player, IPlayerModifiableComponent playerModifiableComponent) {
        if (playerModifiableComponent == null) {
            return false;
        }
        if (playerModifiableComponent instanceof MirrorSupport && !((MirrorSupport) playerModifiableComponent).hasMirrorCurrent()) {
            return false;
        }
        Area area = playerModifiableComponent.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return false;
        }
        if (playerModifiableComponent instanceof IRotatableComponent) {
            return area.isRotationAllowedWithoutPermissions(playerModifiableComponent.getComponentType(), RotationType.UP) || area.isRotationAllowedWithoutPermissions(playerModifiableComponent.getComponentType(), RotationType.RIGHT);
        }
        if (playerModifiableComponent instanceof GravitationalSphere) {
            return area.isAttractionRepulsionSpheresResizingAllowed();
        }
        return false;
    }

    public static void rotate(LEPlayer player, IRotatableComponent component, RotationType rotationType) {
        if (component == null) {
            return;
        }
        MirrorSupport mirrorSupport = null;
        if (component instanceof MirrorSupport) {
            mirrorSupport = (MirrorSupport) component;
            if (!mirrorSupport.hasMirrorCurrent()) {
                return;
            }
        }
        Area area = component.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        boolean rotationAllowed = true;
        boolean save = true;
        boolean hasEditPermission = Permission.EDIT.hasPermission(player.getBukkitPlayer()) && player.getInventoryManager().isInEditionMode();
        boolean isRotationAllowedWithoutPermissions = area.isRotationAllowedWithoutPermissions(component.getComponentType(), rotationType);
        if (mirrorSupport != null) {
            isRotationAllowedWithoutPermissions = isRotationAllowedWithoutPermissions && mirrorSupport.getMirrorSupportMode().isMirrorRotatable();
        }

        if (!hasEditPermission) {
            save = false;
            if (!isRotationAllowedWithoutPermissions) {
                rotationAllowed = false;
            }
        }
        PlayerTryToRotateComponentEvent compEvent = new PlayerTryToRotateComponentEvent(player.getBukkitPlayer(), component, rotationType);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !rotationAllowed) {
            return;
        }
        component.rotate(rotationType, save, ActionCause.PLAYER);
        player.saveActions(component, ActionType.getFromRotation(rotationType));
        LEPlayers.getInstance().onComponentUpdated(component);
        area.getStats().onActionDone(player.getBukkitPlayer());
    }

    public static void changeMode(LEPlayer player, GravitationalSphere gravitationalSphere) {
        changeMode(player, gravitationalSphere, gravitationalSphere.getGravitationalSphereMode().next());
    }

    public static void changeMode(LEPlayer player, GravitationalSphere gravitationalSphere, GravitationalSphereMode newMode) {
        PlayerTryToChangeAttractionRepulsionSphereModeEvent compEvent = new PlayerTryToChangeAttractionRepulsionSphereModeEvent(player.getBukkitPlayer(), gravitationalSphere);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        gravitationalSphere.changeMode(newMode);
        LEPlayers.getInstance().onComponentUpdated(gravitationalSphere);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), newMode.getMessageCode());
    }

    public static void modifySize(LEPlayer p, GravitationalSphere gravitationalSphere, boolean increase) {
        Area area = gravitationalSphere.getArea();
        if (!area.containsLocation(p.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        boolean resizingAllowed = true;
        boolean save = true;
        boolean hasEditPermission = Permission.EDIT.hasPermission(p.getBukkitPlayer()) && p.getInventoryManager().isInEditionMode();
        boolean isResizingAllowedWithoutPermissions = gravitationalSphere.getArea().isAttractionRepulsionSpheresResizingAllowed();
        if (!hasEditPermission) {
            save = false;
            if (!isResizingAllowedWithoutPermissions) {
                resizingAllowed = false;
            }
        }
        int oldSize = gravitationalSphere.getCurrentGravityStrength();
        int newSize = oldSize;
        if (increase) {
            newSize++;
            if (newSize > 9) {
                gravitationalSphere.showSphere();
                return;
            }
        } else {
            newSize--;
            if (newSize < 1) {
                gravitationalSphere.showSphere();
                return;
            }
        }
        PlayerTryToChangeAttractionRepulsionSphereSizeEvent compEvent = new PlayerTryToChangeAttractionRepulsionSphereSizeEvent(p.getBukkitPlayer(), gravitationalSphere, oldSize, newSize);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !resizingAllowed) {
            return;
        }
        gravitationalSphere.setGravityStrength(newSize, save);
        p.saveActions(gravitationalSphere, increase ? ActionType.SPHERE_INCREASE : ActionType.SPHERE_DECREASE);
        LEPlayers.getInstance().onComponentUpdated(gravitationalSphere);
        TranslationUtils.sendTranslatedMessage(p.getBukkitPlayer(), "messages.attraction_repulsion_sphere_resized");
    }

    public static void removeKeyChests(LEPlayer player, Lock lock) {
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        Set<LockKeyChest> lockKeyChestsCopy = (Set<LockKeyChest>) lock.getKeyChests().clone();
        lockKeyChestsCopy.forEach(keyChest -> {
            keyChest.getArea().removeComponent(keyChest);
            LEPlayers.getInstance().onComponentDeleted(keyChest);
        });
        LEPlayers.getInstance().onComponentUpdated(lock);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.all_lock_s_key_chests_deleted");
    }

    public static void setKeyNumber(LEPlayer player, LockKeyChest lockKeyChest, int keyNb) {
        if (lockKeyChest.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        lockKeyChest.setKeyNumber(keyNb);
        LEPlayers.getInstance().onComponentUpdated(lockKeyChest);
        int nbChestsForNumber = lockKeyChest.getLock().countKeyChestsByNumber(lockKeyChest.getKeyNumber());
        if (nbChestsForNumber == 1) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.key_required");
        } else {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.key_optionnal", nbChestsForNumber);
        }
    }

    public static boolean elevatorGoUp(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return false;
        }
        Location cageMin = elevator.getCageMin();
        Location cageMax = elevator.getCageMax();
        Location pLoc = player.getBukkitPlayer().getLocation();
        double pX = pLoc.getBlockX();
        double pY = pLoc.getBlockY();
        double pZ = pLoc.getBlockZ();
        if (cageMin.getBlockX() <= pX && cageMin.getBlockY() <= pY && cageMin.getBlockZ() <= pZ && pX <= cageMax.getBlockX() && pY <= cageMax.getBlockY() && pZ <= cageMax.getBlockZ()) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "ComponentController.elevatorGoUp => true");
            PlayerTryToMoveElevatorEvent compEvent = new PlayerTryToMoveElevatorEvent(player.getBukkitPlayer(), elevator, ElevatorDirection.UP);
            Bukkit.getServer().getPluginManager().callEvent(compEvent);
            if (compEvent.isCancelled()) {
                return false;
            }
            boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
            return elevator.onPlayerJump(isInEditionMode);
        }
        return false;
    }

    public static boolean elevatorGoDown(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return false;
        }
        Location cageMin = elevator.getCageMin();
        Location cageMax = elevator.getCageMax();
        Location pLoc = player.getBukkitPlayer().getLocation();
        double pX = pLoc.getBlockX();
        double pY = pLoc.getBlockY();
        double pZ = pLoc.getBlockZ();
        if (cageMin.getBlockX() <= pX && cageMin.getBlockY() <= pY && cageMin.getBlockZ() <= pZ && pX <= cageMax.getBlockX() && pY <= cageMax.getBlockY() && pZ <= cageMax.getBlockZ()) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "ComponentController.elevatorGoDown => true");
            PlayerTryToMoveElevatorEvent compEvent = new PlayerTryToMoveElevatorEvent(player.getBukkitPlayer(), elevator, ElevatorDirection.DOWN);
            Bukkit.getServer().getPluginManager().callEvent(compEvent);
            if (compEvent.isCancelled()) {
                return false;
            }
            boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
            return elevator.onPlayerSneak(isInEditionMode);
        }
        return false;
    }

    public static boolean elevatorCallButton(LEPlayer player, ElevatorCallButton elevatorCallButton) {
        if (elevatorCallButton.isRemoved()) {
            return false;
        }
        PlayerTryToCallElevatorEvent compEvent = new PlayerTryToCallElevatorEvent(player.getBukkitPlayer(), elevatorCallButton);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return false;
        }
        boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
        return elevatorCallButton.onCallButton(isInEditionMode);
    }

    public static void elevatorCreateFloor(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        elevator.addFloorAtCurrentPosition();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.elevator_floor_created");
    }

    public static void goToFloor(LEPlayer player, Elevator elevator, int index) {
        boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
        elevator.moveToFloor(index, isInEditionMode);
    }

    public static void elevatorGoAsHighAsPossible(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        elevator.goAsHighAsPossible();
    }

    public static void elevatorDeleteFloors(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        elevator.clearFloors();
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.elevator_floors_deleted");
    }

    public static void elevatorDeleteCallButtons(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        Area area = elevator.getArea();
        ArrayList<ElevatorCallButton> elevatorCallButtons = area.getComponents().stream()
                .filter(component -> component instanceof ElevatorCallButton)
                .map(component -> (ElevatorCallButton) component)
                .filter(callButton -> callButton.getElevator() == elevator)
                .collect(Collectors.toCollection(ArrayList::new));
        elevatorCallButtons.forEach(area::removeComponent);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.elevator_call_buttons_deleted");

    }

    public static void elevatorTeleportInside(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        player.getBukkitPlayer().teleport(elevator.getCageCenter());
    }

    public static void saveActionsDone(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (!player.isSavingActions()) {
            return;
        }
        ArrayList<ScheduledAction> scheduledActions = player.stopSavingActions();
        if (scheduledActions != null) {
            component.getScheduledActions().addAll(scheduledActions);
            component.updateActions();
            LEPlayers.getInstance().onComponentUpdated(component);
            int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
            if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
                TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
            }
        }
    }

    public static void clearScheduledActions(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.getScheduledActions().clear();
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
    }

    public static void addNewScheduledActionBefore(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.getScheduledActions().add(0, new ScheduledAction(component));
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
        }
    }

    public static void addNewScheduledActionAfter(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<ScheduledAction> scheduledActions = component.getScheduledActions();
        scheduledActions.add(scheduledActions.size(), new ScheduledAction(component));
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
        }
    }

    public static void deleteScheduledAction(LEPlayer player, IComponent component, ScheduledAction scheduledAction) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<ScheduledAction> scheduledActions = component.getScheduledActions();
        scheduledActions.remove(scheduledAction);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
        }
    }

    public static void moveScheduledActionBefore(LEPlayer player, IComponent component, ScheduledAction scheduledAction) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<ScheduledAction> scheduledActions = component.getScheduledActions();
        int index = scheduledActions.indexOf(scheduledAction);
        if (index == 0) {
            return;
        }
        Collections.swap(scheduledActions, index, index - 1);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
        }
    }

    public static void moveScheduledActionAfter(LEPlayer player, IComponent component, ScheduledAction scheduledAction) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<ScheduledAction> scheduledActions = component.getScheduledActions();
        int index = scheduledActions.indexOf(scheduledAction);
        if (index == scheduledActions.size() - 1) {
            return;
        }
        Collections.swap(scheduledActions, index, index + 1);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
        }
    }

    public static void changeScheduledActionType(LEPlayer player, IComponent component, ScheduledAction action, ActionType actionType) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (!component.getScheduledActions().contains(action)) {
            return;
        }
        action.setType(actionType);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
        }
    }

    public static void modifyScheduledActionDelay(LEPlayer player, IComponent component, ScheduledAction action, int delayDiff) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (!component.getScheduledActions().contains(action)) {
            return;
        }
        int newDelay = action.getDelay() + delayDiff;
        if (newDelay < 1) {
            newDelay = 1;
        } else if (newDelay > 99) {
            newDelay = 99;
        }
        action.setDelay(newDelay);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(ScheduledAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "scheduled_actions.messages.delay_required");
        }
    }

    public static void changeLightLevel(LEPlayer player, ILightComponent component, int diff) {
        int newValue = component.getLightLevel() + diff;

        if (newValue > 15) newValue = 15;
        else if (newValue < 0) newValue = 0;

        PlayerTryToChangeLightLevelEvent compEvent = new PlayerTryToChangeLightLevelEvent(player.getBukkitPlayer(), component, newValue);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);

        if (compEvent.isCancelled()) {
            return;
        }

        if (!compEvent.getBypassPermissions()
                && !player.getInventoryManager().isInEditionMode()
                && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }

        component.setLightLevel(newValue);
        component.getArea().updateLight();
        LEPlayers.getInstance().onComponentUpdated(component);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.light_level_changed", newValue);
    }

    public static void setMaterial(LEPlayer player, BurnableBlock burnableBlock, Material material) {
        PlayerTryToChangeBurnableBlockMaterialEvent compEvent = new PlayerTryToChangeBurnableBlockMaterialEvent(player.getBukkitPlayer(), burnableBlock, material);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        burnableBlock.setMaterial(material);
        LEPlayers.getInstance().onComponentUpdated(burnableBlock);
        TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.burnable_block_material_changed", material.toString());
    }

    public static void toggleLetPassLaser(LEPlayer player, LaserReceiver laserReceiver) {
        setLetPassLaser(player, laserReceiver, !laserReceiver.isLetPassLaserThrough());
    }

    public static void setLetPassLaser(LEPlayer player, LaserReceiver laserReceiver, boolean newLetPassLaser) {
        PlayerTryToToggleLetPassLaserEvent compEvent = new PlayerTryToToggleLetPassLaserEvent(player.getBukkitPlayer(), laserReceiver);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (newLetPassLaser) {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.laser_receiver_let_pass_laser_true");
        } else {
            TranslationUtils.sendTranslatedMessage(player.getBukkitPlayer(), "messages.laser_receiver_let_pass_laser_false");
        }
        laserReceiver.setLetPassLaserThrough(newLetPassLaser);
        LEPlayers.getInstance().onComponentUpdated(laserReceiver);
    }
}
