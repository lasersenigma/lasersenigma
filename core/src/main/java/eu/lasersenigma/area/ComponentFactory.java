package eu.lasersenigma.area;

import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.component.ComponentFace;
import eu.lasersenigma.component.DetectionMode;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.Rotation;
import eu.lasersenigma.component.burnableblock.BurnableBlock;
import eu.lasersenigma.component.concentrator.Concentrator;
import eu.lasersenigma.component.conditionnalwinnerblock.AppearingWinnerBlock;
import eu.lasersenigma.component.conditionnalwinnerblock.DisappearingWinnerBlock;
import eu.lasersenigma.component.elevator.Elevator;
import eu.lasersenigma.component.elevator.ElevatorCallButton;
import eu.lasersenigma.component.filteringsphere.FilteringSphere;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphereMode;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.lasersender.LaserSender;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.mirrorsupport.MirrorSupportMode;
import eu.lasersenigma.component.musicblock.MusicBlock;
import eu.lasersenigma.component.prism.Prism;
import eu.lasersenigma.component.redstonesensor.RedstoneSensor;
import eu.lasersenigma.component.reflectingsphere.ReflectingSphere;
import eu.lasersenigma.component.winnerblock.WinnerBlock;
import eu.lasersenigma.songs.SongsListManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.util.Vector;

import java.util.LinkedList;

public class ComponentFactory {

    /**
     * creates a component from the data saved in the database some parameters
     * will be useless for some components
     *
     * @param componentId            the id of the component in the database
     * @param area                   the area containing this component
     * @param type                   the componentType
     * @param location               the location of the component
     * @param face                   the component facing
     * @param rotation               the rotation of the component
     * @param min                    the minimum number of activated laser receivers needed for
     *                               this block to be activated
     * @param max                    the maximum number of activated laser receivers needed for
     *                               this block to be activated
     * @param songFileName           the name of the song
     * @param loop                   if loop mode is activated
     * @param stopOnExit             if stop on exit mode is activated
     * @param color                  the color of the component
     * @param mode                   the detection mode as string
     * @param linkedComponent        the component linked to this one (for example the
     *                               lock if this component is meant to be a chest)
     * @param floorsCageMin          the list containing each floor's locations (the
     *                               minimum location of the cage)
     * @param cageVectorFromMinToMax the vector determining the position of the
     *                               cage corner with maximal coordinates from the cageCurrentMin location.
     * @return the created component
     */
    public static IComponent createComponentFromDatabase(
            int componentId,
            Area area,
            ComponentType type,
            Location location,
            String face,
            Rotation rotation,
            int min, int max,
            LasersColor color,
            String songFileName,
            boolean loop,
            boolean stopOnExit,
            String mode,
            IComponent linkedComponent,
            LinkedList<Location> floorsCageMin,
            Vector cageVectorFromMinToMax,
            int lightLevel,
            Material material,
            boolean letLasersPass) {
        IComponent component = null;
        switch (type) {
            case LASER_RECEIVER:
                component = new LaserReceiver(area, componentId, location, ComponentFace.valueOf(face), rotation, color, lightLevel, letLasersPass);
                break;
            case LASER_SENDER:
                component = new LaserSender(area, componentId, location, ComponentFace.valueOf(face), rotation, color, DetectionMode.valueOf(mode), min, max, lightLevel);
                break;
            case MIRROR_CHEST:
                component = new MirrorChest(area, componentId, location, min, color, ComponentFace.valueOf(face));
                break;
            case MIRROR_SUPPORT:
                component = new MirrorSupport(area, componentId, location, ComponentFace.valueOf(face), color, rotation, MirrorSupportMode.from(mode));
                break;
            case REDSTONE_WINNER_BLOCK:
                component = new WinnerBlock(area, componentId, location, min, max, DetectionMode.valueOf(mode));
                break;
            case DISAPPEARING_WINNER_BLOCK:
                component = new DisappearingWinnerBlock(area, componentId, location, min, max, DetectionMode.valueOf(mode));
                break;
            case APPEARING_WINNER_BLOCK:
                component = new AppearingWinnerBlock(area, componentId, location, min, max, DetectionMode.valueOf(mode));
                break;
            case BURNABLE_BLOCK:
                component = new BurnableBlock(area, componentId, location, color, material);
                break;
            case PRISM:
                component = new Prism(area, componentId, location, ComponentFace.valueOf(face), rotation);
                break;
            case CONCENTRATOR:
                component = new Concentrator(area, componentId, location, ComponentFace.valueOf(face), rotation);
                break;
            case REFLECTING_SPHERE:
                component = new ReflectingSphere(area, componentId, location, ComponentFace.valueOf(face), color);
                break;
            case FILTERING_SPHERE:
                component = new FilteringSphere(area, componentId, location, ComponentFace.valueOf(face), color);
                break;
            case MUSIC_BLOCK:
                if (SongsListManager.getInstance().isActivated()) {
                    component = new MusicBlock(area, componentId, location, ComponentFace.valueOf(face), rotation, min, max, songFileName, loop, stopOnExit, DetectionMode.valueOf(mode));
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                component = new GravitationalSphere(area, componentId, location, ComponentFace.valueOf(face), rotation, GravitationalSphereMode.valueOf(mode), min);
                break;
            case REDSTONE_SENSOR:
                component = new RedstoneSensor(area, componentId, location, ComponentFace.valueOf(face), rotation);
                break;
            case LOCK:
                component = new Lock(area, componentId, location, ComponentFace.valueOf(face));
                break;
            case KEY_CHEST:
                if (linkedComponent instanceof Lock lock) {
                    component = new LockKeyChest(area, lock, componentId, location, min, ComponentFace.valueOf(face));
                } else {
                    throw new UnsupportedOperationException("Lock is not yet initialized. KeyChest can't be created.");
                }
                break;
            case ELEVATOR:
                component = new Elevator(area, componentId, floorsCageMin, location, cageVectorFromMinToMax, min, max, DetectionMode.valueOf(mode));
                break;
            case CALL_BUTTON:
                if (linkedComponent == null) {
                    throw new UnsupportedOperationException("Elevator is not yet initialized. CallButton can't be created");
                } else {
                    component = new ElevatorCallButton(area, componentId, location, ComponentFace.valueOf(face), rotation, (Elevator) linkedComponent);
                }
                break;
            default:
                throw new UnsupportedOperationException("this ComponentType is not supported");
        }
        return component;
    }

    /**
     * Create a component from a clicked blockface
     *
     * @param area            the area containing this component
     * @param type            the componentType
     * @param location        the location of the component
     * @param face            the component facing
     * @param linkedComponent the component linked to this one (for example the
     *                        lock if this component is meant to be a chest)
     * @param secondLocation  a second location for IAreaComponent
     * @return the created component
     */
    @SuppressWarnings("null")
    public static IComponent createComponentFromBlockFace(Area area, ComponentType type, Location location, ComponentFace face, IComponent linkedComponent, Location secondLocation) {
        IComponent component = null;
        switch (type) {
            case LASER_RECEIVER:
                component = new LaserReceiver(area, location, face);
                break;
            case LASER_SENDER:
                component = new LaserSender(area, location, face);
                break;
            case PRISM:
                component = new Prism(area, location, face);
                break;
            case CONCENTRATOR:
                component = new Concentrator(area, location, face);
                break;
            case REFLECTING_SPHERE:
                component = new ReflectingSphere(area, location, face);
                break;
            case FILTERING_SPHERE:
                component = new FilteringSphere(area, location, face);
                break;
            case MIRROR_SUPPORT:
                component = new MirrorSupport(area, location, face);
                break;
            case MUSIC_BLOCK:
                if (SongsListManager.getInstance().isActivated()) {
                    component = new MusicBlock(area, location, face);
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                component = new GravitationalSphere(area, location, face);
                break;
            case REDSTONE_SENSOR:
                component = new RedstoneSensor(area, location, face);
                break;
            case MIRROR_CHEST:
                component = new MirrorChest(area, location, 1, face);
                break;
            case REDSTONE_WINNER_BLOCK:
                component = new WinnerBlock(area, location);
                break;
            case DISAPPEARING_WINNER_BLOCK:
                component = new DisappearingWinnerBlock(area, location);
                break;
            case APPEARING_WINNER_BLOCK:
                component = new AppearingWinnerBlock(area, location);
                break;
            case BURNABLE_BLOCK:
                component = new BurnableBlock(area, location);
                break;
            case LOCK:
                component = new Lock(area, location, face);
                break;
            case KEY_CHEST:
                component = new LockKeyChest(area, (Lock) linkedComponent, location, face);
                break;
            case ELEVATOR:
                component = new Elevator(area, location, secondLocation);
                break;
            case CALL_BUTTON:
                component = new ElevatorCallButton(area, location, face, (Elevator) linkedComponent);
                break;
            default:
                throw new UnsupportedOperationException("this ComponentType is not supported");
        }
        return component;
    }
}
