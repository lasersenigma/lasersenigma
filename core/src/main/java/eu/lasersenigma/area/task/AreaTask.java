package eu.lasersenigma.area.task;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Areas;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * A task updating areas
 */
public class AreaTask extends BukkitRunnable {

    /**
     * Constructor starting the task
     */
    public AreaTask() {
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 10, 10);
    }

    /**
     * The method runned repeatedly
     */
    @Override
    public void run() {
        Areas.getInstance().updateAreas();
    }

}
