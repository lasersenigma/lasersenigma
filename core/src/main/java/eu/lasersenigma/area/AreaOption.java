package eu.lasersenigma.area;

public enum AreaOption {

    //

    ENABLE_CHECKPOINTS(true),
    USE_VICTORY_AREA_AS_CHECKPOINT(false),
    TELEPORT_ON_LAST_CHECKPOINT_ON_JOIN(false),
    TELEPORT_ON_LAST_CHECKPOINT_ON_DEATH(false),
    TELEPORT_ON_LAST_CHECKPOINT_ON_WORLD_CHANGE(false),
    CLEAR_LAST_CHECKPOINT_ON_QUIT(false),
    ;

    private boolean isEnabled;

    AreaOption(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
