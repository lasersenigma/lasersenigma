package eu.lasersenigma.area.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AShortcutBarInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.inventory.MainShortcutBarInventory;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.CrossableMaterials;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Benjamin
 */
public class AreaVictoryAreaShortcutBarInventory extends AShortcutBarInventory {

    private final Area area;

    public AreaVictoryAreaShortcutBarInventory(LEPlayer player, Area area) {
        super(player);
        this.area = area;
    }

    @Override
    protected ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.VICTORY_AREA_SHORTCUTBAR_SELECT);
        shortcutBar = addEmpty(shortcutBar, 7);
        shortcutBar.add(Item.VICTORY_AREA_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case VICTORY_AREA_SHORTCUTBAR_CLOSE:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new MainShortcutBarInventory(player));
                }, 1);
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new AreaConfigMenuInventory(player, area));
                }, 3);
                break;
            case VICTORY_AREA_SHORTCUTBAR_SELECT:
                Location selectedLocation = player.getBukkitPlayer().getTargetBlock(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20).getLocation();
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
                    return;
                }
                Area area2 = Areas.getInstance().getAreaFromLocation(selectedLocation);
                if (area2 == null || area != area2) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                AreaController.createVictoryArea(player, area, player.getBukkitPlayer().getTargetBlock(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20).getLocation());
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.VICTORY_AREA_SHORTCUTBAR_SELECT, Item.VICTORY_AREA_SHORTCUTBAR_CLOSE)).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.AREA_VICTORY_AREAS;
    }

}
