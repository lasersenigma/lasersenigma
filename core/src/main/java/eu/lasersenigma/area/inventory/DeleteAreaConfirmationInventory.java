package eu.lasersenigma.area.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class DeleteAreaConfirmationInventory extends AOpenableInventory {

    private final Area area;

    public DeleteAreaConfirmationInventory(LEPlayer player, Area area) {
        super(player, "messages.area_delete_confirm_title");
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.HELP_AREA_CONFIRM)));
        inv.add(new ArrayList<>(Arrays.asList(Item.CONFIRM_DELETE_AREA, Item.EMPTY, Item.CANCEL_DELETE_AREA)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEDeleteAreaConfirmationInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != area) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().closeOpenedInventory();
            }, 1);
            return;
        }
        switch (item) {
            case CONFIRM_DELETE_AREA:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                AreaController.deleteArea(player, player.getBukkitPlayer().getLocation());
                break;
            case CANCEL_DELETE_AREA:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.HELP_AREA_CONFIRM,
                Item.CONFIRM_DELETE_AREA,
                Item.CANCEL_DELETE_AREA
        )).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.DELETE_AREA_CONFIRM_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
