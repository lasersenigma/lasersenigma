package eu.lasersenigma.player;

import org.bukkit.Material;

public class CrossableMaterialWrapper {

    private final Material material;
    private final boolean transparent;

    public CrossableMaterialWrapper(Material material) {
        this(material, false);
    }
    public CrossableMaterialWrapper(Material material, boolean transparent) {
        this.material = material;
        this.transparent = transparent;
    }

    public Material getMaterial() {
        return material;
    }

    public boolean isTransparent() {
        return transparent;
    }
}
