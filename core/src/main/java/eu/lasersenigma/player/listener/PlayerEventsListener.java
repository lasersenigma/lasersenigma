package eu.lasersenigma.player.listener;

import com.xxmicloxx.NoteBlockAPI.event.SongStoppedEvent;
import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.player.PlayerInventoryManager;
import eu.lasersenigma.songs.PlayersListSongManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.potion.PotionEffectType;

import java.util.UUID;
import java.util.logging.Level;

public class PlayerEventsListener implements Listener {

    private static final double JUMP_VELOCITY = 0.4;
    private final PlayersListSongManager playersListSongManager;

    public PlayerEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        playersListSongManager = PlayersListSongManager.getInstance();
    }

    /**
     * executed on a ChunkUnloadEvent
     *
     * @param event a ChunkUnloadEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerJoinEvent");
        LEPlayers.getInstance().findLEPlayer(event.getPlayer()).onPlayerJoin(event);
    }

    /**
     * executed on a ChunkUnloadEvent
     *
     * @param event a ChunkUnloadEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerQuitEvent");
        LEPlayers.getInstance().findLEPlayer(event.getPlayer()).onPlayerQuit(event);
    }

    // TODO: 
    @EventHandler(ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerDeathEvent");
        Player player = (Player) event.getEntity();
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(player);
        PlayerInventoryManager inventoryManager = lePlayer.getInventoryManager();

        // Keep inventory in edition mode
        if (inventoryManager.isInEditionMode()) {
            // Doesn't prevent items from dropping
            event.setKeepInventory(true);
        }

        event.getDrops().clear();
    }

    /**
     * executed on a PlayerSwapHandItemsEvent
     *
     * @param event a PlayerSwapHandItemsEvent
     */
    @EventHandler(ignoreCancelled = false, priority = EventPriority.HIGHEST)
    public void onPlayerSwapHandItemsEvent(PlayerSwapHandItemsEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerPickupItemEvent");
        PlayerInventoryManager pim = LEPlayers.getInstance().findLEPlayer(event.getPlayer()).getInventoryManager();
        if (pim.isRotationShortcutBarOpened()) {
            pim.closeRotationShortcutBarInventory(false);
            event.setCancelled(true);
        }
    }

    /**
     * executed on a PlayerToggleSneakEvent
     *
     * @param event a PlayerToggleSneakEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onPlayerToggleSneakEvent(PlayerToggleSneakEvent event) {
        if (event.getPlayer().isSneaking()) return;
        LEPlayers.getInstance().findLEPlayer(event.getPlayer()).onPlayerSneak();
    }

    /**
     * executed on a PlayerPickupItemEventListener
     *
     * @param event a PlayerPickupItemEventListener
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerPickupItemEvent(PlayerPickupItemEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerPickupItemEvent");
        PlayerInventoryManager pim = LEPlayers.getInstance().findLEPlayer(event.getPlayer()).getInventoryManager();
        if (pim.isRotationShortcutBarOpened()) {
            pim.closeRotationShortcutBarInventory(false);
        }
    }

    /**
     * executed on a PlayerMoveEvent
     *
     * @param event a PlayerMoveEvent
     */
    @EventHandler
    public void onPlayerJump(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (player.getVelocity().getY() > 0 && !player.isFlying()) {
            double jumpVelocity = JUMP_VELOCITY;
            if (player.hasPotionEffect(PotionEffectType.JUMP)) {
                jumpVelocity += (double) ((float) (player.getPotionEffect(PotionEffectType.JUMP).getAmplifier() + 1) * 0.1F);
            }
            if (event.getPlayer().getLocation().getBlock().getType() != Material.LADDER) {
                if (!player.isOnGround() && player.getVelocity().getY() > jumpVelocity) {
                    LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerJump");
                    LEPlayers.getInstance().findLEPlayer(player).onPlayerJump();
                }
            }
        }
    }

    /**
     * executed on each PlayerInteractEvent
     *
     * @param event a PlayerInteractEvent
     */
    @EventHandler(ignoreCancelled = false)
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerInteractEvent");
        Player player = event.getPlayer();
        Action action = event.getAction();
        boolean shouldBeCancelled = false;
        if (Action.LEFT_CLICK_AIR == action || Action.LEFT_CLICK_BLOCK == action) {
            shouldBeCancelled = LEPlayers.getInstance().findLEPlayer(player).onLeftClick();
        } else if (Action.RIGHT_CLICK_AIR == action || Action.RIGHT_CLICK_BLOCK == action) {
            shouldBeCancelled = LEPlayers.getInstance().findLEPlayer(player).onRightClick();
        }
        if (event.useInteractedBlock() != Result.DENY) {
            event.setCancelled(shouldBeCancelled);
        }
        if (event.getAction() == Action.PHYSICAL && event.getClickedBlock() != null) {
            Location loc = event.getClickedBlock().getLocation();
            Area area = Areas.getInstance().getAreaFromLocation(loc);
            if (area != null) {
                area.updateRedstone();
            }
        }
    }

    /**
     * executed on each PlayerInteractEvent
     *
     * @param e a PlayerInteractEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onPlayerDropItemEvent(PlayerDropItemEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerDropItemEvent");
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
        lePlayer.onPlayerDropItem(e);
    }

    /**
     * executed on each PlayerInteractEvent
     *
     * @param e a PlayerInteractEvent
     */
    @EventHandler(ignoreCancelled = false)
    public void onPlayerAnimationEvent(PlayerAnimationEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onPlayerAnimationEvent");
        if (!e.getAnimationType().equals(PlayerAnimationType.ARM_SWING)) {
            return;
        }
        Player p = e.getPlayer();
        if (!p.getGameMode().equals(GameMode.ADVENTURE)) {
            return;
        }
        LEPlayers.getInstance().findLEPlayer(p).onLeftClick();
    }

    /**
     * executed on a SongEndEvent
     *
     * @param e a SongEndEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onSongStopEvent(SongStoppedEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onSongEndEvent");
        UUID playerUUID = e.getSongPlayer().getPlayerUUIDs().stream()
                .findFirst()
                .orElse(null);

        if (playerUUID == null) return;

        Player p = Bukkit.getPlayer(playerUUID);
        playersListSongManager.onSongEnd(p);
    }

}
