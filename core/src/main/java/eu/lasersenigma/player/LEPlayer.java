package eu.lasersenigma.player;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.checkpoint.GetBackToCheckpointOnChangeWorld;
import eu.lasersenigma.checkpoint.GetBackToCheckpointOnDeath;
import eu.lasersenigma.checkpoint.GetBackToCheckpointOnJoin;
import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IMirrorContainer;
import eu.lasersenigma.component.IPlayerModifiableComponent;
import eu.lasersenigma.component.elevator.Elevator;
import eu.lasersenigma.component.elevator.ElevatorCallButton;
import eu.lasersenigma.component.laserreceiver.LaserReceiver;
import eu.lasersenigma.component.lock.Lock;
import eu.lasersenigma.component.lock.LockKeyChest;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.scheduledactions.ActionType;
import eu.lasersenigma.component.scheduledactions.PlayerActionsSaver;
import eu.lasersenigma.component.scheduledactions.ScheduledAction;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.stats.AreaStats;
import eu.lasersenigma.stats.PlayerStats;
import eu.lasersenigma.stats.exception.PlayerStatsNotFoundException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.Vector;

import java.time.Duration;
import java.util.*;
import java.util.logging.Level;

/**
 * Lasers-Enigma representation of a player
 */
public class LEPlayer {

    @SuppressWarnings("SetReplaceableByEnumSet")
    private final UUID playerUUID;

    private final PlayerInventoryManager inventoryManager;
    private final HashMap<String, Boolean> perWorldIsCheckpointReachedAVictoryCheckpoint;
    private final HashMap<String, Location> perWorldLastCheckpoint;
    private final HashMap<String, Location> perWorldLastVictoryCheckpoint;
    // private final HashSet<KeyChest> keysChestObtained;
    private Integer antiSpamTaskId;
    private Location lastCheckpointReached;
    private Location lastVictoryCheckpointReached;
    private boolean isLastCheckpointReachedAVictoryCheckpoint;

    private PlayerActionsSaver actionSaver;

    public LEPlayer(UUID playerUUID) {
        this.playerUUID = playerUUID;
        inventoryManager = new PlayerInventoryManager(this);
        isLastCheckpointReachedAVictoryCheckpoint = false;
        this.perWorldIsCheckpointReachedAVictoryCheckpoint = new HashMap<>();
        this.perWorldLastCheckpoint = new HashMap<>();
        this.perWorldLastVictoryCheckpoint = new HashMap<>();
        // this.keysChestObtained = new HashSet<>();
        this.actionSaver = null;
    }

    private static boolean isSafeLocation(Location location) {
        Block feet = location.getBlock();
        if (!feet.getType().isTransparent() && !feet.getLocation().add(0, 1, 0).getBlock().getType().isTransparent()) {
            return false; // not transparent (will suffocate)
        }
        Block head = feet.getRelative(BlockFace.UP);
        if (!head.getType().isTransparent()) {
            return false; // not transparent (will suffocate)
        }
        Block ground = feet.getRelative(BlockFace.DOWN);
        return ground.getType().isSolid();
    }

    public boolean isInArea() {
        final Player player = getBukkitPlayer();
        if (player == null) {
            return false;
        }
        if (!player.isOnline()) {
            return false;
        }
        if (player.getGameMode().equals(org.bukkit.GameMode.SPECTATOR)) {
            return false;
        }
        if (player.isDead()) {
            return false;
        }
        if (Areas.getInstance().getAreaFromLocation(getBukkitPlayer().getLocation()) == null) {
            return false;
        }
        return true;
    }

    public void teleportToNearestSafeLocation(Location location) {
        boolean safeLocationFound = isSafeLocation(location);
        Location tpLoc = location;
        for (int x = 1; x <= 5 && -5 <= x && !safeLocationFound; ) {
            for (int z = 1; z <= 5 && -5 <= z && !safeLocationFound; ) {
                for (int y = 1; y <= 5 && -5 <= y && !safeLocationFound; ) {
                    tpLoc = location.clone().add(x, y, z);
                    if (isSafeLocation(tpLoc)) {
                        safeLocationFound = true;
                    }
                    if (y == 5) {
                        y = -1;
                    } else if (y > 0) {
                        y++;
                    } else if (y < 0) {
                        y--;
                    }
                }
                if (z == 5) {
                    z = -1;
                } else if (z > 0) {
                    z++;
                } else if (z < 0) {
                    z--;
                }
            }
            if (x == 5) {
                x = -1;
            } else if (x > 0) {
                x++;
            } else if (x < 0) {
                x--;
            }
        }
        getBukkitPlayer().teleport(tpLoc);
    }

    public void addKeyFromDB(LockKeyChest lockKeyChest) {
        // this.keysChestObtained.add(keyChest);
    }

    public void setWorldCheckpointsFromDB(Location lastCheckpointReached, Location lastVictoryCheckpointReached, boolean isLastCheckpointReachedAVictoryCheckpoint) {
        if (lastCheckpointReached == null && lastVictoryCheckpointReached == null) {
            throw new IllegalArgumentException("lastCheckpointReached & lastVictoryCheckpointReached are both null.");
        }
        String worldStr;
        if (lastCheckpointReached == null) {
            worldStr = Objects.requireNonNull(lastVictoryCheckpointReached.getWorld()).getName();
        } else {
            worldStr = Objects.requireNonNull(lastCheckpointReached.getWorld()).getName();
        }
        perWorldIsCheckpointReachedAVictoryCheckpoint.put(worldStr, isLastCheckpointReachedAVictoryCheckpoint);
        perWorldLastCheckpoint.put(worldStr, lastCheckpointReached);
        perWorldLastVictoryCheckpoint.put(worldStr, lastVictoryCheckpointReached);
    }

    public void setCheckpointsFromDB(World world, World vworld, boolean isLastCheckpointReachedAVictoryCheckpoint) {
        this.isLastCheckpointReachedAVictoryCheckpoint = isLastCheckpointReachedAVictoryCheckpoint;
        if (world == null) {
            this.lastCheckpointReached = null;
        } else {
            this.lastCheckpointReached = this.perWorldLastCheckpoint.get(world.getName());
        }
        if (vworld == null) {
            this.lastVictoryCheckpointReached = null;
        } else {
            this.lastVictoryCheckpointReached = this.perWorldLastVictoryCheckpoint.get(vworld.getName());
        }
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(playerUUID);
    }

    public UUID getUniqueId() {
        return playerUUID;
    }

    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(playerUUID);
    }

    public PlayerInventoryManager getInventoryManager() {
        return this.inventoryManager;
    }

    public void onInventoryDrag(InventoryDragEvent e) {
        getInventoryManager().onInventoryDrag(e);
    }

    public void onPlayerQuit(PlayerQuitEvent e) {
        getInventoryManager().onQuit();
    }

    public void onPlayerDropItem(PlayerDropItemEvent e) {
        getInventoryManager().onPlayerDropItem(e.getItemDrop().getItemStack(), e);
    }

    public void onInventoryClick(InventoryClickEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEPlayer.onInventoryClick");
        switch (e.getClick()) {
            case WINDOW_BORDER_LEFT:
            case WINDOW_BORDER_RIGHT:
            case UNKNOWN:
            case NUMBER_KEY:
                break;
            case CONTROL_DROP:
            case DROP:
                getInventoryManager().onPlayerDropItem(e.getCurrentItem(), e);
                break;
            case CREATIVE:
                if (this.getInventoryManager().isInEditionMode()) {
                    boolean isCursorItemPluginRelated = (e.getCursor() != null &&
                                                         ItemsFactory.getInstance().getSimilarItem(e.getCursor()) !=
                                                         null
                    );
                    boolean isCurrentItemPluginRelated = (e.getCurrentItem() != null &&
                                                          ItemsFactory.getInstance().getSimilarItem(e.getCurrentItem()) !=
                                                          null
                    );
                    if (isCursorItemPluginRelated || isCurrentItemPluginRelated) {
                        e.setCancelled(true);
                        if (isCursorItemPluginRelated) {
                            getInventoryManager().onInventoryClick(e.getCursor());
                        } else {
                            getInventoryManager().onInventoryClick(e.getCurrentItem());
                        }
                    }
                }
                break;
            case MIDDLE:
            default:
                if (this.getInventoryManager().isInEditionMode()) {
                    getInventoryManager().onInventoryClick(e.getCurrentItem());
                    e.setCancelled(true);
                }
                if (this.getInventoryManager().isRotationShortcutBarOpened()) {
                    e.setCancelled(true);
                }
                break;
        }
    }

    public void onLeftClick(IComponent component) {
        if (antiSpamTaskId != null) {
            return;
        }
        antiSpamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antiSpamTaskId = null;
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEPlayer.onLeftClick({0}, {1}, {2}, {3}", component.getComponentType().toString(), Double.toString(component.getComponentLocation().getX()), Double.toString(component.getComponentLocation().getY()), Double.toString(component.getComponentLocation().getZ()));
            if (!inventoryManager.isInEditionMode()) {
                if (component instanceof IMirrorContainer) {
                    IMirrorContainer mirrorContainer = (IMirrorContainer) component;
                    if (mirrorContainer.hasMirrorCurrent()) {
                        ComponentController.removeMirror((IMirrorContainer) component, this);
                        return;
                    }
                    Item item = getCurrentItem();
                    if (item != null) {
                        if ((component instanceof MirrorSupport && item.isMirror(true)) || item.isMirror(false)) {
                            ComponentController.placeMirror(mirrorContainer, this, item.getColor());
                        }
                    }
                }
            } else {
                ComponentController.removeComponent(component.getArea(), this, component);
            }
        }, 1).getTaskId();
    }

    public boolean onLeftClick() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEPlayer.onLeftClick()");
        Player bukkitPlayer = getBukkitPlayer();
        if (bukkitPlayer == null) {
            return false; // player is disconnected
        }
        // Block block = bukkitPlayer.getTargetBlock(CrossableMaterials.getInstance().getNotSelectableMaterials(), 5);
        Block block = bukkitPlayer.getTargetBlock(CrossableMaterials.getInstance().getNotSelectableMaterials(), 5);
        Location blockLocation = block.getLocation();
        Area area = Areas.getInstance().getAreaFromLocation(bukkitPlayer.getLocation());
        if (area == null) {
            return false;
        }
        IComponent component = area.getComponentFromLocation(blockLocation);
        if (component == null) {
            component = getInvisibleTargetedComponent(area, block);
        }
        if (component != null) {
            onLeftClick(component);
            return true;
        }
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEPlayer.onLeftClick() component==null block");
        return false;
    }

    public void onRightClick(IComponent component) {
        if (antiSpamTaskId != null) {
            return;
        }
        if (!component.getArea().isActivated()) {
            return;
        }
        antiSpamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antiSpamTaskId = null;
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEPlayer.onRightClick({0}, {1}, {2}, {3}", component.getComponentType().toString(), Double.toString(component.getComponentLocation().getX()), Double.toString(component.getComponentLocation().getY()), Double.toString(component.getComponentLocation().getZ()));
            if (inventoryManager.isInEditionMode()) {
                inventoryManager.onRightClick(component);
                return;
            }
            if (component instanceof IMirrorContainer) {
                IMirrorContainer mirrorContainer = (IMirrorContainer) component;
                if (!mirrorContainer.hasMirrorCurrent()) {
                    Item item = getCurrentItem();
                    if (item != null) {
                        if ((component instanceof MirrorSupport && item.isMirror(true)) || item.isMirror(false)) {
                            ComponentController.placeMirror(mirrorContainer, this, item.getColor());
                        }
                    }
                    return;
                }
            }
            if (component instanceof ElevatorCallButton) {
                ElevatorCallButton elevatorCallButton = (ElevatorCallButton) component;
                ComponentController.elevatorCallButton(this, elevatorCallButton);
                return;
            }
            if (component instanceof Lock) {
                Lock lock = (Lock) component;

                lock.tryOpenLock(this);
                return;
            }
            if (component instanceof LockKeyChest) {
                LockKeyChest lockKeyChest = (LockKeyChest) component;
                Lock lock = lockKeyChest.getLock();

                lock.markKeyChestAsFound(this, lockKeyChest);
                return;
            }
            boolean isPlayerModifiableComponent = false;
            if (component instanceof IPlayerModifiableComponent) {
                if (component instanceof LaserReceiver laserReceiver) {
                    if (!laserReceiver.isLetPassLaserThrough()) {
                        isPlayerModifiableComponent = true;
                    }
                } else {
                    isPlayerModifiableComponent = true;
                }
            }
            if (!isPlayerModifiableComponent && getInventoryManager().isRotationShortcutBarOpened()) {
                getInventoryManager().closeRotationShortcutBarInventory();
            }
            if (isPlayerModifiableComponent) {
                Item item = getCurrentItem();
                IPlayerModifiableComponent playerModifiableComponent = (IPlayerModifiableComponent) component;
                boolean isRotationShortcutBarOpened = getInventoryManager().isRotationShortcutBarOpened();
                if (isRotationShortcutBarOpened) {
                    if (item != null) {
                        getInventoryManager().onRightClick(item, component);
                    }
                } else if (ComponentController.canModify(this, playerModifiableComponent)) {
                    getInventoryManager().openRotationShortcutBarInventory(playerModifiableComponent);
                }
            }
        }, 1).getTaskId();
    }

    public boolean onRightClick() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEPlayer.onRightClick()");
        Player bukkitPlayer = getBukkitPlayer();
        if (bukkitPlayer == null) {
            return true; // player is disconnected
        }
        Block block = bukkitPlayer.getTargetBlock(CrossableMaterials.getInstance().getNotSelectableMaterials(), 5);
        Location blockLocation = block.getLocation();
        Area area = Areas.getInstance().getAreaFromLocation(bukkitPlayer.getLocation());
        if (area != null) {
            IComponent component = area.getComponentFromLocation(blockLocation);
            if (component == null) {
                component = getInvisibleTargetedComponent(area, block);
            }
            if (component != null) {
                onRightClick(component);
                return !(component instanceof MirrorChest);
            }
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEPlayer.onRightClick() component==null");
        }
        if (inventoryManager.isInEditionMode()) {
            return inventoryManager.onRightClick();
        }
        if (inventoryManager.isRotationShortcutBarOpened()) {
                Item item = getCurrentItem();
                if (item != null) {
                    return getInventoryManager().onRightClick(item);
                }
                inventoryManager.closeRotationShortcutBarInventory();
                return true;

        }
        return false;
    }

    public Item getCurrentItem() {
        return ItemsFactory.getInstance().getSimilarItem(getBukkitPlayer().getInventory().getItemInMainHand());
    }

    /* Intermediary functions */
    private IComponent getInvisibleTargetedComponent(Area area, Block clickedBlock) {
        if (!getInventoryManager().isInEditionMode()) {
            return null;
        }

        Vector eyeDirection = getBukkitPlayer().getLocation().getDirection().normalize().multiply(0.05);
        Location currentSearchLocation = getBukkitPlayer().getEyeLocation();
        double distance = 0.1;
        while (distance < 5) {
            currentSearchLocation = currentSearchLocation.add(eyeDirection);
            distance = distance + 0.05;
            IComponent component = area.getComponentFromLocation(currentSearchLocation);
            if (component != null) {
                return component;
            }
        }
        return null;
    }

    public void onPlayerJoin(PlayerJoinEvent e) {
        this.getInventoryManager().onJoin();
    }

    private Location getLastCheckpointReached() {
        return lastCheckpointReached == null ? null : lastCheckpointReached.clone();
    }

    private Location getLastVictoryCheckpointReached() {
        return lastVictoryCheckpointReached == null ? null : lastVictoryCheckpointReached.clone();
    }

    public boolean getIsLastCheckpointReachedAVictoryCheckpoint() {
        return isLastCheckpointReachedAVictoryCheckpoint;
    }

    private void setLastCheckpoint(Location checkpointLocation, boolean isVictoryCheckpoint) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        if (checkpointLocation == null) {
            return;
        }
        if (isVictoryCheckpoint) {
            if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("checkpoint_in_victory_area")) {
                return;
            }
            this.lastVictoryCheckpointReached = checkpointLocation;
            this.perWorldLastVictoryCheckpoint.put(Objects.requireNonNull(checkpointLocation.getWorld()).getName(), checkpointLocation);
        } else {
            this.lastCheckpointReached = checkpointLocation;
            this.perWorldLastCheckpoint.put(Objects.requireNonNull(checkpointLocation.getWorld()).getName(), checkpointLocation);
        }
        this.isLastCheckpointReachedAVictoryCheckpoint = isVictoryCheckpoint;
        this.perWorldIsCheckpointReachedAVictoryCheckpoint.put(checkpointLocation.getWorld().getName(), isVictoryCheckpoint);
        LasersEnigmaPlugin.getInstance().getPluginDatabase().updatePlayerCheckpoint(this.getUniqueId(), checkpointLocation, isLastCheckpointReachedAVictoryCheckpoint);
    }

    public void setCurrentVictoryCheckpoint(Location playerLocation) {
        setLastCheckpoint(playerLocation, true);
    }

    public void setCurrentCheckpoint(Location checkpointLocation) {
        setLastCheckpoint(checkpointLocation, false);
    }

    public void clearCheckpoints() {
        this.lastCheckpointReached = null;
        this.lastVictoryCheckpointReached = null;
        this.perWorldIsCheckpointReachedAVictoryCheckpoint.clear();
        this.perWorldLastCheckpoint.clear();
        this.perWorldLastVictoryCheckpoint.clear();
        LasersEnigmaPlugin.getInstance().getPluginDatabase().clearAllCheckpoints(this.getUniqueId());
    }

    public void clearWorldCheckpoints(World from) {
        String worldName = from.getName();
        if (this.lastCheckpointReached != null &&
            Objects.requireNonNull(this.lastCheckpointReached.getWorld()).getName().equals(worldName)) {
            this.lastCheckpointReached = null;
            this.isLastCheckpointReachedAVictoryCheckpoint = true;
            LasersEnigmaPlugin.getInstance().getPluginDatabase().deleteLastCheckpointReached(this.getUniqueId());
        }
        if (this.lastVictoryCheckpointReached != null &&
            Objects.requireNonNull(this.lastVictoryCheckpointReached.getWorld()).getName().equals(worldName)) {
            this.lastVictoryCheckpointReached = null;
            this.isLastCheckpointReachedAVictoryCheckpoint = false;
            LasersEnigmaPlugin.getInstance().getPluginDatabase().deleteLastVictoryCheckpointReached(this.getUniqueId());
        }
        this.perWorldIsCheckpointReachedAVictoryCheckpoint.remove(worldName);
        this.perWorldLastCheckpoint.remove(worldName);
        this.perWorldLastVictoryCheckpoint.remove(worldName);
        LasersEnigmaPlugin.getInstance().getPluginDatabase().deleteWorldCheckpointData(this.getUniqueId(), worldName);
    }

    public Location getOnChangeWorldCheckpoint(World from, World to, GetBackToCheckpointOnChangeWorld getBackToCheckpointOnChangeWorld) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return null;
        }
        switch (getBackToCheckpointOnChangeWorld) {
            case FALSE:
                return null;
            case ONLY_TO_VICTORY_CHECKPOINTS:
                return this.perWorldLastVictoryCheckpoint.get(to.getName());
            case ONLY_TO_NORMAL_CHECKPOINTS:
                return this.perWorldLastCheckpoint.get(to.getName());
            case TRUE:
                Boolean b = this.perWorldIsCheckpointReachedAVictoryCheckpoint.get(to.getName());
                if (b != null) {
                    if (b) {
                        return this.perWorldLastVictoryCheckpoint.get(to.getName());
                    } else {
                        return this.perWorldLastCheckpoint.get(to.getName());
                    }
                }

            default:
                return null;
        }
    }

    public Location getOnJoinCheckpoint(World to, GetBackToCheckpointOnJoin getBackToCheckpointOnJoin) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return null;
        }
        switch (getBackToCheckpointOnJoin) {
            case FALSE:
                return null;
            case PER_WORLD_ONLY_TO_VICTORY_CHECKPOINTS:
                return this.perWorldLastVictoryCheckpoint.get(to.getName());
            case PER_WORLD_ONLY_TO_NORMAL_CHECKPOINTS:
                return this.perWorldLastCheckpoint.get(to.getName());
            case PER_WORLD_TRUE:
                Boolean b = this.perWorldIsCheckpointReachedAVictoryCheckpoint.get(to.getName());
                if (b != null) {
                    if (b) {
                        return this.perWorldLastVictoryCheckpoint.get(to.getName());
                    } else {
                        return this.perWorldLastCheckpoint.get(to.getName());
                    }
                }
                return null;
            case ONLY_TO_NORMAL_CHECKPOINTS:
                return this.lastCheckpointReached;
            case ONLY_TO_VICTORY_CHECKPOINTS:
                return this.lastVictoryCheckpointReached;
            case TRUE:
                if (this.isLastCheckpointReachedAVictoryCheckpoint) {
                    return this.lastVictoryCheckpointReached;
                }
                return this.lastCheckpointReached;
            default:
                return null;
        }

    }

    public Location getOnRespawnCheckpoint(World to, GetBackToCheckpointOnDeath getBackToCheckpointOnDeath) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return null;
        }
        switch (getBackToCheckpointOnDeath) {
            case FALSE:
                return null;
            case ONLY_TO_VICTORY_CHECKPOINTS:
                return this.perWorldLastVictoryCheckpoint.get(to.getName());
            case ONLY_TO_NORMAL_CHECKPOINTS:
                return this.perWorldLastCheckpoint.get(to.getName());
            case TRUE:
                Boolean b = this.perWorldIsCheckpointReachedAVictoryCheckpoint.get(to.getName());
                if (b != null) {
                    if (b) {
                        return this.perWorldLastVictoryCheckpoint.get(to.getName());
                    } else {
                        return this.perWorldLastCheckpoint.get(to.getName());
                    }
                }
                return null;
            default:
                return null;
        }
    }

    public void removeLockKeys(Lock lock) {
        Optional<Set<LockKeyChest>> foundKeyChests;

        foundKeyChests = lock.getFoundKeyChestsForPlayer(this);

        if (foundKeyChests.isEmpty()) return;

        LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(playerUUID, foundKeyChests.get());
        lock.clearFoundKeyChestsForPlayer(this);
    }

    public void removeKey(LockKeyChest lockKeyChest) {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(playerUUID, new HashSet<>(Collections.singletonList(lockKeyChest)));
    }

    public void onPlayerSneak() {
        Area area = Areas.getInstance().getAreaFromLocation(getBukkitPlayer().getLocation());
        if (area == null) {
            return;
        }
        IComponent component = area.getComponentFromLocation(getBukkitPlayer().getLocation());
        if (!(component instanceof Elevator)) {
            return;
        }

        ComponentController.elevatorGoDown(this, (Elevator) component);
    }

    public void onPlayerJump() {
        Area area = Areas.getInstance().getAreaFromLocation(getBukkitPlayer().getLocation());
        if (area == null) {
            return;
        }
        IComponent component = area.getComponentFromLocation(getBukkitPlayer().getLocation());
        if (!(component instanceof Elevator)) {
            return;
        }

        ComponentController.elevatorGoUp(this, (Elevator) component);
    }

    /**
     * Retrieves the player records for each area he has solved
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the player's records for each area solved
     */
    public HashMap<Area, PlayerStats> getPerAreaRecords(World world) {
        HashMap<Area, PlayerStats> result = new HashMap<>();
        Areas.getInstance().getAreas().forEach(area -> {
            if (world != null) {
                if (!world.getUID().equals(Objects.requireNonNull(area.getAreaMinLocation().getWorld()).getUID())) {
                    return; //equivalent to continue in java 8 forEach.
                }
            }
            AreaStats areaStats = area.getStats();

            //count multiple linked area only one
            Area curLinkedArea = area;
            while (curLinkedArea.getStats().isLinked()) {
                curLinkedArea = curLinkedArea.getStats().getLinkedArea();
            }
            if (result.containsKey(areaStats.getLinkedArea())) {
                return;
            }

            PlayerStats playerStatsForThisArea = null;
            try {
                playerStatsForThisArea = areaStats.getStats(getUniqueId());
            } catch (PlayerStatsNotFoundException ignored) {
            }
            if (playerStatsForThisArea != null) {
                result.put(area, playerStatsForThisArea);
            }
        });
        return result;
    }

    /**
     * Retrieves the number of area solved by this player
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the number of area solved by this player
     */
    public int getNbAreaSolved(World world) {
        return getPerAreaRecords(world).size();
    }

    /**
     * Retrieves the total number of step done amongst solved puzzles records
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the total number of step done amongst solved puzzles records
     */
    public long getTotalNbStepsInSolvedAreasRecords(World world) {
        return getPerAreaRecords(world).values().stream()
                .map(PlayerStats::nbStep).mapToLong(Integer::toUnsignedLong).sum();
    }

    /**
     * Retrieves the total time spent in solved puzzles records
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the total time spent in solved puzzles records
     */
    public Duration getTotalTimeSpentInSolvedAreasRecords(World world) {
        return getPerAreaRecords(world).values().stream()
                .map(PlayerStats::duration)
                .reduce(Duration.ZERO, Duration::plus);
    }

    /**
     * Retrieves the total number of actions done amongst solved puzzles records
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the total number of actions done amongst solved puzzles records
     */
    public long getTotalNbActionDoneInSolvedAreasRecords(World world) {
        return getPerAreaRecords(world).values().stream()
                .map(PlayerStats::nbAction).mapToLong(Integer::toUnsignedLong).sum();
    }

    public boolean isSavingActions() {
        return this.actionSaver != null;
    }

    public void startSavingActions(Area area, IComponent component) {
        if (this.actionSaver != null) {
            try {
                this.actionSaver.stopSavingActions();
            } catch (AbstractLasersException ex) {
                TranslationUtils.sendExceptionMessage(this.getBukkitPlayer(), ex);
                this.actionSaver = null;
            }
        }
        this.actionSaver = new PlayerActionsSaver(area, component);
    }

    public void saveActions(IComponent component, ActionType actionType) {
        if (this.actionSaver != null) {
            if (!getInventoryManager().isInEditionMode() || !Permission.EDIT.hasPermission(this.getBukkitPlayer())) {
                forceStopSavingActions();
                return;
            }
            try {
                this.actionSaver.saveAction(component, actionType);
            } catch (AbstractLasersException ex) {
                TranslationUtils.sendExceptionMessage(this.getBukkitPlayer(), ex);
                this.actionSaver = null;
            }
        }
    }

    public ArrayList<ScheduledAction> stopSavingActions() {
        try {
            return this.actionSaver == null ? null : this.actionSaver.stopSavingActions();
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(this.getBukkitPlayer(), ex);
            this.actionSaver = null;
            return null;
        }
    }

    public void forceStopSavingActions() {
        this.actionSaver = null;
    }

    public void onLeaveArea() {
        getInventoryManager().onLeaveArea();
    }

    public void onEnterArea() {
        getInventoryManager().onEnterArea();
    }
}
