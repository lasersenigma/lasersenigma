package eu.lasersenigma.player.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.common.inventory.AShortcutBarInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IMirrorContainer;
import eu.lasersenigma.component.IPlayerModifiableComponent;
import eu.lasersenigma.component.IRotatableComponent;
import eu.lasersenigma.component.RotationType;
import eu.lasersenigma.component.gravitationalsphere.GravitationalSphere;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.mirrorsupport.MirrorSupportMode;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;

/**
 * The ShortcutBar opened when players rotates/resizes components
 */
public class PlayerModifyingComponentShortcutBarInventory extends AShortcutBarInventory {

    private final IPlayerModifiableComponent component;

    private final int taskId;

    public PlayerModifyingComponentShortcutBarInventory(LEPlayer player, IPlayerModifiableComponent component) {
        super(player);
        this.component = component;
        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), (
                () -> {
                    if (player.getBukkitPlayer() == null) {
                        stopTask();
                        return;
                    }

                    if (shouldCloseShortcutBar(player, component)) {
                        player.getInventoryManager().closeRotationShortcutBarInventory();
                        stopTask();
                    }
                }
        ), 20, 20);
        taskId = task.getTaskId();
    }

    private boolean shouldCloseShortcutBar(LEPlayer player, IPlayerModifiableComponent component) {
        if (component instanceof IMirrorContainer mirrorContainer) {
            if (mirrorContainer.isOnGoingAnimation()) {
                return true;
            }
        }
        if (!player.isInArea()) {
            return true;
        }
        final UUID componentWorldUUID = Objects.requireNonNull(component.getComponentLocation().getWorld()).getUID();
        final UUID playerWorldUUID = Objects.requireNonNull(player.getBukkitPlayer().getLocation().getWorld()).getUID();
        if (!componentWorldUUID.equals(playerWorldUUID)) {
            return true;
        }
        final double playerToComponentDistance = component.getComponentLocation().distance(player.getBukkitPlayer().getLocation());
        if (playerToComponentDistance > 6) {
            return true;
        }
        return false;
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        boolean canRotateHorizontally = true;
        boolean canRotateVertically = true;
        boolean canResizeGravitationalSphere = false;
        switch (component.getComponentType()) {
            case LASER_SENDER:
                if (!getArea().isHLaserSendersRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVLaserSendersRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case LASER_RECEIVER:
                if (!getArea().isHLaserReceiversRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVLaserReceiversRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case CONCENTRATOR:
                if (!getArea().isHConcentratorsRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVConcentratorsRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case MIRROR_SUPPORT:
                MirrorSupportMode mode = ((MirrorSupport) component).getMirrorSupportMode();
                if (!getArea().isHMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateVertically = false;
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                canResizeGravitationalSphere = getArea().isAttractionRepulsionSpheresResizingAllowed();
                canRotateVertically = false;
                canRotateHorizontally = false;
                break;
            default:
                canRotateHorizontally = false;
                canRotateVertically = false;
                canResizeGravitationalSphere = false;
                break;
        }
        if (canRotateHorizontally) {
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT);
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT);
        }
        if (canRotateVertically) {
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_UP);
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN);
        }
        if (canResizeGravitationalSphere) {
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE);
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE);
        }
        while (shortcutBar.size() < 8) {
            shortcutBar.add(Item.EMPTY);
        }
        shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEComponentShortcutBarInventory.onClick");
        Area area = component.getArea();

        if (shouldCloseShortcutBar(player, component)) {
            item = Item.COMPONENT_SHORTCUTBAR_CLOSE;
        }
        switch (item) {
            case COMPONENT_SHORTCUTBAR_ROTATION_LEFT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.LEFT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_RIGHT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.RIGHT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_UP:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.UP);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_DOWN:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.DOWN);
                break;
            case COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, ((GravitationalSphere) component), false);
                break;
            case COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, ((GravitationalSphere) component), true);
                break;
            case COMPONENT_SHORTCUTBAR_CLOSE:
                if (!(component instanceof IMirrorContainer mirrorContainer && mirrorContainer.isOnGoingAnimation())) {
                    player.getInventoryManager().closeRotationShortcutBarInventory(false);
                }
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_UP,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN,
                Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_CLOSE
        )).contains(item);
    }

    @Override
    public IPlayerModifiableComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return component.getArea();
    }

    @Override
    public InventoryType getType() {
        return InventoryType.ROTATION_SHORTCUTBAR;
    }

    @Override
    public void onClose() {
        stopTask();
    }

    private void stopTask() {
        Bukkit.getScheduler().cancelTask(taskId);
    }
}
