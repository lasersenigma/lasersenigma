package eu.lasersenigma.player;

import eu.lasersenigma.LasersEnigmaPlugin;
import org.bukkit.Material;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public final class CrossableMaterials {

    private static CrossableMaterials INSTANCE;

    private static final String INVALID_MATERIALS_LOG_MESSAGE = """
            The following materials, defined in crossable_materials list within the configuration file, do not exist: 
            {0}
                                            
            See https://www.google.com/search?q=Material+spigot+javadoc to find the complete list of possible materials according to your minecraft version.
            """;

    private final Set<CrossableMaterialWrapper> crossableMaterials = new HashSet<>();
    private Set<Material> notSelectableMaterials;
    private Set<Material> particleCrossableMaterials;

    private CrossableMaterials() {
        // Transparent blocks
        loadFromMaterial(Material.LIGHT, true);
        loadFromMaterial(Material.AIR, true);
        loadFromMaterial(Material.CAVE_AIR, true);
        loadFromMaterial(Material.VOID_AIR, true);
        loadFromMaterial(Material.STRUCTURE_VOID, true);
        loadFromMaterial(Material.WATER, true);

        // Redstone stuff
        loadFromMaterial(Material.BARRIER);
        loadFromMaterial(Material.COMPARATOR);
        loadFromMaterial(Material.REPEATER);
        loadFromMaterial(Material.REDSTONE);
        loadFromMaterial(Material.REDSTONE_WIRE);
        loadFromMaterial(Material.TRIPWIRE);
        loadFromMaterial(Material.TRIPWIRE_HOOK);
        loadFromMaterial(Material.LEVER);
        loadFromMaterial(Material.RAIL);
        loadFromMaterial(Material.ACTIVATOR_RAIL);
        loadFromMaterial(Material.DETECTOR_RAIL);
        loadFromMaterial(Material.POWERED_RAIL);

        loadFromMaterialSuffix("BUTTON");
        loadFromMaterialSuffix("PRESSURE_PLATE");

        // Miscellaneous
        loadFromMaterial(Material.SCAFFOLDING);
        loadFromMaterial(Material.CHAIN);
        loadFromMaterial(Material.IRON_BARS);
        loadFromMaterialSuffix("CANDLE");

        // Vegetation
        loadFromMaterial(Material.VINE);
        loadFromMaterial(Material.LILY_PAD);

        // Slabs
        loadFromMaterialSuffix("SLAB");

        // Carpets
        loadFromMaterialSuffix("CARPET");

        // TODO: Snow blocks (with {BlockStateTag:{level:1}}. It means be able to filter block by NBT.
    }

    private void loadFromMaterial(Material material) {
        loadFromMaterial(material, false);
    }

    private void loadFromMaterial(Material material, boolean transparent) {
        crossableMaterials.add(new CrossableMaterialWrapper(material, transparent));
    }

    private void loadFromMaterialSuffix(String materialSuffix) {
        loadFromMaterialSuffix(materialSuffix, false);
    }

    private void loadFromMaterialSuffix(String materialSuffix, boolean transparent) {
        Arrays.stream(Material.values())
                .filter(mat -> mat.name().endsWith(materialSuffix))
                .forEach(mat -> crossableMaterials.add(new CrossableMaterialWrapper(mat, transparent)));
    }

    public static CrossableMaterials getInstance() {
        if (INSTANCE == null) INSTANCE = new CrossableMaterials();
        return INSTANCE;
    }

    public Set<Material> getNotSelectableMaterials() {
        if (notSelectableMaterials == null) {
            notSelectableMaterials = crossableMaterials.stream()
                    .filter(CrossableMaterialWrapper::isTransparent)
                    .map(CrossableMaterialWrapper::getMaterial)
                    .collect(Collectors.toSet());
        }
        return notSelectableMaterials;
    }

    public Set<Material> getParticleCrossableMaterials() {
        if (particleCrossableMaterials == null) {
            particleCrossableMaterials = crossableMaterials.stream()
                    .map(CrossableMaterialWrapper::getMaterial)
                    .collect(Collectors.toSet());

            String crossableMaterialsStr = LasersEnigmaPlugin
                    .getInstance()
                    .getConfig()
                    .getString("crossable_materials");

            Set<String> invalidMaterials = new HashSet<>();
            // Set<Material> newCrossableMaterials = new HashSet<>();

            if (crossableMaterialsStr != null) {
                for (String materialStr : crossableMaterialsStr.split(",")) {
                    try {
                        particleCrossableMaterials.add(Material.valueOf(materialStr));
                    } catch (IllegalArgumentException e) {
                        invalidMaterials.add(materialStr);
                    }
                }
            }

            if (!invalidMaterials.isEmpty()) {
                Logger.getLogger(CrossableMaterials.class.getName()).log(Level.SEVERE,
                        INVALID_MATERIALS_LOG_MESSAGE,
                        invalidMaterials
                );
            }

            if ((!invalidMaterials.isEmpty()) || particleCrossableMaterials.isEmpty()) {
                particleCrossableMaterials = Arrays.stream(Material.values())
                        .filter(Material::isBlock)
                        .collect(Collectors.toSet());
            }
        }
        return particleCrossableMaterials;
    }
}
