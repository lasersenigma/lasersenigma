package eu.lasersenigma.player;


import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.ComponentController;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.armors.inventory.ArmorActionMenuInventory;
import eu.lasersenigma.armors.inventory.ArmorOptionsMenuInventory;
import eu.lasersenigma.common.inventory.*;
import eu.lasersenigma.common.inventory.saving.PlayerInventorySaveManager;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.common.items.LasersColor;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IColorableComponent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.IPlayerModifiableComponent;
import eu.lasersenigma.component.common.inventory.ColorSelectorInventory;
import eu.lasersenigma.component.common.inventory.ComponentMenuInventory;
import eu.lasersenigma.component.common.inventory.ComponentSelectorInventory;
import eu.lasersenigma.component.common.inventory.ComponentShortcutBarInventory;
import eu.lasersenigma.component.elevator.inventory.ElevatorSecondLocationShortcutBarInventory;
import eu.lasersenigma.component.mirrorchest.MirrorChest;
import eu.lasersenigma.component.mirrorsupport.MirrorSupport;
import eu.lasersenigma.component.mirrorsupport.MirrorSupportMode;
import eu.lasersenigma.items.ArmorAction;
import eu.lasersenigma.player.inventory.PlayerModifyingComponentShortcutBarInventory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.Cancellable;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.logging.Level;

public class PlayerInventoryManager {

    public static boolean sendComponentBlockedMsg = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("component_blocked_message", true);

    private final LEPlayer player;

    private final PlayerInventorySaveManager playerInventorySaveManager;
    private final boolean isInClearedInventoryPlayMode;
    private final boolean isInArea;
    private AOpenableInventory currentLEOpenedInventory;
    private AShortcutBarInventory currentLEShortcutBarInventory;
    private ComponentType clickedComponentType;
    private Block clickedBlock;
    private BlockFace clickedBlockFace;
    private Material clickedMaterial;
    private InventoryType colorSelectionContext;
    private IComponent component;
    private Integer antispamTaskId;
    private boolean isRotationShortcutBarOpened;
    private boolean isInEditionMode;
    private ArmorAction selectedArmorAction;

    public PlayerInventoryManager(LEPlayer player) {
        this.player = player;
        isRotationShortcutBarOpened = false;
        isInEditionMode = false;
        isInClearedInventoryPlayMode = false;
        isInArea = false;
        playerInventorySaveManager = new PlayerInventorySaveManager(player);
    }

    public boolean isInArea() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.isInArea:{0}", isInArea);
        return isInArea;
    }

    public boolean isRotationShortcutBarOpened() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.isRotationShortcutbarOpened:{0}", isRotationShortcutBarOpened);
        return isRotationShortcutBarOpened;
    }

    public boolean isInClearedInventoryPlayMode() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.isInClearedInventoryPlayMode:{0}", isInClearedInventoryPlayMode);
        return isInClearedInventoryPlayMode;
    }

    public boolean isInEditionMode() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.isInEditionMode:{0}", isInEditionMode);
        return isInEditionMode;
    }

    public PlayerInventorySaveManager getInventorySaveManager() {
        return this.playerInventorySaveManager;
    }

    public void onRightClick(Item item, IComponent component) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick(Item,IComponent)");
        if (isRotationShortcutBarOpened) {
            if (currentLEShortcutBarInventory instanceof PlayerModifyingComponentShortcutBarInventory) {
                IComponent rotatableComponent = ((PlayerModifyingComponentShortcutBarInventory) currentLEShortcutBarInventory).getComponent();
                double distance = this.player.getBukkitPlayer().getLocation().distance(rotatableComponent.getComponentLocation());
                if (rotatableComponent != component || distance > 5) {
                    closeRotationShortcutBarInventory();
                    return;
                }
                currentLEShortcutBarInventory.onClick(item);
            }
            return;
        }

        if (currentLEShortcutBarInventory instanceof ComponentShortcutBarInventory) {
            ComponentShortcutBarInventory currentComponentShortcutBarInventory = (ComponentShortcutBarInventory) currentLEShortcutBarInventory;
            if (currentComponentShortcutBarInventory.getComponent() != component) {
                openLEInventory(new ComponentShortcutBarInventory(player, component));
                return;
            }
        } else {
            openLEInventory(new ComponentShortcutBarInventory(player, component));
        }
        if (item != null) {
            onRightClick(item);
        }
    }

    public LEPlayer getPlayer() {
        return this.player;
    }

    public boolean onRightClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick(Item)");
        boolean shouldEventBeCancelled = false;
        final boolean isItemInCurrentLEOpenedInventory = (
                currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item)
        );
        final boolean isItemInCurrentLEShortcutBarInventory = (
                !isItemInCurrentLEOpenedInventory && currentLEShortcutBarInventory != null &&
                        currentLEShortcutBarInventory.contains(item)
        );
        if (isItemInCurrentLEOpenedInventory || isItemInCurrentLEShortcutBarInventory) {
            shouldEventBeCancelled = true;
        }
        if (antispamTaskId == null) {
            antispamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                antispamTaskId = null;
                if (item != null) {
                    if (isItemInCurrentLEOpenedInventory) {
                        currentLEOpenedInventory.onClick(item);
                    } else if (isItemInCurrentLEShortcutBarInventory) {
                        currentLEShortcutBarInventory.onClick(item);
                    }
                }
            }, 1).getTaskId();
        }
        return shouldEventBeCancelled;
    }

    public void onRightClick(IComponent component) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick(IComponent)");
        if (antispamTaskId != null) {
            return;
        }
        antispamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antispamTaskId = null;
            onRightClick(player.getCurrentItem(), component);
        }, 1).getTaskId();
    }

    public boolean onRightClick() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick()");
        return onRightClick(player.getCurrentItem());
    }

    public void onInventoryClick(ItemStack itemStack) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onInventoryClick");
        if (antispamTaskId != null) {
            return;
        }
        antispamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antispamTaskId = null;
            if (currentLEOpenedInventory == null && currentLEShortcutBarInventory == null) {
                return;
            }
            Item item = ItemsFactory.getInstance().getSimilarItem(itemStack);
            if (item == null) {
                if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(itemStack)) {
                    currentLEOpenedInventory.onClick(itemStack);
                }
                return;
            }
            if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item)) {
                currentLEOpenedInventory.onClick(item);
            } else if (currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.contains(item)) {
                currentLEShortcutBarInventory.onClick(item);
            }
        }, 1).getTaskId();
    }

    public void onInventoryDrag(InventoryDragEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onInventoryDrag");
        if (currentLEOpenedInventory == null && currentLEShortcutBarInventory == null) {
            return;
        }
        Item item = ItemsFactory.getInstance().getSimilarItem(e.getOldCursor());
        if (item == null) {
            return;
        }
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item)) {
            e.setCancelled(true);
        }
        if (currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.contains(item)) {
            e.setCancelled(true);
        }
    }

    public void onPlayerDropItem(ItemStack itemStack, Cancellable event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onPlayerDropItem");
        if (currentLEOpenedInventory == null && currentLEShortcutBarInventory == null) {
            return;
        }
        Item item = ItemsFactory.getInstance().getSimilarItem(itemStack);
        if (item == null) {
            return;
        }
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item) &&
                currentLEOpenedInventory.isDropable(item)) {
            return;
        }
        if (currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.contains(item) &&
                currentLEShortcutBarInventory.isDropable(item)) {
            return;
        }
        event.setCancelled(true);
    }

    public void onArmorMenu() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onArmourMenu");
        List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20);
        if (blocks.size() < 2) {
            return;
        }
        clickedBlock = blocks.get(1);
        clickedBlockFace = clickedBlock.getFace(blocks.get(0));
        openLEInventory(new ArmorActionMenuInventory(player));
    }

    public void onSelectedArmor(ArmorAction armorAction) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onSelectedArmor");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);

        this.selectedArmorAction = armorAction;

        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> openLEInventory(new ArmorOptionsMenuInventory(player)), 2);
    }

    public void onArmorMenuCompleted(boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onArmourMenuCompleted");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);

        Location newMaterialLocation = clickedBlock.getLocation().clone().add(clickedBlockFace.getModX(), clickedBlockFace.getModY(), clickedBlockFace.getModZ());
        clickedBlock = null;
        clickedBlockFace = null;

        ArmorAction action = selectedArmorAction;
        selectedArmorAction = null;

        PlayerController.armorCreation(this.getPlayer(), action, newMaterialLocation, noBurnCheckbox, noDamageCheckbox, noKnockbackCheckbox, reflectionCheckbox, focusCheckbox, prismCheckbox, durabilityNumber);

    }

    public void onPlaceComponent() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onPlaceComponent");
        Area area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
        if (area == null || !area.isActivated()) {
            TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
            return;
        }
        List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20);
        if (blocks.size() < 2) {
            return;
        }
        clickedBlock = blocks.get(1);
        clickedBlockFace = clickedBlock.getFace(blocks.get(0));
        openLEInventory(new ComponentSelectorInventory(player, area));
    }

    public void onChangeComponentColor(IComponent component) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onChangeComponentColor");
        colorSelectionContext = InventoryType.COMPONENT_MENU;
        this.component = component;
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            switch (component.getComponentType()) {
                case FILTERING_SPHERE, BURNABLE_BLOCK, LASER_RECEIVER, LASER_SENDER, REFLECTING_SPHERE:
                    colorSelectionContext = InventoryType.COMPONENT_MENU;
                    openLEInventory(new ColorSelectorInventory(player, false,
                            component, component.getArea()));
                    break;
                case MIRROR_SUPPORT, MIRROR_CHEST:
                    colorSelectionContext = InventoryType.COMPONENT_MENU;
                    openLEInventory(new ColorSelectorInventory(player, true,
                            component, component.getArea()));
                    break;
                default:
                    throw new UnsupportedOperationException("Color selection has no context.");
            }
        }, 2);
    }

    public void onComponentSelected(ComponentType componentType) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onComponentSelected(ComponentType)");
        clickedComponentType = componentType;
        clickedMaterial = null;
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            switch (componentType) {
                case FILTERING_SPHERE, BURNABLE_BLOCK, LASER_RECEIVER, LASER_SENDER, REFLECTING_SPHERE:
                    colorSelectionContext = InventoryType.NEW_COMPONENT_MENU;
                    openLEInventory(new ColorSelectorInventory(player, false, null,
                            Areas.getInstance().getAreaFromLocation(clickedBlock.getLocation())));
                    break;
                case MIRROR_SUPPORT:
                    colorSelectionContext = InventoryType.NEW_COMPONENT_MENU;
                    openLEInventory(new ColorSelectorInventory(player, true, true, null,
                            Areas.getInstance().getAreaFromLocation(clickedBlock.getLocation())));
                    break;
                case MIRROR_CHEST:
                    colorSelectionContext = InventoryType.NEW_COMPONENT_MENU;
                    openLEInventory(new ColorSelectorInventory(player, true, null,
                            Areas.getInstance().getAreaFromLocation(clickedBlock.getLocation())));
                    break;
                case ELEVATOR:
                    Location elevatorFirstLocation = clickedBlock.getLocation();
                    Area area = Areas.getInstance().getAreaFromLocation(elevatorFirstLocation);
                    if (area == null) {
                        TranslationUtils.sendExceptionMessage(getPlayer().getBukkitPlayer(), new NoAreaFoundException());
                    } else {
                        openLEInventory(new ElevatorSecondLocationShortcutBarInventory(player, area, elevatorFirstLocation));
                    }
                    clickedComponentType = null;
                    clickedMaterial = null;
                    clickedBlock = null;
                    clickedBlockFace = null;
                    break;
                default:
                    IComponent newComponent = ComponentController.addComponentFromBlockFace(componentType, clickedBlock, clickedBlockFace, player);
                    if (newComponent != null) {
                        this.component = newComponent;
                        openLEInventory(new ComponentShortcutBarInventory(player, newComponent));
                    }
                    clickedBlock = null;
                    clickedBlockFace = null;
                    break;
            }
        }, 2);
    }

    public void onBlockComponentSelected(Item item) {
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            boolean isBlackIncluded = true;
            switch (item) {
                case WHITE_CONCRETE_POWDER, WHITE_GLASS_PANE, WHITE_GLASS -> {
                    clickedMaterial = item.getMaterial();
                    clickedComponentType = null;
                    isBlackIncluded = false;
                }
            }
            colorSelectionContext = InventoryType.NEW_COMPONENT_MENU;
            openLEInventory(new ColorSelectorInventory(player, isBlackIncluded, null,
                    Areas.getInstance().getAreaFromLocation(clickedBlock.getLocation())));
        }, 2);
    }

    @SuppressWarnings("null")
    public void onColorSelected(LasersColor color) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onColorSelected");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);

        if (colorSelectionContext == null) {
            return;
        }

        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> handleColorSelection(color), 2);
    }

    private void handleColorSelection(LasersColor color) {
        switch (colorSelectionContext) {
            case NEW_COMPONENT_MENU:
                handleNewComponentMenu(color);
                break;
            case COMPONENT_MENU:
                handleComponentMenu(color);
                break;
            default:
                throw new UnsupportedOperationException("Color selection has no context.");
        }
    }

    private void handleNewComponentMenu(LasersColor color) {
        if (clickedMaterial == null) {
            IComponent newComponent = ComponentController.addComponentFromBlockFace(clickedComponentType, clickedBlock, clickedBlockFace, player);

            if (color == null) {
                if (newComponent instanceof MirrorSupport mirrorSupport) {
                    mirrorSupport.setHasMirror(false, true);
                }
            } else {
                ((IColorableComponent) newComponent).setColor(color, true);
            }

            component = newComponent;
            openLEInventory(new ComponentShortcutBarInventory(player, newComponent));
        } else {
            ComponentController.addColoredMaterialFromBlockFace(clickedMaterial, clickedBlock, clickedBlockFace, player, color);
        }
        clickedComponentType = null;
        clickedMaterial = null;
        clickedBlock = null;
        clickedBlockFace = null;
    }

    private void handleComponentMenu(LasersColor color) {
        try {
            ComponentController.setColor(component.getArea(), (IColorableComponent) component, player, color);
        } catch (IllegalArgumentException e) {
            if (component instanceof MirrorSupport mirrorSupport) {
                mirrorSupport.setHasMirror(false, player.getInventoryManager().isInEditionMode());
            }
        }
        openLEInventory(new ComponentMenuInventory(player, component));
    }

    public void closeOpenedInventory() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.closeOpenedInventory");
        player.getBukkitPlayer().closeInventory();
        if (currentLEOpenedInventory != null) {
            currentLEOpenedInventory.onClose();
            currentLEOpenedInventory = null;
        }
    }

    public void closeShortcutBarInventory() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.closeShortcutBarInventory");
        if (currentLEShortcutBarInventory != null) {
            currentLEShortcutBarInventory.onClose();
            currentLEShortcutBarInventory = null;
        }
    }

    public void openLEInventory(AInventory inventory) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.openLEInventory");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            AreaController.clearTemporaryLocations(player);
            closeOpenedInventory();
            ItemStack[] items = inventory.getContents();
            if (inventory instanceof AOpenableInventory) {
                if (currentLEOpenedInventory != null) {
                    closeOpenedInventory();
                }
                currentLEOpenedInventory = (AOpenableInventory) inventory;
                Inventory bukkitInventory = currentLEOpenedInventory.getBukkitInventory();
                player.getBukkitPlayer().openInventory(bukkitInventory);
            } else if (inventory instanceof AShortcutBarInventory) {
                if (currentLEShortcutBarInventory != null) {
                    currentLEShortcutBarInventory.onClose();
                }
                currentLEShortcutBarInventory = (AShortcutBarInventory) inventory;
                Inventory bukkitInventory = player.getBukkitPlayer().getInventory();
                for (int i = 0; i < items.length; i++) {
                    bukkitInventory.setItem(i, items[i]);
                }
            }
        }, 1);
    }

    public boolean isUsing(IComponent component) {
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.getComponent() == component) {
            return true;
        }
        return currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.getComponent() == component;
    }

    public boolean isUsing(Area area) {
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.getArea() == area) {
            return true;
        }
        return currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.getArea() == area;
    }

    public void updateInventories() {
        if (currentLEOpenedInventory != null) {
            currentLEOpenedInventory.updateInventory();
        }
        if (currentLEShortcutBarInventory != null) {
            currentLEShortcutBarInventory.updateInventory();
        }
    }

    public void setSavedInventory(Inventory inventoryFromDatabase, boolean isInEditionMode, boolean isRotationShortcutBarOpened) {
        this.isInEditionMode = isInEditionMode;
        this.isRotationShortcutBarOpened = isRotationShortcutBarOpened;
        this.getInventorySaveManager().addInventoryFromDatabase(inventoryFromDatabase.getContents());
    }

    public void onCloseInventory() {
        if (currentLEOpenedInventory != null) {
            currentLEOpenedInventory.onClose();
            currentLEOpenedInventory = null;
        }
    }


    public void toggleEditionMode() {
        if (isRotationShortcutBarOpened) {
            return;
        }
        if (isInEditionMode()) {
            exitEditionMode();
        } else {
            openEditionMode();
        }
    }

    public void exitEditionMode() {
        if (isInEditionMode()) {
            closeOpenedInventory();
            closeShortcutBarInventory();
            playerInventorySaveManager.onExitEditionMode();
            isInEditionMode = false;
            isRotationShortcutBarOpened = false;
        }
    }

    public void openEditionMode() {
        if (!isInEditionMode()) {
            playerInventorySaveManager.onOpenEditionMode();
            isInEditionMode = true;
            isRotationShortcutBarOpened = false;
            openLEInventory(new MainShortcutBarInventory(player));
        }
    }

    public void closeRotationShortcutBarInventory() {
        closeRotationShortcutBarInventory(false);
    }

    public void closeRotationShortcutBarInventory(boolean delay) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.closeRotationShortcutbarInventory");
        Runnable doCloseRotation = () -> {
            closeShortcutBarInventory();
            playerInventorySaveManager.onExitRotationShortcutBar();
            isRotationShortcutBarOpened = false;
            isInEditionMode = false;
        };
        if (delay) {
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), doCloseRotation, 1);
        } else {
            doCloseRotation.run();
        }
    }

    public void openRotationShortcutBarInventory(IPlayerModifiableComponent playerModifiableComponent) {
        if (isInEditionMode()) {
            return;
        }
        if (isRotationShortcutBarOpened()) {
            closeRotationShortcutBarInventory();
            return;
        }
        Area area = playerModifiableComponent.getArea();
        boolean canRotateHorizontally = true;
        boolean canRotateVertically = true;
        boolean canResizeGravitationalSphere = false;
        switch (playerModifiableComponent.getComponentType()) {
            case LASER_SENDER:
                if (!area.isHLaserSendersRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVLaserSendersRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case LASER_RECEIVER:
                if (!area.isHLaserReceiversRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVLaserReceiversRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case CONCENTRATOR:
                if (!area.isHConcentratorsRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVConcentratorsRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case MIRROR_SUPPORT:
                MirrorSupportMode mode = ((MirrorSupport) playerModifiableComponent).getMirrorSupportMode();
                if (!area.isHMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateVertically = false;
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                canResizeGravitationalSphere = area.isAttractionRepulsionSpheresResizingAllowed();
                canRotateVertically = false;
                canRotateHorizontally = false;
                break;
            default:
                canRotateHorizontally = false;
                canRotateVertically = false;
                canResizeGravitationalSphere = false;
                break;
        }
        if (!(canResizeGravitationalSphere || canRotateHorizontally || canRotateVertically)) {
            if (sendComponentBlockedMsg) {
                TranslationUtils.sendTranslatedMessage(getPlayer().getBukkitPlayer(), "messages.cannot_interact_with_component");
            }
            return;
        }
        this.playerInventorySaveManager.onOpenRotationShortcutBar();
        isRotationShortcutBarOpened = true;
        isInEditionMode = false;
        openLEInventory(new PlayerModifyingComponentShortcutBarInventory(player, playerModifiableComponent));
    }

    public void onEnterArea() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onEnterArea");
        getInventorySaveManager().onEnterArea();
    }

    public void onLeaveArea() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onLeaveArea");
        if (isRotationShortcutBarOpened) {
            closeRotationShortcutBarInventory();
        }
        getInventorySaveManager().onLeaveArea();
    }

    public void onQuit() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onQuit");
        onCloseInventory();
        exitEditionMode();
        getInventorySaveManager().onQuit();
    }

    public void onJoin() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "PlayerInventoryManager.onJoin");
        getInventorySaveManager().onJoin();
    }
}
