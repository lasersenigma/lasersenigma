package eu.lasersenigma.checkpoint;

public enum GetBackToCheckpointOnJoin {
    TRUE,
    ONLY_TO_NORMAL_CHECKPOINTS,
    ONLY_TO_VICTORY_CHECKPOINTS,
    FALSE,
    PER_WORLD_TRUE,
    PER_WORLD_ONLY_TO_NORMAL_CHECKPOINTS,
    PER_WORLD_ONLY_TO_VICTORY_CHECKPOINTS;

    public static GetBackToCheckpointOnJoin fromString(String str) {
        return GetBackToCheckpointOnJoin.valueOf(str.toUpperCase());
    }
}
