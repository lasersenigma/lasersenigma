package eu.lasersenigma.checkpoint.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * EventListener for BlockBreakEvent
 */
public class CheckpointClearOnPlayerQuitEventListener implements Listener {

    @SuppressWarnings("LeakingThisInConstructor")
    public CheckpointClearOnPlayerQuitEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        LEPlayers.getInstance().findLEPlayer(e.getPlayer()).clearCheckpoints();
    }
}
