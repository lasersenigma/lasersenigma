package eu.lasersenigma.checkpoint.listener;

import eu.lasersenigma.checkpoint.GetBackToCheckpointOnChangeWorld;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Location;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class CheckpointPlayerChangedWorldEventListener implements Listener {

    private final GetBackToCheckpointOnChangeWorld backToCheckpointOnChangeWorld;

    private final boolean deleteCheckpointOnExitWorld;

    public CheckpointPlayerChangedWorldEventListener(GetBackToCheckpointOnChangeWorld backToCheckpointOnChangeWorld, boolean deleteCheckpointOnExitWorld) {
        this.backToCheckpointOnChangeWorld = backToCheckpointOnChangeWorld;
        this.deleteCheckpointOnExitWorld = deleteCheckpointOnExitWorld;
    }

    public void onPlayerChangedWorld(PlayerChangedWorldEvent e) {
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
        Location checkpoint = lePlayer.getOnChangeWorldCheckpoint(e.getFrom(), e.getPlayer().getWorld(), backToCheckpointOnChangeWorld);
        if (checkpoint != null) {
            lePlayer.getBukkitPlayer().teleport(checkpoint, PlayerTeleportEvent.TeleportCause.PLUGIN);
        }
        if (deleteCheckpointOnExitWorld) {
            lePlayer.clearWorldCheckpoints(e.getFrom());
        }
    }

}
