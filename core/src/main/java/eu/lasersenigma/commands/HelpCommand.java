package eu.lasersenigma.commands;

import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.common.message.TranslationUtils;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class HelpCommand extends LasersCommand {

    public HelpCommand() {
        super("help", "commands.help.description", "h", "?");
        super.setPermission("lasers.edit");
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        TranslationUtils.sendTranslatedMessage(player, "commands.help.message");

        return true;
    }
}
