package eu.lasersenigma.schematic;

import eu.lasersenigma.area.schematic.AreasSchematic;

import java.io.Serializable;

public class Schematic implements Serializable {

  public static final long serialVersionUID = 1;

  private AreasSchematic areasSchematic;

  private byte[] worleditSchematic;

  private String version;

  public Schematic() {}

  public AreasSchematic getAreasSchematic() {
    return areasSchematic;
  }

  public void setAreasSchematic(AreasSchematic areasSchematic) {
    this.areasSchematic = areasSchematic;
  }

  public byte[] getWorleditSchematic() {
    return worleditSchematic;
  }

  public void setWorleditSchematic(byte[] worleditSchematic) {
    this.worleditSchematic = worleditSchematic;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}
