package eu.lasersenigma.schematic.command;

import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.schematic.SchematicManager;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

public class DeleteCommand extends LasersCommand {

    public DeleteCommand() {
        super("delete", "commands.schematic.delete.description");
        super.setPermission("lasers.admin");
        super.addArgument("file_name", true, ArgumentType.string());
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "file_name": {
                super.setAutoCompleteValuesArg(currentArgumentName, SchematicManager.list());
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) throws AbstractLasersException {
        String fileName = super.getArgumentValue(player, "file_name", ArgumentType.string());

        SchematicManager.deleteSchematic(fileName);

        return true;
    }
}
