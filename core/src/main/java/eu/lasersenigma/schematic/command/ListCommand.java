package eu.lasersenigma.schematic.command;

import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.common.command.LasersCommand;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

public class ListCommand extends LasersCommand {

    public ListCommand() {
        super("list", "commands.schematic.list.description");
        super.setPermission("lasers.admin");
        super.addArgument("page_number", false, ArgumentType.integerOnly());
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "page_number": {
                super.setAutoCompleteValuesArg(currentArgumentName, ClipboardManager.getInstance().getPages());
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        int pageNumber = super.getArgumentValue(player, "page_number", ArgumentType.integerOnly(), 1);
        ClipboardManager.getInstance().list(player, pageNumber - 1);
        return true;
    }
}
