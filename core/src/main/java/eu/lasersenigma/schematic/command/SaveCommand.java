package eu.lasersenigma.schematic.command;

import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.common.command.LasersCommand;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

public class SaveCommand extends LasersCommand {

    public SaveCommand() {
        super("save", "commands.schematic.save.description");
        super.setPermission("lasers.admin");
        super.addArgument("file_name", true, ArgumentType.string());
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        String fileName = super.getArgumentValue(player, "file_name", ArgumentType.string());

        ClipboardManager.getInstance().save(player, fileName);

        return true;
    }
}
