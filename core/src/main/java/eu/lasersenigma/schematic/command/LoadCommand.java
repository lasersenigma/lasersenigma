package eu.lasersenigma.schematic.command;

import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.schematic.SchematicManager;
import fr.skytale.commandlib.Commands;
import fr.skytale.commandlib.arguments.ArgumentType;
import org.bukkit.entity.Player;

public class LoadCommand extends LasersCommand {

    public LoadCommand() {
        super("load", "commands.schematic.load.description");
        super.setPermission("lasers.admin");
        super.addArgument("file_name", true, ArgumentType.string());
    }

    @Override
    public void reloadAutoCompleteValues(Player executor, String[] args, String currentArgumentName) {
        switch (currentArgumentName) {
            case "file_name": {
                super.setAutoCompleteValuesArg(currentArgumentName, SchematicManager.list());
                break;
            }
        }
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        String fileName = super.getArgumentValue(player, "file_name", ArgumentType.string());
        ClipboardManager.getInstance().load(player, fileName);
        return true;
    }
}
