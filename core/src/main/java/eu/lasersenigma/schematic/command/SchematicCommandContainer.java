package eu.lasersenigma.schematic.command;

import eu.lasersenigma.common.command.LasersCommand;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class SchematicCommandContainer extends LasersCommand {

    public SchematicCommandContainer() {
        super("schematic", "commands.schematic.description", "schem");
        super.setPermission("lasers.admin");
        super.registerSubCommand(new DeleteCommand());
        super.registerSubCommand(new ListCommand());
        super.registerSubCommand(new LoadCommand());
        super.registerSubCommand(new SaveCommand());
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        return super.defaultProcessForContainer(commands, player, strings);
    }
}
