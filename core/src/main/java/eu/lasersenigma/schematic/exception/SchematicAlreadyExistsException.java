package eu.lasersenigma.schematic.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class SchematicAlreadyExistsException extends AbstractLasersException {

    public SchematicAlreadyExistsException() {
        super("errors.schematic.already_exists");
    }

}
