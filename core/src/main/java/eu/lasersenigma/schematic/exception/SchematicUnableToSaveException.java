package eu.lasersenigma.schematic.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class SchematicUnableToSaveException extends AbstractLasersException {

    public SchematicUnableToSaveException() {
        super("errors.schematic.unable_to_save");
    }

}
