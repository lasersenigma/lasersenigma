package eu.lasersenigma.schematic.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class PasteOutsideWorldException extends AbstractLasersException {
    public PasteOutsideWorldException() {
        super("errors.paste_outside_world");
    }

}
