package eu.lasersenigma.schematic.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class SchematicInvalidException extends AbstractLasersException {

    public SchematicInvalidException() {
        super("errors.invalid_schematic");
    }

}
