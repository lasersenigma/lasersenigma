package eu.lasersenigma.schematic.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class SchematicNotExistsException extends AbstractLasersException {

    public SchematicNotExistsException() {
        super("errors.schematic.not_exists");
    }

}
