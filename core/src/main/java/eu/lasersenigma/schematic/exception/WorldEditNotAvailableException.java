package eu.lasersenigma.schematic.exception;

import eu.lasersenigma.common.exception.AbstractLasersException;

public class WorldEditNotAvailableException extends AbstractLasersException {

    public WorldEditNotAvailableException() {
        super("errors.worldedit_not_available");
    }

}
