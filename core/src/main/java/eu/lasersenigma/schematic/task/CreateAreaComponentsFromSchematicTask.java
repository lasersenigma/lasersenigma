package eu.lasersenigma.schematic.task;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.component.schematic.ComponentSchematic;
import eu.lasersenigma.component.schematic.ComponentSchematicManager;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

/**
 * A task creating a component from a component shematic
 */
public class CreateAreaComponentsFromSchematicTask extends BukkitRunnable {

    private final Area area;
    private final List<ComponentSchematic> componentsSchematics;
    private final Location playerLocation;

    /**
     * Constructor saving the task data
     *
     * @param componentsSchematics the schematics of each area's component
     * @param area                 the area where the component will be created
     * @param playerLocation       the pasting player location
     */
    public CreateAreaComponentsFromSchematicTask(List<ComponentSchematic> componentsSchematics, Area area, Location playerLocation) {
        super();
        this.area = area;
        this.componentsSchematics = componentsSchematics;
        this.playerLocation = playerLocation;
    }

    /**
     * The method runned repeatedly
     */
    @Override
    public void run() {
        componentsSchematics.forEach(cs -> {
            ComponentSchematicManager.createComponent(cs, area, playerLocation);
        });
    }

}
