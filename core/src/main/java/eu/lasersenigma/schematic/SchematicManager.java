package eu.lasersenigma.schematic;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.exception.*;
import eu.lasersenigma.area.schematic.AreasSchematic;
import eu.lasersenigma.area.schematic.AreasSchematicManager;
import eu.lasersenigma.common.dependency.worldedit.WECopy;
import eu.lasersenigma.common.dependency.worldedit.WEPaste;
import eu.lasersenigma.common.dependency.worldedit.WorldEditHelper;
import eu.lasersenigma.component.AArmorStandComponent;
import eu.lasersenigma.schematic.exception.PasteOutsideWorldException;
import eu.lasersenigma.schematic.exception.SchematicInvalidException;
import eu.lasersenigma.schematic.exception.SchematicUnableToSaveException;
import eu.lasersenigma.schematic.exception.WorldEditNotAvailableException;
import eu.lasersenigma.schematic.task.CreateAreaComponentsFromSchematicTask;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.*;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public class SchematicManager {

    public static final String SCHEMATIC_VERSION = "1.1";

    public static final int NB_MAXIMUM_BLOCK = 1000000000;

    public static final String SCHEMATIC_FORLDER_NAME = "schematics";

    public static final String SCHEMATIC_EXTENSION = ".leschem";

    public static boolean deleteSchematic(String schematicFileName) throws SchematicInvalidException {
        File schematicFile = getSchematicFile(schematicFileName, true, false);
        return schematicFile.delete();
    }

    public static Schematic createSchematic(Player player) throws WorldEditNotAvailableException, SelectionNotCuboidException, SelectionOverlapPartiallyAreaException, InvalidSelectionException, SchematicUnableToSaveException {
        Schematic schematic = new Schematic();
        WECopy copiedData = WorldEditHelper.worldEditCopy(player);
        schematic.setVersion(SCHEMATIC_VERSION);
        schematic.setAreasSchematic(AreasSchematicManager.createSchematic(copiedData.getPlayerLocation(), copiedData.getMinimumLocation(), copiedData.getMaximumLocation()));
        schematic.setWorleditSchematic(copiedData.getBytes());
        return schematic;
    }

    public static Schematic createSchematic(Player player, Location min, Location max) throws WorldEditNotAvailableException, SelectionNotCuboidException, SelectionOverlapPartiallyAreaException, InvalidSelectionException, SchematicUnableToSaveException {
        Schematic schematic = new Schematic();
        WECopy copiedData = WorldEditHelper.worldEditCopy(player, min, max);
        schematic.setVersion(SCHEMATIC_VERSION);
        schematic.setAreasSchematic(AreasSchematicManager.createSchematic(
                copiedData.getPlayerLocation(),
                copiedData.getMinimumLocation(),
                copiedData.getMaximumLocation())
        );
        schematic.setWorleditSchematic(copiedData.getBytes());
        return schematic;
    }

    public static void pasteSchematic(Player player, Schematic schematic) throws WorldEditNotAvailableException, PasteOutsideWorldException, AreaOverlapException, InvalidSelectionException, SchematicInvalidException {
        WEPaste wePaste = WorldEditHelper.worldEditPaste(player, schematic.getWorleditSchematic());
        wePaste.initializePaste();
        List<CreateAreaComponentsFromSchematicTask> createAreasComponentsTasks = AreasSchematicManager.createAreas(schematic.getAreasSchematic(), wePaste.getPlayerLocation());
        wePaste.finalizePaste();
        cleanArmorStandEntities(schematic.getAreasSchematic(), wePaste.getPlayerLocation());
        createComponents(createAreasComponentsTasks);
    }

    public static Schematic importSchematic(String schematicFileName) throws SchematicInvalidException {
        try {
            FileInputStream fis = new FileInputStream(getSchematicFile(schematicFileName, true, false));
            BufferedInputStream bis = new BufferedInputStream(fis);
            Schematic schematic;
            try (ObjectInputStream ois = new ObjectInputStream(bis)) {
                try {
                    schematic = (Schematic) ois.readObject();
                    if (!schematic.getVersion().equals(SCHEMATIC_VERSION)) {
                        throw new SchematicInvalidException();
                    }
                } catch (ClassNotFoundException ex) {
                    LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, null, ex);
                    throw new SchematicInvalidException();
                }
            }
            return schematic;
        } catch (IOException e) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, MessageFormat.format("Error writting schematic file ({0}):", schematicFileName), e);
            throw new SchematicInvalidException();
        }
    }

    public static void exportSchematic(Schematic schematic, String schematicFileName) throws SchematicUnableToSaveException, SchematicInvalidException {

        try (FileOutputStream fos = new FileOutputStream(getSchematicFile(schematicFileName, false, true));
             BufferedOutputStream bos = new BufferedOutputStream(fos);
             ObjectOutputStream oos = new ObjectOutputStream(bos);) {
            oos.writeObject(schematic);

        } catch (IOException e) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, MessageFormat.format("Error writting schematic file ({0}):", schematicFileName), e);
            throw new SchematicUnableToSaveException();
        }
    }

    public static List<String> list() {
        return Arrays.stream(getSchematicFolder().list())
                .filter(file -> file.endsWith(SCHEMATIC_EXTENSION)).toList();
    }

    private static File getSchematicFolder() {
        File schematicFolder = new File(LasersEnigmaPlugin.getInstance().getDataFolder(), SCHEMATIC_FORLDER_NAME);
        if (!schematicFolder.exists()) {
            schematicFolder.mkdirs();
        }
        return schematicFolder;
    }

    private static File getSchematicFile(String schematicFileName, boolean checkExist, boolean create) throws SchematicInvalidException {
        File schematic = new File(getSchematicFolder(), schematicFileName + SCHEMATIC_EXTENSION);
        if (schematic.exists() && schematic.isDirectory()) {
            throw new SchematicInvalidException();
        }
        if (checkExist) {
            if (!schematic.exists()) {
                throw new SchematicInvalidException();
            }
        }
        if (create) {
            try {
                schematic.createNewFile();
            } catch (IOException ex) {
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, MessageFormat.format("Error creating schematic file ({0}):", schematicFileName), ex);
                throw new SchematicInvalidException();
            }
        }
        return schematic;
    }

    public static String getSchematicDisplayName(String schematicFileName) {
        return new StringBuilder(SCHEMATIC_FORLDER_NAME)
                .append(File.separator)
                .append(schematicFileName)
                .append(SCHEMATIC_EXTENSION).toString();
    }

    private static void cleanArmorStandEntities(AreasSchematic schematic, Location playerLocation) {
        Location min = AreasSchematicManager.getMinLocation(schematic, playerLocation);
        Location max = AreasSchematicManager.getMaxLocation(schematic, playerLocation);
        playerLocation.getWorld().getEntities()
                .stream()
                .filter(e -> e.getCustomName() != null && e.getCustomName() != null && e.getCustomName().equals(AArmorStandComponent.ARMOR_STAND_CUSTOM_NAME) && Area.containsLocation(e.getLocation(), min, max))
                .forEach(e -> e.remove());
    }

    private static void createComponents(List<CreateAreaComponentsFromSchematicTask> createAreasComponentsTasks) {
        createAreasComponentsTasks.forEach(task -> task.runTask(LasersEnigmaPlugin.getInstance()));
    }
}
