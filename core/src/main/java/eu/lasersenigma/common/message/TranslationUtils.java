package eu.lasersenigma.common.message;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.common.exception.AbstractLasersException;
import fr.skytale.translationlib.player.PlayerLangManager;
import fr.skytale.translationlib.translation.TranslationManager;
import fr.skytale.translationlib.translation.json.data.Language;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;

/**
 * Utility class for handling and sending messages.
 */
public class TranslationUtils {

    private static final TranslationManager translationManager = LasersEnigmaPlugin.getInstance().getTranslationManager();
    private static final PlayerLangManager playerLangManager = LasersEnigmaPlugin.getInstance().getPlayerLangManager();

    public static String[] toArray(Location l) {
        return new String[]{Integer.toString(l.getBlockX()), Integer.toString(l.getBlockY()), Integer.toString(l.getBlockZ())};
    }

    public static Object[] fromLocation(Player player, Location location) {
        return new String[]{Integer.toString(location.getBlockX()),
                Integer.toString(location.getBlockY()),
                Integer.toString(location.getBlockZ())};
    }

    public static String fromBoolean(Player player, boolean b) {
        Language language = playerLangManager.getPlayerLang(player.getUniqueId());
        String code;
        if (b) {
            code = "messages.activated";
        } else {
            code = "messages.deactivated";
        }
        return translationManager.getTranslation(language, code);
    }

    public static String getTranslation(String code, Object... params) {
        return translationManager.getTranslation(translationManager.getDefaultLang(), code, params);
    }

    public static String getTranslation(CommandSender commandSender, String code, Object... params) {
        Language language;
        if (commandSender != null && commandSender instanceof Player player) {
            language = playerLangManager.getPlayerLang(player.getUniqueId());
        } else {
            language = translationManager.getDefaultLang();
        }
        return translationManager.getTranslation(language, code, params);
    }

    public static void sendTranslatedMessage(CommandSender commandSender, String code, Object... params) {
        commandSender.sendMessage(getTranslation(commandSender, code, params));
    }

    public static void sendExceptionMessage(CommandSender commandSender, AbstractLasersException ex) {
        final String translation = getTranslation(commandSender, ex.getCode(), ex.getParams());
        commandSender.sendMessage(translation);
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.INFO, commandSender.getName() + " : " + translation);
    }

    public static void sendExceptionMessage(AbstractLasersException ex) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().severe(ex.getMessage(), ex);
    }
}
