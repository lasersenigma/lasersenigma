package eu.lasersenigma.common.dependency.worldedit;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.io.BuiltInClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardWriter;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.world.World;
import eu.lasersenigma.area.exception.InvalidSelectionException;
import eu.lasersenigma.area.exception.SelectionNotCuboidException;
import eu.lasersenigma.schematic.exception.SchematicUnableToSaveException;
import eu.lasersenigma.schematic.exception.WorldEditNotAvailableException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

public class WECopy {

    private final WorldEditPlugin worldEditPlugin;

    private final Player player;
    private final Location playerLocation;
    private byte[] bytes = new byte[0];

    private Location minimumLocation;
    private Location maximumLocation;

    public WECopy(Player player) throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicUnableToSaveException {
        this.worldEditPlugin = this.getWorldEditPlugin();
        this.player = player;
        this.playerLocation = player.getLocation();
        initializeClipboardCopy();
    }

    public WECopy(Player player, Location min, Location max) throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicUnableToSaveException {
        this.worldEditPlugin = this.getWorldEditPlugin();
        this.player = player;
        this.playerLocation = min.clone();
        initializeClipboardCopy(min, max);
    }

    public Location getMinimumLocation() {
        return minimumLocation;
    }

    public Location getMaximumLocation() {
        return maximumLocation;
    }

    public Location getPlayerLocation() {
        return playerLocation;
    }

    public byte[] getBytes() {
        return bytes;
    }


    private void initializeClipboardCopy(Location min, Location max) throws SelectionNotCuboidException, InvalidSelectionException, SchematicUnableToSaveException  {
        LocalSession session;
        EditSession editSession;
        World world;
        Region selectedRegion;
        BlockArrayClipboard clipboard;
        ForwardExtentCopy copy;
        world = BukkitAdapter.adapt(Objects.requireNonNull(min.getWorld()));
        editSession = worldEditPlugin.createEditSession(player);
        Region region = new CuboidRegion(world, BukkitAdapter.asBlockVector(min), BukkitAdapter.asBlockVector(max));
        clipboard = new BlockArrayClipboard(region);
        clipboard.setOrigin(BukkitAdapter.asBlockVector(min));
        copy = new ForwardExtentCopy(editSession, region, clipboard, BukkitAdapter.asBlockVector(min));
        copy.setCopyingEntities(false);
        copy.setCopyingBiomes(false);
        editSession = worldEditPlugin.getWorldEdit().newEditSession(region.getWorld());

        this.minimumLocation = min;
        this.maximumLocation = max;

        clipboard.setOrigin(BlockVector3.at(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ()));

        copy = new ForwardExtentCopy(editSession, region, clipboard, region.getMinimumPoint());
        copy.setRemovingEntities(true);
        copy.setCopyingBiomes(false);

        try {
            Operations.complete(copy);
        } catch (WorldEditException ex) {
            Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, null, ex);
            throw new SchematicUnableToSaveException();
        }

        ByteArrayOutputStream baos = null;
        ClipboardWriter writer = null;
        GZIPOutputStream gzos = null;
        BufferedOutputStream bos = null;
        boolean error = false;
        try {
            baos = new ByteArrayOutputStream();
            gzos = new GZIPOutputStream(baos);
            bos = new BufferedOutputStream(gzos);
            writer = BuiltInClipboardFormat.SPONGE_SCHEMATIC.getWriter(gzos);
            writer.write(clipboard);
            writer.close();
            bos.close();
            gzos.close();
            baos.close();
            this.bytes = baos.toByteArray();
        } catch (IOException e) {
            Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "An error occurred while saving schematic.", e);
            error = true;
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }

            if (gzos != null) {
                try {
                    gzos.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }

            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }

            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }
        }

        if (this.bytes.length == 0) {
            Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "An error occurred while saving schematic.");
            throw new SchematicUnableToSaveException();
        }

        if (error) {
            throw new SchematicUnableToSaveException();
        }
    }
    private void initializeClipboardCopy() throws SelectionNotCuboidException, InvalidSelectionException, SchematicUnableToSaveException {
        LocalSession session;
        EditSession editSession;
        World world;
        Region selectedRegion;
        BlockArrayClipboard clipboard;
        ForwardExtentCopy copy;

        session = worldEditPlugin.getSession(player);
        world = session.getSelectionWorld();
        try {
            selectedRegion = session.getSelection(world);
        } catch (IncompleteRegionException e) {
            throw new InvalidSelectionException();
        }

        // Check if a cuboid region exist
        if (!(selectedRegion instanceof CuboidRegion)) {
            throw new SelectionNotCuboidException();
        }

        this.minimumLocation = new Location(player.getWorld(), selectedRegion.getMinimumPoint().getX(), selectedRegion.getMinimumPoint().getY(), selectedRegion.getMinimumPoint().getZ());
        this.maximumLocation = new Location(player.getWorld(), selectedRegion.getMaximumPoint().getX(), selectedRegion.getMaximumPoint().getY(), selectedRegion.getMaximumPoint().getZ());

        clipboard = new BlockArrayClipboard(selectedRegion);
        editSession = worldEditPlugin.getWorldEdit().newEditSession(selectedRegion.getWorld());

        clipboard.setOrigin(BlockVector3.at(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ()));

        copy = new ForwardExtentCopy(editSession, selectedRegion, clipboard, selectedRegion.getMinimumPoint());
        copy.setRemovingEntities(true);
        copy.setCopyingBiomes(false);

        try {
            Operations.complete(copy);
        } catch (WorldEditException ex) {
            Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, null, ex);
            throw new SchematicUnableToSaveException();
        }

        ByteArrayOutputStream baos = null;
        ClipboardWriter writer = null;
        GZIPOutputStream gzos = null;
        BufferedOutputStream bos = null;
        boolean error = false;
        try {
            baos = new ByteArrayOutputStream();
            gzos = new GZIPOutputStream(baos);
            bos = new BufferedOutputStream(gzos);
            writer = BuiltInClipboardFormat.SPONGE_SCHEMATIC.getWriter(gzos);
            writer.write(clipboard);
            writer.close();
            bos.close();
            gzos.close();
            baos.close();
            this.bytes = baos.toByteArray();
        } catch (IOException e) {
            Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "An error occurred while saving schematic.", e);
            error = true;
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }

            if (gzos != null) {
                try {
                    gzos.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }

            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }

            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ex) {
                    Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }
        }

        if (this.bytes.length == 0) {
            Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "An error occurred while saving schematic.");
            throw new SchematicUnableToSaveException();
        }

        if (error) {
            throw new SchematicUnableToSaveException();
        }
    }

    protected final WorldEditPlugin getWorldEditPlugin() throws WorldEditNotAvailableException {
        WorldEditPlugin worldEditPlugin = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        if (worldEditPlugin == null) {
            throw new WorldEditNotAvailableException();
        }
        return worldEditPlugin;
    }
}
