package eu.lasersenigma.common.dependency.dungeonsxl;

import de.erethon.dungeonsxl.api.event.world.ResourceWorldInstantiateEvent;
import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Areas;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

/**
 * EventListener for SongEndEvent
 */
public class ResourceWorldInstantiateDungeonsXLListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public ResourceWorldInstantiateDungeonsXLListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a DungeonsXL ResourceWorldInstantiateEvent
     * On INSTANCE / EDIT session creation. This is called BEFORE the world is loaded.
     *
     * @param e a ResourceWorldInstantiateEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onResourceWorldInstantiateEvent(ResourceWorldInstantiateEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onResourceWorldInstantiateEvent");
        Areas.getInstance().copyWorldData(e.getResource().getName(), e.getInstanceWorldName(), true);
    }
}
