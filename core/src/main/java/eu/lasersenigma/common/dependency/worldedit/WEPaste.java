package eu.lasersenigma.common.dependency.worldedit;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.BuiltInClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.sk89q.worldedit.util.io.Closer;
import com.sk89q.worldedit.world.World;
import eu.lasersenigma.area.exception.InvalidSelectionException;
import eu.lasersenigma.schematic.exception.SchematicInvalidException;
import eu.lasersenigma.schematic.exception.WorldEditNotAvailableException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

public class WEPaste {

    public static final int NB_MAXIMUM_BLOCK = 1_000_000_000;

    private final WorldEditPlugin worldEditPlugin;
    private final World world;

    private final Player player;
    private final Location playerLocation;
    private final byte[] dataToPaste;

    private Clipboard clipboard;

    public WEPaste(Player player, byte[] dataToPaste) throws WorldEditNotAvailableException {
        this.worldEditPlugin = getWorldEditPlugin();
        this.world = worldEditPlugin.wrapPlayer(player).getWorld();

        this.player = player;
        this.playerLocation = player.getLocation();
        this.dataToPaste = dataToPaste;
    }

    public Location getPlayerLocation() {
        return this.playerLocation;
    }

    public boolean initializePaste() throws InvalidSelectionException, SchematicInvalidException {

        ClipboardFormat format = BuiltInClipboardFormat.SPONGE_SCHEMATIC;
        Closer closer = Closer.create();

        try {
            ByteArrayInputStream bas = closer.register(new ByteArrayInputStream(dataToPaste));
            GZIPInputStream gis = closer.register(new GZIPInputStream(bas));
            BufferedInputStream bis = closer.register(new BufferedInputStream(gis));
            ClipboardReader reader = format.getReader(bis);
            clipboard = reader.read();
            closer.close();
            return true;
        } catch (IOException e) {
            Logger.getLogger(WECopy.class.getName()).log(Level.SEVERE, "An error occurred while restoring schematic.", e);
            try {
                closer.close();
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
            throw new SchematicInvalidException();
        }
    }

    public void finalizePaste() throws SchematicInvalidException {
        try (EditSession editSession = worldEditPlugin.getWorldEdit().newEditSession(world)) {
            Operation operation = new ClipboardHolder(clipboard)
                    .createPaste(editSession)
                    .copyEntities(false)
                    .to(BlockVector3.at(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ()))
                    .build();
            Operations.complete(operation);
        } catch (WorldEditException ex) {
            Logger.getLogger(WECopy.class.getName()).log(Level.WARNING, "Could not paste schematic {0}", new Object[]{ex.getMessage()});
            throw new SchematicInvalidException();
        }
    }

    protected final WorldEditPlugin getWorldEditPlugin() throws WorldEditNotAvailableException {
        WorldEditPlugin worldEditPlugin = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        if (worldEditPlugin == null) {
            throw new WorldEditNotAvailableException();
        }
        return worldEditPlugin;
    }

}