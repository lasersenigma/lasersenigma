package eu.lasersenigma.common.dependency.dungeonsxl;

import de.erethon.dungeonsxl.api.event.world.EditWorldSaveEvent;
import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Areas;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

/**
 * EventListener for SongEndEvent
 */
public class EditWorldSaveDungeonsXLListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public EditWorldSaveDungeonsXLListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a DungeonsXL EditWorldSaveEvent
     * On EDIT session save.
     *
     * @param e a EditWorldSaveEvent
     */

    @EventHandler(ignoreCancelled = false)
    public void onEditWorldSaveEvent(EditWorldSaveEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onEditWorldSaveEvent");
        Areas.getInstance().copyWorldData(e.getInstance().getWorld().getName(), e.getEditWorld().getName(), true);
    }
}
