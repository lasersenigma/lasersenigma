package eu.lasersenigma.common.dependency.worldedit;

import eu.lasersenigma.area.exception.InvalidSelectionException;
import eu.lasersenigma.area.exception.SelectionNotCuboidException;
import eu.lasersenigma.schematic.exception.SchematicUnableToSaveException;
import eu.lasersenigma.schematic.exception.WorldEditNotAvailableException;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class WorldEditHelper {

    public static WECopy worldEditCopy(Player player) throws WorldEditNotAvailableException, SelectionNotCuboidException, InvalidSelectionException, SchematicUnableToSaveException {
        return new WECopy(player);
    }

    public static WECopy worldEditCopy(Player player, Location min, Location max) throws WorldEditNotAvailableException, SelectionNotCuboidException, SchematicUnableToSaveException, InvalidSelectionException {
        return new WECopy(player, min, max);
    }

    public static WEPaste worldEditPaste(Player player, byte[] worldeditSchematic) throws WorldEditNotAvailableException {
        return new WEPaste(player, worldeditSchematic);
    }
}
