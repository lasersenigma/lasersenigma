package eu.lasersenigma.common.dependency.dungeonsxl;

import de.erethon.dungeonsxl.api.event.world.InstanceWorldPostUnloadEvent;
import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Areas;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

/**
 * EventListener for SongEndEvent
 */
public class InstanceWorldPostUnloadEventDungeonsXLListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public InstanceWorldPostUnloadEventDungeonsXLListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a DungeonsXL InstanceWorldPostUnloadEvent
     * On INSTANCE / EDIT session deletion. This is called AFTER the world is unloaded.
     *
     * @param e a InstanceWorldPostUnloadEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onInstanceWorldPostUnloadEvent(InstanceWorldPostUnloadEvent e) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onInstanceWorldPostUnloadEvent");
        Areas.getInstance().removeWorldData(e.getInstanceWorldName());
    }
}
