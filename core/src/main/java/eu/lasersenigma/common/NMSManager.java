package eu.lasersenigma.common;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.nms.ICoreNMS;
import org.bukkit.Bukkit;

import java.util.logging.Level;

public class NMSManager {

    private final static String SERVER_PACKAGE_PATH = Bukkit.getServer().getClass().getPackage().getName();
    private final static String SERVER_PACKAGE_VERSION = SERVER_PACKAGE_PATH.substring(SERVER_PACKAGE_PATH.lastIndexOf('.') + 1);
    private final static ICoreNMS NMS = _getNMS();

    private static ICoreNMS _getNMS() {
        try {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.INFO, "Trying to Load: {0}", SERVER_PACKAGE_VERSION);
            return (ICoreNMS) Class.forName("eu.lasersenigma.nms." + SERVER_PACKAGE_VERSION + ".NMS").newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "Failed to load NMS for this server version (expected package: eu.lasersenigma.nms." + SERVER_PACKAGE_VERSION + ") ", ex);
            return null;
        }
    }

    public static ICoreNMS getNMS() {
        return NMS;
    }

    public static boolean hasNMS() {
        return NMS != null;
    }
}
