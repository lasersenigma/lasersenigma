package eu.lasersenigma.common.config;

import eu.lasersenigma.LasersEnigmaPlugin;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * @author Benjamin
 */
public class ConfigData {

    /**
     * the path to the selected language in the configuration
     */
    public static final String LANGUAGE = "language";

    public static final String CONFIG_HEADER = "\n"
            + "     ██╗      █████╗ ███████╗███████╗██████╗ ███████╗      ███████╗███╗   ██╗██╗ ██████╗ ███╗   ███╗ █████╗ \n"
            + "     ██║     ██╔══██╗██╔════╝██╔════╝██╔══██╗██╔════╝      ██╔════╝████╗  ██║██║██╔════╝ ████╗ ████║██╔══██╗\n"
            + "     ██║     ███████║███████╗█████╗  ██████╔╝███████╗█████╗█████╗  ██╔██╗ ██║██║██║  ███╗██╔████╔██║███████║\n"
            + "     ██║     ██╔══██║╚════██║██╔══╝  ██╔══██╗╚════██║╚════╝██╔══╝  ██║╚██╗██║██║██║   ██║██║╚██╔╝██║██╔══██║\n"
            + "     ███████╗██║  ██║███████║███████╗██║  ██║███████║      ███████╗██║ ╚████║██║╚██████╔╝██║ ╚═╝ ██║██║  ██║\n"
            + "     ╚══════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝╚══════╝      ╚══════╝╚═╝  ╚═══╝╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝\n"
            + "\n"
            + "   ================================     ======================================     ==============================\n"
            + "-> Website: http://lasers-enigma.eu <-> Discord: https://discord.gg/z677bNY84q <-> Mail: contact@lasers-enigma.eu <-\n"
            + "   ================================     ======================================     ==============================\n"
            + "\n"
            + "\n"
            + "language :\n"
            + "  en, fr, de and ru translations are available but you can also create your own.\n"
            + "  /!\\ Restart the server 2 times after any change of this config option.\n"
            + "only_needed_colors:\n"
            + "  false: on every components that test the color(s) of received laser(s) (laser receivers and melting clay),\n"
            + "         the test will be successful if AT LEAST the needed colors are received.\n"
            + "         => a green laser receiver will be activated if it receives a white laser.\n"
            + "  true: the test will be successful if EXACTLY the needed colors are received AND ONLY THOSE needed colors.\n"
            + "         => a green laser receiver will not be activated if it receives a white laser.only be activated with: \n"
            + "            a green laser receiver will only be activated if it receives a green laser or blue laser + yellow laser\n"
            + "music_blocks:\n"
            + "  activates or deactivate music blocks\n"
            + "music_blocks_scoreboards:\n"
            + "  activates or deactivate the scoreboard used to scroll the sound list\n"
            + "show_stats_on_player_left_area:\n"
            + "  activates or deactivate the scores display when a player leaves an area\n"
            + "save_victory_even_if_player_changed_world:\n"
            + "  activates the saving of the player's victory inside the area (and its record) even if he left by changing world.\n"
            + "save_victory_even_if_player_teleported:\n"
            + "  activates the saving of the player's victory inside the area (and its record) even if he teleported more than 50 blocks away.\n"
            + "laser_affect_mobs:\n"
            + "  activates or deactivate the fact touching a laser will affect mobs (monsters and animals)\n"
            + "laser_affect_players:\n"
            + "  activates or deactivate the fact touching a laser will affect players\n"
            + "laser_burns_tick_duration:\n"
            + "  changes the burning duration (in ticks) of entities touching a laser. Set to 0 to deactivate.\n"
            + "laser_additional_damage:\n"
            + "  adds additional damages that will be taken by an entity touching a laser (in addition to the damage done by the burning). Set to 0 to deactivate.\n"
            + "laser_knockback_multiplier:\n"
            + "  Changes the intensity of the knockback taken by an entity when it touches a laser. Set to 0 to deactivate.\n"
            + "debug_level:\n"
            + "  Defines how much informations will be wrote on the console.\n"
            + "  Possible values: OFF|SEVERE|WARNING|INFO|FINE|FINER|FINEST|ALL\n"
            + "opening_locks_consumes_keys:\n"
            + "  When on false, players will keep their keys even if they open a lock using them.\n"
            + "  So they will be able to open the lock again if they enter in the same area again later.\n"
            + "clear_keys_on_exit_world:\n"
            + "  When activated, players will loose their keys whenever they exit the world.\n"
            + "clear_keys_on_death:\n"
            + "  When activated, players will loose their keys whenever they die.\n"
            + "clear_keys_on_quit:\n"
            + "  When activated, players will loose their keys whenever they quit the server.\n"
            + "use_checkpoint_system:\n"
            + "  Deactivates all the checkpoint system.\n"
            + "checkpoint_in_victory_area:\n"
            + "  Puts a new checkpoint in the victory area for every players entering it.\n"
            + "player_respawn_event_priority, player_changed_world_event_priority and player_join_event_priority:\n"
            + "  Defines if the plugin will overide other plugin teleportations/respawn systems.\n"
            + "  Put highest if you want to be sure this plugin will get the priority.\n"
            + "  Possible values: LOWEST|LOW|NORMAL|HIGH|HIGHEST\n"
            + "get_back_to_checkpoint_on_join:\n"
            + "  Defines if, when joining the server, you will get back on the last checkpoint you reached.\n"
            + "  Possible values: true|only_to_normal_checkpoints|only_to_victory_checkpoints|false|per_world_true|per_world_only_to_normal_checkpoints|per_world_only_to_victory_checkpoints\n"
            + "get_back_to_checkpoint_on_death:\n"
            + "  When Dying you will get back on the last checkpoint you reached.\n"
            + "  Possible values: true|only_to_normal_checkpoints|only_to_victory_checkpoints|false\n"
            + "get_back_to_checkpoint_on_change_world:\n"
            + "  When changing world you will get back on the last checkpoint you reached in this world.\n"
            + "  Possible values: true|only_to_normal_checkpoints|only_to_victory_checkpoints|false\n"
            + "clear_checkpoints_on_quit:\n"
            + "  Removes all the players checkpoints when they leaves the server.\n"
            + "clear_world_checkpoints_on_exit_world:\n"
            + "  Removes the players checkpoints of the current world when a player leaves it.\n"
            + "armor_no_knockback_tag:\n"
            + "  Lasers knockback will be ignored if you carry a chestplate with this text in the lore/tag.\n"
            + "armor_no_burn_tag:\n"
            + "  Lasers burning will be ignored if you carry a chestplate with this text in the lore/tag.\n"
            + "armor_no_damage_tag:\n"
            + "  Lasers damages will be ignored if you carry a chestplate with this text in the lore/tag (except burning damages).\n"
            + "armor_reflect_tag:\n"
            + "  Lasers will be reflected on you if you carry a chestplate with this text in the lore/tag.\n"
            + "armor_prism_tag:\n"
            + "  Lasers will be divided into many colors if you carry a chestplate with this text in the lore/tag.\n"
            + "armor_focus_tag:\n"
            + "  Lasers will be redirected if you carry a chestplate with this text in the lore/tag.\n"
            + "armor_lasers_reduces_durability_tag:\n"
            + "   By defining adding this text inside a chestplate's lore, you will be able to modify the consequences a laser hit will have on the chestplate's durability.\n"
            + "   In order for the lasers to have no impact on the chestplate's durability (= infinite durability) replace X by 0.\n"
            + "   In order for the lasers to have a very hight impact on the chestplate's durability (= low durability) replace X by 1.\n"
            + "   In order for the lasers to have a very low impact on the chestplate's durability (= big durability) replace X by 9.\n"
            + "   By default every armors (with no specific lore/tag) durability get impacted by the lasers hit. Their resistance level is the same as if you would have put X = 3.\n"
            + "check_for_new_versions:\n"
            + "   Notify administrator in console and ingame whenever a new version of the plugin is available.\n"
            + "check_for_new_beta_versions:\n"
            + "   Notify administrator in console and ingame whenever a new beta version of the plugin is available.\n"
            + "crossable_materials:\n"
            + "   The list of materials the laser can pass through.\n"
            + "laser_light:\n"
            + "   If LightAPI is within the plugins folder, lasers will illuminate the surrounding blocks, unless this variable is set to false.\n"
            + "laser_default_light_level:\n"
            + "   In Minecraft, the brightness level of a block can vary between 0 (absolute darkness) and 15 (maximum brightness).\n"
            + "   Thus, the value entered here defines the brightness level of the blocks through which the laser will pass.\n"
            + "   Caution: Increasing this brightness necessarily has an impact on performance.\n"
            + "clear_inventory_on_enter_area:\n"
            + "   If you want players to loose their inventory when entering puzzle areas, you can activate this option.\n"
            + "   They will get back their original inventory when leaving the puzzle area.\n"
            + "component_blocked_message:\n"
            + "   Set to true in order to send a message to players who try to interact with blocked components.\n"
            + "music_blocks_sound_category:\n"
            + "   The channel where the music blocks songs will be played (possible values: MASTER,MUSIC,RECORDS,WEATHER,BLOCKS,HOSTILE,NEUTRAL,PLAYERS,AMBIENT,VOICE).\n"
            + "other_sounds_sound_category:\n"
            + "   The channel where the sounds unrelated to music blocks will be played (possible values: MASTER,MUSIC,RECORDS,WEATHER,BLOCKS,HOSTILE,NEUTRAL,PLAYERS,AMBIENT,VOICE).\n";


    public static final String CONFIG_TRANSLATION_HEADER = "\n"
            + "     ██╗      █████╗ ███████╗███████╗██████╗ ███████╗      ███████╗███╗   ██╗██╗ ██████╗ ███╗   ███╗ █████╗ \n"
            + "     ██║     ██╔══██╗██╔════╝██╔════╝██╔══██╗██╔════╝      ██╔════╝████╗  ██║██║██╔════╝ ████╗ ████║██╔══██╗\n"
            + "     ██║     ███████║███████╗█████╗  ██████╔╝███████╗█████╗█████╗  ██╔██╗ ██║██║██║  ███╗██╔████╔██║███████║\n"
            + "     ██║     ██╔══██║╚════██║██╔══╝  ██╔══██╗╚════██║╚════╝██╔══╝  ██║╚██╗██║██║██║   ██║██║╚██╔╝██║██╔══██║\n"
            + "     ███████╗██║  ██║███████║███████╗██║  ██║███████║      ███████╗██║ ╚████║██║╚██████╔╝██║ ╚═╝ ██║██║  ██║\n"
            + "     ╚══════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝╚══════╝      ╚══════╝╚═╝  ╚═══╝╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝\n"
            + "\n"
            + "   ================================     ===================================     ==============================\n"
            + "-> Website: http://lasers-enigma.eu <-> Discord: https://discord.gg/8naYYE5 <-> Mail: contact@lasers-enigma.eu <-\n"
            + "   ================================     ===================================     ==============================\n"
            + "\n"
            + "\n";

    public static void initializeConfig() {

        FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();

        config.addDefault("language", "en");
        config.addDefault("only_needed_colors", true);
        config.addDefault("check_for_new_versions", true);
        config.addDefault("check_for_new_beta_versions", false);
        config.addDefault("music_blocks", true);
        config.addDefault("music_blocks_scoreboards", true);
        config.addDefault("show_stats_on_player_left_area", true);
        config.addDefault("save_victory_even_if_player_teleported", false);
        config.addDefault("save_victory_even_if_player_changed_world", false);
        config.addDefault("laser_affect_mobs", true);
        config.addDefault("laser_affect_players", true);
        config.addDefault("entities_stops_lasers", true);
        config.addDefault("laser_burns_tick_duration", 40);
        config.addDefault("laser_additional_damage", 1);
        config.addDefault("laser_knockback_multiplier", 0.8);
        config.addDefault("backend", "sqlite");
        config.addDefault("backends.mysql.host", "localhost");
        config.addDefault("backends.mysql.database", "databasename");
        config.addDefault("backends.mysql.user", "myUsername");
        config.addDefault("backends.mysql.password", "myComplicatedPassword");
        config.addDefault("backends.sqlite.file", "database.sqlite");
        config.addDefault("debug_level", "INFO");
        config.addDefault("opening_locks_consumes_keys", true);
        config.addDefault("clear_keys_on_change_world", false);
        config.addDefault("clear_keys_on_death", false);
        config.addDefault("clear_keys_on_quit", false);
        config.addDefault("use_checkpoint_system", true);
        config.addDefault("checkpoint_in_victory_area", true);
        config.addDefault("player_respawn_event_priority", "HIGH");
        config.addDefault("player_changed_world_event_priority", "HIGH");
        config.addDefault("player_join_event_priority", "HIGH");
        config.addDefault("get_back_to_checkpoint_on_join", "per_world_true");
        config.addDefault("get_back_to_checkpoint_on_death", true);
        config.addDefault("get_back_to_checkpoint_on_change_world", true);
        config.addDefault("clear_checkpoints_on_quit", false);
        config.addDefault("clear_world_checkpoints_on_exit_world", false);
        config.addDefault("armor_no_knockback_tag", "le_no_knockback");
        config.addDefault("armor_no_burn_tag", "le_no_burn");
        config.addDefault("armor_no_damage_tag", "le_no_damage");
        config.addDefault("armor_reflect_tag", "le_reflect");
        config.addDefault("armor_prism_tag", "le_prism");
        config.addDefault("armor_focus_tag", "le_focus");
        config.addDefault("armor_lasers_reduces_durability_tag", "le_durabilityX");
        config.addDefault("crossable_materials", "AIR,GLASS,GLASS_PANE,BARRIER,REDSTONE_WIRE,REDSTONE,WATER,LEGACY_STATIONARY_WATER,TRIPWIRE,WATER,CYAN_CARPET,BLACK_CARPET,BLUE_CARPET,BROWN_CARPET,GRAY_CARPET,GREEN_CARPET,LIGHT_BLUE_CARPET,LIGHT_GRAY_CARPET,LIME_CARPET,MAGENTA_CARPET,ORANGE_CARPET,PINK_CARPET,PURPLE_CARPET,RED_CARPET,WHITE_CARPET,YELLOW_CARPET,ACACIA_SLAB,SANDSTONE_SLAB,BIRCH_SLAB,SPRUCE_SLAB,STONE_BRICK_SLAB,STONE_SLAB,BRICK_SLAB,COBBLESTONE_SLAB,DARK_OAK_SLAB,DARK_PRISMARINE_SLAB,JUNGLE_SLAB,NETHER_BRICK_SLAB,OAK_SLAB,PETRIFIED_OAK_SLAB,PRISMARINE_BRICK_SLAB,PURPUR_SLAB,PRISMARINE_SLAB,RED_SANDSTONE_SLAB,QUARTZ_SLAB");
        config.addDefault("laser_light", false);
        config.addDefault("laser_default_light_level", 12);
        config.addDefault("clear_inventory_on_enter_area", false);
        config.addDefault("component_blocked_message", true);
        config.addDefault("music_blocks_sound_category", "MASTER");
        config.addDefault("other_sounds_sound_category", "MASTER");

        config.options().copyDefaults(true);
        config.options().header(ConfigData.CONFIG_HEADER);
        LasersEnigmaPlugin.getInstance().saveConfig();
    }

}
