package eu.lasersenigma.common.items;

/**
 * the item attributes
 */
public enum ItemAttribute {
    TEXTURE,
    MATERIAL,
    COLOR,
    ENCHANT,
    COMPONENT_TYPE,
    ARMOR_ACTION
}
