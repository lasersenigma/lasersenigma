package eu.lasersenigma.common.items;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.common.NMSManager;
import eu.lasersenigma.common.util.ItemUtils;
import eu.lasersenigma.items.ArmorAction;
import fr.skytale.itemlib.item.utils.ItemStackTrait;
import fr.skytale.itemlib.item.utils.ItemStackUtils;
import fr.skytale.itemlib.item.utils.ItemStackWrapper;
import fr.skytale.translationlib.translation.TranslationManager;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * This class handles the itemStack creation
 */
public class ItemsFactory {

    /**
     * the static instance of this class
     */
    private static ItemsFactory instance = null;
    /**
     * Map containing the items
     */
    private final Map<Item, ItemStack> items;

    /**
     * constructor
     */
    private ItemsFactory() {
        TranslationManager translationManager = LasersEnigmaPlugin.getInstance().getTranslationManager();

        this.items = Arrays.stream(Item.values()).collect(
                Collectors.toMap(item -> item, item -> getItemStack(item, translationManager))
        );
    }

    private ItemStack getItemStack(Item item, TranslationManager translationManager) {
        if (item == null) {
            throw new IllegalArgumentException("Error during item initialization: item should not be null.");
        }

        if (translationManager == null) {
            throw new IllegalArgumentException("Error during item initialization: translationManager should not be null.");
        }

        ItemStackWrapper itemStack;
        switch (item.getType()) {
            case HEAD -> {
                itemStack = new ItemStackWrapper(Material.PLAYER_HEAD);
                itemStack.setCustomHead(item.getTexture());
            }
            case BLOCK -> itemStack = new ItemStackWrapper(item.getMaterial());
            default -> throw new UnsupportedOperationException(
                    "Error during item initialization, type is not supported \"" + item.getType() + "\"" +
                            " on item \"" + item.name() + "\""
            );
        }

        if (item.hasNameAndLore()) {
            itemStack.setName(ItemUtils.getItemName(item.getConfigPath()));
            itemStack.setLore(ItemUtils.getItemLore(item.getConfigPath()));
        }

        Boolean enchant = item.getEnchant();
        if (enchant != null && enchant) {
            itemStack.addEnchant(Enchantment.DURABILITY, 1);
            itemStack.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            itemStack.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        }
        return itemStack;
    }

    /**
     * Retrieves the instance of this class
     *
     * @return the instance of this class
     */
    public static ItemsFactory getInstance() {
        if (instance == null) {
            instance = new ItemsFactory();
        }
        return instance;
    }

    /**
     * Reload all items
     */
    public static void reload() {
        instance = new ItemsFactory();
    }

    public static boolean isSimilar(ItemStack item1, ItemStack item2) {
        return ItemStackUtils.compare(
                item1,
                item2,
                ItemStackTrait.TYPE,
                ItemStackTrait.DURABILITY,
                ItemStackTrait.NAME,
                ItemStackTrait.LORE,
                ItemStackTrait.ENCHANTMENTS,
                ItemStackTrait.FLAGS,
                ItemStackTrait.SKULL_TEXTURE
        );
    }

    /**
     * retrieves an itemStack in the cache of this class
     *
     * @param item the item we want to retrieve
     * @return the item
     */
    public ItemStack getItemStack(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Item should not be null");
        }
        ItemStack itemStack = items.get(item);
        if (itemStack == null) {
            throw new IllegalStateException("No ItemStack for item " + item.toString());
        }
        return itemStack.clone();
    }

    public Item getSimilarItem(ItemStack itemStack) {
        if (itemStack == null) {
            return null;
        }
        for (Map.Entry<Item, ItemStack> entry : this.items.entrySet()) {
            if (isSimilar(entry.getValue(), itemStack)) {

                return entry.getKey();
            }
        }
        return null;
    }

    public ItemStack getPlayerStatsItem(UUID key, String loreStr) {
        return new ItemStackWrapper(key)
                .setLore(loreStr.split("\n"));
    }

    public ItemStack getArmorItemStack(boolean noBurn, boolean noDamage, boolean noKnockback, boolean reflection, boolean focus, boolean prism, Integer durability) {
        String armor_no_knockback_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_knockback_tag");
        String armor_no_burn_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_burn_tag");
        String armor_no_damage_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_damage_tag");
        String armor_reflect_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_reflect_tag");
        String armor_prism_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_prism_tag");
        String armor_focus_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_focus_tag");
        String armor_lasers_durability_regex = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_lasers_reduces_durability_tag");

        return NMSManager.getNMS().getArmorActionProcessor(
                        null, ArmorAction.GIVE_TO_NEAREST, null,
                        noBurn, noDamage, noKnockback, reflection, focus, prism, durability,
                        armor_no_knockback_tag, armor_no_burn_tag, armor_no_damage_tag, armor_reflect_tag, armor_prism_tag, armor_focus_tag, armor_lasers_durability_regex)
                .getArmorItemStack();
    }
}
