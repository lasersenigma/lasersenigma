package eu.lasersenigma.common.items;

/**
 * The item types
 */
public enum ItemType {
    HEAD,
    BLOCK
}
