package eu.lasersenigma.common.command;

import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.common.message.TranslationUtils;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public abstract class LasersCommand extends Command<Player> {

    private final String descriptionTranslationCode;

    public LasersCommand(String name, String descriptionTranslationCode, String... aliases) {
        super(Player.class, name, aliases);
        this.descriptionTranslationCode = descriptionTranslationCode;
    }

    public LasersCommand(String name, String descriptionTranslationCode) {
        super(Player.class, name);
        this.descriptionTranslationCode = descriptionTranslationCode;
    }

    protected abstract boolean execute(Commands commands, Player player, String... strings) throws AbstractLasersException;

    @Override
    protected final boolean process(Commands commands, Player player, String... strings) {
        try {
            return execute(commands, player, strings);
        } catch (Exception e) {
            player.sendMessage(TranslationUtils.getTranslation(player, descriptionTranslationCode));
            return false;
        }
    }

    @Override
    protected final String description(Player player) {
        return TranslationUtils.getTranslation(player, descriptionTranslationCode);
    }
}
