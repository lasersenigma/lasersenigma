package eu.lasersenigma.common.command.event;

import eu.lasersenigma.common.command.ACommandEvent;
import eu.lasersenigma.component.event.IPlayerEvent;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class PlayerTryToUseLasersClipboardCommandEvent extends ACommandEvent implements IPlayerEvent {

    private final Player player;

    public PlayerTryToUseLasersClipboardCommandEvent(Player player, Command cmd, String label, String[] args) {
        super(player, cmd, label, args);
        this.player = player;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

}
