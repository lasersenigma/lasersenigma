package eu.lasersenigma.common.command;

import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.common.message.TranslationUtils;
import fr.skytale.commandlib.Command;
import fr.skytale.commandlib.Commands;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;

public abstract class LasersSenderCommand extends Command<CommandSender> {

    private final String descriptionTranslationCode;

    public LasersSenderCommand(String name, String descriptionTranslationCode, String... aliases) {
        super(CommandSender.class, name, aliases);
        this.descriptionTranslationCode = descriptionTranslationCode;
    }

    public LasersSenderCommand(String name, String descriptionTranslationCode) {
        super(CommandSender.class, name);
        this.descriptionTranslationCode = descriptionTranslationCode;
    }

    protected abstract boolean execute(Commands commands, CommandSender sender, String... strings) throws AbstractLasersException;

    @Override
    protected final boolean process(Commands commands, CommandSender sender, String... strings) {
        try {
            return execute(commands, sender, strings);
        } catch (Exception e) {
            sender.sendMessage(TranslationUtils.getTranslation(sender, descriptionTranslationCode));
            return false;
        }
    }

    @Override
    protected final String description(CommandSender sender) {
        return TranslationUtils.getTranslation(sender, descriptionTranslationCode);
    }
}
