package eu.lasersenigma.common.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public interface ICommandLEEvent {

    public CommandSender getCommandSender();

    public Command getCommand();

    public String getLabel();

    public String[] getArgs();

}
