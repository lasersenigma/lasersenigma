package eu.lasersenigma.common.util;

import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.RedstoneWire;

public class RedstoneUtils {

    public static void powerBlock(Block block, byte power) {
        BlockData blockData = block.getBlockData();
        if (blockData instanceof RedstoneWire redstoneWire) {
            redstoneWire.setPower(power);
            block.setBlockData(redstoneWire);
        }
    }

}
