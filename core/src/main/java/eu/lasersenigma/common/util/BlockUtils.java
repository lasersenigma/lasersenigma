package eu.lasersenigma.common.util;

import org.bukkit.DyeColor;
import org.bukkit.Material;

import java.util.Arrays;

public class BlockUtils {

    private BlockUtils() {
    }

    public static boolean isLightOrVoid(Material type) {
        return switch (type) {
            case LIGHT, AIR, VOID_AIR, CAVE_AIR, STRUCTURE_VOID -> true;
            default -> false;
        };
    }

    public static boolean isConcretePowder(Material type) {
        return switch (type) {
            case WHITE_CONCRETE_POWDER, RED_CONCRETE_POWDER, GREEN_CONCRETE_POWDER, YELLOW_CONCRETE_POWDER,
                    MAGENTA_CONCRETE_POWDER, LIGHT_BLUE_CONCRETE_POWDER, BLACK_CONCRETE_POWDER -> true;
            default -> false;
        };
    }

    public static boolean isGlassOrStainedGlass(Material type) {
        return switch (type) {
            case BLACK_STAINED_GLASS_PANE, BLUE_STAINED_GLASS_PANE, BROWN_STAINED_GLASS_PANE,
                    CYAN_STAINED_GLASS_PANE, GRAY_STAINED_GLASS_PANE, GREEN_STAINED_GLASS_PANE,
                    LIME_STAINED_GLASS_PANE, MAGENTA_STAINED_GLASS_PANE, ORANGE_STAINED_GLASS_PANE,
                    PINK_STAINED_GLASS_PANE, PURPLE_STAINED_GLASS_PANE, RED_STAINED_GLASS_PANE,
                    WHITE_STAINED_GLASS_PANE, YELLOW_STAINED_GLASS_PANE, LIGHT_GRAY_STAINED_GLASS_PANE,
                    LIGHT_BLUE_STAINED_GLASS_PANE, BLACK_STAINED_GLASS, BLUE_STAINED_GLASS,
                    BROWN_STAINED_GLASS, CYAN_STAINED_GLASS, GRAY_STAINED_GLASS,
                    GREEN_STAINED_GLASS, LIME_STAINED_GLASS, MAGENTA_STAINED_GLASS,
                    ORANGE_STAINED_GLASS, PINK_STAINED_GLASS, PURPLE_STAINED_GLASS,
                    RED_STAINED_GLASS, WHITE_STAINED_GLASS, YELLOW_STAINED_GLASS,
                    LIGHT_GRAY_STAINED_GLASS, LIGHT_BLUE_STAINED_GLASS -> true;
            default -> false;
        };
    }

    public static DyeColor getDyeColorFromMaterial(Material material) {
        return Arrays.stream(DyeColor.values())
                .filter(dyeColor -> material.name().startsWith(dyeColor.name()))
                .findAny()
                .orElse(null);
    }
}
