package eu.lasersenigma.common.util;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.common.items.Item;
import fr.skytale.translationlib.translation.TranslationManager;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class ItemUtils {

    private static final TranslationManager translationManager = LasersEnigmaPlugin.getInstance().getTranslationManager();

    private static final int MAX_CHARACTERS_PER_LINE = 35;
    private static final char[] AUTHORIZED_COLOURS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'k', 'l', 'm', 'n', 'o', 'r'};

    private static boolean isAuthorizedColour(char colourCharacter) {
        for (char c : AUTHORIZED_COLOURS) {
            if (c == colourCharacter) return true;
        }
        return false;
    }

    public static String getItemName(String itemConfigPath) {

        if (itemConfigPath.equalsIgnoreCase("-") ||
                itemConfigPath.equalsIgnoreCase("0")
                || itemConfigPath.equalsIgnoreCase("1")
                || itemConfigPath.equalsIgnoreCase("2")
                || itemConfigPath.equalsIgnoreCase("3")
                || itemConfigPath.equalsIgnoreCase("4")
                || itemConfigPath.equalsIgnoreCase("5")
                || itemConfigPath.equalsIgnoreCase("6")
                || itemConfigPath.equalsIgnoreCase("7")
                || itemConfigPath.equalsIgnoreCase("8")
                || itemConfigPath.equalsIgnoreCase("9")) {
            return ChatColor.translateAlternateColorCodes('&', "&r");
        }

        String translation = translationManager.getTranslation(
                translationManager.getDefaultLang(), // TODO: Use player's default lang
                Item.BASE_PATH + itemConfigPath + Item.NAME_PATH);

        return ChatColor.translateAlternateColorCodes('&', translation);
    }

    public static String[] getItemLore(String itemConfigPath) {

        if (itemConfigPath.equalsIgnoreCase("-") ||
                itemConfigPath.equalsIgnoreCase("0")
                || itemConfigPath.equalsIgnoreCase("1")
                || itemConfigPath.equalsIgnoreCase("2")
                || itemConfigPath.equalsIgnoreCase("3")
                || itemConfigPath.equalsIgnoreCase("4")
                || itemConfigPath.equalsIgnoreCase("5")
                || itemConfigPath.equalsIgnoreCase("6")
                || itemConfigPath.equalsIgnoreCase("7")
                || itemConfigPath.equalsIgnoreCase("8")
                || itemConfigPath.equalsIgnoreCase("9")) {
            return new String[]{};
        }
        String translation = translationManager.getTranslation(
                translationManager.getDefaultLang(), // TODO: Use player's default lang
                Item.BASE_PATH + itemConfigPath + Item.DESCRIPTION_PATH);

        return formatItemLore(translation);
    }

    public static String[] formatItemLore(String message) {
        // Lines
        List<String> lines = new ArrayList<>();
        int current_line_characters_count = 0;

        // Current line
        StringBuilder current_line = new StringBuilder();
        int current_word_characters_count;

        // Color management
        char lastColor = 'r';

        // Normalize message
        message = message.replaceAll("\\n", " <nl> ");

        for (String current_word : message.split(" ")) {

            // Handle color detection
            if (current_word.startsWith("&")) {
                lastColor = current_word.charAt(1);
            }

            // Handle new line
            if (current_word.equalsIgnoreCase("<nl>")) {
                lines.add(ChatColor.translateAlternateColorCodes('&', current_line.toString()));
                current_line = new StringBuilder();
                current_line_characters_count = 0;
                continue;
            }

            // Current word's characters count
            current_word_characters_count = current_word.length();

            if (current_word.contains("&")) {
                int pos = current_word.indexOf('&');
                try {
                    char nextChar = current_word.charAt(pos + 1);
                    if (isAuthorizedColour(nextChar)) current_word_characters_count -= 2;
                } catch (IndexOutOfBoundsException ignored) {
                }
            }

            if ((current_line_characters_count + current_word_characters_count) > MAX_CHARACTERS_PER_LINE) {
                // Case when (current line + current word) characters count is higher than limit

                lines.add(ChatColor.translateAlternateColorCodes('&', current_line.toString()));

                // Create a new line with the overflowed word
                current_line = new StringBuilder();
                current_line.append("&");
                current_line.append(lastColor);
                current_line.append(current_word);
                current_line_characters_count = current_word_characters_count;
            } else {
                // Case when we are building the sentence where characters count lower than limit

                if (current_line_characters_count > 0) {
                    current_line.append(" ");
                    current_line_characters_count++;
                }
                current_line.append(current_word);
                current_line_characters_count += current_word_characters_count;
            }
        }

        // Don't forget the final line
        lines.add(ChatColor.translateAlternateColorCodes('&', current_line.toString()));

        return lines.toArray(new String[0]);
    }

}
