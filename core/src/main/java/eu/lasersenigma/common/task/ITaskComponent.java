package eu.lasersenigma.common.task;

/**
 * Used for components that have an associated task
 */
public interface ITaskComponent {

    /**
     * Starts the task
     */
    void startTask();

    /**
     * Cancels the task
     */
    void cancelTask();
}
