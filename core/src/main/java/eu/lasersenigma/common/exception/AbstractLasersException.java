package eu.lasersenigma.common.exception;

import eu.lasersenigma.common.message.TranslationUtils;

public abstract class AbstractLasersException extends Exception {
    private String code;
    private final Object[] params;

    public AbstractLasersException(String translationCode) {
        this(translationCode, new Object[0]);
    }

    public AbstractLasersException(String translationCode, Object... params) {
        super(TranslationUtils.getTranslation(translationCode, params));
        this.code = translationCode;
        this.params = params;
    }

    public String getCode() {
        return code;
    }

    public Object[] getParams() {
        return params;
    }
}
