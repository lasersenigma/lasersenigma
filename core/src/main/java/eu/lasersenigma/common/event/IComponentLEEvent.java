package eu.lasersenigma.common.event;

import eu.lasersenigma.component.IComponent;

public interface IComponentLEEvent {

    IComponent getComponent();

}
