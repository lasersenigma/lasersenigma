package eu.lasersenigma.common.event;

import org.bukkit.event.Cancellable;

public abstract class ABeforeActionEvent extends AEvent implements Cancellable {

    private boolean cancelled = false;

    @Override
    public final boolean isCancelled() {
        return cancelled;
    }

    @Override
    public final void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

}
