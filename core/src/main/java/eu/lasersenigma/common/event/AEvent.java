package eu.lasersenigma.common.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public abstract class AEvent extends Event {

    private static final HandlerList HANDLERS = new HandlerList();

    public final static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public final HandlerList getHandlers() {
        return HANDLERS;
    }

}
