package eu.lasersenigma.common.event;

import eu.lasersenigma.area.Area;

public interface IAreaEvent {

    Area getArea();

}
