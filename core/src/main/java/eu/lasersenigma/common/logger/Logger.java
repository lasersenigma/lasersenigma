package eu.lasersenigma.common.logger;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

/**
 * Wrapper around the logger as the log level features of Bukkit seem to be
 * lacking
 */
public class Logger {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    private final java.util.logging.Logger logger;
    private Level level;

    public Logger(JavaPlugin plugin) {
        this.logger = plugin.getLogger();
        this.level = Level.INFO;
    }

    private static String getLevelColor(Level level) {
        return switch (level.getName()) {
            case "SEVERE" -> ANSI_RED;
            case "WARNING" -> ANSI_YELLOW;
            case "INFO" -> ANSI_GREEN;
            default -> ANSI_BLUE;
        };
    }

    private static String prefix(Level level, String msg) {
        return getLevelColor(level) + "[" + level.getName() + "] " + ANSI_RESET + msg;
    }

    public java.util.logging.Logger getSpleefLogger() {
        return logger;
    }

    public Level getLogLevel() {
        return level;
    }

    public void setLogLevel(Level level) {
        this.level = level;
    }

    public void setLogLevel(String level) {
        setLogLevel(Level.parse(level.toUpperCase()));
    }

    private boolean shouldLog(Level level) {
        return level.intValue() >= this.level.intValue();
    }

    public void log(Level level, String message, Object param1) {
        if (!shouldLog(level)) return;

        logger.log(Level.INFO, prefix(level, message), param1);
    }

    public void log(Level level, String message, Object... params) {
        if (!shouldLog(level)) return;

        logger.log(Level.INFO, prefix(level, message), params);
    }

    public void log(Level level, String message) {
        if (!shouldLog(level)) return;

        logger.log(Level.INFO, prefix(level, message));
    }

    public void fine(String message) {
        log(Level.FINE, message);
    }

    public void fine(String message, Object param1) {
        log(Level.FINE, message, param1);
    }

    public void fine(String message, Object... params) {
        log(Level.FINE, message, params);
    }

    public void finer(String message) {
        log(Level.FINER, message);
    }

    public void finer(String message, Object param1) {
        log(Level.FINER, message, param1);
    }

    public void finer(String message, Object... params) {
        log(Level.FINER, message, params);
    }

    public void finest(String message) {
        log(Level.FINEST, message);
    }

    public void finest(String message, Object param1) {
        log(Level.FINEST, message, param1);
    }

    public void finest(String message, Object... params) {
        log(Level.FINEST, message, params);
    }

    public void info(String message) {
        log(Level.INFO, message);
    }

    public void info(String message, Object param1) {
        log(Level.INFO, message, param1);
    }

    public void info(String message, Object... params) {
        log(Level.INFO, message, params);
    }

    public void warning(String message) {
        log(Level.WARNING, message);
    }

    public void warning(String message, Object param1) {
        log(Level.WARNING, message, param1);
    }

    public void warning(String message, Object... params) {
        log(Level.WARNING, message, params);
    }

    public void severe(String message) {
        log(Level.SEVERE, message);
    }

    public void severe(String message, Object param1) {
        log(Level.SEVERE, message, param1);
    }

    public void severe(String message, Object... params) {
        log(Level.SEVERE, message, params);
    }

    public void warning(String message, Exception err) {
        warning(message);
        if (err != null) {
            warning(err.getMessage());
            err.printStackTrace();
        }
    }

    public void severe(String message, Exception err) {
        severe(message);
        if (err != null) {
            severe(err.getMessage());
            err.printStackTrace();
        }
    }
}
