package eu.lasersenigma.common.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.logger.Logger;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Abstract LEInventory
 */
public abstract class AInventory {

    protected final LEPlayer player;

    protected AInventory(LEPlayer player) {
        this.player = player;
    }

    public static ArrayList<Item> getNumberAsItems(int number, boolean forceZero) {
        if (number < 0) {
            number = 0;
        } else if (number > 99) {
            number = 99;
        }
        ArrayList<Integer> numbers = new ArrayList<>();
        int first = number / 10;
        if (first != 0 || forceZero) {
            numbers.add(first);
        }
        numbers.add(number % 10);
        return numbers.stream()
                .map((Integer n) -> {
                    switch (n) {
                        case 0:
                            return Item.COMPONENT_MENU_ZERO;
                        case 1:
                            return Item.COMPONENT_MENU_ONE;
                        case 2:
                            return Item.COMPONENT_MENU_TWO;
                        case 3:
                            return Item.COMPONENT_MENU_THREE;
                        case 4:
                            return Item.COMPONENT_MENU_FOUR;
                        case 5:
                            return Item.COMPONENT_MENU_FIVE;
                        case 6:
                            return Item.COMPONENT_MENU_SIX;
                        case 7:
                            return Item.COMPONENT_MENU_SEVEN;
                        case 8:
                            return Item.COMPONENT_MENU_EIGHT;
                        case 9:
                            return Item.COMPONENT_MENU_NINE;
                        default:
                            throw new UnsupportedOperationException();
                    }
                }).collect(Collectors.toCollection(ArrayList::new));
    }

    public abstract ItemStack[] getContents();

    public abstract void onClick(Item item);

    public abstract boolean contains(Item item);

    public abstract void updateInventory();

    public abstract IComponent getComponent();

    public abstract Area getArea();

    public boolean isDropable(Item item) {
        return (item == null || (item.isPlacableAsBlock() && item.isPlacableOutsideAnArea()));
    }

    protected final boolean checkPermissionClearInvAndSendMessageOnFail(Permission permission) {
        if (!permission.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().closeOpenedInventory();
            }, 1);
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().exitEditionMode();
            }, 2);
            return false;
        }
        return true;
    }

    public abstract InventoryType getType();

    protected final ArrayList<Item> addEmpty(List<Item> list, int nb) {
        ArrayList<Item> newList = new ArrayList<>(list);
        for (int i = 0; i < nb; i++) {
            newList.add(Item.EMPTY);
        }
        return newList;
    }

    protected final ArrayList<Item> addEmptyUntil(List<Item> list, int finalSize) {
        ArrayList<Item> newList = new ArrayList<>(list);
        while (newList.size() < finalSize) {
            newList.add(Item.EMPTY);
        }
        return newList;
    }

    protected void logInventory(List<? extends List<Item>> inventoryList) {
        Logger logger = LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger();
        inventoryList.stream().map((line) -> {
            String str = line.size() + "| ";
            str = line.stream().map((item) -> item.toString() + " | ").reduce(str, String::concat);
            return str;
        }).forEachOrdered((str) -> {
            logger.log(Level.SEVERE, str);
        });
    }

    protected void logInventoryItemStack(List<List<ItemStack>> inventoryList) {
        Logger logger = LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger();
        inventoryList.stream().map((line) -> {
            String str = line.size() + "| ";
            str = line.stream().map((itemStack) -> {
                if (itemStack.hasItemMeta()) {
                    ItemMeta itemMeta = itemStack.getItemMeta();
                    return itemMeta.hasDisplayName() ? itemMeta.getDisplayName() + " | " : "No DisplayName | ";
                }
                return itemStack.toString() + " | ";
            }).reduce(str, String::concat);
            return str;
        }).forEachOrdered((str) -> {
            logger.log(Level.SEVERE, str);
        });
    }

    public void onClose() {
    }
}
