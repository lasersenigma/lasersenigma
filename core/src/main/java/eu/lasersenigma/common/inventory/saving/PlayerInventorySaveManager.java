package eu.lasersenigma.common.inventory.saving;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;
import java.util.ListIterator;
import java.util.logging.Level;

/* Management of players inventories

This class is made to handle the following test cases

    Not in edition mode => Enter => Leave
     --- Enter area does: item clear if clearInventoryOnEnterArea is enabled
     --- Leaving area: item clear + get back items if clearInventoryOnEnterArea is enabled, else just remove mirrors

    Not in edition mode => Enter => Edition mode => Leave => Exit edition mode
     --- Enter area: item clear (if clearInventoryOnEnterArea is enabled)
     --- Edition mode: item clear + get edition items
     --- Leaving area: Nothing
     --- Exiting edition mode: getting back items lost when entering area if clearInventoryOnEnterArea is enabled, else get back items lost when entering edition mode

    Not in edition mode => Enter => Edition mode => Exit edition mode => Leave
     --- Enter area: item clear if clearInventoryOnEnterArea is enabled
     --- Edition mode: item clear + get edition items
     --- Exiting edition mode: item clear + get back items lost when entering edition mode
     --- Leaving area: item clear + get back items lost when entering area if clearInventoryOnEnterArea is enabled,

    Edition mode => Enter => Leave => Exit edition mode
     --- Edition mode: item clear + get edition items
     --- Enter area: Nothing
     --- Leaving area: Nothing
     --- Leaving edition mode: item clear + get back items lost when entering edition mode

    Edition mode => Enter => Exit edition mode => Leave:
     --- Edition mode: item clear + get edition items
     --- Enter area: Nothing
     --- Exiting edit mode:item clear anyway but DO NOT get back items if clearInventoryOnEnterArea is enabled,
     --- Leaving area: item clear + get back items lost when entering edition mode if clearInventoryOnEnterArea is enabled

    Not in edition mode => Enter => Open rotation ShortcutBar => Exit rotation ShortcutBar => Leave
     --- Enter area: item clear if clearInventoryOnEnterArea is enabled
     --- Rotation ShortcutBar: item clear + get rotation items
     --- Exit Rotation ShortcutBar: item clear + get back items lost when opening rotation ShortcutBar
     --- Leaving area: item clear + get back items lost when entering area if clearInventoryOnEnterArea is enabled,

    Not in edition mode => Enter => Open rotation ShortcutBar => Edition mode => Exit edition mode => Leave
     --- Enter area: item clear if clearInventoryOnEnterArea is enabled
     --- Rotation ShortcutBar: item clear + get rotation items
     --- Edition mode: item clear + get edition items.
     --- Exiting edition mode: item clear + get back items lost when opening rotation ShortcutBar
     --- Leaving area: item clear + get back items lost when entering area if clearInventoryOnEnterArea is enabled,

     Not in edition mode => Enter => Open rotation ShortcutBar => Edition mode => Quit game => Join game
     --- Enter area: item clear if clearInventoryOnEnterArea is enabled
     --- Rotation ShortcutBar: item clear + get rotation items
     --- Edition mode: item clear + get edition items.
     --- Quit + Join: get back initial inventory
 */
public class PlayerInventorySaveManager {

    private static final boolean LOOSE_INVENTORY_ON_ENTER_AREA = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("clear_inventory_on_enter_area", false);
    private final HashMap<PlayerInventorySaveLocation, ItemStack[]> saves;
    private final LEPlayer player;

    public PlayerInventorySaveManager(LEPlayer player) {
        this.saves = new HashMap<>();
        this.player = player;
    }

    private static void clearMirrors(PlayerInventory inventory) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.clearMirrors");
        ListIterator<ItemStack> it = inventory.iterator();
        while (it.hasNext()) {
            ItemStack itemStack = it.next();
            Item.getMirrors()
                    .stream()
                    .map((itemToDelete) -> ItemsFactory.getInstance().getItemStack(itemToDelete))
                    .filter((mirrorItemStack) -> (mirrorItemStack.isSimilar(itemStack)))
                    .forEachOrdered((_item) -> it.set(new ItemStack(Material.AIR)));
        }
    }

    private static ItemStack[] copyInventory(Inventory inv) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.copyInventory");
        ItemStack[] original = inv.getContents();
        ItemStack[] copy = new ItemStack[original.length];
        for (int i = 0; i < original.length; ++i) {
            if (original[i] == null) {
                copy[i] = ItemsFactory.getInstance().getItemStack(Item.EMPTY);
            } else {
                copy[i] = original[i].clone();
            }
        }
        return copy;
    }

    public void onEnterArea() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onEnterArea");
        if (LOOSE_INVENTORY_ON_ENTER_AREA) {
            if (!player.getInventoryManager().isInEditionMode()) {
                saveAndClearInventory(PlayerInventorySaveLocation.OUTSIDE_AREA);
            } else {
                saves.put(PlayerInventorySaveLocation.INSIDE_AREA, createEmptyInventoryItemStacks());
            }
        }
    }

    public void onLeaveArea() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onLeaveArea");
        if (LOOSE_INVENTORY_ON_ENTER_AREA) {
            if (player.getInventoryManager().isInEditionMode()) {
                saves.remove(PlayerInventorySaveLocation.INSIDE_AREA);
            } else {
                getBackSave(PlayerInventorySaveLocation.OUTSIDE_AREA);
            }
        }
        if (player.getBukkitPlayer() != null) {
            clearMirrors(player.getBukkitPlayer().getInventory());
        }
    }

    public void onOpenEditionMode() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onOpenEditionMode");
        if (!player.getInventoryManager().isRotationShortcutBarOpened()) {
            saveAndClearInventory(PlayerInventorySaveLocation.fromIsInArea(LOOSE_INVENTORY_ON_ENTER_AREA && player.isInArea()));
        }
    }

    public void onExitEditionMode() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onExitEditionMode");
        getBackSave(PlayerInventorySaveLocation.fromIsInArea((LOOSE_INVENTORY_ON_ENTER_AREA && player.isInArea())));
    }

    public void onOpenRotationShortcutBar() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onOpenRotationShortcutBar");
        saveAndClearInventory(PlayerInventorySaveLocation.fromIsInArea(LOOSE_INVENTORY_ON_ENTER_AREA));
    }

    public void onExitRotationShortcutBar() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onExitRotationShortcutBar");
        getBackSave(PlayerInventorySaveLocation.fromIsInArea((LOOSE_INVENTORY_ON_ENTER_AREA && player.isInArea())));
    }

    public void onQuit() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onQuit");
    }

    public void onJoin() {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.onJoin");
        if (saves.get(PlayerInventorySaveLocation.OUTSIDE_AREA) != null) {
            getBackSave(PlayerInventorySaveLocation.OUTSIDE_AREA);
        }
        clearMirrors(player.getBukkitPlayer().getInventory());
    }

    public void addToSavedInventory(ItemStack itemStack) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.addToInsideAreaSavedInventory");

        ItemStack[] savedInAreaInventory = saves.get(PlayerInventorySaveLocation.fromIsInArea(LOOSE_INVENTORY_ON_ENTER_AREA));
        for (int i = 0; i < savedInAreaInventory.length; ++i) {
            ItemStack curItemStack = savedInAreaInventory[i];
            if (curItemStack.getType() == Material.AIR) {
                savedInAreaInventory[i] = itemStack;
                break;
            }
        }
    }

    public void addInventoryFromDatabase(ItemStack[] contents) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.addInventoryFromDatabase");
        saves.put(PlayerInventorySaveLocation.OUTSIDE_AREA, contents);
        saves.remove(PlayerInventorySaveLocation.INSIDE_AREA);
    }

    private void saveAndClearInventory(PlayerInventorySaveLocation location) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "PlayerInventorySaveManager.saveAndClearInventory");
        PlayerInventory playerInventory = player.getBukkitPlayer().getInventory();
        ItemStack[] inventoryContent = copyInventory(playerInventory);
        saves.put(location, inventoryContent);
        if (location == PlayerInventorySaveLocation.OUTSIDE_AREA) {
            LasersEnigmaPlugin.getInstance().getPluginDatabase().saveInventory(player.getUniqueId(), player.getInventoryManager().isInEditionMode(), player.getInventoryManager().isRotationShortcutBarOpened(), playerInventory);
        }
        playerInventory.clear();
    }

    private void getBackSave(PlayerInventorySaveLocation location) {
        ItemStack[] inventoryContent = saves.get(location);
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "InventorySaveManager.getBackSave");
        if (player.getBukkitPlayer() == null) {
            //The player disconnected
            return;
        }
        player.getBukkitPlayer().getInventory().clear();
        if (inventoryContent != null) {
            player.getBukkitPlayer().getInventory().setContents(inventoryContent);
        }
        if (location == PlayerInventorySaveLocation.OUTSIDE_AREA) {
            saves.clear();
            LasersEnigmaPlugin.getInstance().getPluginDatabase().clearSavedInventory(player.getUniqueId());
        } else {
            saves.remove(location);
        }
    }

    private ItemStack[] createEmptyInventoryItemStacks() {
        int size = player.getBukkitPlayer().getInventory().getContents().length;
        ItemStack[] content = new ItemStack[size];
        for (int i = 0; i < size; i++) {
            content[i] = new ItemStack(Material.AIR);
        }
        return content;
    }
}
