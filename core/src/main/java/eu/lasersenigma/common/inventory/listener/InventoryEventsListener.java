package eu.lasersenigma.common.inventory.listener;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.logging.Level;

/**
 * EventListener for InventoryClickEvent
 */
public class InventoryEventsListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public InventoryEventsListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onInventoryClickEvent");
        HumanEntity human = event.getWhoClicked();

        if (!(human instanceof Player)) return;

        LEPlayers.getInstance().findLEPlayer((Player) human).onInventoryClick(event);
    }

    @EventHandler(ignoreCancelled = true)
    public void onInventoryClose(InventoryCloseEvent event) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINEST, "onInventoryCloseEvent");

        if (!(event.getPlayer() instanceof Player)) return;

        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer((Player) event.getPlayer());
        lePlayer.getInventoryManager().onCloseInventory();
    }
}
