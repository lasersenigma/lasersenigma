package eu.lasersenigma.common.inventory;

import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Abstract LEInventory for shortcut bars
 */
public abstract class AShortcutBarInventory extends AInventory {

    public AShortcutBarInventory(LEPlayer player) {
        super(player);
    }

    @Override
    public final ItemStack[] getContents() {
        ItemStack[] itemStacks = new ItemStack[9];
        ArrayList<Item> shortcutBarInventory = getShortcutBar();
        if (shortcutBarInventory.size() > 9) {
            throw new IllegalStateException("Too much lines in the inventory.");
        }
        int columnIndex = 0;
        for (Item item : shortcutBarInventory) {
            itemStacks[columnIndex] = ItemsFactory.getInstance().getItemStack(item);
            columnIndex++;
        }
        if (columnIndex < 8) {
            while (columnIndex < 8) {
                itemStacks[columnIndex] = ItemsFactory.getInstance().getItemStack(Item.EMPTY);
                columnIndex++;
            }
        }
        return itemStacks;
    }

    @Override
    public final void updateInventory() {
        ItemStack[] items = getContents();
        Inventory bukkitInventory = player.getBukkitPlayer().getInventory();
        for (int i = 0; i < items.length; i++) {
            bukkitInventory.setItem(i, items[i]);
        }
    }

    protected abstract ArrayList<Item> getShortcutBar();

}
