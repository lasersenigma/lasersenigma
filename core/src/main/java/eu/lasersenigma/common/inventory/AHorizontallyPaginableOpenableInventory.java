package eu.lasersenigma.common.inventory;

import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Abstract LEInventory for openable inventories
 */
public abstract class AHorizontallyPaginableOpenableInventory extends AOpenableInventory {

    protected static final int COLUMN_MAX_SIZE = 5;

    protected static final Item SCROLLBAR_PREV = Item.PREVIOUS;
    protected static final Item SCROLLBAR_NEXT = Item.NEXT;
    protected static final Item SCROLLBAR_INDICATOR = Item.PAGINABLE_INDICATOR;

    private int currentColumnIndex;

    protected AHorizontallyPaginableOpenableInventory(LEPlayer player, String titleMessageCode) {
        this(player, titleMessageCode, 0);
    }

    protected AHorizontallyPaginableOpenableInventory(LEPlayer player, String titleMessageCode, int currentIndex) {
        super(player, titleMessageCode);
        this.currentColumnIndex = currentIndex;
    }

    @Override
    public ItemStack[] getContents() {
        List<List<ItemStack>> columns = getColumnsItemStacks();
        int lastColumnIndex = columns.size() - 1;
        ItemsFactory itemFactory = ItemsFactory.getInstance();

        // Normalize column height
        final int maxColumnHeight = columns.stream().map(List::size).max(Integer::compare).orElse(0);
        if (maxColumnHeight > COLUMN_MAX_SIZE) {
            throw new IllegalStateException("Columns must not be higher than " + COLUMN_MAX_SIZE + " lines.");
        }
        columns.stream()
                .filter(column -> column.size() < maxColumnHeight)
                .forEach(column -> {
                    for (int i = 0; i < maxColumnHeight - column.size(); i++) {
                        column.add(itemFactory.getItemStack(Item.EMPTY));
                    }
                });

        // Build the inventory
        List<List<ItemStack>> content = new ArrayList<>();
        List<List<ItemStack>> columnsToShow = columns.subList(currentColumnIndex, Math.min(currentColumnIndex + 9, columns.size()));

        for (int i = 0; i < maxColumnHeight; i++) {
            content.add(new ArrayList<>());
        }

        columnsToShow.forEach(column -> {
            for (int i = 0; i < column.size(); i++) {
                ItemStack item = column.get(i);
                content.get(i).add(item);
            }
        });

        // addPagination
        addPagination(content, lastColumnIndex);

        // check the result
        int lineIndex = 0;
        ItemStack[] itemStacks = new ItemStack[content.size() * 9];
        if (content.size() > 9) {
            logInventoryItemStack(content);
            throw new IllegalStateException("Too much lines in the inventory.");
        }

        for (List<ItemStack> line : content) {
            if (line.size() > 9) {
                logInventoryItemStack(content);
                throw new IllegalStateException("An inventory line has more than 9 items.");
            }
            int columnIndex = 0;
            for (ItemStack itemStack : line) {
                itemStacks[columnIndex + lineIndex * 9] = itemStack;
                columnIndex++;
            }
            if (columnIndex < 9) {
                while (columnIndex < 9) {
                    itemStacks[columnIndex + lineIndex * 9] = ItemsFactory.getInstance().getItemStack(Item.EMPTY);
                    columnIndex++;
                }
            }
            lineIndex++;
        }
        return itemStacks;
    }

    @Override
    public final List<List<Item>> getOpenableInventory() {
        return null;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case PREVIOUS:
                if (currentColumnIndex > 0) {
                    currentColumnIndex--;
                }
                updateInventory();
                break;
            case NEXT:
                if (currentColumnIndex + 8 < getColumnsItemStacks().size() - 1) {
                    currentColumnIndex++;
                }
                updateInventory();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.PREVIOUS,
                Item.NEXT
        )).contains(item);
    }

    private void addPagination(List<List<ItemStack>> content, int lastColumnIndex) {
        ItemsFactory itemFactory = ItemsFactory.getInstance();
        ArrayList<ItemStack> scrollbarLine = new ArrayList<>();
        content.add(scrollbarLine);
        if (lastColumnIndex <= 8) {
            return;
        }

        Item itemPrevious = (currentColumnIndex > 0) ? Item.PREVIOUS : Item.EMPTY;
        scrollbarLine.add(itemFactory.getItemStack(itemPrevious));

        int slotWhereToPlaceScrollIndicator = Math.round(((float) currentColumnIndex / (lastColumnIndex - 8)) * 7 + 1);
        if (slotWhereToPlaceScrollIndicator < 1) {
            slotWhereToPlaceScrollIndicator = 1;
        } else if (slotWhereToPlaceScrollIndicator > 8) {
            slotWhereToPlaceScrollIndicator = 8;
        }
        for (int i = 1; i < 8; i++) {
            if (slotWhereToPlaceScrollIndicator == i) {
                scrollbarLine.add(itemFactory.getItemStack(Item.PAGINABLE_INDICATOR));
            } else {
                scrollbarLine.add(itemFactory.getItemStack(Item.EMPTY));
            }
        }
        Item itemNext = (currentColumnIndex + 8 < lastColumnIndex) ? Item.NEXT : Item.EMPTY;
        scrollbarLine.add(itemFactory.getItemStack(itemNext));
    }

    protected abstract List<List<ItemStack>> getColumnsItemStacks();
}
