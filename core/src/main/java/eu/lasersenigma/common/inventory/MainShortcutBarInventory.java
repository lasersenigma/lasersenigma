package eu.lasersenigma.common.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.area.AreaController;
import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.exception.NoAreaFoundException;
import eu.lasersenigma.area.inventory.AreaConfigMenuInventory;
import eu.lasersenigma.area.inventory.DeleteAreaConfirmationInventory;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.CrossableMaterials;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.stats.inventory.AreaStatsMenuInventory;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Main edition shortcut bar
 */
public class MainShortcutBarInventory extends AShortcutBarInventory {

    public MainShortcutBarInventory(LEPlayer player) {
        super(player);
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>(Arrays.asList(
                Item.MAIN_SHORTCUTBAR_PLACE_COMPONENT,
                Item.EMPTY,
                Item.MAIN_SHORTCUTBAR_AREA_CONFIG,
                Item.MAIN_SHORTCUTBAR_STATS_MENU,
                Item.EMPTY,
                Item.MAIN_SHORTCUTBAR_CREATE_AREA,
                Item.MAIN_SHORTCUTBAR_DELETE_AREA,
                Item.MAIN_SHORTCUTBAR_ARMOR_MENU,
                Item.MAIN_SHORTCUTBAR_CLOSE
        ));
        return shortcutBar;
    }

    /**
     * @param item an item
     */
    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEMainShortcutBarInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        Area area;
        switch (item) {
            case MAIN_SHORTCUTBAR_AREA_CONFIG:
                area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    break;
                }
                player.getInventoryManager().openLEInventory(new AreaConfigMenuInventory(player, area));
                break;
            case MAIN_SHORTCUTBAR_CLOSE:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().exitEditionMode();
                }, 2);
                break;
            case MAIN_SHORTCUTBAR_CREATE_AREA:
                AreaController.createArea(player, player.getBukkitPlayer().getTargetBlock(CrossableMaterials.getInstance().getNotSelectableMaterials(), 20).getLocation());
                break;
            case MAIN_SHORTCUTBAR_STATS_MENU:
                area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    break;
                }
                player.getInventoryManager().openLEInventory(new AreaStatsMenuInventory(player, area));
                break;
            case MAIN_SHORTCUTBAR_DELETE_AREA:
                area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null) {
                    TranslationUtils.sendExceptionMessage(player.getBukkitPlayer(), new NoAreaFoundException());
                    break;
                }
                player.getInventoryManager().openLEInventory(new DeleteAreaConfirmationInventory(player, area));
                break;
            case MAIN_SHORTCUTBAR_ARMOR_MENU:
                player.getInventoryManager().onArmorMenu();
                break;
            case MAIN_SHORTCUTBAR_PLACE_COMPONENT:
                player.getInventoryManager().onPlaceComponent();
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.MAIN_SHORTCUTBAR_PLACE_COMPONENT,
                Item.MAIN_SHORTCUTBAR_AREA_CONFIG,
                Item.MAIN_SHORTCUTBAR_STATS_MENU,
                Item.MAIN_SHORTCUTBAR_CREATE_AREA,
                Item.MAIN_SHORTCUTBAR_DELETE_AREA,
                Item.MAIN_SHORTCUTBAR_ARMOR_MENU,
                Item.MAIN_SHORTCUTBAR_CLOSE
        )).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return null;
    }

    @Override
    public InventoryType getType() {
        return InventoryType.MAIN_SHORTCUTBAR;
    }
}
