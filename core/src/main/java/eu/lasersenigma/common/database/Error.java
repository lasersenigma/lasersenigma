package eu.lasersenigma.common.database;

import eu.lasersenigma.LasersEnigmaPlugin;

import java.util.logging.Level;

/**
 * database errors handling
 */
public class Error {

    /**
     * show an error in the logs
     *
     * @param plugin the plugin instance
     * @param ex     the exception to show an error from
     */
    public static void execute(LasersEnigmaPlugin plugin, Exception ex) {
        plugin.getLasersEnigmaLogger().log(Level.SEVERE, "Couldn't execute MySQL statement: ", ex);
    }

    /**
     * show an error when the sql connection isn't successfully closed
     *
     * @param plugin the plugin instance
     * @param ex     the exception to show an error from
     */
    public static void close(LasersEnigmaPlugin plugin, Exception ex) {
        plugin.getLasersEnigmaLogger().log(Level.SEVERE, "Failed to close MySQL connection: ", ex);
    }
}
