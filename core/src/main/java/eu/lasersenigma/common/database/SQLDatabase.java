package eu.lasersenigma.common.database;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.common.items.ComponentType;
import eu.lasersenigma.component.DetectionMode;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public class SQLDatabase extends Database {

    /**
     * the name of the database
     */
    public static final String BACKEND_CONFIG_PATH = "backend";
    private static final String BACKENDS_CONFIG_PATH = "backends.";
    private static final String FILE_CONFIG_PATH = "file";
    private static final String HOST_CONFIG_PATH = "host";
    private static final String DATABASE_CONFIG_PATH = "database";
    private static final String USER_CONFIG_PATH = "user";
    private static final String PASSWORD_CONFIG_PATH = "password";

    /**
     * the sql request to create the tables
     */
    public List<String> SQLiteCreateTables = Arrays.asList(""
            + "CREATE TABLE IF NOT EXISTS " + AREA_TABLE + "(\n"
            + "    `_id` INTEGER PRIMARY KEY,\n"
            + "    `minX` REAL NOT NULL,\n"
            + "    `minY` REAL NOT NULL,\n"
            + "    `minZ` REAL NOT NULL,\n"
            + "    `maxX` REAL NOT NULL,\n"
            + "    `maxY` REAL NOT NULL,\n"
            + "    `maxZ` REAL NOT NULL,\n"
            + "    `world` TEXT NOT NULL,\n"
            + "    `vrotation` INTEGER NOT NULL,\n"
            + "    `hrotation` INTEGER NOT NULL,\n"
            + "    `cvrotation` INTEGER NOT NULL,\n"
            + "    `chrotation` INTEGER NOT NULL,\n"
            + "    `lsvrotation` INTEGER NOT NULL,\n"
            + "    `lshrotation` INTEGER NOT NULL,\n"
            + "    `lrvrotation` INTEGER NOT NULL,\n"
            + "    `lrhrotation` INTEGER NOT NULL,\n"
            + "    `arspheresizing` INTEGER NOT NULL,\n"
            + "    `linked_area_id` INTEGER NOT NULL,\n"
            + "    `checkpointX` REAL,\n"
            + "    `checkpointY` REAL,\n"
            + "    `checkpointZ` REAL,\n"
            + "    `checkpointPitch` FLOAT,\n"
            + "    `checkpointYaw` FLOAT,\n"
            + "    `mode` TEXT,\n"
            + "    `minRange` INTEGER,\n"
            + "    `maxRange` INTEGER\n"
            + "); CREATE TABLE IF NOT EXISTS " + COMPONENT_TABLE + "(\n"
            + "    `_id` INTEGER PRIMARY KEY,\n"
            + "    `area_id` INTEGER NOT NULL,\n"
            + "    `type` TEXT NOT NULL,\n"
            + "    `x` REAL NOT NULL,\n"
            + "    `y` REAL NOT NULL,\n"
            + "    `z` REAL NOT NULL,\n"
            + "    `face` TEXT,\n"
            + "    `rotationX` FLOAT,\n"
            + "    `rotationY` FLOAT,\n"
            + "    `rotationZ` FLOAT,\n"
            + "    `min` INTEGER,\n"
            + "    `max` INTEGER,\n"
            + "    `color` INTEGER,\n"
            + "    `songFileName` TEXT,\n"
            + "    `loopmode` INTEGER,\n"
            + "    `stopOnExit` INTEGER,\n"
            + "    `mode` TEXT,\n"
            + "    `corresponding_lock_area_id` INTEGER,\n"
            + "    `corresponding_lock_component_id` INTEGER,\n"
            + "    `locations` TEXT,\n"
            + "    `vectorX` REAL NOT NULL,\n"
            + "    `vectorY` REAL NOT NULL,\n"
            + "    `vectorZ` REAL NOT NULL,\n"
            + "    `lightLevel` INTEGER,\n"
            + "    `material` TEXT,\n"
            + "    `letPassLaser` INTEGER\n"
            + "); CREATE TABLE IF NOT EXISTS " + STATS_TABLE + "(\n"
            + "    `area_id` INTEGER NOT NULL,\n"
            + "    `playeruuid` TEXT NOT NULL,\n"
            + "    `nbAction` INTEGER NOT NULL,\n"
            + "    `nbStep` INTEGER NOT NULL,\n"
            + "    `duration` TEXT NOT NULL\n"
            + "); CREATE TABLE IF NOT EXISTS " + INVENTORIES_TABLE + "(\n"
            + "    `playeruuid` TEXT NOT NULL,\n"
            + "    `savedinventory` TEXT NOT NULL,\n"
            + "    `isineditionmode` INTEGER NOT NULL,\n"
            + "    `isrotationshortcutbaropened` INTEGER NOT NULL\n"
            + "); CREATE TABLE IF NOT EXISTS " + PLAYERS_KEYS_TABLE + "(\n"
            + "    `playeruuid` TEXT NOT NULL,\n"
            + "    `keychest_area_id` INTEGER NOT NULL,\n"
            + "    `keychest_component_id` INTEGER NOT NULL\n"
            + "); CREATE TABLE IF NOT EXISTS " + CHECKPOINTS_TABLE + "(\n"
            + "    `playeruuid` TEXT NOT NULL,\n"
            + "    `world` TEXT NOT NULL,\n"
            + "    `vworld` TEXT NOT NULL,\n"
            + "    `islastcheckpointreachedavictorycheckpoint` INTEGER NOT NULL\n"
            + "); CREATE TABLE IF NOT EXISTS " + CHECKPOINTS_WORLD_TABLE + "(\n"
            + "    `playeruuid` TEXT NOT NULL,\n"
            + "    `world` TEXT NOT NULL,\n"
            + "    `islastcheckpointreachedavictorycheckpoint` INTEGER NOT NULL,\n"
            + "    `checkpointX` REAL,\n"
            + "    `checkpointY` REAL,\n"
            + "    `checkpointZ` REAL,\n"
            + "    `checkpointPitch` FLOAT,\n"
            + "    `checkpointYaw` FLOAT,\n"
            + "    `vcheckpointX` REAL,\n"
            + "    `vcheckpointY` REAL,\n"
            + "    `vcheckpointZ` REAL,\n"
            + "    `vcheckpointPitch` FLOAT,\n"
            + "    `vcheckpointYaw` FLOAT\n"
            + "); CREATE TABLE IF NOT EXISTS " + VICTORY_AREA_TABLE + "(\n"
            + "    `area_id` INTEGER NOT NULL,\n"
            + "    `minX` REAL NOT NULL,\n"
            + "    `minY` REAL NOT NULL,\n"
            + "    `minZ` REAL NOT NULL,\n"
            + "    `maxX` REAL NOT NULL,\n"
            + "    `maxY` REAL NOT NULL,\n"
            + "    `maxZ` REAL NOT NULL\n"
            + "); CREATE TABLE IF NOT EXISTS " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + "(\n"
            + "    `component_id` INTEGER NOT NULL,\n"
            + "    `position` INTEGER,\n"
            + "    `type` TEXT,\n"
            + "    `delay` INTEGER\n"
            + ");");
    public List<String> SQLCreateTables = Arrays.asList(""
                    + "CREATE TABLE IF NOT EXISTS `" + AREA_TABLE + "` (\n"
                    + "  `_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,\n"
                    + "  `minX` double NOT NULL,\n"
                    + "  `minY` double NOT NULL,\n"
                    + "  `minZ` double NOT NULL,\n"
                    + "  `maxX` double NOT NULL,\n"
                    + "  `maxY` double NOT NULL,\n"
                    + "  `maxZ` double NOT NULL,\n"
                    + "  `world` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `vrotation` int(1) NOT NULL,\n"
                    + "  `hrotation` int(1) NOT NULL,\n"
                    + "  `cvrotation` int(1) NOT NULL,\n"
                    + "  `chrotation` int(1) NOT NULL,\n"
                    + "  `lsvrotation` int(1) NOT NULL,\n"
                    + "  `lshrotation` int(1) NOT NULL,\n"
                    + "  `lrvrotation` int(1) NOT NULL,\n"
                    + "  `lrhrotation` int(1) NOT NULL,\n"
                    + "  `arspheresizing` int(1) NOT NULL,\n"
                    + "  `linked_area_id` int(11) NOT NULL,\n"
                    + "  `checkpointX` double NOT NULL,\n"
                    + "  `checkpointY` double NOT NULL,\n"
                    + "  `checkpointZ` double NOT NULL,\n"
                    + "  `checkpointPitch` float NOT NULL,\n"
                    + "  `checkpointYaw` float NOT NULL,\n"
                    + "  `mode` text COLLATE utf8_bin,"
                    + "  `minRange` int(11) DEFAULT NULL,\n"
                    + "  `maxRange` int(11) DEFAULT NULL\n"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS `" + COMPONENT_TABLE + "` (\n"
                    + "  `_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,\n"
                    + "  `area_id` int(11) DEFAULT NULL,\n"
                    + "  `type` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `x` double NOT NULL,\n"
                    + "  `y` double NOT NULL,\n"
                    + "  `z` double NOT NULL,\n"
                    + "  `face` text COLLATE utf8_bin,\n"
                    + "  `rotationX` float DEFAULT NULL,\n"
                    + "  `rotationY` float DEFAULT NULL,\n"
                    + "  `rotationZ` float DEFAULT NULL,\n"
                    + "  `min` int(11) DEFAULT NULL,\n"
                    + "  `max` int(11) DEFAULT NULL,\n"
                    + "  `color` int(2) DEFAULT NULL,\n"
                    + "  `songFileName` text COLLATE utf8_bin,\n"
                    + "  `loopmode` int(1) DEFAULT NULL,\n"
                    + "  `stopOnExit` int(1) DEFAULT NULL,\n"
                    + "  `mode` text COLLATE utf8_bin,"
                    + "  `corresponding_lock_area_id` int(11),\n"
                    + "  `corresponding_lock_component_id` int(11),\n"
                    + "  `locations` text COLLATE utf8_bin,\n"
                    + "  `vectorX` double NOT NULL,\n"
                    + "  `vectorY` double NOT NULL,\n"
                    + "  `vectorZ` double NOT NULL,\n"
                    + "  `lightLevel` int(11) NOT NULL,\n"
                    + "  `material` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `letPassLaser` int(1) DEFAULT NULL\n"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS `" + STATS_TABLE + "` (\n"
                    + "  `area_id` int(11) NOT NULL,\n"
                    + "  `playeruuid` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `nbAction` int(11) NOT NULL,\n"
                    + "  `nbStep` int(11) NOT NULL,\n"
                    + "  `duration` text COLLATE utf8_bin NOT NULL\n"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS " + INVENTORIES_TABLE + "(\n"
                    + "  `playeruuid` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `savedinventory` text COLLATE utf8_bin,\n"
                    + "  `isineditionmode` int(1) NOT NULL,\n"
                    + "  `isrotationshortcutbaropened` int(1) NOT NULL\n"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS " + PLAYERS_KEYS_TABLE + "(\n"
                    + "  `playeruuid` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `keychest_area_id` int(11) NOT NULL,\n"
                    + "  `keychest_component_id` int(11) NOT NULL\n"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS " + CHECKPOINTS_TABLE + "(\n"
                    + "  `playeruuid` text COLLATE utf8_bin,\n"
                    + "  `world` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `vworld` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `islastcheckpointreachedavictorycheckpoint` int(1) NOT NULL\n"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS " + CHECKPOINTS_WORLD_TABLE + "(\n"
                    + "  `playeruuid` text COLLATE utf8_bin,\n"
                    + "  `world` text COLLATE utf8_bin NOT NULL,\n"
                    + "  `islastcheckpointreachedavictorycheckpoint` int(1) NOT NULL,\n"
                    + "  `checkpointX` double NOT NULL,\n"
                    + "  `checkpointY` double NOT NULL,\n"
                    + "  `checkpointZ` double NOT NULL,\n"
                    + "  `checkpointPitch` float NOT NULL,\n"
                    + "  `checkpointYaw` float NOT NULL,\n"
                    + "  `vcheckpointX` double NOT NULL,\n"
                    + "  `vcheckpointY` double NOT NULL,\n"
                    + "  `vcheckpointZ` double NOT NULL,\n"
                    + "  `vcheckpointPitch` float NOT NULL,\n"
                    + "  `vcheckpointYaw` float NOT NULL\n"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS " + VICTORY_AREA_TABLE + "(\n"
                    + "  `area_id` int(11) NOT NULL,\n"
                    + "  `minX` double NOT NULL,\n"
                    + "  `minY` double NOT NULL,\n"
                    + "  `minZ` double NOT NULL,\n"
                    + "  `maxX` double NOT NULL,\n"
                    + "  `maxY` double NOT NULL,\n"
                    + "  `maxZ` double NOT NULL"
                    + "); \n",
            "CREATE TABLE IF NOT EXISTS " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + "(\n"
                    + "  `component_id` int(11) NOT NULL,\n"
                    + "  `position` int(11),\n"
                    + "  `type` text COLLATE utf8_bin,\n"
                    + "  `delay` int(11)\n"
                    + "); \n");

    /**
     * constructor
     *
     * @param instance the plugin instance
     */
    public SQLDatabase(LasersEnigmaPlugin instance) {
        super(instance);
    }

    @SuppressWarnings("null")
    private List<String> getLoadQuerys() {
        String backend = LasersEnigmaPlugin.getInstance().getConfig().getString(BACKEND_CONFIG_PATH);
        switch (backend) {
            case "sqlite":
                return SQLiteCreateTables;
            default:
                return SQLCreateTables;
        }
    }

    /**
     * gets and sql connection
     *
     * @return a connection
     */
    @Override
    public Connection getSQLConnection() {
        FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
        String backend = config.getString(BACKEND_CONFIG_PATH);
        switch (backend) {
            case "sqlite":
                try {
                    if (connection != null && !connection.isClosed()) {
                        return connection;
                    }
                    String databaseFileNamePath = BACKENDS_CONFIG_PATH + backend + "." + FILE_CONFIG_PATH;
                    String databaseFileName = config.getString(databaseFileNamePath, "database.sqlite");
                    File databaseFile = new File(plugin.getDataFolder(), databaseFileName);
                    if (!databaseFile.exists()) {
                        try {
                            databaseFile.createNewFile();
                        } catch (IOException e) {
                            plugin.getLogger().log(Level.SEVERE, "File write error: {0}", databaseFileName);
                        }
                    }
                    Class.forName("org.sqlite.JDBC");
                    connection = DriverManager.getConnection("jdbc:sqlite:" + databaseFile);
                    return connection;
                } catch (SQLException ex) {
                    plugin.getLogger().log(Level.SEVERE, "SQLite exception on initialize", ex);
                } catch (ClassNotFoundException ex) {
                    plugin.getLogger().log(Level.SEVERE, "SQLITE JDBC Driver not found");
                }
                return null;
            default:
                try {
                    if (connection != null && !connection.isClosed()) {
                        return connection;
                    }
                    String baseConfigPath = BACKENDS_CONFIG_PATH + backend + ".";
                    String uri = "jdbc:mysql://" + config.getString(baseConfigPath + HOST_CONFIG_PATH) + "/" + config.getString(baseConfigPath + DATABASE_CONFIG_PATH);
                    String user = config.getString(baseConfigPath + USER_CONFIG_PATH);
                    String password = config.getString(baseConfigPath + PASSWORD_CONFIG_PATH);
                    Class.forName("com.mysql.jdbc.Driver");
                    connection = DriverManager.getConnection(uri, user, password);
                    return connection;
                } catch (SQLException ex) {
                    plugin.getLogger().log(Level.SEVERE, "SQLite exception on initialize", ex);
                } catch (ClassNotFoundException ex) {
                    plugin.getLogger().log(Level.SEVERE, "SQLITE JDBC Driver not found");
                }
                return null;
        }

    }

    /**
     * loads the database to create the tables
     */
    @Override
    public void load() {
        connection = getSQLConnection();
        try (Statement createSchema = connection.createStatement()) {
            for (String query : getLoadQuerys()) {
                createSchema.executeUpdate(query);
            }
            createSchema.close();
            initialize();
            update();
        } catch (SQLException e) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "Loading error", e);
        }
    }

    private void update() throws SQLException {
        update_5_0_7_to_5_1_0();
        update_5_1_0_to_5_1_1();
        update_5_1_1_to_5_2_0();
        update_5_2_0_to_6_4_0();
        update_6_7_2_to_6_8_0();
        update_6_21_10_to_6_22_0();
        update_6_22_1_to_6_23_0();
    }

    private void update_5_0_7_to_5_1_0() throws SQLException {

        connection = getSQLConnection();
        PreparedStatement alterStatement = null;

        boolean updateFrom5_0to5_1 = !isColumnInTable("checkpointX", AREA_TABLE);
        try {
            FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
            String backend = config.getString(BACKEND_CONFIG_PATH);

            ResultSet rs;

            if (updateFrom5_0to5_1) {
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Updating database schema from 5.0.7 to 5.1.0 version.");
                ArrayList<String> alterTables = new ArrayList<>();
                switch (backend) {
                    case "sqlite":
                        alterTables.add("`checkpointX` REAL DEFAULT 0");
                        alterTables.add("`checkpointY` REAL DEFAULT 0");
                        alterTables.add("`checkpointZ` REAL DEFAULT 0");
                        alterTables.add("`checkpointPitch` FLOAT DEFAULT 0");
                        alterTables.add("`checkpointYaw` FLOAT DEFAULT 0");
                        break;
                    default:
                        alterTables.add("`checkpointX` double NOT NULL");
                        alterTables.add("`checkpointY` double NOT NULL");
                        alterTables.add("`checkpointZ` double NOT NULL");
                        alterTables.add("`checkpointPitch` float NOT NULL");
                        alterTables.add("`checkpointYaw` float NOT NULL");
                        break;
                }
                for (String sqlStrColumnInformation : alterTables) {
                    alterStatement = connection.prepareStatement("ALTER TABLE " + AREA_TABLE + " ADD " + sqlStrColumnInformation + ";");
                    alterStatement.executeUpdate();
                    alterStatement.close();
                }
                alterTables.clear();
                switch (backend) {
                    case "sqlite":
                        alterTables.add("`corresponding_lock_area_id` INTEGER");
                        alterTables.add("`corresponding_lock_component_id` INTEGER");
                        break;
                    default:
                        alterTables.add("`corresponding_lock_area_id` int(11)");
                        alterTables.add("`corresponding_lock_component_id` int(11)");
                        break;
                }
                for (String sqlStrColumnInformation : alterTables) {
                    alterStatement = connection.prepareStatement("ALTER TABLE " + COMPONENT_TABLE + " ADD " + sqlStrColumnInformation + ";");
                    alterStatement.executeUpdate();
                    alterStatement.close();
                }
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "database schema update successfully completed.");
            }
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer.");
        } finally {
            if (alterStatement != null) {
                alterStatement.close();
            }
        }
    }

    private void update_5_1_0_to_5_1_1() throws SQLException {

        connection = getSQLConnection();
        PreparedStatement alterStatement = null, dropStatement = null, createStatement = null;
        boolean updateFrom5_1_0to5_1_1 = !isColumnInTable("isineditionmode", INVENTORIES_TABLE);

        if (updateFrom5_1_0to5_1_1) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Updating database schema from 5.1.0 to 5.1.1 version.");
            try {
                FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
                String backend = config.getString(BACKEND_CONFIG_PATH);

                if (!backend.equals("sqlite")) { //MYSQL
                    ArrayList<String> alterTables = new ArrayList<>();
                    alterTables.add("`isineditionmode` int(1) NOT NULL");
                    alterTables.add("`isrotationshortcutbaropened` int(1) NOT NULL");
                    for (String sqlStrColumnInformation : alterTables) {
                        alterStatement = connection.prepareStatement("ALTER TABLE " + INVENTORIES_TABLE + " ADD " + sqlStrColumnInformation + ";");
                        alterStatement.executeUpdate();
                        alterStatement.close();
                    }
                } else { //SQLITE
                    dropStatement = connection.prepareStatement("DROP TABLE IF EXISTS " + INVENTORIES_TABLE + ";");
                    dropStatement.executeUpdate();
                    dropStatement.close();
                    createStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + INVENTORIES_TABLE + "(\n"
                            + "    `playeruuid` TEXT NOT NULL,\n"
                            + "    `savedinventory` TEXT NOT NULL,\n"
                            + "    `isineditionmode` INTEGER NOT NULL,\n"
                            + "    `isrotationshortcutbaropened` INTEGER NOT NULL\n"
                            + ");");
                    createStatement.executeUpdate();
                    createStatement.close();
                }

                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "database schema update successfully completed.");
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer.");
            } finally {
                if (alterStatement != null) {
                    alterStatement.close();
                }
                if (dropStatement != null) {
                    dropStatement.close();
                }
                if (createStatement != null) {
                    createStatement.close();
                }
            }
        }
    }

    private void update_5_1_1_to_5_2_0() throws SQLException {

        connection = getSQLConnection();
        PreparedStatement alterStatement = null;
        boolean updateFrom5_1_0to5_1_1 = !isColumnInTable("locations", COMPONENT_TABLE);

        if (updateFrom5_1_0to5_1_1) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Updating database schema from 5.1.1 to 5.2.0 version.");
            try {
                FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
                String backend = config.getString(BACKEND_CONFIG_PATH);

                ArrayList<String> alterTables = new ArrayList<>();
                switch (backend) {
                    case "sqlite":
                        alterTables.add("`locations` TEXT");
                        alterTables.add("`vectorX` REAL");
                        alterTables.add("`vectorY` REAL");
                        alterTables.add("`vectorZ` REAL");
                        break;
                    default:
                        alterTables.add("`locations` text COLLATE utf8_bin");
                        alterTables.add("`vectorX` double");
                        alterTables.add("`vectorY` double");
                        alterTables.add("`vectorZ` double");
                        break;
                }
                for (String sqlStrColumnInformation : alterTables) {
                    alterStatement = connection.prepareStatement("ALTER TABLE " + COMPONENT_TABLE + " ADD " + sqlStrColumnInformation + ";");
                    alterStatement.executeUpdate();
                    alterStatement.close();
                }

                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "database schema update successfully completed.");
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer.");
            } finally {
                if (alterStatement != null) {
                    alterStatement.close();
                }
            }
        }
    }

    private void update_5_2_0_to_6_4_0() throws SQLException {

        connection = getSQLConnection();
        PreparedStatement alterStatement = null, updateStatement = null;
        boolean updateFrom5_1_0to5_1_1 = !isColumnInTable("mode", AREA_TABLE);

        if (updateFrom5_1_0to5_1_1) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Updating database schema from 5.2.0 to 6.4.0 version.");
            try {
                FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
                String backend = config.getString(BACKEND_CONFIG_PATH);

                ArrayList<String> alterTables = new ArrayList<>();
                switch (backend) {
                    case "sqlite":
                        alterTables.add("`mode` TEXT");
                        alterTables.add("`minRange` INTEGER");
                        alterTables.add("`maxRange` INTEGER");
                        break;
                    default:
                        alterTables.add("`mode` text COLLATE utf8_bin");
                        alterTables.add("`minRange` int(11) DEFAULT NULL");
                        alterTables.add("`maxRange` int(11) DEFAULT NULL");
                        break;
                }
                for (String sqlStrColumnInformation : alterTables) {
                    alterStatement = connection.prepareStatement("ALTER TABLE " + AREA_TABLE + " ADD " + sqlStrColumnInformation + ";");
                    alterStatement.executeUpdate();
                    alterStatement.close();
                }

                updateStatement = connection.prepareStatement("UPDATE " + AREA_TABLE + " SET mode = ?, minRange = ?, maxRange = ?");
                updateStatement.setString(1, DetectionMode.DETECTION_VICTORY_AREA.toString());
                updateStatement.setInt(2, 1);
                updateStatement.setInt(3, 10);
                updateStatement.executeUpdate();

                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "database schema update successfully completed.");
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer.");
            } finally {
                if (alterStatement != null) {
                    alterStatement.close();
                }
                if (updateStatement != null) {
                    updateStatement.close();
                }
            }
        }
    }

    private void update_6_7_2_to_6_8_0() throws SQLException {
        connection = getSQLConnection();
        PreparedStatement alterStatement = null, updateStatement = null;
        boolean update_6_7_2_to_6_8_0 = !isColumnInTable("lightLevel", COMPONENT_TABLE);

        if (update_6_7_2_to_6_8_0) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Updating database schema from 6.7.2 to 6.8.0 version.");
            try {
                FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
                String backend = config.getString(BACKEND_CONFIG_PATH);

                ArrayList<String> alterTables = new ArrayList<>();
                switch (backend) {
                    case "sqlite":
                        alterTables.add("`lightLevel` INTEGER");
                        break;
                    default:
                        alterTables.add("`lightLevel` int(11) DEFAULT 0");
                        break;
                }
                for (String sqlStrColumnInformation : alterTables) {
                    alterStatement = connection.prepareStatement("ALTER TABLE " + COMPONENT_TABLE + " ADD " + sqlStrColumnInformation + ";");
                    alterStatement.executeUpdate();
                    alterStatement.close();
                }

                updateStatement = connection.prepareStatement("UPDATE " + COMPONENT_TABLE + " SET lightLevel = ? WHERE type = ? OR type = ?");
                updateStatement.setInt(1, LasersEnigmaPlugin.getInstance().getConfig().getInt("laser_default_light_level", 12));
                updateStatement.setString(2, ComponentType.LASER_RECEIVER.toString());
                updateStatement.setString(3, ComponentType.LASER_SENDER.toString());
                updateStatement.executeUpdate();

                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "database schema update successfully completed.");
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer.");
            } finally {
                if (alterStatement != null) {
                    alterStatement.close();
                }
                if (updateStatement != null) {
                    updateStatement.close();
                }
            }
        }

    }

    private void update_6_21_10_to_6_22_0() throws SQLException {
        connection = getSQLConnection();
        PreparedStatement alterStatement = null, updateStatement = null;
        boolean update_6_21_10_to_6_22_0 = !isColumnInTable("material", COMPONENT_TABLE);
        if (update_6_21_10_to_6_22_0) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Updating database schema from 6.21.10 to 6.22.0 version.");
            try {
                FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
                String backend = config.getString(BACKEND_CONFIG_PATH);

                ArrayList<String> alterTables = new ArrayList<>();
                switch (backend) {
                    case "sqlite":
                        alterTables.add("`material` TEXT");
                        break;
                    default:
                        alterTables.add("`material` text COLLATE utf8_bin NOT NULL");
                        break;
                }
                for (String sqlStrColumnInformation : alterTables) {
                    alterStatement = connection.prepareStatement("ALTER TABLE " + COMPONENT_TABLE + " ADD " + sqlStrColumnInformation + ";");
                    alterStatement.executeUpdate();
                    alterStatement.close();
                }

                updateStatement = connection.prepareStatement("UPDATE " + COMPONENT_TABLE + " SET type = ? WHERE type = 'MELTABLE_CLAY'");
                updateStatement.setString(1, ComponentType.BURNABLE_BLOCK.name());
                updateStatement.executeUpdate();

                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "database schema update successfully completed.");
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer.");
            } finally {
                if (alterStatement != null) {
                    alterStatement.close();
                }
                if (updateStatement != null) {
                    updateStatement.close();
                }
            }
        }
    }

    private void update_6_22_1_to_6_23_0() throws SQLException {
        connection = getSQLConnection();
        PreparedStatement alterStatement = null;
        boolean update_6_22_2_to_6_23_0 = !isColumnInTable("letPassLaser", COMPONENT_TABLE);
        if (update_6_22_2_to_6_23_0) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "Updating database schema from 6.22.2 to 6.23.0 version.");
            try {
                FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
                String backend = config.getString(BACKEND_CONFIG_PATH);

                ArrayList<String> alterTables = new ArrayList<>();
                switch (backend) {
                    case "sqlite":
                        alterTables.add("`letPassLaser` INTEGER");
                        break;
                    default:
                        alterTables.add("`letPassLaser` int(1) DEFAULT 0");
                        break;
                }
                for (String sqlStrColumnInformation : alterTables) {
                    alterStatement = connection.prepareStatement("ALTER TABLE " + COMPONENT_TABLE + " ADD " + sqlStrColumnInformation + ";");
                    alterStatement.executeUpdate();
                    alterStatement.close();
                }

                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.WARNING, "database schema update successfully completed.");
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer.");
            } finally {
                if (alterStatement != null) {
                    alterStatement.close();
                }
            }
        }
    }

    public boolean isColumnInTable(String columnName, String tableName) throws SQLException {
        connection = getSQLConnection();
        PreparedStatement checkTableStatement = null;
        try {
            FileConfiguration config = LasersEnigmaPlugin.getInstance().getConfig();
            String backend = config.getString(BACKEND_CONFIG_PATH);
            String baseConfigPath = BACKENDS_CONFIG_PATH + backend + ".";

            ResultSet rs;
            boolean exists = false;
            switch (backend) {
                case "sqlite":
                    checkTableStatement = connection.prepareStatement("PRAGMA table_info(" + tableName + ");");
                    rs = checkTableStatement.executeQuery();
                    while (rs.next()) {
                        if (columnName.equals(rs.getString("name"))) {
                            exists = true;
                            break;
                        }
                    }
                    rs.close();
                    break;
                default:
                    exists = true;
                    checkTableStatement = connection.prepareStatement("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" + config.getString(baseConfigPath + DATABASE_CONFIG_PATH) + "' AND TABLE_NAME='" + tableName + "' and COLUMN_NAME = '" + columnName + "';");
                    rs = checkTableStatement.executeQuery();
                    if (!rs.next()) {
                        exists = false;
                    }
                    rs.close();
                    break;
            }
            checkTableStatement.close();
            return exists;
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, "SQLite exception during update", ex);
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "error during database schema update, please contact the plugin developer");
        } finally {
            if (checkTableStatement != null) {
                checkTableStatement.close();
            }
        }
        return false;
    }
}
