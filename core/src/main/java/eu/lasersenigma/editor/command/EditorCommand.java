package eu.lasersenigma.editor.command;

import eu.lasersenigma.common.command.LasersCommand;
import eu.lasersenigma.player.LEPlayers;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class EditorCommand extends LasersCommand {

    public EditorCommand() {
        super("editor", "commands.editor.description", "edit", "e");
        super.setPermission("lasers.edit");
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        LEPlayers.getInstance().findLEPlayer(player).getInventoryManager().toggleEditionMode();
        return true;
    }
}
