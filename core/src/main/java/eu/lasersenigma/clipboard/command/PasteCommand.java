package eu.lasersenigma.clipboard.command;

import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.common.command.LasersCommand;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class PasteCommand extends LasersCommand {

    public PasteCommand() {
        super("paste", "commands.clipboard.paste.description");
        super.setPermission("lasers.admin");
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        ClipboardManager.getInstance().paste(player);
        return true;
    }
}
