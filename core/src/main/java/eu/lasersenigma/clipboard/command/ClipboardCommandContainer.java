package eu.lasersenigma.clipboard.command;

import eu.lasersenigma.common.command.LasersCommand;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class ClipboardCommandContainer extends LasersCommand {

    public ClipboardCommandContainer() {
        super("clipboard", "commands.clipboard.description");
        super.setPermission("lasers.admin");
        super.registerSubCommand(new CopyCommand());
        super.registerSubCommand(new PasteCommand());
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        return super.defaultProcessForContainer(commands, player, strings);
    }
}
