package eu.lasersenigma.clipboard.command;

import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.common.command.LasersCommand;
import fr.skytale.commandlib.Commands;
import org.bukkit.entity.Player;

public class CopyCommand extends LasersCommand {

    public CopyCommand() {
        super("copy", "commands.clipboard.copy.description");
        super.setPermission("lasers.admin");
    }

    @Override
    protected boolean execute(Commands commands, Player player, String... strings) {
        ClipboardManager.getInstance().copy(player);
        return true;
    }
}
