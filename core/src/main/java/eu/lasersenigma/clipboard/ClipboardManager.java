package eu.lasersenigma.clipboard;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.exception.InvalidSelectionException;
import eu.lasersenigma.common.exception.AbstractLasersException;
import eu.lasersenigma.common.message.TranslationUtils;
import eu.lasersenigma.schematic.Schematic;
import eu.lasersenigma.schematic.SchematicManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class ClipboardManager {

    private static ClipboardManager instance;
    private final HashMap<UUID, Schematic> playersLESchematicClipboard;
    private final int PAGE_SIZE = 10;

    private ClipboardManager() {
        playersLESchematicClipboard = new HashMap<>();
    }

    public static ClipboardManager getInstance() {
        if (instance == null) {
            instance = new ClipboardManager();
        }
        return instance;
    }

    public void copyAndSave(Player player, Location min, Location max, String schematicName) {
        try {
            Schematic schematic = SchematicManager.createSchematic(player, min, max);
            SchematicManager.exportSchematic(schematic, schematicName);
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player, ex);
        }
    }

    public void copy(Player player) {
        try {
            Schematic schematic = SchematicManager.createSchematic(player);
            playersLESchematicClipboard.put(player.getUniqueId(), schematic);
            TranslationUtils.sendTranslatedMessage(player, "commands.clipboard.copy.success");
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player, ex);
        }
    }

    public void paste(Player player) {
        try {
            Schematic schematic = playersLESchematicClipboard.get(player.getUniqueId());
            if (schematic == null) {
                throw new InvalidSelectionException();
            }
            SchematicManager.pasteSchematic(player, schematic);
            TranslationUtils.sendTranslatedMessage(player, "commands.clipboard.paste.success");
        } catch (AbstractLasersException ex) {
            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "An error occured while restoring schematic.", ex);
            TranslationUtils.sendExceptionMessage(ex);

            TranslationUtils.sendExceptionMessage(player, ex);
        }
    }

    public void load(Player player, String schematicName) {

        if (schematicName.contains(".leschem")) {
            schematicName = schematicName.replace(".leschem", "");
        }

        try {
            Schematic schematic = SchematicManager.importSchematic(schematicName);
            playersLESchematicClipboard.put(player.getUniqueId(), schematic);

            TranslationUtils.sendTranslatedMessage(player, "schematic.successes.loaded", SchematicManager.getSchematicDisplayName(schematicName));
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player, ex);
        }
    }

    public void save(Player player, String schematicName) {
        try {
            Schematic schematic = playersLESchematicClipboard.get(player.getUniqueId());
            if (schematic == null) {
                throw new InvalidSelectionException();
            }
            SchematicManager.exportSchematic(schematic, schematicName);
            TranslationUtils.sendTranslatedMessage(player, "schematic.successes.saved", SchematicManager.getSchematicDisplayName(schematicName));
        } catch (AbstractLasersException ex) {
            TranslationUtils.sendExceptionMessage(player, ex);
        }
    }

    private int maximumPageCount() {
        List<String> schematics = SchematicManager.list();
        return (int) Math.ceil((double) schematics.size() / PAGE_SIZE);
    }

    public List<String> getPages() {
        List<String> pages = new ArrayList<>();
        for (int i = 0; i < maximumPageCount(); i++) {
            pages.add(String.format("%d", i));
        }
        return pages;
    }

    public void list(Player player, int page) {
        if (page > maximumPageCount() || page < 0) {
            TranslationUtils.sendTranslatedMessage(player, "errors.schematic.page_number_out_of_bounds", page);
            return;
        }

        List<String> schematics = SchematicManager.list();

        if (schematics.isEmpty()) {
            TranslationUtils.sendTranslatedMessage(player, "errors.schematic.empty_directory");
            return;
        }

        int startingPageIndex = PAGE_SIZE * page;
        int endingPageIndex = ((PAGE_SIZE * page) + PAGE_SIZE - 1);

        TranslationUtils.sendTranslatedMessage(player, "commands.schematic.list.header", schematics.size());
        schematics.subList(startingPageIndex, Math.min(schematics.size(), endingPageIndex)).forEach(schematic -> {
            TranslationUtils.sendTranslatedMessage(player, "commands.schematic.list.item", schematic);
        });
        TranslationUtils.sendTranslatedMessage(player, "commands.schematic.list.footer", (page + 1), maximumPageCount());

    }
}
