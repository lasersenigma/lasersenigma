package eu.lasersenigma.songs;

import com.xxmicloxx.NoteBlockAPI.NBSDecoder;
import com.xxmicloxx.NoteBlockAPI.Song;
import eu.lasersenigma.LasersEnigmaPlugin;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;

public class SongsLoader {

    private static final String SONGS_FOLDER_NAME = "songs";
    private static final String SONGS_FOLDER_NAME_IN_JAR = "defaultsongs";
    private static final String SONGS_ACTIVATED_CONFIG_PATH = "music_blocks";
    private static final String NBS_EXTENSION = "nbs";
    private static SongsLoader instance;
    private File songsFolder;
    private Boolean songsActivated;
    private ArrayList<Song> songs;

    private SongsLoader() {
        songs = new ArrayList<>();
        reload();
    }

    protected static SongsLoader getInstance() {
        if (instance == null) {
            instance = new SongsLoader();
        }
        return instance;
    }

    protected static String removeExtension(String fileName) {

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            return fileName.substring(0, i);
        }
        return "";
    }

    private static String getExtension(String fileName) {
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            return fileName.substring(i + 1);
        }
        return "";
    }

    protected Boolean isActivated() {
        return songsActivated;
    }

    protected final void reload() {
        songsActivated = LasersEnigmaPlugin.getInstance().getConfig().getBoolean(SONGS_ACTIVATED_CONFIG_PATH, true);
        if (!songsActivated) {
            return;
        }
        boolean isFolderCreated = createSongsFolder();
        if (isFolderCreated) {

            LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.INFO, "Extracting default songs");
            extractDefaultSongs();
        }
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.INFO, "Loading songs from " + SONGS_FOLDER_NAME + " folder");
        loadSongs();
        if (songs.isEmpty()) {
            songsActivated = false;
        }
    }

    private boolean createSongsFolder() {
        songsFolder = new File(LasersEnigmaPlugin.getInstance().getDataFolder(), SONGS_FOLDER_NAME);
        if (!songsFolder.exists() && !songsFolder.isDirectory()) {
            try {
                if (songsFolder.mkdir()) {
                    return true;
                }
            } catch (Exception e) {
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "Error creating " + SONGS_FOLDER_NAME + " folder.", e);
            }
        }
        return false;
    }

    private void extractDefaultSongs() {
        final String path = SONGS_FOLDER_NAME_IN_JAR + "/";
        final File jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());

        if (jarFile.isFile()) {
            try {
                JarFile jar = new JarFile(jarFile);
                final Enumeration<JarEntry> entries = jar.entries();
                while (entries.hasMoreElements()) {
                    JarEntry jarEntry = entries.nextElement();
                    final String name = jarEntry.getName();
                    if (name.startsWith(path) && name.endsWith("." + NBS_EXTENSION)) {
                        final File newFile = new File(songsFolder, StringUtils.substringAfterLast(name, "/"));
                        final InputStream inStream = SongsLoader.class.getResourceAsStream("/" + name);
                        FileUtils.copyInputStreamToFile(inStream, newFile);
                    }
                }
            } catch (IOException ex) {
                LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.SEVERE, "Error extracting default songs", ex);
            }
        }
    }

    private void loadSongs() {
        songs = new ArrayList<>();
        int i = 0;
        for (File f : songsFolder.listFiles()) {
            if (getExtension(f.getName()).equals(NBS_EXTENSION)) {
                songs.add(NBSDecoder.parse(f));
                i++;
            }
        }
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.INFO, "Loaded {0} songs.", i);
    }

    protected ArrayList<Song> getSongs() {
        return songs;
    }

}
