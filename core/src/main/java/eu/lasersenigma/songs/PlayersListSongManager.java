package eu.lasersenigma.songs;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class PlayersListSongManager {

    private static PlayersListSongManager instance;
    private final SongsListManager songListManager;
    private final HashMap<UUID, PlayerSongManager> playersSongManagers;

    private PlayersListSongManager() {
        songListManager = SongsListManager.getInstance();
        playersSongManagers = new HashMap<>();
    }

    public static PlayersListSongManager getInstance() {
        if (instance == null) {
            instance = new PlayersListSongManager();
        }
        return instance;
    }

    public void reload() {
        songListManager.reload();
        playersSongManagers.forEach((k, v) -> {
            v.onReload();
        });
        playersSongManagers.clear();
    }

    public void startSong(Player p, String songFileName, boolean loop, boolean stopWhenPlayerLeftArea) {
        UUID playerUUID = p.getUniqueId();
        PlayerSongManager playerSongManager = playersSongManagers.get(playerUUID);
        if (playerSongManager != null) {
            playerSongManager.startSong(songFileName, loop, stopWhenPlayerLeftArea);
        } else {
            playerSongManager = new PlayerSongManager(playerUUID, songFileName, loop, stopWhenPlayerLeftArea);
            playersSongManagers.put(playerUUID, playerSongManager);
        }
    }

    public void fadeOut(Player p, int maximumFadeDuration) {
        UUID playerUUID = p.getUniqueId();
        PlayerSongManager playerSongManager = playersSongManagers.get(playerUUID);
        if (playerSongManager != null) {
            playerSongManager.fadeOut(maximumFadeDuration);
        }
    }

    public void onSongEnd(Player p) {
        if (p == null) {
            return;
        }
        UUID playerUUID = p.getUniqueId();
        PlayerSongManager playerSongManager = playersSongManagers.get(playerUUID);
        if (playerSongManager != null) {
            playerSongManager.onSongEnd();
        }
    }

    public void onPlayerLeftArea(Player p) {
        UUID playerUUID = p.getUniqueId();
        PlayerSongManager playerSongManager = playersSongManagers.get(playerUUID);
        if (playerSongManager != null) {
            playerSongManager.onPlayerLeftArea();
        }
    }
}
