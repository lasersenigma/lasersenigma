package eu.lasersenigma.particles.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.particles.LaserParticle;

public class ParticleTryToMoveThroughCrossableMaterialsEvent extends ALaserParticleEvent {

    public ParticleTryToMoveThroughCrossableMaterialsEvent(Area area, LaserParticle particle) {
        super(particle, area);
    }

}
