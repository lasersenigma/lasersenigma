package eu.lasersenigma.particles.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.entity.LivingEntity;

public class ParticleTryToHitEntityEvent extends ALaserParticleEvent {

    private final LivingEntity entity;
    private int laserBurnsTickDuration;
    private double additionalDamage;
    private double knockbackMultiplier;
    private boolean entityStopLasers;

    public ParticleTryToHitEntityEvent(Area area, LaserParticle particle, LivingEntity entity, int laserBurnsTickDuration, double additionalDamage, double knockbackMultiplier, boolean entityStopLasers) {
        super(particle, area);
        this.entity = entity;
        this.laserBurnsTickDuration = laserBurnsTickDuration;
        this.additionalDamage = additionalDamage;
        this.knockbackMultiplier = knockbackMultiplier;
        this.entityStopLasers = entityStopLasers;
    }

    public int getLaserBurnsTickDuration() {
        return laserBurnsTickDuration;
    }

    public void setLaserBurnsTickDuration(int laserBurnsTickDuration) {
        this.laserBurnsTickDuration = laserBurnsTickDuration;
    }

    public double getAdditionalDamage() {
        return additionalDamage;
    }


    public void setAdditionalDamage(double additionalDamage) {
        this.additionalDamage = additionalDamage;
    }

    public double getKnockbackMultiplier() {
        return knockbackMultiplier;
    }

    public void setKnockbackMultiplier(double knockbackMultiplier) {
        this.knockbackMultiplier = knockbackMultiplier;
    }

    public boolean getEntityStopLasers() {
        return this.entityStopLasers;
    }

    public void setEntityStopLasers(boolean entityStopLasers) {
        this.entityStopLasers = entityStopLasers;
    }

    public LivingEntity getEntity() {
        return entity;
    }
}
