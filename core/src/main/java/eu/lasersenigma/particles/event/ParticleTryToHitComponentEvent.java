package eu.lasersenigma.particles.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.IComponentLEEvent;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.component.laserreceiver.LaserReceptionResult;
import eu.lasersenigma.particles.LaserParticle;

public class ParticleTryToHitComponentEvent extends ALaserParticleEvent implements IComponentLEEvent {

    private final IComponent component;

    private LaserReceptionResult laserReceptionResult;

    public ParticleTryToHitComponentEvent(Area area, LaserParticle particle, IComponent component, LaserReceptionResult laserReceptionResult) {
        super(particle, area);
        this.component = component;
        this.laserReceptionResult = laserReceptionResult;
    }

    @Override
    public final IComponent getComponent() {
        return component;
    }

    public final LaserReceptionResult getLaserReceptionResult() {
        return laserReceptionResult;
    }

    public final void setLaserReceptionResult(LaserReceptionResult laserReceptionResult) {
        this.laserReceptionResult = laserReceptionResult;
    }
}
