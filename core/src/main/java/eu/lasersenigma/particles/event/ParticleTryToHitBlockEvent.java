package eu.lasersenigma.particles.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.block.Block;

public class ParticleTryToHitBlockEvent extends ALaserParticleEvent {

    private final Block block;

    public ParticleTryToHitBlockEvent(Area area, Block block, LaserParticle particle) {
        super(particle, area);
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }

}
