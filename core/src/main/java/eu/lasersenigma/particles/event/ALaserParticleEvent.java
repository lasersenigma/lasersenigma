package eu.lasersenigma.particles.event;

import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.event.ABeforeActionEvent;
import eu.lasersenigma.common.event.IAreaEvent;
import eu.lasersenigma.particles.LaserParticle;

public abstract class ALaserParticleEvent extends ABeforeActionEvent implements IParticleEvent, IAreaEvent {

    private final LaserParticle particle;

    private final Area area;

    private boolean ignoreDefaults = false;

    public ALaserParticleEvent(LaserParticle particle, Area area) {
        super();
        this.area = area;
        this.particle = particle;
    }

    public final boolean getIgnoreDefaults() {
        return ignoreDefaults;
    }

    public final void setIgnoreDefaults(boolean ignoreDefaults) {
        this.ignoreDefaults = ignoreDefaults;
    }

    @Override
    public final LaserParticle getLaserParticle() {
        return particle;
    }

    @Override
    public final Area getArea() {
        return area;
    }

}
