package eu.lasersenigma.particles.event;

import eu.lasersenigma.particles.LaserParticle;

public interface IParticleEvent {

    LaserParticle getLaserParticle();

}
