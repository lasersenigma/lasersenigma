package eu.lasersenigma.armors.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Skyhighjinks (alias Skyhighjinks) a.m.d.rogers@hotmail.co.uk
 */
public class ArmorOptionsMenuInventory extends AOpenableInventory {

    private boolean noBurnCheckbox = false;
    private boolean focusCheckbox = false;
    private boolean noDamageCheckbox = false;
    private boolean noKnockbackCheckbox = false;
    private boolean prismCheckbox = false;
    private boolean reflectionCheckbox = false;
    private boolean durabilityCheckbox = false;
    private int durabilityNumber = 5;

    public ArmorOptionsMenuInventory(LEPlayer player) {
        super(player, "messages.armor_options_menu_inventory_title");
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {

        Item noBurnCheckboxL = noBurnCheckbox ? Item.ARMOR_CONFIG_NO_BURNING_CHECKED : Item.ARMOR_CONFIG_NO_BURNING_UNCHECKED;
        Item focusCheckboxL = focusCheckbox ? Item.ARMOR_CONFIG_FOCUS_CHECKED : Item.ARMOR_CONFIG_FOCUS_UNCHECKED;
        Item noDamageCheckboxL = noDamageCheckbox ? Item.ARMOR_CONFIG_NO_DAMAGE_CHECKED : Item.ARMOR_CONFIG_NO_DAMAGE_UNCHECKED;
        Item noKnockbackCheckboxL = noKnockbackCheckbox ? Item.ARMOR_CONFIG_NO_KNOCKBACK_CHECKED : Item.ARMOR_CONFIG_NO_KNOCKBACK_UNCHECKED;
        Item prismCheckboxL = prismCheckbox ? Item.ARMOR_CONFIG_PRISM_CHECKED : Item.ARMOR_CONFIG_PRISM_UNCHECKED;
        Item reflectionCheckboxL = reflectionCheckbox ? Item.ARMOR_CONFIG_REFLECTION_CHECKED : Item.ARMOR_CONFIG_REFLECTION_UNCHECKED;
        Item durabilityCheckboxL = durabilityCheckbox ? Item.ARMOR_CONFIG_DURABILITY_CHECKED : Item.ARMOR_CONFIG_DURABILITY_UNCHECKED;
        Item decreaseDurability = (durabilityNumber > 1) && (durabilityCheckbox) ? Item.ARMOR_CONFIG_DURABILITY_DECREMENT : Item.EMPTY;
        Item increaseDurability = (durabilityNumber < 9) && (durabilityCheckbox) ? Item.ARMOR_CONFIG_DURABILITY_INCREMENT : Item.EMPTY;
        Item durabilityNum = durabilityCheckbox ? getNumberAsItems(durabilityNumber, false).get(0) : Item.EMPTY;

        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.ARMOR_CONFIG_NO_BURNING_HEADER, Item.ARMOR_CONFIG_NO_KNOCKBACK_HEADER, Item.ARMOR_CONFIG_NO_DAMAGE_HEADER, Item.EMPTY, Item.EMPTY, Item.ARMOR_CONFIG_DURABILITY_HEADER, Item.EMPTY, Item.EMPTY, Item.EMPTY)));
        inv.add(new ArrayList<>(Arrays.asList(noBurnCheckboxL, noKnockbackCheckboxL, noDamageCheckboxL, Item.EMPTY, Item.EMPTY, durabilityCheckboxL, Item.EMPTY, Item.EMPTY)));
        inv.add(new ArrayList<>(Arrays.asList(Item.ARMOR_CONFIG_FOCUS_HEADER, Item.ARMOR_CONFIG_REFLECTION_HEADER, Item.ARMOR_CONFIG_PRISM_HEADER, Item.EMPTY, decreaseDurability, durabilityNum, increaseDurability, Item.EMPTY, Item.EMPTY)));
        inv.add(new ArrayList<>(Arrays.asList(focusCheckboxL, reflectionCheckboxL, prismCheckboxL, Item.EMPTY, Item.EMPTY, Item.EMPTY, Item.EMPTY, Item.EMPTY, Item.ARMOR_CONFIG_VALIDATE)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEArmorActionMenuInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        switch (item) {
            case ARMOR_CONFIG_NO_BURNING_CHECKED:
            case ARMOR_CONFIG_NO_BURNING_UNCHECKED:
                noBurnCheckbox = !noBurnCheckbox;
                updateInventory();
                break;
            case ARMOR_CONFIG_FOCUS_CHECKED:
            case ARMOR_CONFIG_FOCUS_UNCHECKED:
                focusCheckbox = !focusCheckbox;
                updateInventory();
                break;
            case ARMOR_CONFIG_NO_DAMAGE_CHECKED:
            case ARMOR_CONFIG_NO_DAMAGE_UNCHECKED:
                noDamageCheckbox = !noDamageCheckbox;
                updateInventory();
                break;
            case ARMOR_CONFIG_NO_KNOCKBACK_CHECKED:
            case ARMOR_CONFIG_NO_KNOCKBACK_UNCHECKED:
                noKnockbackCheckbox = !noKnockbackCheckbox;
                updateInventory();
                break;
            case ARMOR_CONFIG_PRISM_CHECKED:
            case ARMOR_CONFIG_PRISM_UNCHECKED:
                prismCheckbox = !prismCheckbox;
                updateInventory();
                break;
            case ARMOR_CONFIG_REFLECTION_CHECKED:
            case ARMOR_CONFIG_REFLECTION_UNCHECKED:
                reflectionCheckbox = !reflectionCheckbox;
                updateInventory();
                break;
            case ARMOR_CONFIG_DURABILITY_CHECKED:
            case ARMOR_CONFIG_DURABILITY_UNCHECKED:
                durabilityCheckbox = !durabilityCheckbox;
                updateInventory();
                break;
            case ARMOR_CONFIG_DURABILITY_INCREMENT:
                if (durabilityCheckbox && (durabilityNumber < 9)) {
                    durabilityNumber++;
                }
                updateInventory();
                break;
            case ARMOR_CONFIG_DURABILITY_DECREMENT:
                if (durabilityCheckbox && (durabilityNumber > 1)) {
                    durabilityNumber--;
                }
                updateInventory();
                break;
            case ARMOR_CONFIG_VALIDATE:
                if (!durabilityCheckbox) {
                    durabilityNumber = 0;
                }
                player.getInventoryManager().onArmorMenuCompleted(noBurnCheckbox, noDamageCheckbox, noKnockbackCheckbox, reflectionCheckbox, focusCheckbox, prismCheckbox, durabilityNumber);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.ARMOR_CONFIG_NO_BURNING_HEADER,
                Item.ARMOR_CONFIG_NO_KNOCKBACK_HEADER,
                Item.ARMOR_CONFIG_NO_DAMAGE_HEADER,
                Item.ARMOR_CONFIG_DURABILITY_HEADER,
                Item.ARMOR_CONFIG_FOCUS_HEADER,
                Item.ARMOR_CONFIG_REFLECTION_HEADER,
                Item.ARMOR_CONFIG_PRISM_HEADER,
                Item.ARMOR_CONFIG_DURABILITY_DECREMENT,
                Item.ARMOR_CONFIG_DURABILITY_INCREMENT,
                Item.ARMOR_CONFIG_NO_BURNING_CHECKED, Item.ARMOR_CONFIG_NO_BURNING_UNCHECKED,
                Item.ARMOR_CONFIG_NO_KNOCKBACK_CHECKED, Item.ARMOR_CONFIG_NO_KNOCKBACK_UNCHECKED,
                Item.ARMOR_CONFIG_NO_DAMAGE_CHECKED, Item.ARMOR_CONFIG_NO_DAMAGE_UNCHECKED,
                Item.ARMOR_CONFIG_DURABILITY_CHECKED, Item.ARMOR_CONFIG_DURABILITY_UNCHECKED,
                Item.ARMOR_CONFIG_FOCUS_CHECKED, Item.ARMOR_CONFIG_FOCUS_UNCHECKED,
                Item.ARMOR_CONFIG_REFLECTION_CHECKED, Item.ARMOR_CONFIG_REFLECTION_UNCHECKED,
                Item.ARMOR_CONFIG_PRISM_CHECKED, Item.ARMOR_CONFIG_PRISM_UNCHECKED,
                Item.ARMOR_CONFIG_VALIDATE)).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.ARMOR_ACTION_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return null;
    }

}
