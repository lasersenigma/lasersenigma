package eu.lasersenigma.armors.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.area.Area;
import eu.lasersenigma.common.inventory.AOpenableInventory;
import eu.lasersenigma.common.inventory.InventoryType;
import eu.lasersenigma.common.items.Item;
import eu.lasersenigma.component.IComponent;
import eu.lasersenigma.permission.Permission;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Skyhighjinks (alias Skyhighjinks) a.m.d.rogers@hotmail.co.uk
 */
public class ArmorActionMenuInventory extends AOpenableInventory {

    public ArmorActionMenuInventory(LEPlayer player) {
        super(player, "messages.armor_action_menu_inventory_title");
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.ARMOR_ACTION_MENU_SELF_GIVE, Item.ARMOR_ACTION_MENU_GIVE_NEAREST_PLAYER))); // Not linked to CMD Block
        inv.add(new ArrayList<>(Arrays.asList(Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_FILLS_CHEST, Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_GIVE_NEAREST_PLAYER, Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_PUT_ARMOR_ON_NEAREST_PLAYER))); // Linked to CMD Block
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getLasersEnigmaLogger().log(Level.FINER, "LEArmorActionMenuInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        player.getInventoryManager().onSelectedArmor(item.getArmorAction());
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.ARMOR_ACTION_MENU_SELF_GIVE,
                Item.ARMOR_ACTION_MENU_GIVE_NEAREST_PLAYER,
                Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_FILLS_CHEST,
                Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_GIVE_NEAREST_PLAYER,
                Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_PUT_ARMOR_ON_NEAREST_PLAYER)).contains(item);
    }

    @Override
    public InventoryType getType() {
        return InventoryType.ARMOR_ACTION_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return null;
    }

}
