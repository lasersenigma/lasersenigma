package eu.lasersenigma.sound;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.sound.event.PlaySoundEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.SoundCategory;

import java.util.Set;

public class SoundLauncher {


    private static final String SOUND_CATEGORY = LasersEnigmaPlugin.getInstance().getConfig().getString("other_sounds_sound_category");

    private SoundLauncher() {
    }

    public static void playSound(Location location, PlaySoundCause playSoundCause) {
        PlaySoundEvent playSoundLEEvent = new PlaySoundEvent(location, playSoundCause, playSoundCause.getSoundSequence());
        Bukkit.getServer().getPluginManager().callEvent(playSoundLEEvent);
        if (playSoundLEEvent.isCancelled() || playSoundLEEvent.getSoundSequenceToPlay() == null) {
            return;
        }
        playSoundSequence(location, playSoundLEEvent.getSoundSequenceToPlay());
    }

    public static void playSoundSequence(Location location, ISoundSequence soundSequenceToPlay) {
        soundSequenceToPlay.getSoundsPerTick().forEach((tick, soundDataSet) -> {
            if (tick == 0) {
                playSoundsNow(location, soundDataSet);
            } else {
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    playSoundsNow(location, soundDataSet);
                }, tick);
            }
        });
    }

    private static void playSoundsNow(Location location, Set<SoundData> soundDataSet) {
        if (location.getWorld() == null) return;

        soundDataSet.forEach(soundData -> location.getWorld().playSound(
                location,
                soundData.sound(),
                SoundCategory.valueOf(SOUND_CATEGORY),
                soundData.volume(),
                soundData.pitch()));
    }
}
