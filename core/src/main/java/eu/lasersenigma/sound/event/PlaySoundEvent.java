package eu.lasersenigma.sound.event;

import eu.lasersenigma.common.event.ABeforeActionEvent;
import eu.lasersenigma.sound.ISoundSequence;
import eu.lasersenigma.sound.PlaySoundCause;
import org.bukkit.Location;

public class PlaySoundEvent extends ABeforeActionEvent {

    private final Location location;
    private final PlaySoundCause playSoundCause;
    private ISoundSequence soundSequenceToPlay;

    public PlaySoundEvent(Location location, PlaySoundCause playSoundCause, ISoundSequence soundSequenceToPlay) {
        super();
        this.location = location;
        this.playSoundCause = playSoundCause;
        this.soundSequenceToPlay = soundSequenceToPlay;
    }

    public Location getLocation() {
        return location;
    }

    public PlaySoundCause getPlaySoundCause() {
        return playSoundCause;
    }

    public ISoundSequence getSoundSequenceToPlay() {
        return soundSequenceToPlay;
    }

    public void setSoundSequenceToPlay(ISoundSequence soundSequenceToPlay) {
        this.soundSequenceToPlay = soundSequenceToPlay;
    }
}
