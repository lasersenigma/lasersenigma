package eu.lasersenigma.sound;

import org.bukkit.Sound;

public record SoundData(Sound sound, float volume, float pitch, int tickFromStart) {

    public SoundData {
        if (sound == null) throw new IllegalArgumentException("sound should not be null");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SoundData soundData = (SoundData) o;

        if (Float.compare(soundData.volume, volume) != 0) return false;
        if (Float.compare(soundData.pitch, pitch) != 0) return false;
        if (tickFromStart != soundData.tickFromStart) return false;
        return sound.equals(soundData.sound);
    }

}
