package eu.lasersenigma.sound;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public interface ISoundSequence {
    SoundData[] getSounds();

    default Map<Integer, Set<SoundData>> getSoundsPerTick() {
        Map<Integer, Set<SoundData>> soundsPerTick = new HashMap<>();
        for (SoundData sound : getSounds()) {
            Set<SoundData> soundDataSet = soundsPerTick.computeIfAbsent(sound.tickFromStart(), k -> new HashSet<>());
            soundDataSet.add(sound);
        }
        return soundsPerTick;
    }
}
