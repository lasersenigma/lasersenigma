package eu.lasersenigma.sound;

public enum PlaySoundCause {
    LASER_SENDER_ROTATE(SoundSequence.COMPONENT_ROTATE),
    CONCENTRATOR_ROTATE(SoundSequence.COMPONENT_ROTATE),
    LASER_RECEIVER_ROTATE(SoundSequence.COMPONENT_ROTATE),
    MIRROR_SUPPORT_ROTATE(SoundSequence.COMPONENT_ROTATE),

    PARTICLE_IMPACT_PLAYER(SoundSequence.PARTICLE_IMPACT_PLAYER),
    COMPONENT_CHANGE_COLOR(SoundSequence.COMPONENT_CHANGE_COLOR),
    CLAY_MELTDOWN(SoundSequence.CLAY_MELTDOWN),

    MIRROR_INSERT_SPHERE(SoundSequence.MIRROR_INSERT),
    MIRROR_EXTRACT_SPHERE(SoundSequence.MIRROR_EXTRACT),
    MIRROR_INSERT_SUPPORT(SoundSequence.MIRROR_INSERT),
    MIRROR_EXTRACT_SUPPORT(SoundSequence.MIRROR_EXTRACT),

    LASER_SENDER_ACTIVATED(SoundSequence.COMPONENT_ACTIVATED),
    LASER_RECEIVER_ACTIVATED(SoundSequence.COMPONENT_ACTIVATED),
    CONCENTRATOR_COLOR_CHANGE(SoundSequence.COMPONENT_ACTIVATED),

    REACHED_AREA_WIN_CONDITIONS(SoundSequence.VICTORY),

    CREATE_AREA(SoundSequence.VICTORY),
    ELEVATOR_ARRIVED(SoundSequence.VICTORY),
    OBTAIN_KEY(SoundSequence.VICTORY),
    OPEN_LOCK(SoundSequence.VICTORY),

    DELETE_AREA(SoundSequence.DEFEAT),
    ELEVATOR_CANT_GO_DOWN(SoundSequence.DEFEAT),
    ELEVATOR_CANT_GO_UP(SoundSequence.DEFEAT),
    ELEVATOR_CANT_CALL(SoundSequence.DEFEAT),
    ELEVATOR_MOVE_BLOCKED(SoundSequence.DEFEAT),
    ;

    private final ISoundSequence soundSequence;

    PlaySoundCause(ISoundSequence soundSequence) {
        this.soundSequence = soundSequence;
    }

    public ISoundSequence getSoundSequence() {
        return soundSequence;
    }
}
