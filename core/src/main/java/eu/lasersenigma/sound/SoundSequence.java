package eu.lasersenigma.sound;

import org.bukkit.Sound;

public enum SoundSequence implements ISoundSequence {
    
    COMPONENT_ROTATE(
            new SoundData(Sound.ENTITY_IRON_GOLEM_ATTACK, 10, 0.7f, 0)
    ),
    PARTICLE_IMPACT_PLAYER(
            new SoundData(Sound.ENTITY_GENERIC_EXTINGUISH_FIRE, 0.2f, 2, 0)
    ),
    COMPONENT_CHANGE_COLOR(
            new SoundData(Sound.ENTITY_FIREWORK_ROCKET_LAUNCH, 10, 0.7f, 0)
    ),
    VICTORY(
            new SoundData(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 0.8f, 0),
            new SoundData(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 1.0f, 4),
            new SoundData(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 1.2f, 8)
    ),
    DEFEAT(
            new SoundData(Sound.BLOCK_NOTE_BLOCK_BASS, 10, 0.9f, 0),
            new SoundData(Sound.BLOCK_NOTE_BLOCK_BASS, 10, 0.7f, 4),
            new SoundData(Sound.BLOCK_NOTE_BLOCK_BASS, 10, 0.5f, 8)
    ),
    CLAY_MELTDOWN(
            new SoundData(Sound.BLOCK_LAVA_EXTINGUISH, 10, 0.3f, 0),
            new SoundData(Sound.ENTITY_LIGHTNING_BOLT_IMPACT, 10, 1.5f, 49)
    ),
    MIRROR_EXTRACT(
            new SoundData(Sound.BLOCK_PORTAL_AMBIENT, 10, 2f, 0),
            new SoundData(Sound.BLOCK_WOODEN_DOOR_CLOSE, 10, 1.7f, 25)
    ),
    MIRROR_INSERT(
            new SoundData(Sound.BLOCK_PORTAL_AMBIENT, 10, 2f, 0),
            new SoundData(Sound.BLOCK_WOODEN_DOOR_CLOSE, 10, 1.7f, 5)
    ),
    COMPONENT_ACTIVATED(
            new SoundData(Sound.ENTITY_ILLUSIONER_MIRROR_MOVE, 10, 1.5f, 0),
            new SoundData(Sound.BLOCK_SHULKER_BOX_OPEN, 10, 2f, 0),
            new SoundData(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, 1.2f, 0)
    );

    private final SoundData[] sounds;

    SoundSequence(SoundData... sounds) {
        this.sounds = sounds;
    }

    @Override
    public SoundData[] getSounds() {
        return sounds;
    }

}
