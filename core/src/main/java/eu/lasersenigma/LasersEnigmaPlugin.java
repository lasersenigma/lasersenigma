package eu.lasersenigma;

import eu.lasersenigma.area.Areas;
import eu.lasersenigma.area.command.AreaCommandContainer;
import eu.lasersenigma.area.listener.AreaEventsListener;
import eu.lasersenigma.area.task.AreaTask;
import eu.lasersenigma.checkpoint.GetBackToCheckpointOnChangeWorld;
import eu.lasersenigma.checkpoint.GetBackToCheckpointOnDeath;
import eu.lasersenigma.checkpoint.GetBackToCheckpointOnJoin;
import eu.lasersenigma.checkpoint.listener.CheckpointClearOnPlayerQuitEventListener;
import eu.lasersenigma.checkpoint.listener.CheckpointPlayerChangedWorldEventListener;
import eu.lasersenigma.checkpoint.listener.CheckpointPlayerJoinEventListener;
import eu.lasersenigma.checkpoint.listener.CheckpointPlayerRespawnEventListener;
import eu.lasersenigma.clipboard.command.ClipboardCommandContainer;
import eu.lasersenigma.commands.HelpCommand;
import eu.lasersenigma.common.config.CheckpointPriority;
import eu.lasersenigma.common.config.ConfigData;
import eu.lasersenigma.common.database.Database;
import eu.lasersenigma.common.database.SQLDatabase;
import eu.lasersenigma.common.dependency.dungeonsxl.EditWorldSaveDungeonsXLListener;
import eu.lasersenigma.common.dependency.dungeonsxl.InstanceWorldPostUnloadEventDungeonsXLListener;
import eu.lasersenigma.common.dependency.dungeonsxl.ResourceWorldInstantiateDungeonsXLListener;
import eu.lasersenigma.common.inventory.listener.InventoryEventsListener;
import eu.lasersenigma.common.items.ItemsFactory;
import eu.lasersenigma.common.logger.Logger;
import eu.lasersenigma.component.command.ComponentCommandContainer;
import eu.lasersenigma.component.concentrator.listener.ConcentratorEventsListener;
import eu.lasersenigma.component.laserreceiver.listener.LaserReceiverEventsListener;
import eu.lasersenigma.component.lasersender.listener.LaserSenderEventsListener;
import eu.lasersenigma.component.lock.listener.LockEventsListener;
import eu.lasersenigma.component.mirrorsupport.listener.MirrorSupportEventsListener;
import eu.lasersenigma.editor.command.EditorCommand;
import eu.lasersenigma.particles.task.LaserTask;
import eu.lasersenigma.player.listener.PlayerEventsListener;
import eu.lasersenigma.schematic.command.SchematicCommandContainer;
import eu.lasersenigma.songs.PlayersListSongManager;
import eu.lasersenigma.stats.BStatsConfig;
import eu.lasersenigma.updatenotifier.UpdateNotifier;
import eu.lasersenigma.updatenotifier.listener.UpdateNotifierEventsListener;
import fr.skytale.commandlib.Commands;
import fr.skytale.translationlib.TranslationLib;
import fr.skytale.translationlib.player.PlayerLangManager;
import fr.skytale.translationlib.translation.TranslationManager;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;
import java.util.logging.Level;

/**
 * Main class of this plugin
 */
public class LasersEnigmaPlugin extends JavaPlugin {

    /**
     * the static instance of this plugin
     */
    private static LasersEnigmaPlugin lasersEnigmaPlugin;

    private TranslationManager translationManager;
    private PlayerLangManager playerLangManager;

    /**
     * the database
     */
    private Database db;
    /**
     * Logger
     */
    private Logger logger;

    /**
     * true if the plugin is disabling itself
     */
    private boolean disabling;

    // BStats
    private final int BSTATS_SERVICE_ID = 4004;

    /**
     * gets the only instance of this class
     *
     * @return the instance of this class
     */
    public static LasersEnigmaPlugin getInstance() {
        return lasersEnigmaPlugin;
    }

    /**
     * gets the plugin database
     *
     * @return the plugin database
     */
    public Database getPluginDatabase() {
        return this.db;
    }

    /**
     * Retrieves a logger
     *
     * @return the logger
     */
    public Logger getLasersEnigmaLogger() {
        return logger;
    }

    /**
     * @return true if the plugin is currently being disabled.
     */
    public boolean isDisabling() {
        return disabling;
    }

    /**
     * The method called when the plugin is enabled
     */
    @Override
    @SuppressWarnings({"ResultOfObjectAllocationIgnored", "null"})
    public void onEnable() {
        LasersEnigmaPlugin.lasersEnigmaPlugin = this;
        this.disabling = false;

        loadConfig();
        registerTranslations();

        // Logger
        this.logger = new Logger(lasersEnigmaPlugin);
        getLasersEnigmaLogger().setLogLevel(Level.parse(Objects.requireNonNull(getConfig().getString("debug_level", "INFO"))));

        // Plugin usage statistics
        Metrics metrics = new Metrics(this, BSTATS_SERVICE_ID);

        // Commands
        registerCommands();

        getLasersEnigmaLogger().info("Connecting to database.");
        //Database
        this.db = new SQLDatabase(this);
        this.db.load();

        getLasersEnigmaLogger().info("Loading plugin data from database.");
        Areas.getInstance().loadAreas();
        getLasersEnigmaLogger().info("Data loading ended. Loaded worlds contains {0} areas.", String.valueOf(Areas.getInstance().getAreas().size()));

        //Event handlers
        loadEventHandlers();

        //TimerTask
        new AreaTask();
        new LaserTask();

        BStatsConfig.addCustomCharts(metrics);

        //Check for new updates and notify in console.
        UpdateNotifier.getInstance();

        getLasersEnigmaLogger().info("Initialization end.");
    }

    @Override
    public void onDisable() {
        getLasersEnigmaLogger().info("Unloading process has started");

        this.disabling = true;
        // Areas.getInstance().clearAreas();
        Areas.getInstance().deactivateAreas();
        unregisterTranslations();

        getLasersEnigmaLogger().info("Unloading process has ended");
    }

    public TranslationManager getTranslationManager() {
        return this.translationManager;
    }

    public PlayerLangManager getPlayerLangManager() {
        return this.playerLangManager;
    }

    private void registerTranslations() {
        TranslationLib translationLib = TranslationLib.getInstance(this);
        translationManager = translationLib.getTranslationManager();
        playerLangManager = translationLib.getPlayerLangManager();
    }

    private void unregisterTranslations() {
        TranslationLib.getInstance(this).onDisable();
    }

    private void registerCommands() {
        Commands commands = Commands.setup(Objects.requireNonNull(this.getCommand("lasersenigma")), "");

        // Components
        commands.registerCommand(new ComponentCommandContainer());

        // Area
        commands.registerCommand(new AreaCommandContainer());

        // Clipboard
        commands.registerCommand(new ClipboardCommandContainer());

        // Schematics
        commands.registerCommand(new SchematicCommandContainer());

        // Editor
        var editorCommand = new EditorCommand();
        commands.registerCommand(editorCommand);
        commands.setRootCommand(new EditorCommand());

        // Help
        commands.registerCommand(new HelpCommand());
    }

    public boolean isLaserEmitsLight() {
        return this.getConfig().getBoolean("laser_light");
    }

    /**
     * Reloads configurations
     */
    public void reloadAllConfigs() {
        loadConfig();
        ItemsFactory.reload();
        PlayersListSongManager.getInstance().reload();
    }

    private void loadConfig() {
        ConfigData.initializeConfig();
    }

    private void loadEventHandlers() {
        // Update notifier events listener
        new UpdateNotifierEventsListener();

        // Area events listener
        new AreaEventsListener();

        // Components events listeners
        new ConcentratorEventsListener();
        new LaserReceiverEventsListener();
        new LaserSenderEventsListener();
        new LockEventsListener();
        new MirrorSupportEventsListener();

        new PlayerEventsListener();
        new InventoryEventsListener();
        loadCheckpointEventHandlers();

        if (Bukkit.getPluginManager().isPluginEnabled("DungeonsXL")) {
            getLasersEnigmaLogger().log(Level.INFO, "DungeonsXL support enabled");
            new EditWorldSaveDungeonsXLListener();
            new InstanceWorldPostUnloadEventDungeonsXLListener();
            new ResourceWorldInstantiateDungeonsXLListener();
        }
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void loadCheckpointEventHandlers() {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        GetBackToCheckpointOnDeath getBackToCheckpointOnDeath = GetBackToCheckpointOnDeath.fromString(LasersEnigmaPlugin.getInstance().getConfig().getString("get_back_to_checkpoint_on_death"));
        GetBackToCheckpointOnJoin getBackToCheckpointOnJoin = GetBackToCheckpointOnJoin.fromString(LasersEnigmaPlugin.getInstance().getConfig().getString("get_back_to_checkpoint_on_join"));
        GetBackToCheckpointOnChangeWorld getBackToCheckpointOnChangeWorld = GetBackToCheckpointOnChangeWorld.fromString(LasersEnigmaPlugin.getInstance().getConfig().getString("get_back_to_checkpoint_on_change_world"));
        if (getBackToCheckpointOnDeath != GetBackToCheckpointOnDeath.FALSE) {
            EventPriority playerRespawnEventPriority = CheckpointPriority.fromString(
                    Objects.requireNonNull(LasersEnigmaPlugin.getInstance().getConfig().getString("player_respawn_event_priority", "HIGH"))
            ).getPriority();

            CheckpointPlayerRespawnEventListener checkpointPlayerRespawnEventListener = new CheckpointPlayerRespawnEventListener(getBackToCheckpointOnDeath);

            this.getServer().getPluginManager().registerEvent(
                    PlayerRespawnEvent.class,
                    checkpointPlayerRespawnEventListener,
                    playerRespawnEventPriority,
                    (ll, event) -> ((CheckpointPlayerRespawnEventListener) ll).onPlayerRespawnEvent((PlayerRespawnEvent) event),
                    this
            );
        }
        if (getBackToCheckpointOnJoin != GetBackToCheckpointOnJoin.FALSE) {
            EventPriority playerJoinEventPriority = CheckpointPriority.fromString(
                    Objects.requireNonNull(LasersEnigmaPlugin.getInstance().getConfig().getString("player_join_event_priority", "HIGH"))
            ).getPriority();

            CheckpointPlayerJoinEventListener checkpointPlayerJoinEventListener = new CheckpointPlayerJoinEventListener(getBackToCheckpointOnJoin);

            this.getServer().getPluginManager().registerEvent(
                    PlayerJoinEvent.class,
                    checkpointPlayerJoinEventListener,
                    playerJoinEventPriority,
                    (ll, event) -> ((CheckpointPlayerJoinEventListener) ll).onPlayerJoinEvent((PlayerJoinEvent) event),
                    this
            );
        }
        EventPriority changedWorldEventPriority = CheckpointPriority.fromString(
                Objects.requireNonNull(LasersEnigmaPlugin.getInstance().getConfig().getString("player_changed_world_event_priority", "HIGH"))
        ).getPriority();
        boolean deleteCheckpointOnExitWorld = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("delete_checkpoint_on_exit_world");
        CheckpointPlayerChangedWorldEventListener checkpointPlayerChangedWorldEventListener = new CheckpointPlayerChangedWorldEventListener(getBackToCheckpointOnChangeWorld, deleteCheckpointOnExitWorld);

        this.getServer().getPluginManager().registerEvent(
                PlayerChangedWorldEvent.class,
                checkpointPlayerChangedWorldEventListener,
                changedWorldEventPriority,
                (ll, event) -> ((CheckpointPlayerChangedWorldEventListener) ll).onPlayerChangedWorld((PlayerChangedWorldEvent) event),
                this
        );
        boolean deleteCheckpointOnQuit = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("delete_checkpoint_on_leave");
        if (deleteCheckpointOnQuit) {
            new CheckpointClearOnPlayerQuitEventListener();
        }
    }


}
